<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Block;
use App\Hostel;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class BlocksController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.blocks.access');

        # Get all the blocks
        $blocks = Block::with('hostel')->get();

        
        # Return the view
        return view('ohms/blocks/index', ['blocks' => $blocks]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.blocks.access');

        # Get the block
        $block = Block::find($id);

        # Return the view
        return view('admin/blocks/show', ['block' => $block]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.blocks.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.blocks.admin');

        # Find the block
        $row = Block::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'blocks';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/blocks/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.blocks.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.blocks.admin');

        # Find the row
        $row = Block::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'blocks';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::blocks')->with('success', trans('ohms.msg_block_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.blocks.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.blocks.admin');

         $hostels = Hostel::orderBy('name', 'asc')->get();
        
        # Return the view
        return view('ohms/blocks/create', ['hostels' => $hostels]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.blocks.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.blocks.admin');

        # create new role
        $row = new Block;

        # Save the data
        $data_index = 'blocks';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::blocks')->with('success', trans('ohms.msg_block_created'));

    }
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.blocks.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.blocks.admin');

        # Select Item
        $block = OHMS::block('id', $id);

        if(!$block->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $block->delete();

        # Redirect the admin
        return redirect()->route('OHMS::blocks')->with('success', trans('ohms.msg_block_deleted'));
    }
}
