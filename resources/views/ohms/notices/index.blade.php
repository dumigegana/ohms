@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.notices_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.notices_title'))
@section('icon', "announcement")
@section('subtitle', trans('ohms.notices_subtitle'))
@section('content')
  <div class="ui two column doubling stackable grid container">

  </div>
 
  <br><br>

  <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea;">
       {!! $notices->links() !!}
        @foreach($notices as $notice)
        
      <div class="ui feed">
          <div class="event">
              <div class="content">
              <article class="post" data-articleid="{{ $notice->id }}">
                  <center><h3 class="lebel" style="color: #980000;">{{$notice->title}} </h3></center>                
                  <div class="text">{!! substr($notice->body, 0, 300)!!}{{ strlen($notice->body) > 300 ? "..." : ""}} <a href="{{ route('OHMS::notices_show', ['id' => $notice->id]) }}"><b>Read more</b></a></div><br>
                  <div class="ui doubling stackable grid">
                  <div class="two wide column">
                    <div> {{$notice->created_at->diffForHumans()}}</div></div><br>                    
                  <div class="ten wide column">
                  @if ($notice->enable == 1)
                        <div class="ui green tiny circular label pop" data-variation="wide" data-content="Accssible to students" data-position="top center"><span>
                          <i class="large checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny circular label pop" data-variation="wide" data-content="Unaccssible to students." data-position="top center">
                          <span><i class="large close icon"></i></span>
                        </div> 
                      @endif
                    </div>
                  <div class="four wide column ">
                        @if($notice->allow_editing or OHMS::loggedInUser()->su)
                       <div class=""><a href="{{ route('OHMS::notices_edit', ['id' => $notice->id]) }}" class="ui btn button">{{ trans('ohms.notices_edit') }}</a></div>
                             @else
                         <div class="ui disabled btn icon button">
                             <i class="lock icon"></i>
                         </div></div>
                 @endif
                  </div>
                  </article>
                  <hr>
              </div>
          </div>
          </div>
        @endforeach
          
         {!! $notices->links() !!}
      </div>
        <br>
    </div>
  </div>
  
@endsection
