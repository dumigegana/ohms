@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" href="{{ route('OHMS::users') }}">{{ trans('ohms.user_list') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ $user->name }}</div>
    </div>
@endsection
@section('title', $user->name)
@section('icon', "user")
@section('subtitle', $user->email)
@section('content')
<div class="ui doubling stackable grid container">
    <div class="four wide column"></div>
    <div class="eight wide column">
        <div class="ui segment">
            <div class="ui {{ OHMS::settings()->button_color }} ribbon label">{{ trans('ohms.avatar') }}</div>
            <br><br>
            <center>
                <img class="ui small circular image" src="{{ $user->avatar(150) }}">
            </center>
            <br><br>
            <div class="ui {{ OHMS::settings()->button_color }} ribbon label">{{ trans('ohms.information') }}</div>
            <br><br>
            <center>
                <?php $countries = OHMS::countries(); ?>
                <div class="ui list">
                    <div class="item">
                    <div class="header">{{ trans('ohms.country') }}</div>
                        @if(in_array($user->country_code, OHMS::noFlags()))<i class="help icon"></i> {{ $countries[$user->country_code] }}@else<i class="{{ strtolower($user->country_code) }} flag"></i> {{ $countries[$user->country_code] }}@endif
                    </div>
                    <div class="item">
                        <div class="header">{{ trans('ohms.join_date') }}:</div>
                        {{ OHMS::fancyDate($user->created_at) }}
                    </div>
                    <div class="item">
                        <div class="header">{{ trans('ohms.last_updated') }}:</div>
                        {{ OHMS::fancyDate($user->updated_at) }}
                    </div>
                </div>
            </center>
            <br><br>
            <div class="ui {{ OHMS::settings()->button_color }} ribbon label">{{ trans('ohms.roles') }}</div>
            <br><br>
            @foreach($user->roles as $role)
                <a href="{{ route('OHMS::roles_edit', ['id' => $role->id]) }}" class="ui {{ $role->color }} tag label">{{ $role->name }}</a>
            @endforeach
        </div>
    </div>
    <div class="four wide column"></div>
</div>
@endsection
