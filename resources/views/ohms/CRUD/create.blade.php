@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::CRUD') }}">{{ trans('ohms.database_CRUD') }}</a>
        <i class="right angle icon divider"></i>
        <a class="section" href="{{ route('OHMS::CRUD_table', ['table' => $name]) }}">{{ trans('ohms.CRUD_table_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{  trans('ohms.CRUD_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.CRUD_create_title'))
@section('icon', "plus")
@section('subtitle', trans('ohms.CRUD_create_subtitle', ['table' => $name]))
@section('content')
<div class="ui doubling stackable grid container">
    <div class="three wide column"></div>
    <div class="ten wide column">
        <div class="ui very padded segment" style="background: #ffeaea !important;">
            <form class="ui form" method="POST">
                {{ csrf_field() }}
                @include('ohms/forms/master')
                <br>
                <button type="submit" class="ui orange submit button btn">{{ trans('ohms.submit') }}</button>
            </form>
        </div>
        <br>
    </div>
    <div class="three wide column"></div>
</div>
@endsection
