<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use OHMS;
use App\Student;
use App\Room;
use App\Hostel;
use App\Block;
use App\Major;
use Illuminate\Support\Collection;

class DashboardController extends Controller
{ 
    public function index()
    {
       $hmales = Hostel::where('gender', 'Male')->wherehas('blocks.rooms', function($q) {$q->where('enable', true);})->get();
       $hfemales = Hostel::where('gender', 'Female')->wherehas('blocks.rooms', function($q) {$q->where('enable', true);})->get();
       $hmales_cap = Room::where('enable', true)->wherehas('block.hostel', function($o) {$o->where('gender',  'Male' );})->sum('capacity');
       $hmales_occ = Room::where('enable', true)->wherehas('block.hostel', function($o) {$o->where('gender',  'Male' );})->sum('occupants');
       $hfemales_cap = Room::where('enable', true)->wherehas('block.hostel', function($o) {$o->where('gender',  'Female' );})->sum('capacity');
       $hfemales_occ = Room::where('enable', true)->wherehas('block.hostel', function($o) {$o->where('gender',  'Female' );})->sum('occupants');

       foreach ($hmales as $key => $hmale) {
         $occupance_m[] = [ $key*3, 100*$hmale->rooms->sum('occupants')/$hmale->rooms->sum('capacity') ];
         $ticks[] = [ $key*3, $hmale->name ];
         $pie_cm[] = ['label' => $hmale->name, 'data' => $hmale->rooms->sum('occupants')];
         $pie_m = collect($pie_cm); 
       }

       foreach ($hfemales as $key => $hfemale) {
         $occupance_f[] = [ $key*3, 100*$hfemale->rooms->sum('occupants')/$hfemale->rooms->sum('capacity') ];
         $ticksf[] = [ $key*3, $hfemale->name ];
         $pie_cf[] = ['label' => $hfemale->name, 'data' => $hfemale ->rooms->sum('occupants')];
         $pie_f = collect($pie_cf); 
       }
      $hFemales = Hostel::where('gender', 'Female')->with('rooms')->get();
       return view('ohms/dashboard/index', [
                        'occupance_m' => $occupance_m, 
                        'ticks' => $ticks,
                        'occupance_f' => $occupance_f, 
                        'ticksf' => $ticksf,
                        'hmales_cap' => $hmales_cap,
                        'hmales_occ' => $hmales_occ,
                        'hfemales_cap' => $hfemales_cap,
                        'hfemales_occ' => $hfemales_occ,
                        'pie_m' => $pie_m,
                        'pie_f' => $pie_f,
                        ]);
    }
}
 