<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House_allocation extends Model
{
	 protected $dates = [
        'created_at', 'updated_at',  
    ];
    protected $fillable = [
        'house_allocable_id', 'house_allocable_type', 'house_id', 'accepted', 'currrent', 'spause'
    ];
    
    public function house_allocable()
    {
        return $this->morphTo();
    }
    
    public function house()
    {
        return $this->belongsTo('App\House');
    }
}
