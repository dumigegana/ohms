<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('solusi_id')->unique();
            $table->string('title');
            $table->string('email');
            $table->string('gender');
            $table->date('date_of_birth');
            $table->string('national_id');
            $table->string('home_address');
            $table->string('next_kin');
            $table->string('next_of_kin_number');
            $table->integer('department_id')->nullable();
            $table->string('status')->default('Serving');
            $table->string('marital');
            $table->string('cell_number');
            $table->boolean('has_house')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs');
    }
}
