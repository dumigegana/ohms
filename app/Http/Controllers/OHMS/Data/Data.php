<?php


/*
+---------------------------------------------------------------------------+
| OHMS Data Configuration												|
+---------------------------------------------------------------------------+
|                                                               			|
| * Available settings:                   									|
|																			|
| table: The table name 												    +-------------+
| hidden: Columns that will not be displayed in the edit form, and they won't be updated +----------------------------+
| empty: Columns that will not have their current value when editing them (eg: password field is hidden in the model) |
| confirmed: fields that will need to be confirmed twice                                                              +-+
| encrypted: Fields that will be encrypted using: Crypt::encrypt(); when they are saved and decrypted when editing them +---------------------------+
| hashed: Fields that will be hashed when they are saved in the database, will be empty on editing, and if saved as empty they will not be modified |
| masked: Fields that will be displayed as a type='password', so their content when beeing modified won't be visible +------------------------------+
| default_random: Fields that if no data is set, they will be randomly generated (10 characters) +-------------------+
| su_hidden: Columns that will be added to the hidden array if the user is su +------------------+
| code: Fields that can be edited using a code editor                       +-+
| wysiwyg: Fields that can be edited using a wysiwyg editor                 |
| validator: validator settings when executing: $this->validate();          |
| relations: a relationship between a column and a table, or a dropdown     |
|																			|
| Note: Do not change the first index               						|
|																			|
+---------------------------------------------------------------------------+
|																			|
| This file allows you to setup all the information                         |
| to be able to manage your app without problems            				|
|																			|
+---------------------------------------------------------------------------+
*/
use Illuminate\Validation\Rule;

if(!isset($row)){
    # the row will be the user logged in if no row is set
    $row = Auth::user();
}

$data = [


    'users' =>  [

        'table'     =>  'users',
        'create'    =>  [
            'hidden'            =>  ['id', 'su', 'active', 'banned',  'locale', 'remember_token', 'created_at', 'updated_at', 'deleted_at', 'has_room'],
            'default_random'    =>  ['password'],
            'confirmed'         =>  ['password'],
            'encrypted'         =>  [],
            'hashed'            =>  ['password'],
            'masked'            =>  ['password'],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'full_name'         => 'required|max:255',
				'username'          => 'required|max:255|unique:users',
                'email'             => 'required|email|unique:users',
                'password'          => 'required|confirmed',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'su', 'username', 'remember_token', 'created_at', 'updated_at', 'password', 'deleted_at', 'has_room'],
            'su_hidden'         =>  ['full_name', 'active', 'banned', 'password'],
            'empty'             =>  ['password'],
            'default_random'    =>  [],
            'confirmed'         =>  ['password'],
            'encrypted'         =>  [],
            'hashed'            =>  ['password'],
            'masked'            =>  ['password'],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
				'full_name'         => 'sometimes|required|max:255',
                'password'          => 'sometimes|required|min:8|regex:/^(?=.*[a-zA-Z])(?=.*[A-Z])(?=.*\d).+$/|confirmed',
                'email'             => ['required', 'max:255', Rule::unique('users')->ignore($row->id) ],
            ],
        ],
    ],
    'students' =>  [

        'table'     =>  'students',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'gender', 'degree_level'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [], 
            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  OHMS::users(),
                        'value' =>  'id',
                        'show'  =>  'full_name', 
                         ],
                'major_id'       =>  [
                        'data'  =>  OHMS::majors(),
                        'value' =>  'id',
                        'show'  =>  'programme', ],
                        ],
            'validator'         =>  [
                'solusi_id'         => 'required|max:255|unique:students',
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'user_id', 'created_at','updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  OHMS::users(),
                        'value' =>  'id',
                        'show'  =>  ['full_name'],
                         ],
                'major_id'       =>  [
                        'data'  =>  OHMS::majors(),
                        'value' =>  'id',
                        'show'  =>  'programme', ],
                        ],
            'validator'         =>  [
                'solusi_id'         => ['required', 'max:255', Rule::unique('students')->ignore($row->id) ],
                
            ],
            
        ],
    ],

   
    'staffs' =>  [

        'table'     =>  'staffs',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'gender',],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [], 
            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  OHMS::users(),
                        'value' =>  'id',
                        'show'  =>  'full_name', 
                         ],
                'major_id'       =>  [
                        'data'  =>  OHMS::majors(),
                        'value' =>  'id',
                        'show'  =>  'programme', ],
                        ],
            'validator'         =>  [
                'solusi_id'         => 'required|max:255|unique:staffs',
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'user_id', 'created_at','updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  OHMS::users(),
                        'value' =>  'id',
                        'show'  =>  ['full_name'],
                         ],
                'major_id'       =>  [
                        'data'  =>  OHMS::majors(),
                        'value' =>  'id',
                        'show'  =>  'programme', ],
                        ],
            'validator'         =>  [
                'solusi_id'         => ['required', 'max:255', Rule::unique('staffs')->ignore($row->id) ],
                
            ],
            
        ],
    ],
    
    'visitors' =>  [

        'table'     =>  'visitors',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  OHMS::users(),
                        'value' =>  'id',
                        'show'  =>  ['full_name', 'username' ],
                         ],
                     ],
            'validator'         =>  [
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'user_id', 'created_at','updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  OHMS::users(),
                        'value' =>  'id',
                        'show'  =>  ['full_name'],
                         ],
                     ],
            'validator'         =>  [
                
            ],
            
        ],
    ],
    'hostels' =>  [

        'table'     =>  'hostels',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => 'required|max:255|unique:hostels',
                'gender'         => 'required|max:7',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => ['required', 'max:255', Rule::unique('hostels')->ignore($row->id) ],
                'gender'         => 'sometimes|required|max:7',
            ],
            
        ],
    ],
    'blocks' =>  [

        'table'     =>  'blocks',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'hostels_id'       =>  [
                        'data'  =>  OHMS::hostels(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'num'         => 'required|max:255|unique:blocks',
                'stud_year'         => 'required|max:1',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'hostel_id'       =>  [
                        'data'  =>  OHMS::hostels(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'name'         => 'sometimes|required|max:255',
                'stud_year'         => 'sometimes|required|max:1',
            ],
            
        ],
    ],
    'house_categories' =>  [

        'table'     =>  'house_categories',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => 'required|max:255|unique:house_categories',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => ['required', 'max:255', Rule::unique('house_categories')->ignore($row->id) ],
            ],
            
        ],
    ],
    'houses' =>  [

        'table'     =>  'houses',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'house_category_id'       =>  [
                'data'  =>  OHMS::house_categories(),
                'value' =>  'id',
                'show'  =>  'name', ],
                ],
            'validator'         =>  [
                'number'         => 'required|max:255|unique:houses',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'house_category_id'       =>  [
                        'data'  =>  OHMS::house_categories(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'number'         => 'sometimes|required|max:255',
            ],
            
        ],
    ],
    
    'rooms' =>  [

        'table'     =>  'rooms',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'block_id'       =>  [
                        'data'  =>  OHMS::blocks(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'room_number'   => 'required|max:25',
                'block_id'         => 'required',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'block_id'       =>  [
                        'data'  =>  OHMS::blocks(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
               'room_number'   => 'sometimes|required|max:25',
                
            ],
            
        ],
    ],

    'profile' =>  [

        'table'     =>  'users',
        'edit'      =>  [
            'hidden'            =>  ['id', 'su', 'username', 'email', 'active', 'banned', 'locale', 'remember_token', 'created_at', 'updated_at'],
            'empty'             =>  ['password'],
            'default_random'    =>  [],
            'confirmed'         =>  ['password'],
            'encrypted'         =>  [],
            'hashed'            =>  ['password'],
            'masked'            =>  ['password'],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
				'full_name'         => 'required|max:255',
                'username'          => 'sometimes|required|max:255',
                'password'          => 'sometimes|confirmed|min:6',
            ],
        ],
    ],
'notices' =>  [

        'table'     =>  'notices',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'title'         => 'required|max:255',
                'body'          => 'required|max:3000',
            ],
            
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'title'         => 'sometimes|required|max:255,title,'.$row->id,
                'body'          => 'sometimes|required|max:3000,body,'.$row->id,
            ],
            
        ],
    ],   

    'roles' =>  [

        'table'     =>  'roles',
        'create'    =>  [
            'hidden'            =>  ['id', 'su', 'created_at', 'updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'  => 'required|unique:roles',
                
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'su', 'created_at', 'updated_at'],
            'su_hidden'         =>  ['name'],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                
            ],
        ],
    ],


    'permissions'   => [
        'table'     =>  'permissions',
        'create'    =>  [
            'hidden'            =>  ['id', 'su', 'created_at', 'updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'slug'  => 'required|max:255|unique:permissions',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'su', 'created_at', 'updated_at'],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'slug'  => ['required', 'max:255', Rule::unique('permissions')->ignore($row->id) ],
            ],
        ],
    ],
    'semesters'   => [
        'table'     =>  'semesters',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'  => 'required|max:255|unique:semesters',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at'],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'  => ['required', 'max:255', Rule::unique('semesters')->ignore($row->id) ],
            ],
        ],
    ],

    'maintenances' =>  [

        'table'     =>  'maintenances',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
                'validator'         =>  [
                        'comments'   => 'required|max:120',
                        'maintenable_type'    => 'required',
                        'maintenable_id'    => 'required',
                        'item_id'    => 'required',
                    ],
            
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
                'validator'         =>  [
                        'comments'   => 'sometimes|required|max:120',
                        'maintenable_type'    => 'sometimes|required',
                        'maintenable_id'    => 'sometimes|required',
                        'item_id'    => 'sometimes|required',
                    ],
            
        ],
    ],

    'majors' =>  [

        'table'     =>  'majors',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'programme'         => 'required|max:255',
            ],            
        ],

        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
             'validator'         =>  [
                'programme'         => ['required', 'max:255', Rule::unique('semesters')->ignore($row->id) ],
            ],            
        ],
    ],

'maint_deps' =>  [

        'table'     =>  'maint_deps',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => 'required|max:255',
            ],
            
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => ['required', 'max:255', Rule::unique('maint_deps')->ignore($row->id) ],
            ],
            
        ],
    ],

'departments' =>  [

        'table'     =>  'departments',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => 'required|max:255',
            ],
            
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => ['required', 'max:255', Rule::unique('departments')->ignore($row->id) ],
            ],
            
        ],
    ], 
    'items' =>  [

        'table'     =>  'items',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => 'required|max:50',
            ],
            'relations'         =>  [
                'maint_dep_id'       =>  [
                        'data'  =>  OHMS::maint_deps(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
            ],            
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
               'validator'        =>  [
                'name'         => ['required', 'max:255', Rule::unique('items')->ignore($row->id) ],
            ],
            'relations'         =>  [
                'maint_dep_id'       =>  [
                        'data'  =>  OHMS::maint_deps(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
            ],            
        ],
    ],

    'facilities' =>  [

        'table'     =>  'facilities',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name' =>'required|max:50',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id',  'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
             'validator'         =>  [
                'name'         => ['required', 'max:50', Rule::unique('facilities')->ignore($row->id) ],
            ],
            
        ],
    ],

    'facility_statuses' =>  [

        'table'     =>  'facility_statuses',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'status' =>'required|max:50',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id',  'created_at', 'updated_at' ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
             'validator'         =>  [
                'status'         => ['required', 'max:255', Rule::unique('facility_statuses')->ignore($row->id) ],
            ],
            
        ],
    ],   


    'flights' =>  [

        'table'     =>  'flights',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'status', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'flight' =>'required|max:50',
                'flight_date' =>'required|max:50',
                'flight_time' =>'required|max:50',
                'status' =>'required|max:50',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id',  'created_at', 'updated_at', 'status',],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
             'validator'         =>  [
                'flight'         => ['required', 'max:255', Rule::unique('flights')->ignore($row->id) ],                
                'flight_date' =>'required|max:50',
                'flight_time' =>'required|max:50',
                'status' =>'required|max:50',
            ],
            
        ],
    ],

        'sessions'  =>  [

        'table'     =>  'sessions',
        'create'    =>  [
            'hidden'            =>  ['id', 'active', 'created_at', 'updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [                
                'course_id'       =>  [
                        'data'  =>  ohms::courses(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                ],    
            'validator'         =>  [
                'name' => 'required|max:255',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at'],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'course_id'       =>  [
                        'data'  =>  ohms::courses(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                ],    
            'validator'         =>  [
                
            ],
        ],
    ],


    'courses' =>  [

        'table'     =>  'courses',
        'create'    =>  [
            'hidden'            =>  ['id',  'created_at', 'updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],            
            'validator'         =>  [
                'name'  => 'required|unique:courses',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at'],
            'su_hidden'         =>  [],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name' => 'sometimes|required|unique:courses,name,'.$row->id,
            ],
        ],
    ],


    'attendances'  =>  [

        'table'     =>  'attendances',
        'create'    =>  [
            'hidden'            =>  ['id', 'session_id', 'ip', 'date_time', 'created_at', 'updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],            
            'relations'         =>  [
                'student_id'       =>  [
                        'data'  =>  ohms::students(),
                        'value' =>  'id',
                        'show'  =>  'solusi_id', ],
                ],    
            'validator'         =>  [],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'session_id', 'ip', 'date_time', 'created_at', 'updated_at'],
            'default_random'    =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [],
        ],
    ],

    'policies' =>  [

        'table'     =>  'policies',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => 'required|max:255|unique:policies',
                'description'         => 'required|max:255',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => ['required', 'max:255', Rule::unique('policies')->ignore($row->id) ],
                'description'         => 'sometimes|required|max:255',
            ],
            
        ],
    ],
    'desciplinaries' =>  [

        'table'     =>  'desciplinaries',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'name'         => 'required',
                'offence'         => 'required',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'name'         => 'sometimes|required',
                'offence'         => 'sometimes|required',
            ],
        ],
    ],
    
    
    'benefit_records' =>  [

        'table'     =>  'benefit_records',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                'benefit_id'       =>  [
                        'data'  =>  OHMS::benefits(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                'dependant_id'       =>  [
                        'data'  =>  OHMS::dependants(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'qualification'   => 'required',
                'staff_id'         => 'required',
                'benefit_id'         => 'required',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                'benefit_id'       =>  [
                        'data'  =>  OHMS::benefits(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                'dependant_id'       =>  [
                        'data'  =>  OHMS::dependants(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'qualification'   => 'sometimes|required',
                'staff_id'         => 'sometimes|required',
                'benefit_id'         => 'sometimes|required',
                
            ],
            
        ],
    ],  

    'benefits' =>  [

        'table'     =>  'benefits',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => 'required|max:255|unique:benefits',
                'description'         => 'required|max:255',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'         => ['required', 'max:255', Rule::unique('benefits')->ignore($row->id) ],
                'description'         => 'sometimes|required|max:255',
            ],
            
        ],
    ],
    'accademics' =>  [

        'table'     =>  'accademics',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'name'         => 'required',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'name'         => 'sometimes|required',
            ],
        ],
    ],
    
    
    'dependants' =>  [

        'table'     =>  'dependants',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'name'   => 'required',
                'staff_id'         => 'required',
                'relationship'         => 'required',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'qualification'   => 'sometimes|required',
                'staff_id'         => 'sometimes|required',
                'benefit_id'         => 'sometimes|required',
                
            ],
            
        ],
    ],
        'work_records' =>  [

        'table'     =>  'work_records',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'changes'         => 'required',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'changes'         => 'sometimes|required',
            ],
        ],
    ],
    
    
    'resumes' =>  [

        'table'     =>  'resumes',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'date_of_joining'   => 'required|date',
                'staff_id'         => 'required',
                'department'         => 'required',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', ],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'staff_id'       =>  [
                        'data'  =>  OHMS::staffs(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'date_of_joining'   => 'sometimes|required',
                'staff_id'         => 'sometimes|required',
                'department'         => 'sometimes|required',
                
            ],
            
        ],
    ],  
];
