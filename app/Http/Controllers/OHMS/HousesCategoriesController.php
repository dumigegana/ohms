<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\House_category;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class HousesCategoriesController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Get all the HouseCategories
        $house_categories = House_category::all();

        
        # Return the view
        return view('ohms/houseCategories/index', ['house_categories' => $house_categories]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Get the House_category
        $house_category = House_category::find($id);

        # Return the view
        return view('admin/houseCategories/show', ['House_category' => $house_category]);
    }

    public function edit($id)
    {
       OHMS::permissionToAccess('ohms.houses.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

        # Find the House_category
        $row = House_category::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'house_categories';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/houseCategories/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

        # Find the row
        $row = House_category::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'house_categories';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::house_categories')->with('success', trans('ohms.msg_houseCategory_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

        # Return the view
        //return view('ohms/houseCategories/create');

        # Get all the data
        $data_index = 'house_categories';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/houseCategories/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

        # create new role
        $row = new House_category;

        # Save the data
        $data_index = 'house_categories';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::house_categories')->with('success', trans('ohms.msg_houseCategory_created'));

    }
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.houses.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

        # Select Item
        $row = House_category::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $row->delete();

        # Redirect the admin
        return redirect()->route('OHMS::house_categories')->with('success', trans('ohms.msg_houseCategory_deleted'));
    }
}
