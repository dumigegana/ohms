@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.roles_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.roles_title'))
@section('icon', "star")
@section('subtitle', trans('ohms.roles_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;"></div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.roles.admin'))
             <center><a href="{{ route('OHMS::roles_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_role') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
  			<table class="ui basic table ">
          <thead style="background: #ff8b8b">
  			    <tr>
                  <th style="color: #980000;">{{ trans('ohms.name') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.users') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.permissions') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.options') }}</th>
  			    </tr>
  			  </thead>
  			  <tbody>
                @foreach($roles as $role)
					<tr style="color: #980000;">
						<td>
                            <div class="text">
                                {{ $role->name }}
                                @if($role->su)
                                  <div class="ui red tiny left pointing basic label pop" data-title="{{ trans('ohms.super_user_role') }}" data-variation="wide" data-content="{{ trans('ohms.super_user_role_desc') }}" data-position="top center" >{{ trans('ohms.super_user_role') }}</div>
                                @elseif($role->hasPermission('ohms.access'))
                                  <div class="ui blue tiny left pointing basic label pop" data-title="{{ trans('ohms.admin_access_role') }}" data-variation="wide" data-content="{{ trans('ohms.admin_access_role_desc') }}" data-position="top center">{{ trans('ohms.admin_access_role') }}</div>
                                @endif
                            </div>
                        </td>
                        <td>{{ trans('ohms.roles_users', ['number' => count($role->users)]) }}</td>
                        <td>{{ trans('ohms.roles_permissions', ['number' => count($role->permissions)]) }}</td>
                        <td>
                          @if($role->allow_editing or OHMS::loggedInUser()->su)
                              <div class="ui btn top icon left pointing dropdown button">
                                <i class="options icon"></i>
                                <div class="menu">
                                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                                  <a href="{{ route('OHMS::roles_edit', ['id' => $role->id]) }}" class="item">
                                    <i class="edit icon"></i>
                                    {{ trans('ohms.roles_edit') }}
                                  </a>
                                  <a href="{{ route('OHMS::roles_permissions', ['id' => $role->id]) }}" class="item">
                                    <i class="lightning icon"></i>
                                    {{ trans('ohms.roles_edit_permissions') }}
                                  </a>
                                  
                                </div>
                              </div>
                          @else
                              <div class="ui disabled btn icon button">
                                  <i class="lock icon"></i>
                              </div>
                          @endif
						</td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection