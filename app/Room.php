<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Room extends Model
{
	protected $fillable = [
        'block_id', 'room_number', 'capacity', 'occupants', 'enable'   
    ];

	
    public function block()
    {
        return $this->belongsTo('App\Block');
    }

    public function allocations()
    {
        return $this->hasMany('App\Allocation');
    }

    public function visitors()
    {
        return $this->hasMany('App\Visitor');
    }

    public function maintenances()
    {
        return $this->morphMany('App\Maintenance', 'maintenable');
    }
   
} 
