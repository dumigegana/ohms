 @foreach($dependants as $dependant) 
            <tr style="color: #980000;">
              <td>{{ $dependant->dependant_number }}</td>
              <td>{{ $dependant->block->hostel->gender }}</td>
              <td>{{ $dependant->block->name }}</td>
              <td>{{ $dependant->capacity }}</td>
              <td class="center alligned">
                      @if ($dependant->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Dependant ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this dependant " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::dependants_edit', ['id' => $dependant->id]) }}" class="ui btn button">{{ trans('ohms.dependants_edit') }}</a>
              </td>       
            </tr>
        @endforeach