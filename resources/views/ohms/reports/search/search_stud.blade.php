<table class="table table-bordered" id="myTable" >
            <thead>
              <tr>
                <th>Hostel</th>
                <th>Room #</th>
                <th>Name</th>
                <th>Student Id</th>
                <th>Year</th>
                <th>Major</th>
                <th>Date of Birth</th>
                <th>Home Address</th>
                <th>Cell</th>
                <th>Nat Id</th>
                <th>Date</th>
              </tr> 
            </thead>
            <tbody >
            @foreach($allocations as $allocation)
              @if($allocation->student->gender === $gender)
              <tr>
                <td>{{$allocation->room->block->name}}</td>
                <td>{{$allocation->room->room_number}}</td>
                <td>{{$allocation->student->user->full_name}}</td>
                <td>{{$allocation->student->solusi_id}}</td>
                <td>{{$allocation->student->year}}</td>
                <td>{{$allocation->student->major->name}}</td>
                <td>{{$allocation->student->date_of_birth}}</td>
                <td>{{$allocation->student->home_address}}</td>
                <td>{{$allocation->student->cell_number}}</td>
                <td>{{$allocation->student->national_id}}</td>
                <td>{{$allocation->created_at}}</td>               
              </tr>
              @endif
             @endforeach
            </tbody>
          </table>  