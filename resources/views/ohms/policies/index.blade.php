@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.policies_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.policies_title')) 
@section('icon', "bed")
@section('subtitle', trans('ohms.policies_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $policies->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.users.admin'))
             <center><a href="{{ route('OHMS::policies_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add Policy</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr>
              <th style="color: #980000;">Policy</th>
              <th style="color: #980000;">Subject</th>
              <th style="color: #980000;">Details</th>
              <th style="color: #980000;">Options</th>
            </tr> 
          </thead>
          <tbody id="read">
            @foreach($policies as $policy) 
            <tr style="color: #980000;">
              <td>{{ $policy->name }}</td>
              <td>{{ $policy->description }}</td>
              <td>{{ $policy->contants }}</td>
              <td>                
                <a href="{{ route('OHMS::policies_edit', ['id' => $policy->id]) }}" class="ui btn button">{{ trans('ohms.policies_edit') }}</a>
              </td>       
            </tr>
        @endforeach
          </tbody>
        </table>
        {!! $policies->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_policy') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection