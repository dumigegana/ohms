@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.houses_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.houses_title'))
@section('icon', "hospital")
@section('subtitle', trans('ohms.houses_subtitle'))
@section('content')
  <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $houses->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.houses.admin'))
             <center><a href="{{ route('OHMS::houses_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add House</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b;">
            <tr >
                  <th style="color: #980000;">Name</th>
                  <th style="color: #980000;">HouseCategory</th>
                  <th style="color: #980000;"> Action</th>
            </tr>
          </thead>
          <tbody>
                @foreach($houses as $house)
          <tr style="color: #980000;">
            <td>
                <div class="text">
                    {{ $house->number }}
                </div>
              </td>
              <td>{{ $house->house_category->name }}</td>
              <td>
                 @if($house->allow_editing or OHMS::loggedInUser()->su)
                  <a href="{{ route('OHMS::houses_edit', ['id' => $house->id]) }}" class="ui btn button"><i class="options icon"></i>{{ trans('ohms.houses_edit') }}</a>
                @else
                  <div class="ui disabled btn icon button">
                    <i class="lock icon"></i>
                  </div>
                 @endif 
                </td>
                        
          </tr>
        @endforeach
          </tbody>
        </table>
        {!! $houses->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection