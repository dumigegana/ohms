<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hostel extends Model
{
	protected $dates = [
        'created_at', 'updated_at',  
    ];
    public function blocks()
    {
        return $this->hasMany('App\Block');
    }
     
     public function rooms()
    {
        return $this->hasManyThrough('App\Room', 'App\Block');
    }


}
