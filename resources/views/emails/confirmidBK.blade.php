<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title>OHMS</title> <!-- The title tag shows in email notifications, like Android 4.4. -->
    <style>
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }
        img {
            -ms-interpolation-mode:bicubic;
        }            
        .x-gmail-data-detectors,
        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
        }
        .a6S {
	        display: none !important;
	        opacity: 0.01 !important;
        }
        img.g-img + div {
	        display:none !important;
	   	}

        .button-link {
            text-decoration: none !important;
        }        
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { 
            .email-container {
                min-width: 375px !important;
            }
        }
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #ff7070  !important;
            border-color: #ff7070 !important;
        }
        @media screen and (max-width: 480px) {
        .fluid {
            width: 100% !important;
            max-width: 100% !important;
            height: auto !important;
            margin-left: auto !important;
            margin-right: auto !important;
            }
        .stack-column,
        .stack-column-center {
            display: block !important;
            width: 100% !important;
            max-width: 100% !important;
            direction: ltr !important;
            }
        .stack-column-center {
            text-align: center !important;
            }
        .center-on-narrow {
            text-align: center !important;
            display: block !important;
            margin-left: auto !important;
            margin-right: auto !important;
            float: none !important;
            }
        table.center-on-narrow {
            display: inline-block !important;
            }
        }

    </style>

</head>
@component('mail::message')
<body width="100%" bgcolor="#ff8b8b" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #ffeaea !important; text-align: left;">
        <div style="max-width: 680px; margin: auto;" class="email-container">
            
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                   <tr>
                    <td style="background:#ffeaea !important;">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #980000 !important;">                                    
                                <h1 style=" color: #980000 !important;">{{ $contents['title'] }}</h1>
                                <p style=" color: #980000 !important;">{{ $contents['body'] }}</p>
                                    <br><br>
                                    <!-- Button : BEGIN -->
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto">
                                        <tr>
                                            <td style="border-radius: 3px; background: #980000 !important; text-align: center;" class="button-td">
                                                <a href="{{ $contents['link'] }}" style="background: #980000; border: 15px solid #980000; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a">
                                                    <span style="color:#ffeaea !important;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;Confirm&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- Button : END -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
             </table>
            <h3><p style="color: #990000 !important;">
            Thanks,
            {{ config('app.name') }}</p></h2>
            @endcomponent                                   
                                              
    </center>
</body>
</html>
