@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::CRUD') }}">{{ trans('ohms.database_CRUD') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{  trans('ohms.CRUD_table_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.CRUD_table_title'))
@section('icon', "edit")
@section('subtitle', trans('ohms.CRUD_table_subtitle', ['table' => $name]))
@section('content')
<?php require(OHMS::dataPath() . '/Edit/DevGet.php'); $allow_edit = $allow; ?>
<?php require(OHMS::dataPath() . '/Create/DevGet.php'); ?>
<div class="ui doubling stackable one column grid container">
    <div class="column">
        <div class="ui very padded segment" style="background: #ffeaea !important;">
           
            <div style="overflow-x:auto;">
                <table class="ui basic table"> 
      			  <thead style="background: #ff8b8b;">
      			    <tr>
                        @foreach($columns as $column)
                            <th  style="color: #980000;">{{$column}}</th>
                        @endforeach
                        <th  style="color: #980000;">{{ trans('ohms.edit') }}</th>
                        <th  style="color: #980000;">{{ trans('ohms.delete') }}</th>
      			    </tr>
      			  </thead>
      			  <tbody>
                      <?php
                          require(OHMS::dataPath() . '/DevData.php');
                          $hide = [];
                          if(array_key_exists($name, $data)){
                              if(array_key_exists('hide_display', $data[$name])){
                                  $hide = $data[$name]['hide_display'];
                              }
                          }
                      ?>
                      @foreach($rows as $row)
                          <tr  style="color: #980000;">
                              @foreach($columns as $column)
                                  <td>@if(in_array($column,$hide))<i>HIDDEN</i>@else @if($row->$column == "")<i>EMPTY</i>@else {{ $row->$column }} @endif @endif</td>
                              @endforeach
                              @if($allow_edit and \Schema::hasColumn($name, 'id'))
                                  <td>
                                      <a href="{{ route('OHMS::CRUD_edit', ['table' => $name, 'id' => $row->id]) }}" class="ui btn button">{{ trans('ohms.edit') }}</a>
                                  </td>
                              @else
                                  <td>
                                      <a class="ui disabled btn button">{{ trans('ohms.edit') }}</a>
                                  </td>
                              @endif
                              <?php
                                  # Check if you're allowed to delete rows
                                  require(OHMS::dataPath() . '/DevData.php');
                                  $del = true;
                                  if(array_key_exists($name, $data)){
                                      if(array_key_exists('delete', $data[$name])) {
                                          if(!$data[$name]['delete']){
                                              $del = false;
                                          }
                                      }
                                  }
                              ?>
                              @if($del)
                                  <td>
                                      <a href="{{ route('OHMS::CRUD_delete', ['table' => $name, 'id' => $row->id]) }}" class="ui btn button">{{ trans('ohms.delete') }}</a>
                                  </td>
                              @else
                                  <td>
                                      <a class="ui disabled btn button">{{ trans('ohms.delete') }}</a>
                                  </td>
                              @endif
                          </tr>
                      @endforeach
      			  </tbody>
      			</table>
            </div>
        </div>
        <br>
    </div>
</div>
@endsection
