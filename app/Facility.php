<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
	protected $dates = [
        'created_at', 'updated_at',  
    ];
    
	public function checklists()
    {
        return $this->hasMany('App\Checklist');
    }

    public function check_temps()
    {
        return $this->hasMany('App\Check_temps');
    }
} 
