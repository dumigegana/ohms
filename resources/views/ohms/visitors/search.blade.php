 @foreach($visitors as $visitor)
 @if(OHMS::isVisitor($visitor))
<tr style="color: #980000;">
                      <td>
                                      <div class="text">                                
                                          {{ $visitor->full_name }}
                                            @if($visitor->has_room == true)
                                            <div class="ui blue tiny left pointing basic label pop" data-title="{{ trans('ohms.admin_access') }}" data-variation="wide" data-content="{{ trans('ohms.admin_access_desc') }}"
                                             data-position="top center">Has room</div>
                                             @else
                                             <div class="ui red tiny left pointing basic label pop" data-title="{{ trans('ohms.admin_access') }}" data-variation="wide" data-content="{{ trans('ohms.admin_access_desc') }}"
                                             data-position="top center">No room</div> 
                                             @endif

                                      </div>
                          </td>
                
                <td >
               @if(!OHMS::isAdmin($visitor) or OHMS::loggedInUser()->su)
               @if($visitor->has_room == false)
                 <div class="five wide column">
                 <center><a href="{{ route('OHMS::visitors_create', ['id' => $visitor->id]) }}" type="submit" class="ui btn btn submit button"> <i class="edit icon"></i>{{ trans('ohms.visitor_create') }}</a></center>
                 @endif
                 
                  @if($visitor->has_room)
              
                 <div class="five wide column">
                 <center><a href="{{ route('OHMS::visitors_view', ['id' => $visitor->id]) }}" type="submit" class="ui btn btn submit button"> <i class="edit icon"></i>View</a></center></div>
                 
                 @endif
                 @endif
                 </div>
                      
            </td>
                <td >
               @if(!OHMS::isAdmin($visitor) or OHMS::loggedInUser()->su)
                 @if($visitor->has_room)
              
                 <div class="five wide column">
                 <center><a href="{{ route('OHMS::visitors_clear', ['id' => $visitor->id, 'room' => $visitor->room_id]) }}" type="submit" class="ui btn btn submit button"> <i class="edit icon"></i>Clear</a></center></div>
                 
                 @endif
                 @endif
                 </div>
                      
            </td>
                
          </tr>
          @endif
          @endforeach