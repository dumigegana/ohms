<table class="table table-bordered" " id="myTable">
  <thead>
    <tr>
      <th>Hostel</th>  
      <th>Room number</th>
      <th>Item</th>
      <th>Comments</th>
      <th>Date Reported</th>
      <th>Reported By</th>
    </tr> 
  </thead>
  <tbody>
      @foreach($rooms as $room)
      @foreach($room->maintenances as $maintenance) 
         
    <tr>
      <td>{{ $room->block->name }}</td>
      <td>{{ $room->room_number }}</td>
      <td>{{ $maintenance->item->name }}</td>
      <td>{{ $maintenance->comments }} </td>          
      <td>{{ $maintenance->created_at}}</td>
      <td>{{ $maintenance->user->username }}</td>
    </tr>
    @endforeach
    @endforeach
  </tbody>
</table> 