<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">

	<title>{{ trans('ohms.installer') }}</title>
	<meta name="description" content="OHMS - Laravel administration panel">
	<meta name="author" content="Èrik Campobadal Forés">

	{!! OHMS::includeAssets('ohms_header') !!}


  
</head>
 
<body style="background-color: #ffeaea;">

	<div class="ui inverted dimmer"><div class="ui text loader">{{ trans('ohms.installing') }}</div></div>


        
        <div style="padding-top: 10px;">
			<div id="welcome" style="display:none;">
			    <center>
			        <h1 class="ui huge header">{{ trans('ohms.install_welcome') }}</h1>
			    </center>
            </div>
            <div id="form" style="display:none;">
                <div class="ui doubling stackable one column grid container">
                    <div class="column">
                        <div class="ui very padded segment">

							<div class="ui equal width form">
								<form method="POST">
									{{ csrf_field() }}
									<div style="position: absolute; top: -70px; left: -80;">
            
						                <img class="ui small image" src="{{ OHMS::ohmsLogo() }}">
						            
						        	</div>
									<h1 class="ui header"><center>Solusi University Online Hostel Management System</center></h1><br>
										<div class="fields">
											<div class="field">
												<div class="field">
												<input name="APP_NAME" type="hidden" value="Solusi OHMS">
												</div>
											</div>
										</div>
									<br>

									<h1 class="ui header">{{ trans('ohms.install_personal_info') }}</h1><br>
									<div class="fields">
										<div class="required field">
											<label>Full Name</label>
										 	<input name="USER_FULL_NAME" required type="text" placeholder="Full Name" value="{{ old('USER_FULL_NAME')}}" >
										</div>
										
										<div class="required field">
											<label>Username</label>
										 	<input name="USER_NAME" required type="text" placeholder="Administrator Username" value="{{ old('USER_NAME')}}">
										</div>
										
									</div>
									<div class="fields">
									<div class="required field">
										  	<label>{{ trans('ohms.install_your_email') }}</label>
										  	<input name="USER_EMAIL" required type="email" placeholder="{{ trans('ohms.install_your_email') }}" value="{{ old('USER_EMAIL')}}">
										</div>
										<div class="required field">
										  	<label>{{ trans('ohms.install_your_password') }}</label>
										  	<input name="USER_PASSWORD" required type="password" placeholder="{{ trans('ohms.install_your_password') }}">
										</div>
										<div class="required field">
										  	<label>{{ trans('ohms.install_your_password_r') }}</label>
										  	<input name="USER_PASSWORD_confirmation" required type="password" placeholder="{{ trans('ohms.install_your_password_r') }}" >
										</div>
										</div>
										<div class="field">
											<div class="">
						                         <input type="hidden" name="USER_LOCALE" value="en">
						                      </div>
										</div>									
											<input name="ADMINISTRATOR_ROLE_NAME" value="Administrator" type="hidden">								
									
									<br>

									<h1 class="ui header">{{ trans('ohms.install_database_info') }} (MySQL/PostgreSQL)</h1><br>
									<div class="fields">
										<div class="required field">
										  	<label>{{ trans('ohms.install_database_host') }}</label>
										  	<input name="DB_HOST" required value="localhost" type="text" placeholder="{{ trans('ohms.install_database_host') }}" value="{{ old('DB_HOST')}}">
										</div>
										<div class="required field">
										  	<label>{{ trans('ohms.install_database_port') }}</label>
										  	<input name="DB_PORT" required value="3306" type="number" placeholder="{{ trans('ohms.install_database_port') }}" value="{{ old('DB_PORT')}}">
										</div>
									</div>
									<div class="fields">
										<div class="required field">
										  	<label>{{ trans('ohms.install_database_name') }}</label>
										  	<input name="DB_DATABASE" required type="text" placeholder="{{ trans('ohms.install_database_name') }}" value="{{ old('DB_DATABASE')}}">
										</div>
										<div class="required field">
										  	<label>{{ trans('ohms.install_database_username') }}</label>
										  	<input name="DB_USERNAME" required type="text" placeholder="{{ trans('ohms.install_database_username') }}" value="{{ old('DB_USERNAME')}}">
										</div>
										<div class="field">
										  	<label>{{ trans('ohms.install_database_password') }}</label>
										  	<input name="DB_PASSWORD" type="password" placeholder="{{ trans('ohms.install_database_password') }}">
										</div>
									</div>
									<br>

									<h1 class="ui header">{{ trans('ohms.install_mail_info') }}</h1><br>
									<div class="fields">
										<div class="field">
										  	<label>{{ trans('ohms.install_mail_driver') }}</label>
											<div class="ui fluid search selection dropdown">
												<input type="hidden" name="MAIL_DRIVER">
												<i class="dropdown icon"></i>
												<div class="default text">{{ trans('ohms.install_mail_driver') }}</div>
												<div class="menu">
													<div class="item" data-value="smtp">SMTP</div>
													<div class="item" data-value="mail">Mail</div>
													<div class="item" data-value="sendmail">Sendmail</div>
													<div class="item" data-value="mailgun">Mailgun</div>
													<div class="item" data-value="mandrill">Mandrill</div>
													<div class="item" data-value="ses">SES</div>
													<div class="item" data-value="sparkpost">Sparkpost</div>
													<div class="item" data-value="log">Log</div>
												</div>
											</div>
										</div>
										<div class="field">
										  	<label>{{ trans('ohms.install_mail_host') }}</label>
										  	<input name="MAIL_HOST" type="text" placeholder="{{ trans('ohms.install_mail_host') }}" value="{{ old('MAIL_HOST')}}">
										</div>
										<div class="field">
										  	<label>{{ trans('ohms.install_mail_port') }}</label>
										  	<input name="MAIL_PORT" type="number" placeholder="{{ trans('ohms.install_mail_port') }}" value="{{ old('MAIL_PORT')}}">
										</div>
									</div>
									<div class="fields">
										<div class="field">
											<label>{{ trans('ohms.install_mail_username') }}</label>
											<input name="MAIL_USERNAME" type="text" placeholder="{{ trans('ohms.install_mail_username') }}" value="{{ old('MAIL_USERNAME')}}">
										</div>
										<div class="field">
										  	<label>{{ trans('ohms.install_mail_password') }}</label>
										  	<input name="MAIL_PASSWORD" type="password" placeholder="{{ trans('ohms.install_mail_password') }}">
										</div>
									</div>
									<div class="fields">
										<div class="field">
											<label>{{ trans('ohms.install_mail_encryption') }}</label>
											<input name="MAIL_ENCRYPTION" type="text" placeholder="{{ trans('ohms.install_mail_encryption') }}">
										</div>
										<div class="field">
										  	<label>{{ trans('ohms.install_mail_from') }}</label>
										  	<input name="MAIL_FROM" type="email" placeholder="{{ trans('ohms.install_mail_from') }}" value="{{ old('MAIL_FROM')}}">
										</div>
										<div class="field">
										  	<label>{{ trans('ohms.install_mail_name') }}</label>
										  	<input name="MAIL_NAME" type="text" placeholder="{{ trans('ohms.install_mail_name') }}" value="{{ old('MAIL_NAME')}}">
										</div>
									</div>


									<br><br>

									<center>
										<button class="ui huge positive button" type="submit">{{ trans('ohms.install_ohms') }}</button>
									</center>
									<br>
								</form>
							</div>

                        </div>
						<br>
    				</div>
                </div>
            </div>
        </div>


	{!! OHMS::includeAssets('ohms_bottom') !!}

	<script>
		$('#welcome').fadeIn(1000, function(){
			$(this).fadeOut(1000, function(){
				$('#form').fadeIn(1000);
			});
		});
	</script>

</body>
</html>
