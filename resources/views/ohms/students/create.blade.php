@extends('layouts.admin.panel')
@section('css')
<link href="{{ URL::to('intlTelInput/build/css/intlTelInput.min.css') }}" rel="stylesheet">
<style type="text/css">
  #error-msg {
    color: red;
  }
  #valid-msg {
    color: #00C900;
  }
  #error-msg1 {
    color: red;
  }
  #valid-msg1 {
    color: #00C900;
  }
  
  #cell_number{
    text-indent: 30px;
  }
  #next_of_kin{
    text-indent: 30px;
  }
  input.error {
    border: 1px solid #FF7C7C;
  }
</style>
@endsection
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::students') }}">Students List</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.students_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.students_create_title'))
@section('icon', "add user")
@section('subtitle', trans('ohms.students_create_subtitle'))
@section('content')
<div class="ui container">
  <form id="create" class="ui form" method="POST">
    <div class="ui doubling stackable grid">
      <div class="row">
        <div class="eight wide column">
          <div class="ui very padded segment" style="background: #ffeaea">
                     {{ csrf_field() }}
                       
            <div class="required field">
                <label style="color: #980000;">Full Name</label>
                <input name='full_name' type="text" value="{{ old('full_name')}}" required>
            </div><br>
            <div class="required field">
              <label style="color: #980000;">Personal Email</label>
                <input name='email' type="email" value="{{ old('email')}}" required>
            </div><br>
            <div class="required field">
              <label style="color: #980000;">Password</label>
                <input name='password' type="password" placeholder="Password">
            </div><br>
            <div class="required field">
            <label style="color: #980000;">Confirm Password</label>
                <input name='password_confirmation' type="password" placeholder="Confirm Password">
            </div>
                 <div>
                <input name='role_id' type="hidden" value="2" >
            </div>
            <br>
             <div class="required field">
              <label style="color: #980000;">Student ID number</label>
              <input name='solusi_id' type="text" value="{{ old('solusi_id')}}" required>
            </div><br>
            <div class="required field">
            <label style="color: #980000 !important;">Date of Birth</label>
            <input name='date_of_birth' type="date" placeholder="">
            </div>
           <!-- <div class="required field">
            <label style="color: #980000;">Date of Birth</label>
            <div class="ui equal width form">
              <div class="fields"> 
                <div class="required field"><select name="month" onchange="call()" required >
                   <option value="">Month</option>
                   <option value="01" {{ (old("month") == '01' ? "selected":"") }}>Jan</option>
                   <option value="02" {{ (old("month") == '02' ? "selected":"") }}>Feb</option>
                   <option value="03" {{ (old("month") == '03' ? "selected":"") }}>Mar</option>
                   <option value="04" {{ (old("month") == '04' ? "selected":"") }}>Apr</option>
                   <option value="05" {{ (old("month") == '05' ? "selected":"") }}>May</option>
                   <option value="06" {{ (old("month") == '06' ? "selected":"") }}>Jun</option>
                   <option value="07" {{ (old("month") == '07' ? "selected":"") }}>Jul</option>
                   <option value="08" {{ (old("month") == '08' ? "selected":"") }}>Aug</option>
                   <option value="09" {{ (old("month") == '09' ? "selected":"") }}>Sep</option>
                   <option value="10" {{ (old("month") == '10' ? "selected":"") }}>Oct</option>
                   <option value="11" {{ (old("month") == '11' ? "selected":"") }}>Nov</option>
                   <option value="12" {{ (old("month") == '12' ? "selected":"") }}>Dec</option>
                </select></div>
                <div class="required field"><select name="day" required>
                   <option value="">Day</option>
                  </select></div>
                <div class="required field"><select name="yr" onchange="call()" required>
                   <option value="">Year</option>
                  </select></div>
                </div>
              </div>
            </div> -->                       
          </div>
        </div>
          
        <div class="eight wide column">
          <div class="ui very padded segment" style="background: #ffeaea">
            <div class="ui equal width form">
              <div class="fields">                         
                <div class="required field">
                <label style="color: #980000;">Cell number</label>
                <input type="hidden" value="" name="cell_phone_number" id="cell_phone_number">
                <input id="cell_number" name='cell' type="tel" value="{{ old('cell')}}" required>
                <input id="hidden" type="hidden" name="cell_number"><br>
                <span id="valid-msg" class="hide">✓ Valid</span>
                <span id="error-msg" class="hide">&times; Invalid number</span>
                </div>
                
                <div class="required field">
                <label style="color: #980000;">Next of Kin contact number</label>
                <input type="hidden" value="" name="next_of_kin_number" id="next_of_kin_number">
                <input id="next_of_kin" name='next' type="tel" value="{{ old('next')}}" required>
                <input id="hidden_n" type="hidden" name="next_of_kin"><br>
                <span id="valid-msg1" class="hide">✓ Valid</span>
                <span id="error-msg1" class="hide">&times; Invalid number</span>
                </div>
               </div>
            </div>                                            
            <div class="required field">
              <label style="color: #980000;">Home Address</label>
              <input name='home_address' type="text" value="{{ old('home_address')}}" required>
            </div>
            <div class="required field">
              <label style="color: #980000;">National ID number</label>
              <input name='national_id' type="text" value="{{ old('national_id')}}" required>
            </div>
            <div class="required field">
              <label style="color: #980000;">Major</label>
              <select name="major_id" class="form-control" required>
                <option value=""> Enter major</option>
                @foreach($majors as $major)
                <option value="{{ $major->id }}" {{ (old("major_id") == '$major->id' ? "selected":"") }}>{{ $major->name }}</option>
                 @endforeach
              </select>
            </div>
            <br/>
                 
            <div class="ui equal width form">
              <div class="fields">
                <div class="required field">
                  <label style="color: #980000;">Gender</label>
                  <input type="radio" name="gender" value="Male" {{ (old("gender") == 'Male' ? "checked":"") }}> Male<br>
                  <input type="radio" name="gender" value="Female" {{ (old("gender") == 'Female' ? "checked":"") }}> Female<br>
                </div>
                <div class="required field">
              <label style="color: #980000;">Marital Status</label>
              <select name="marital" class="form-control" required>
                <option value="Single" {{ (old("marital") == 'Single' ? "selected":"") }}> Single</option>
                <option value="Maried" {{ (old("marital") == 'Maried' ? "selected":"") }}>Maried</option>
                
              </select>
            </div>
                 
                <div class="required field" >
                  <label style="color: #980000;">Degree Level</label>                              
                  <input id="show" type="radio" name="degree_level" value="Undergraduate" {{ (old("degree_level") == 'Undergraduate' ? "checked":"") }}> Undergraduate<br>
                  <input id="hide" type="radio" name="degree_level" value="Postgraduate" {{ (old("degree_level") == 'Postgraduate' ? "checked":"") }} > Postgraduate<br>                                
                </div>
                <div class="field" id="shw">
                  <label style="color: #980000;">Year of study</label>
                    <input type="radio" name="year" value="1" {{ (old("year") == 1 ? "checked":"") }}> First<br>
                    <input type="radio" name="year" value="2" {{ (old("year") == 2 ? "checked":"") }}> Second<br>
                    <input type="radio" name="year" value="3" {{ (old("year") == 3 ? "checked":"") }}> Third<br>
                    <input type="radio" name="year" value="4" {{ (old("year") == 4 ? "checked":"") }}> Fouth<br>
                </div>
              </div>
            </div>
            <div class="inline field">
              <div class="ui slider checkbox">
                <input type="checkbox" value="1" class="hidden" name="active" checked>
                <label style="color: #980000 !important;">Activate Student account</label>
              </div>
            </div>
            <br>
            <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
          </div>
          <input type="hidden" name="banned" value="0"> 
        </div>
      </div>
    </div> 
  </form>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/intlTelInput-jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/utils.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#hide").click(function(){
        $("#shw").hide();
    });
    $("#show").click(function(){
        $("#shw").addClass("required field").show();
    });
  });

  var telInput = $("#cell_number");
  $("#error-msg").hide();
  $("#valid-msg").hide();

// initialise plugin
  telInput.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset = function() {
  $("#error-msg").hide();
  $("#valid-msg").hide();
  $("#cell_phone_number").val("");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      $("#error-msg").hide();
      $("#valid-msg").show();
      $("#cell_phone_number").val(1);
    } else {
      telInput.addClass("error");
      $("#valid-msg").hide();
      $("#error-msg").show();
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
// End of Cell phone input
//-----------------------------------------------------------------------------------------------------
$("form").submit(function() {
  $("#hidden").val($("#cell_number").intlTelInput("getNumber"));
  $("#hidden_n").val($("#next_of_kin").intlTelInput("getNumber"));
});

//---------------------------------------------------------------------------------------------------
// Next of kin input Starts
  var telInput1 = $("#next_of_kin");
  $("#error-msg1").hide();
  $("#valid-msg1").hide();

// initialise plugin
  telInput1.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset1 = function() {
  $("#error-msg1").hide();
  $("#valid-msg1").hide();  
  $("#next_of_kin_number").val("");
};

// on blur: validate
telInput1.blur(function() {
  reset1();
  if ($.trim(telInput1.val())) {
    if (telInput1.intlTelInput("isValidNumber")) {
      $("#error-msg1").hide();
      $("#valid-msg1").show();  
      $("#next_of_kin_number").val(1);

    } else {
      telInput1.addClass("error");
      $("#valid-msg1").hide();
      $("#error-msg1").show();

    }
  }
});

// on keyup / change flag: reset
telInput1.on("keyup change", reset1);
 // End of Next of kin contact inputs
 //--------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------- 
// Date selector
/**function call(){
 var kcyear = document.getElementsByName("yr")[0],
  kcmonth = document.getElementsByName("month")[0],
  kcday = document.getElementsByName("day")[0];
       
var d = new Date();
var n = d.getFullYear();
for (var i = n; i >= 1950; i--) {
  var opt = new Option();
  opt.value = opt.text = i;
  kcyear.add(opt);
  }
kcyear.addEventListener("change", validate_date);
kcmonth.addEventListener("change", validate_date);

function validate_date() {
  var y = +kcyear.value, m = kcmonth.value, d = kcday.value;
  if (m === "2")
    var mlength = 28 + (!(y & 3) && ((y % 100) !== 0 || !(y & 15)));
  else var mlength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1];
  kcday.length = 0;
  for (var i = 1; i <= mlength; i++) {
    var opt = new Option();
    opt.value = opt.text = i;
    if (i == d) opt.selected = true;
    kcday.add(opt);
   }
  }
  validate_date();
} **/
  //End of date input.
  //------------------------------------------------------------------------------------------------
 </script>
@endsection
