<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role_User;
use Illuminate\Support\Facades\DB;

class Role_UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         Model::unguard();
        DB::table('role_user');
        $role_user = array(
            ['role_id' => '2', 'user_id' => '2'],
            ['role_id' => '2', 'user_id' => '3'],
            ['role_id' => '2', 'user_id' => '4'],
            ['role_id' => '2', 'user_id' => '5'],
            ['role_id' => '4', 'user_id' => '6'],
            ['role_id' => '4', 'user_id' => '7'],
            ['role_id' => '4', 'user_id' => '8'],
            ['role_id' => '4', 'user_id' => '9'],
            ['role_id' => '4', 'user_id' => '10'],
            ['role_id' => '4', 'user_id' => '11'],
            ['role_id' => '4', 'user_id' => '12'],
            ['role_id' => '4', 'user_id' => '13'],
            ['role_id' => '4', 'user_id' => '14'],
            ['role_id' => '4', 'user_id' => '15'],
            ['role_id' => '4', 'user_id' => '16'],
            ['role_id' => '4', 'user_id' => '17'],
            ['role_id' => '4', 'user_id' => '18'],
            ['role_id' => '4', 'user_id' => '19'],
            ['role_id' => '4', 'user_id' => '20'],            
        );

        // Loop through each user above and create the record for them in the database
        foreach ($role_user as $role_user)
        {
            Role_User::create($role_user);
        }
        Model::reguard();
    }
}


