@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::flights') }}">{{ trans('ohms.flights_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.flights_edit_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.flights_edit_title'))
@section('icon', "edit")
@section('subtitle', trans('ohms.flights_edit_subtitle', ['name' => $row->name]))
@section('content')
<div class="ui doubling stackable grid container">
    <div class="three wide column"></div>
    <div class="ten wide column">
        <div class="ui very padded segment" style="background: #ffeaea; color: #980000;">
            <form class="ui form" method="POST">
                {{ csrf_field() }}
                @include('ohms/forms/master')
                <div class="required field" id="block">
                    <label style="color: #980000;">Block</label> 
                    <select name="status" id="sel_blck">              
                      <option value="" disabled selected>select </option>
                      <option value="Return">Return </option>
                      <option value="Arrival">Arrival </option> 
                    </select>
                  </div>
                <br>
                <button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button>
            </form>
        </div>
    </div>
    <div class="three wide column"></div>
</div>
@endsection
