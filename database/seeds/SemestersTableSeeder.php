<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Semester;
use Illuminate\Support\Facades\DB;


class SemestersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         Model::unguard();
        DB::table('semesters');
        $semesters = array(
                ['name' => 'Regular May', 'term' => 'May-August' ],
                ['name' => 'Regular Sept', 'term' => 'September-December' ],
                ['name' => 'Summer', 'term' => 'February-March' ],
                ['name' => 'Block April', 'term' => 'April' ],
                ['name' => 'Block August', 'term' => 'August' ],
                ['name' => 'Block December', 'term' => 'December' ],
        );


        // Loop through each user above and create the record for them in the database
        foreach ($semesters as $semester)
        {
            Semester::create($semester);
        }
        Model::reguard();
    }
}
