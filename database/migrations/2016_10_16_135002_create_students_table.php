<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('solusi_id')->unique();
            $table->string('gender');
            $table->string('email');
            $table->date('date_of_birth');
            $table->string('national_id');
            $table->string('home_address');
            $table->string('next_of_kin');
            $table->string('degree_level');
            $table->integer('year')->nullable();
            $table->integer('major_id');
            $table->boolean('marital')->default(false);
            $table->string('cell_number');
            $table->boolean('has_room')->default(false);
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
