<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->boolean('assignable');
            $table->boolean('allow_editing');
            $table->boolean('su')->default(false);
            $table->timestamps();
        });

        $role = \OHMS::newRole();
        $role->name = env('ADMINISTRATOR_ROLE_NAME', 'Administrator');
        $role->assignable = false;
        $role->allow_editing = false;
        $role->su = true;
        $role->save();

        $roles = [
            'Student',
            'Dean',
            'Visitor',
            'Staff'
        ];

        foreach($roles as $role) {
            $rol = \OHMS::newRole();
            $rol->name = $role;
            $rol->assignable = true;
            $rol->allow_editing = true;
            $rol->su = false;
            $rol->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
    }
}
