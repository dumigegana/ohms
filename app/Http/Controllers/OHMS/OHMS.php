<?php

namespace App\Http\Controllers\OHMS;

use Request;
use Auth;
use Storage;
use Schema;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Hostel; 
use App\Block;
use App\Room;
use App\Role_User;
use App\Permission; 
use App\Student;
use App\Staff;
use App\Visitor;
use App\Major;
use App\Semester;
use App\Check_list;
use App\Check_in_out;
use App\Maintenance; 
use App\Department;
use App\Facility;
use App\Notice;
use App\Category;
use App\Item;
use App\Maint_dep;
use App\Allocation;
use App\Facility_status;
use App\Flight;
use App\House_category;
use App\House;
use App\Session;
use App\Attendance;
use App\Course;
use App\Desciplinary;
use App\Benefit_Record;
use App\Policy;
use App\Benefit;
use App\Dependant;
use App\Accademic;
use App\Work_record;
use App\Resume;
use Location;
use URL;


/** 
 * The OHMS class
 */

class OHMS extends Controller
{

    public static function users($type = null, $data = null)
    {
        if($type and $data) {
            return User::where($type, $data)->get();
        }
        return User::paginate(50);
    }
    public static function students($type = null, $data = null)
    {
        if($type and $data) {
            return Student::where($type, $data)->get();
        }
        return Student::paginate(50);
    }
    public static function visitors($type = null, $data = null)
    {
        if($type and $data) {
            return Visitor::where($type, $data)->get();
        }
        return Visitor::paginate(50);
    }
    public static function majors($type = null, $data = null)
    {
        if($type and $data) {
            return Major::where($type, $data)->get();
        }
        return Major::all();
    }
    public static function hostels($type = null, $data = null)
    {
        if($type and $data) {
            return Hostel::where($type, $data)->get();
        }
        return Hostel::all();
    }
    public static function blocks($type = null, $data = null)
    {
        if($type and $data) {
            return Block::where($type, $data)->get();
        }
        return Block::all();
    }
    public static function rooms($type = null, $data = null)
    {
        if($type and $data) {
            return Room::where($type, $data)->get();
        }
        return Room::all();
    }

    public static function courses($type = null, $data = null)
    {
        if($type and $data) {
            return Course::where($type, $data)->get();
        }
        return Course::all();
    }
    
    public static function semesters($type = null, $data = null)
    {
        if($type and $data) {
            return Semester::where($type, $data)->get();
        }
        return Semester::all();
    }
    public static function maintenances($type = null, $data = null)
    {
        if($type and $data) {
            return Maintenance::where($type, $data)->get();
        }
        return Maintenance::all();
    }
    public static function statuses($type = null, $data = null)
    {
        if($type and $data) {
            return Status::where($type, $data)->get();
        }
        return Status::all();
    }
    public static function facilities($type = null, $data = null)
    {
        if($type and $data) {
            return Facility::where($type, $data)->get();
        }
        return Facility::all();
    }
    
    public static function checklists($type = null, $data = null)
    {
        if($type and $data) {
            return Check_list::where($type, $data)->get();
        }
        return Check_list::all();
    }

    public static function facility_statuses($type = null, $data = null)
    {
        if($type and $data) {
            return Facility_status::where($type, $data)->get();
        }
        return Facility_status::all();
    }

    public static function sessions($type = null, $data = null)
    {
        if($type and $data) {
            return Session::where($type, $data)->get();
        }
        return Session::all();
    }

    public static function course($type, $data)
    {
        if($type == 'id') {
            return Course::findOrFail($data);
        }
        return Course::where($type, $data)->first();
    }
    public static function attendances($type = null, $data = null)
    {
        if($type and $data) {
            return Attendance::where($type, $data)->get();
        }
        return Attendance::all();
    }

    
    public static function houses($type = null, $data = null)
    {
        if($type and $data) {
            return House::where($type, $data)->get();
        }
        return House::all();
    }

    public static function house_categories($type = null, $data = null)
    {
        if($type and $data) {
            return House_category::where($type, $data)->get();
        }
        return House_category::all();
    }
    
    public static function flights($type = null, $data = null)
    {
        if($type and $data) {
            return Flight::where($type, $data)->get();
        }
        return Flight::all();
    }

    public static function roles($type = null, $data = null)
    {
        if($type and $data) {
            return Role::where($type, $data)->get();
        }
        return Role::all();
    }

    public static function permissions($type = null, $data = null)
    {
        if($type and $data) {
            return Permission::where($type, $data)->get();
        }
        return Permission::all();
    }

    

    public static function user($type, $data)
    {
        if($type == 'id') {
            return User::find($data);
        }
        return User::where($type, $data)->first();
    }

    public static function role($type, $data)
    {
        if($type == 'id') {
            return Role::find($data);
        }
        return Role::where($type, $data)->first();
    }

    public static function permission($type, $data)
    {
        if($type == 'id') {
            return Permission::find($data);
        }
        return Permission::where($type, $data)->first();
    }

    public static function student($type, $data)
    {
        if($type == 'id') {
            return Student::find($data);
        }
        return Student::where($type, $data)->first();
    }

    public static function visitor($type, $data)
    {
        if($type == 'id') {
            return Visitor::find($data);
        }
        return Visitor::where($type, $data)->first();
    }

    public static function major($type, $data)
    {
        if($type == 'id') {
            return Major::find($data);
        }
        return Major::where($type, $data)->first();
    }
    public static function hostel($type, $data)
    {
        if($type == 'id') {
            return Hostel::find($data);
        }
        return Hostel::where($type, $data)->first();
    }
    public static function block($type, $data)
    {
        if($type == 'id') {
            return Block::find($data);
        }
        return Block::where($type, $data)->first();
    }
    public static function room($type, $data)
    {
        if($type == 'id') {
            return Room::find($data);
        }
        return Room::where($type, $data)->first();
    }
    public static function semester($type, $data)
    {
        if($type and $data) {
            return Semester::find($data);
        }
        return Semester::where($type, $data)->first();
    }
     public static function status($type, $data)
    {
        if($type and $data) {
            return Status::find($data);
        }
        return Status::where($type, $data)->first();
    }
    
    public static function facility($type, $data)
    {
        if($type == 'id') {
            return Facility::find($data);
        }
        return Facility::where($type, $data)->first();
    }

    public static function facility_status($type, $data)
    {
        if($type == 'id') {
            return Facility_status::find($data);
        }
        return Facility_status::where($type, $data)->first();
    }
    
    public static function house($type, $data)
    {
        if($type == 'id') {
            return Facility::find($data);
        }
        return Facility::where($type, $data)->first();
    }

    public static function house_category($type, $data)
    {
        if($type == 'id') {
            return Facility_status::find($data);
        }
        return Facility_status::where($type, $data)->first();
    }
    
    public static function newFacility_status()
    {
        return new Facility_status;
    }


    public static function flight($type, $data)
    {
        if($type == 'id') {
            return Flight::find($data);
        }
        return Flight::where($type, $data)->first();
    }
    
    public static function newFlight()
    {
        return new Flight;
    }


    public static function notices($type = null, $data = null)
    {
        if($type and $data) {
            return Notice::where($type, $data)->get();
        }
        return Notice::all();
    }
    public static function notice($type, $data)
    {
        if($type and $data) {
            return Notice::find($data);
        }
        return Notice::where($type, $data)->first();
    }   
    public static function newNotice()
    {
        return new Notice;
    }
    public static function items($type = null, $data = null)
    {
        if($type and $data) {
            return Item::where($type, $data)->get();
        }
        return Item::all();
    }
    public static function item($type, $data)
    {
        if($type and $data) {
            return Item::find($data);
        }
        return Item::where($type, $data)->first();
    }   
    public static function newItem()
    {
        return new Item;
    }
    public static function newCourse()
    {
        return new Course;
    }


    public static function maint_deps($type = null, $data = null)
    {
        if($type and $data) {
            return Maint_dep::where($type, $data)->get();
        }
        return Maint_dep::all();
    }
    public static function maint_dep($type, $data)
    {
        if($type and $data) {
            return Maint_dep::find($data);
        }
        return Maint_dep::where($type, $data)->first();
    }   
    public static function newMaint_dep()
    {
        return new Maint_dep;
    }
     
     public static function allocations($type = null, $data = null)
    {
        if($type and $data) {
            return Allocation::where($type, $data)->get();
        }
        return Allocation::all();
    }
    public static function allocation($type, $data)
    {
        if($type and $data) {
            return Allocation::find($data);
        }
        return Allocation::where($type, $data)->first();
    } 
     public static function newAllocation()
    {
        return new Allocation;
    }
     public static function allocationViews($type = null, $data = null)
    {
        if($type and $data) {
            return Allocation_View::where($type, $data)->get();
        }
        return Allocation_View::all();
    }
    
    public static function session($type, $data)
    {
        if($type == 'id') {
            return Session::findOrFail($data);
        }
        return Session::where($type, $data)->first();
    }


    public static function attendance($type, $data)
    {        if($type == 'id') {
            return Attendance::findOrFail($data);
        }
        return Attendance::where($type, $data)->first();
    }





    
    public static function newUser()
    {
        return new User;
    }

    public static function newStudent()
    {
        return new Student;
    }

    public static function newVisitor()
    {
        return new Visitor;
    }
    
    public static function newMajor()
    {
        return new Major;
    }

    public static function newRole()
    {
        return new Role;
    }

    public static function newPermission()
    {
        return new Permission;
    }
     public static function newHostel()
    {
        return new Hostel;
    }

    public static function newBlock()
    {
        return new Block;
    }

    public static function newRoom()
    {
        return new Room;
    }
     public static function newSemester()
    {
        return new Semester;
    }
    

    public static function newFacility()
    {
        return new Facility;
    }

    public static function newMaintenance()
    {
        return new Maintenance;
    }


    public static function newHouse()
    {
        return new House;
    }

    public static function newHouse_category()
    {
        return new House_category;
    }

    public static function newSession()
    {
        return new Session;
    }

    public static function newAttendance()
    {
        return new Attendance;
    }
 

     public static function locales()
    {
        require('Data/Locales.php');
        if($locales){
            return $locales;
        }
    }

    public static function permissionToAccess($slug)
    {
        if(!OHMS::loggedInUser()->hasPermission($slug)) {
            abort(401, "You don't have permissions to access this area");
        }
    }

    public static function scanFiles($directory){
        return scandir($directory);
    }

    public static function files()
    {
        $files = Storage::files();
        $ignore = ['.gitignore'];
        $final_files = [];
        foreach($files as $file) {
            $add = true;
            foreach($ignore as $ign){
                if($ign == $file) {
                    $add = false;
                }
            }
            if($add) {
                array_push($final_files, $file);
            }
        }
        $files = $final_files;

        return $files;
    }

    public static function isDocument($file_name)
    {
        if(OHMS::document('name', $file_name)) {
            return true;
        } else {
            return false;
        }
    }

    
    public static function imageFormats()
    {
        return ['png', 'jpg', 'jpeg', 'gif', 'bmp'];
    }

    public static function checkInstalled()
    {
        if(env('OHMS_INSTALLED', false)){
            return true;
        }
        return false;
    }

    public static function loggedInUser() {
        return Auth::user();
    }

    
    public static function allowEditingField()
    {
        return 'allow_editing';
    }

    public static function dropdown($slug, $array = null)
    {
        require('Data/Dropdowns.php');
        if($array) {
            return $dropdowns[$slug];
        } else {
            $dropdowns_object = [];
            foreach($dropdowns[$slug] as $drop) {
                array_push($dropdowns_object, OHMS::rettype($drop, 'object'));
            }
            return $dropdowns_object;
        }
    }

    public static function checkValueInRelation($data, $value, $value_index)
    {
        foreach($data as $dta) {
            if($dta->$value_index == $value) {
                return true;
            }
        }
        return false;
    }

    public static function rettype($mixed, $type = NULL) {
        $type === NULL || settype($mixed, $type);
        return $mixed;
    }

   
    public static function mustBeAdmin($user)
    {
        if(!OHMS::isAdmin($user)) {
            abort(403, trans('ohms.error_must_be_admin'));
        }
    }
    public static function mustBeStudent($user)
        {
            if(!OHMS::isStudent($user)) {
                abort(403, trans('ohms.error_must_be_student'));
            }
        }

    public static function mustBeVisitor($user)
        {
            if(!OHMS::isVisitor($user)) {
                abort(403, trans('ohms.error_must_be_visitor'));
            }
        }
 
    public static function mustNotBeAdmin($user)
    {
        if(OHMS::isAdmin($user) and !OHMS::loggedInUser()->su) {
            abort(403, trans('ohms.error_no_rights_against_admin'));
        }
    }

    public static function isAdmin($user)
    {
        return $user->isAdmin();
    }

    public static function isStudent($user)
    {
        return $user->isStudent();
    }

    public static function isVisitor($user)
    {
        return $user->isVisitor();
    }
    
    public static function permissionName($slug)
    {
        $perm_file = 'permissions';
        $trans = trans($perm_file.'.'.$slug);
        if($perm_file.'.'.$slug == $trans) {
            return $slug;
        } else {
            return $trans;
        }
    }

    public static function permissionDescription($slug)
    {
        $perm_file = 'permissions';
        $trans = trans($perm_file.'.'.$slug.'_desc');
        $slug = $slug . '_desc';
        if($perm_file.'.'.$slug == $trans) {
            return "No description";
        } else {
            return $trans;
        }
    }

    ### Grouped by Model

    #Staff Model
    public static function staffs($type = null, $data = null)
    {
        if($type and $data) {
            return Staff::where($type, $data)->get();
        }
        return Staff::paginate(50);
    }
    
    public static function staff($type, $data)
    {
        if($type == 'id') {
            return Staff::find($data);
        }
        return Staff::where($type, $data)->first();
    }
    
    public static function mustBeStaff($user)
        {
            if(!OHMS::isStaff($user)) {
                abort(403, trans('ohms.error_must_be_staff'));
            }
        }
        
    public static function newStaff()
    {
        return new Staff;
    }
    
    
    public static function isStaff($user)
    {
        return $user->isStaff();
    }

    
    public static function departments($type = null, $data = null)
    {
        if($type and $data) {
            return Department::where($type, $data)->get();
        }
        return Department::paginate(50);
    }
    
    public static function department($type, $data)
    {
        if($type == 'id') {
            return Department::find($data);
        }
        return Department::where($type, $data)->first();
    }
    
         
    public static function newDepartment()
    {
        return new Department;
    }
     #Desciplinary Model
    public static function desciplinaries($type = null, $data = null)
    {
        if($type and $data) {
            return Desciplinary::where($type, $data)->get();
        }
        return Desciplinary::paginate(50);
    }
    
    public static function desciplinary($type, $data)
    {
        if($type == 'id') {
            return Desciplinary::find($data);
        }
        return Desciplinary::where($type, $data)->first();
    }
            
    public static function newDesciplinary()
    {
        return new Desciplinary;
    }
    
    public static function benefit_records($type = null, $data = null)
    {
        if($type and $data) {
            return Benefit_record::where($type, $data)->get();
        }
        return Benefit_record::paginate(50);
    }
    
    public static function benefit_record($type, $data)
    {
        if($type == 'id') {
            return Benefit_record::find($data);
        }
        return Benefit_record::where($type, $data)->first();
    }
        
    public static function newBenefit_record()
    {
        return new Benefit_record;
    }
    
     #Policy Model
    public static function policies($type = null, $data = null)
    {
        if($type and $data) {
            return Policy::where($type, $data)->get();
        }
        return Policy::paginate(50);
    }
    
    public static function policy($type, $data)
    {
        if($type == 'id') {
            return Policy::find($data);
        }
        return Policy::where($type, $data)->first();
    }
        
    public static function newPolicy()
    {
        return new Policy;
    }
    
    public static function benefits($type = null, $data = null)
    {
        if($type and $data) {
            return Benefit::where($type, $data)->get();
        }
        return Benefit::paginate(50);
    }
    
    public static function benefit($type, $data)
    {
        if($type == 'id') {
            return Benefit::find($data);
        }
        return Benefit::where($type, $data)->first();
    }
        
    public static function newBenefit()
    {
        return new Benefit;
    }
    
    #Accademic Model
    public static function accademics($type = null, $data = null)
    {
        if($type and $data) {
            return Accademic::where($type, $data)->get();
        }
        return Accademic::paginate(50);
    }
    
    public static function accademic($type, $data)
    {
        if($type == 'id') {
            return Accademic::find($data);
        }
        return Accademic::where($type, $data)->first();
    }
        
    public static function newAccademic()
    {
        return new Accademic;
    }
    #Dependant Model
    public static function dependants($type = null, $data = null)
    {
        if($type and $data) {
            return Dependant::where($type, $data)->get();
        }
        return Dependant::paginate(50);
    }
    
    public static function dependant($type, $data)
    {
        if($type == 'id') {
            return Dependant::find($data);
        }
        return Dependant::where($type, $data)->first();
    }
        
    public static function newDependant()
    {
        return new Dependant;
    }
    
    
    public static function depandants($type = null, $data = null)
    {
        if($type and $data) {
            return Depandant::where($type, $data)->get();
        }
        return Depandant::paginate(50);
    }
    
    public static function depandant($type, $data)
    {
        if($type == 'id') {
            return Depandant::find($data);
        }
        return Depandant::where($type, $data)->first();
    }
        
    public static function newDepandant()
    {
        return new Depandant;
    }
    
    #Work_record Model
    public static function work_records($type = null, $data = null)
    {
        if($type and $data) {
            return Work_record::where($type, $data)->get();
        }
        return Work_record::paginate(50);
    }
    
    public static function work_record($type, $data)
    {
        if($type == 'id') {
            return Work_record::find($data);
        }
        return Work_record::where($type, $data)->first();
    }
        
    public static function newWork_record()
    {
        return new Work_record;
    }
    
    public static function resumes($type = null, $data = null)
    {
        if($type and $data) {
            return Resume::where($type, $data)->get();
        }
        return Resume::paginate(50);
    }
    
    public static function resume($type, $data)
    {
        if($type == 'id') {
            return Resume::find($data);
        }
        return Resume::where($type, $data)->first();
    }
        
    public static function newResume()
    {
        return new Resume;
    }
    
    
   

    /**
     * Formats a string date into a fancy, human readable date
     *
     * Returns a date formatted for human readable
     *
     * @param string $date_string The date in string format
     *
     * @return date
     */

    public static function fancyDate($date_string)
    {
        return date('F j, Y, g:i A',strtotime($date_string));
    }

    
    public static function randomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    public static function randomColor()
    {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }

    public static function currentURL()
    {
        return Request::url();
    }

    public static function previousURL()
    {
        return URL::previous();
    }

    public static function includeAssets($name)
    {
        require('Data/Includes.php');
        if($includes){
            return $includes[$name];
        }
    }

    public static function publicPath()
    {
        return 'ohms_public';
    }

    
    public static function widget($name)
    {
        require('Data/Widgets.php');
        if($widgets and array_key_exists($name, $widgets)){
            return $widgets[$name];
        } else {
            return "<span style='color:red;'>ERROR: </span> Unknown widget";
        }
    }

    public static function ohmsLogo()
    {
        return asset(OHMS::publicPath() . '/images/logo-text.png');
    }

    public static function ohmsFavicon()
        {
            return asset(OHMS::publicPath() . '/favicon/favicon.ico');
        }

    public static function dataPath()
    {
        return app_path() . '/Http/Controllers/OHMS/Data';
    }

    public static function apiData()
    {
        require('Data/API.php');
        if($api){
            return $api;
        }
    }


}
