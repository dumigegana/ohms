
  
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="ohmssani Ndlovu">
        <meta name="description" content="OHMS - Students panel">
        <meta name="viewport" content="width=device-width, initial-scale=1">
 
        <title>OHMS</title>
       <link rel="shortcut icon" href="{{ URL::to('favicon.ico') }}" >
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link href="{{ URL::to('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ URL::to('css/simple-sidebar.css') }}" rel="stylesheet">
        <link rel='stylesheet' type='text/css' href="{{ URL::to( 'ohms_public/sweetalert/sweetalert.css') }}">
        <script type="text/javascript" src="{{ asset('ohms_public/sweetalert/sweetalert.min.js') }}"></script>
       
         
@include('student.partials.css')

  @yield('css')
   