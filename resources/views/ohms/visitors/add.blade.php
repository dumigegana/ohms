 @extends('layouts.admin.panel')
@section('breadcrumb')
<div class="ui breadcrumb">
    <div class="active section">{{ trans('ohms.visitor_list') }}</div>
</div>
@endsection
@section('title', trans('ohms.visitor_list'))
@section('icon', "visitors")
@section('subtitle', trans('ohms.visitors_subtitle'))

@section('content')
  <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="twelve wide column" style="padding-top: 0 !important;">{!! $visitors->links() !!}</div>
            <div class="four wide column">
              <div class="input-group input-group-sm">
                <input type="text" class="form-control" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                <span class="input-group-btn">
                  <button type="input-group-btn" class="btn btn-info btn-flat">
                    <i class="search icon"></i>
                  </button>
                </span>  
              </div>
            </div>   
        </div>  
        <table class="ui basic table " id="myTable">
          <thead style="background: #ff8b8b;">
            <tr>
                  <th style="color: #980000;">{{ trans('ohms.full_name') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.options') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.options') }}</th>
                  
            </tr> 
          </thead>          
          <tbody id="read">
                @foreach($visitors as $visitor)
                 @if(OHMS::isVisitor($visitor))                    
                    <tr style="color: #980000;">
                      <td>
                                      <div class="text">                                
                                          {{ $visitor->full_name }}
                                            @if($visitor->has_room == true)
                                            <div class="ui blue tiny left pointing basic label pop" data-title="{{ trans('ohms.admin_access') }}" data-variation="wide" data-content="{{ trans('ohms.admin_access_desc') }}"
                                             data-position="top center">Has room</div>
                                             @else
                                             <div class="ui red tiny left pointing basic label pop" data-title="{{ trans('ohms.admin_access') }}" data-variation="wide" data-content="{{ trans('ohms.admin_access_desc') }}"
                                             data-position="top center">No room</div> 
                                             @endif

                                      </div>
                          </td>
                
                <td >
               @if(!OHMS::isAdmin($visitor) or OHMS::loggedInUser()->su)
               @if($visitor->has_room == false)
                 <div class="five wide column">
                 <center><a href="{{ route('OHMS::visitors_create', ['id' => $visitor->id]) }}" type="submit" class="ui btn btn submit button"> <i class="edit icon"></i>{{ trans('ohms.visitor_create') }}</a></center>
                 @endif
                 
                  @if($visitor->has_room)
              
                 <div class="five wide column">
                 <center><a href="{{ route('OHMS::visitors_view', ['id' => $visitor->id]) }}" type="submit" class="ui btn btn submit button"> <i class="edit icon"></i>View</a></center></div>
                 
                 @endif
                 @endif
                 </div>
                      
            </td>
                <td >
               @if(!OHMS::isAdmin($visitor) or OHMS::loggedInUser()->su)
                 @if($visitor->has_room)
              
                 <div class="five wide column">
                 <center><a href="{{ route('OHMS::visitors_clear', ['id' => $visitor->id, 'room' => $visitor->visitor->room_id]) }}" type="submit" class="ui btn btn submit button"> <i class="edit icon"></i>Clear</a></center></div>
                 
                 @endif
                 @endif
                 </div>
                      
            </td>
                
          </tr>
          @endif
        @endforeach
          </tbody>
        </table>
        {!! $visitors->links() !!}
      </div>
        <br>
    </div>
  </div>
@endsection
@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
    var keywords = $(this).val(); 
    $.ajax({
      url:"{{ url('admin/search_visitor') }}" ,
      type:"POST",
      data:{"_token": $('#token').val(), 'keywords' : keywords},
      success : function(search){
        $('#read').html(search);
        }
      });
              
    });

</script>
@endsection