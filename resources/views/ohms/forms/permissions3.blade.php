<center>
    <div id="check_all" class="w3-button theme">{{ trans('dumi.check_all') }}</div>
    <div id="uncheck_all" class="w3-button theme">{{ trans('dumi.uncheck_all') }}</div>
</center><br><br>
    <?php $found = false; ?>
    <?php $counter = 0; ?>            
    <div class="w3-row-padding">
    @foreach($permissions as $perm)
        <?php $found = true ?>
        <div class="w3-third">
            <label class="switch">
                <input
                    <?php
                        $disabled = false;
                        if(isset($role)){
                            if($role->hasPermission($perm->slug)) {
                                echo "checked='checked' ";
                            }

                            if($role->su) {
                                if($perm->su) {
                                    $disabled = true;
                                 }
                            }
                        }
                        if(!$disabled and (!$perm->assignable and !Dumi::loggedInUser()->su)){
                            $disabled = true;
                        }
                        if($disabled) {
                            echo "disabled ";
                        }
                    ?>
                    name="{{ $perm->id }}" type="checkbox"  tabindex="0" class="@if(!$disabled) checkable @endif hidden">

                <span class="@if($disabled) slider-off @else slider @endif round"></span>
                <label class="label">{{ Dumi::permissionName($perm->slug) }}
                   <i class="w3-text-grey fa fa-question tooltip">
                      <span class="tooltiptext w3-tag w3-round-xlarge">{{ Dumi::permissionDescription($perm->slug) }}. 
                        @if(!$perm->assignable and !Dumi::loggedInUser()->su) <br> {{ trans('dumi.unassignable_permission_desc') }} @endif
                        @if(!$perm->assignable and Dumi::loggedInUser()->su and !$disabled) <br> {{ trans('dumi.unassignable_permission_unlocked_desc') }} @endif
                        @if(Dumi::loggedInUser()->su and $disabled) <br>{{ trans('dumi.su_permission_and_role_desc') }}
                        @endif
                        </span></i>
                </label>
            </label>
        </div>      
            <?php if($counter == 2){echo "<div class='w3-right' style='margin: 10px 50px; vertical-align: left'></div>";$counter=0;}else{$counter++;} ?>
            @endforeach
    </div> 
        <?php if($counter == 2){echo "<div class='w3-right' style='margin: 10px 50px;vertical-align: left'></div>";}    ?>
        
        @if(!$found)
            <div class="col-md-6 col-lg-4 down-spacer">
                <p>No permissions found</p>
            </div>
        @endif
