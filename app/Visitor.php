<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Visitor extends Model
{
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    protected $fillable = [
        'user_id', 'gender', 'arrival_date', 'departure_date', 'transport', 'spause', 'has_room', 'a_flight', 'r_flight', 'room_id'
    ];

    protected $with = ['user'];
    
     public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function room()
    {
         return $this->belongsTo('App\Room');
    }
     
    /* Visitor Arrive by flight
    public function hasA_Flight()
    {
        return isset($this->a_flight);
    }

    // Visitor Return by flight
    public function hasR_Flight()
    {
        return isset($this->r_flight);
    }*/

    // Arrival Flights
    public function ar_flight() {
        return $this->belongsTo('App\Flight', 'a_flight', 'id');
    }

    // Arrival Flights
    public function re_flight() {
        return $this->belongsTo('App\Flight', 'r_flight', 'id');
    }
    
     public function house_allocations()
    {
        return $this->morphMany('App\House_allocations', 'house_allocable');
    }

}
