<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    protected $fillable = [
        'user_id', 'solusi_id', 'gender', 'date_of_birth', 'national_id', 'home_address', 'next_kin', 'degree_level', 'year', 'major_id','marital', 'cell_number', 'has_room', 
    ];
    
    protected $with = ['user'];
    
     public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function major()
    {
        return $this->belongsTo('App\Major');
    }

    public function allocations()
    {
        return $this->hasMany('App\Allocation');
    }
    public function checkists()
    {
        return $this->hasManyThrough('App\Checkist', 'App\Allocation', 'student_id', 'allocation_id', 'id');
    }
     public function house_allocations()
    {
        return $this->morphMany('App\House_allocations', 'house_allocable');
    }

    public function attendances()
    {
        return $this->hasMany('App\Attendance');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'course_student', 'student_id', 'course_id');
    }

    public function hasCourse($short_code)
    {
        foreach($this->courses as $course) {
            if($course->short_code == $short_code) {
                return true;
            }
        }
        return false;
    }


}
