<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" id="token">
  <title>OHMS</title>
  <link rel="shortcut icon" href="{{ OHMS::ohmsFavicon() }}" >
  <link href="{{ URL::to( 'css/bootstrap.min.css') }}" rel="stylesheet">
  <style type="text/css">
    .row{
      padding: 0px;
      margin: 0px;
    }
    .main{
      min-height:100%;
      background-color:#ff8b8b; 
    }
    .ohms_body{
      background-color: #ffeaea;
    }
    .btn{
      background-color: #980000;
      color: #ffeaea;
    }
    .btn:hover{
      background-color: #ff8b8b;
    }
    .btn:active{
      background-color: #ff7070;
    }
    .nav{
      color: #ffeaea;
      font-weight: bolder;
    } 
       
  </style>
  @yield('css')
        
  </head>
    <body>
         @include('layouts.partials.navr')
         <br><br>
         @yield('page-content') 
      <br>
      <!-- <script type="text/javascript" src="{{ asset('js/bootstrap-3.3.7.min.js') }}"></script> -->

    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/printThis.js') }}"></script>
    @yield('js')

    </body>
</html>
