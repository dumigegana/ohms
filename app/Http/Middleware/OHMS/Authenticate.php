<?php

namespace App\Http\Middleware\OHMS;

use Closure;
use Auth;
use OHMS;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(OHMS::checkInstalled()) {
            if(Auth::check()) {
                OHMS::mustBeAdmin(OHMS::loggedInUser());
            } else {
                return redirect('/')->with('error', 'You are not logged in');
            }
        }
        return $next($request);
    }
}
