 @foreach($resumes as $resume) 
            <tr style="color: #980000;">
              <td>{{ $resume->resume_number }}</td>
              <td>{{ $resume->block->hostel->gender }}</td>
              <td>{{ $resume->block->name }}</td>
              <td>{{ $resume->capacity }}</td>
              <td class="center alligned">
                      @if ($resume->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Resume ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this resume " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::resumes_edit', ['id' => $resume->id]) }}" class="ui btn button">{{ trans('ohms.resumes_edit') }}</a>
              </td>       
            </tr>
        @endforeach