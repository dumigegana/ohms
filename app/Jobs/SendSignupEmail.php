<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Mail\Mailer;
use App\Mail\ConfirmId;
use Mail;
use App\Conf_stud;

class SendSignupEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $conf_stud;
    /**
     * Create a new job instance.
     *
     * @param  Conf_stud  $conf_stud
     * @return void
     */ 
    public $tries = 5;
    public function __construct(Conf_stud $conf_stud)
    {
        $this->conf_stud = $conf_stud;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $conf_code = str_random(30);
        $emailAd = $this->conf_stud->student_id.'@solusi.ac.zw';
        $contents = [
            'title'=> 'Verify your Student Id: ',
            'body'=> 'Hello,
                 You recieved this email because your student Id number was used to attempt creating an account with Solusi OHMS. If you recognise this attempt please confirm by clicking the button bellow or else just ignore this email.',
                    'link'=> url('/').'/registr/'.$this->conf_stud->conf,
                ];
        
        Mail::to($emailAd)->send(new ConfirmId($contents)); 
        
    }
}
