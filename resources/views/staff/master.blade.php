<!DOCTYPE html>
<html lang="en">
<head>
 @include('student.partials.header')
 </head>
<body style="background-color: #ff8b8b;">
<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        <!-- <div class="ui inverted dimmer"><div class="ui text loader">Loading</div></div> -->


@if(session('success'))
  <script>
    swal({
      title: "Success!",
      text: "{!! session('success') !!}.",
      type: "success",
      timer: 4000,
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('error'))
  <script>
    swal({
      title: "Sorry!",
      text: "{!! session('error') !!}",
      type: "error",
      timer: 4700,
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('warning'))
  <script>
    swal({
      title: "Watch out!",
      text: "{!! session('warning') !!}",
      type: "warning",
      timer: 4000,
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('info'))
  <script>
    swal({
      title: "Watch out!",
      text: "{!! session('info') !!}",
      type: "info",
      timer: 4000,
      confirmButtonText: "OK",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if (count($errors) > 0)
  <script>
    swal({
      title: "Sorry!",
      text: "<?php foreach($errors->all() as $error){ echo "$error<br>"; } ?>",
      type: "error",
      timer: 4000,
      confirmButtonText: "OK",
      confirmButtonColor: "#980000",
      html: true
    });
  </script> 
  @endif 

	@include('student.partials.sidebar')
	
	<div class="main col-xs-12">
       @yield('breadcrumb')
       @yield('slider')
       @yield('main-contant')
  </div>
  <div class="navbar" style="background: #6b0000 ; color: #ffeaea; margin-top: -10px;height: 80px;clear:both;padding-top:20px;">
    <div class="container">
      <p><center>&copy; 2017 Copyright OHMS 1.0- Developed By 
           <a href="https://ohmssaniblog.wordpress.com/" target="_blank" >ohmssani Ndlovu</a>
      </center></p>
    </div>
</div>
  
  <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/bootstrap-3.3.7.min.js') }}"></script>

  @yield('js')
<script type="text/javascript">
  function htmlbodyHeightUpdate(){
    var height3 = $( window ).height()
    var height1 = $('.nav').height()+50
    height2 = $('.main').height()
    if(height2 > height3){
      $('html').height(Math.max(height1,height3,height2)+10);
      $('body').height(Math.max(height1,height3,height2)+10);
    }
    else
    {
      $('html').height(Math.max(height1,height3,height2));
      $('body').height(Math.max(height1,height3,height2));
    }
    
  }
  $(document).ready(function () {
    htmlbodyHeightUpdate()
    $( window ).resize(function() {
      htmlbodyHeightUpdate()
    });
    $( window ).scroll(function() {
      height2 = $('.main').height()
        htmlbodyHeightUpdate()
    });
  });
  </script>
  </body>
</html>