@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.permissions_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.permissions_title'))
@section('icon', "lightning")
@section('subtitle', trans('ohms.permissions_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $permissions->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.permissions.admin'))
             <center><a href="{{ route('OHMS::permissions_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_permission') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
  		<table class="ui basic table " style="background: #ffeaea">
          <thead >
  			  <thead>
  			    <tr>
                  <th style="color: #980000;">{{ trans('ohms.name') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.description') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.slug') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.roles') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.options') }}</th>
  			    </tr>
  			  </thead>
  			  <tbody>
                @foreach($permissions as $perm)
					<tr style="color: #980000;">
						<td>
                            <div class="text">
                                {{ OHMS::permissionName($perm->slug) }}
                            </div>
                        </td>
                        <td>
                            <div class="text">
                                {{ OHMS::permissionDescription($perm->slug) }}
                            </div>
                        </td>
                        <td>
                            <div class="text">
                                {{ $perm->slug }}
                            </div>
                        </td>
                        <td>{{ trans('ohms.permissions_roles', ['number' => count($perm->roles)]) }}</td>
                        <td>
                          @if(OHMS::loggedInUser()->hasPermission('ohms.permissions.edit') or (OHMS::loggedInUser()->hasPermission('ohms.permissions.delete') and !$perm->su))
                              <div class="ui orange top icon left pointing dropdown button">
                                <i class="configure icon"></i>
                                <div class="menu">
                                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                                  <a href="{{ route('OHMS::permissions_edit', ['id' => $perm->id]) }}" class="item">
                                    <i class="edit icon"></i>
                                    {{ trans('ohms.permissions_edit') }}
                                  </a>
                                  @if(OHMS::loggedInUser()->hasPermission('ohms.permissions.delete') and !$perm->su)
                                  <div class="header">{{ trans('ohms.advanced_options') }}</div>
                                  <a href="{{ route('OHMS::permissions_delete', ['id' => $perm->id]) }}" class="item">
                                    <i class="trash bin icon"></i>
                                    {{ trans('ohms.permissions_delete') }}
                                  </a>
                                  @endif
                                </div>
                              </div>
                          @else
                              <div class="ui disabled orange icon button">
                                  <i class="lock icon"></i>
                              </div>
                          @endif
						</td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
        {!! $permissions->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection