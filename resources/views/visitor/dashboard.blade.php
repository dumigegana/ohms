@extends('student.master')

@section('main-contant')
 {!! $notices->links() !!}

@foreach($notices as $notice)

                {{--Notices Section--}}
            <div class="col-md-12" style="padding: 0;">
                <div class="panel panel">
                    <div class="panel-heading" style="background-color: #980000; color: #ffeaea;">
                        <center><h4><i class="fa fa-fw fa-gift"></i> {{ $notice->title }}</h4></center>
                    </div>
                    <div class="panel-body ohms_body">
                    <p>{{ substr($notice->body, 0, 300) }}{{ strlen($notice->body) > 300 ? "..." : ""}} <a href="{{ route('read_notice', ['id' => $notice->id]) }}"><b>Read more</b></a>{{ Carbon\Carbon::parse($notice->created_at)->diffForHumans() }}</p>
                    </div>
                </div>
            </div>
                       @endforeach
@endsection
@section('links')

<style>
#circle {
  
   box-shadow: 5px 5px 5px #888888;
}

#circle:hover{
        box-shadow: 10px 10px 5px #fff;
          -webkit-transform: scale(1.5);
    transform: scale(1.2);
    -webkit-transition: .3s ease-in-out;
    transition: .3s ease-in-out;
}

</style>
 <div class="row">
    <div class="col-md-4">
      <a href="http://www.solusi.ac.zw" target="_blank">
        <img class="img-circle img-responsive" src="img/img/logo.png" title="Visit our website" width="140" height="140" id ="circle" >
      </a>  
    </div><!-- /.col-md-4 -->

    <div class="col-md-4">
      <a href="https://www.facebook.com/Solusi-University-441288199368674/" target="_blank">
        <img class="img-circle img-responsive" src="img/img/facebook.png" title="Like us on facebook" width="140" height="140" id="circle">
      </a>
    </div><!-- /.col-md-4 -->

    <div class="col-md-4">
      <a href="http://reg.solusi.ac.zw/su/" target="_blank">
        <img class="img-circle img-responsive" src="img/img/iutus.jpg" title="Check your IUTUS" width="140" height="140" id="circle">
      </a>
    </div><!-- /.col-lg-4 -->
  </div><!-- /.row -->
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-3.3.7.min.js') }}"></script>
@endsection