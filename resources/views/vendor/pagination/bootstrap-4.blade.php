<ul class="pagination">
    <!-- Previous Page Link -->
    @if ($paginator->onFirstPage())
        <li class="page-item disabled"><span class="page-link" style="background-color: #990000; color:#ffeaea;">&laquo;</span></li>
    @else
        <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" style="background-color: #990000; color:#ffeaea;">&laquo;</a></li>
    @endif

    <!-- Pagination Elements -->
    @foreach ($elements as $element)
        <!-- "Three Dots" Separator -->
        @if (is_string($element))
            <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
        @endif

        <!-- Array Of Links -->
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="page-item active" ><span class="page-link" style="background-color: #ff8b8b; color: #980000">{{ $page }}</span></li>
                @else
                    <li class="page-item"><a class="page-link" style="background-color: #ffeaea; color: #980000"  href="{{ $url }}">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif
    @endforeach

    <!-- Next Page Link -->
    @if ($paginator->hasMorePages())
        <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" style="background-color: #990000; color:#ffeaea;" rel="next">&raquo;</a></li>
    @else
        <li class="page-item disabled"><span style="background-color: #990000; color:#ffeaea;" class="page-link">&raquo;</span></li>
    @endif
</ul>


