<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Role_User;
use App\Major;
use App\Mail\ConfirmId;
use App\User;
use App\Conf_stud;
use Mail;  
use Carbon\Carbon;
use App\Student;
use Hash;
use Crypt;
Use DB;
use OHMS;
use Session;
use App\Jobs\SendSignupEmail;
use App\Jobs\SendResetEmail;

class RegisterController extends Controller
{ 
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new students as well as their
    | validation and creation. 
    |
    */
   

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void 
     */

    /*
    |--------------------------------------------------------------------------
    | Signup Functions
    |--------------------------------------------------------------------------
    | 
    */

    public function confirm(Request $data)
    {
        $this->validate($data, [
            'student_id' => 'required',
            ]);
         $epxire = Carbon::now()->subHours(2);
         Conf_stud::where('created_at', '<', $epxire)->delete();
         Conf_stud::where('student_id', $data['student_id'])->delete();
        $avail=User::where('username', $data['student_id'])->first();
     
         if(!$avail)
            {
                $conf_code = str_random(30);
                $emailAd = $data['student_id'].'@solusi.ac.zw';

                $conf_stud = new Conf_stud;
                $conf_stud->student_id = $data['student_id'];
                $conf_stud->conf = $conf_code;
                $conf_stud->save();

                // $contents = [
                //     'title'=> 'Verify your Student Id: ',
                //     'body'=> 'Hello,
                //     You recieved this email because your student Id number was used to attempt creating an account with Solusi OHMS. If you recognise this attempt please confirm by clicking the button bellow or else just ignore this email.',
                //     'link'=> url('/').'/registr/'.$conf_code,
                // ];

                // Mail::to($emailAd)->send(new ConfirmId($contents));
                $this->dispatch(new SendSignupEmail($conf_stud));
                Session::flash('success', trans('ohms.msg_mail_sent', ['subject' => 'SignUp', 'emailAd' => $emailAd]));
                    return redirect('/');
                // }
            }
        else
            {
               Session::flash('error', trans('ohms.msg_account_exist', ['student_id' => $data['student_id']]));
               return redirect('/');
            }
    }


    public function create($conf_code)
    {
         $epxire = Carbon::now()->subHours(2);
         Conf_stud::where('created_at', '<', $epxire)->delete();
        $majors = Major::orderBy('name', 'asc')->get();
        $id = Conf_stud::where('conf', $conf_code)->first();
         if($id) {
             return view('auth/register', ['majors' => $majors, 'id' => $id]);
        }
        else{
            Session::flash('error', trans('Your request expired, you need to restart and complite the whole process within 4hrs'));
            return redirect('/');
        }
       
    }
    

    public function store(Request $data)
    {
        $this->validate($data, [
            'full_name' => 'required|max:255',
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-zA-Z])(?=.*[A-Z])(?=.*\d).+$/',
            'gender'    => 'required',
            'degree_level' => 'required',
            'cell_number'  => 'required|unique:students',
            'cell_phone_number'  => 'accepted',
            'national_id'    => 'required',
            'home_address' => 'required',
            'next_of_kin'  => 'required',
            'next_of_kin_number'  => 'accepted',
            'date_of_birth'    => 'required',
        ]);
         Conf_stud::where('student_id', $data['username'])->delete();

        $avail=User::where('username', $data['username'])->first();
     
         if(!$avail)
            {

            # Create the user
            $user_data = [
                'full_name' => $data['full_name'],
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']), 
                 'active' => $data['active'],
                 'banned' => $data['banned'],
            ];
            $user = User::create($user_data);

            # Add Relationshop
            $rel = new Role_User; 
            $rel->user_id = $user->id;
            $rel->role_id = $data['role_id'];
        
            # Add Relationshop
            $rel = new Student;
            $rel->user_id = $user->id;
            $rel->solusi_id = $data['student_id'];
            $rel->gender = $data['gender'];
            $rel->degree_level = $data['degree_level'];
            $rel->year = $data['year'];
            $rel->major_id = $data['major_id'];
            $rel->cell_number = $data['cell_number'];
            $rel->home_address = $data['home_address'];
            $rel->next_of_kin = $data['next_of_kin'];
            $rel->date_of_birth = $data['date_of_birth'];
            $rel->national_id = $data['national_id'];
           
            $rel->save();

            if(Auth::attempt(['username' => $data['username'], 'password' => $data['password']])) 
            {
                Session::flash('success', trans('ohms.msg_student_created'));
                return redirect('/');          
            }
            if($avail)
            {
                Session::flash('error', trans('ohms.msg_account_exist', ['student_id' => $data['student_id']]));
               return redirect('/');
            }
        }    

    }

    /*
     |----------------------------------------------------------------------------
     |  Recover Password
     |----------------------------------------------------------------------------
     |
     | The Functions below are for Changing or Recovering Password.
      |
    */ 
    
     public function confirmp(Request $data)
    {
        $this->validate($data, [
            'student_id' => 'required',
            ]);
        $epxire = Carbon::now()->subHours(2);
         Conf_stud::where('created_at', '<', $epxire)->delete(); 
        Conf_stud::where('student_id', $data['student_id'])->delete();
        $avail=User::where('username', $data['student_id'])->first();
     
         if(!$avail)
            {
               Session::flash('error', trans('ohms.msg_account_not_exist', ['student_id' => $data['student_id']]));
               return redirect('/');
            }
            else
            {
                $conf_code = str_random(30);
                $emailAd = $data['student_id'].'@solusi.ac.zw';

                $conf_stud = new Conf_stud;
                $conf_stud->student_id = $data['student_id'];
                $conf_stud->conf = $conf_code;
                $conf_stud->save();

                // $contents = [
                //     'title'=> 'Password Recovery ',
                //     'body'=> 'Hello, You recieved this email because your student Id number was used to attempt changing your Solusi OHMS password. If you recognise this attempt please confirm by clicking the button bellow or else just ignore this email.',
                //     'link'=> env('APP_URL').'/recov/'.$conf_code ,
                // ];

                // Mail::to($emailAd)->send(new ConfirmId($contents));
                $this->dispatch(new SendResetEmail($conf_stud));

                    Session::flash('success', trans('ohms.msg_mail_sent', ['subject' => 'Password Reset', 'emailAd' => $emailAd]));
                    return redirect('/home');
            }
        
    }

    public function edit($conf_code)
    {
        $epxire = Carbon::now()->subHours(2);
        Conf_stud::where('created_at', '<', $epxire)->delete();
        $id = Conf_stud::where('conf', $conf_code)->first();
         if(empty($id)) {
            Session::flash('error', 'Your request expired, you need to restart and complite the whole process within 2hrs');
                
            return redirect('/home');
        }
        # Return the view
        return view('auth/update', ['id'=> $id]);
    }

    public function update(Request $data)
    {
        $this->validate($data, [
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-zA-Z])(?=.*[A-Z])(?=.*\d).+$/',
        ]);
         Conf_stud::where('student_id', $data['username'])->delete();

            $user = User::where('username', $data['username'])->first();
            if($user)
            {
            $user->password = Hash::make($data['password']);
            $user->save();
            
            Session::flash('success', trans('passwords.reset'));
            return redirect('/home');         
            }

        if(!$user)
           {
               Session::flash('error', trans('ohms.msg_account_not_exist', ['student_id' => $data['student_id']]));
               return redirect('/home');   
            }
            
    }
}

