<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Course;
use App\Student;
use App\Course_Student;
use OHMS;

class CoursesController extends Controller
{
    public function index()
    {
        OHMS::permissionToAccess('ohms.courses.access');

        # Get all the courses
        $courses = Course::paginate(50);

        # Return the view
        return view('ohms/courses/index', ['courses' => $courses]);
    }

    
     public function show($id)
    {
        OHMS::permissionToAccess('ohms.courses.admin');

        # Get the course
        $course = Course::findOrFail($id);

        # Return the view 
        return view('admin/courses/show', ['course' => $course]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.courses.admin');

        # Find the course
        $row = Course::findOrFail($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'courses';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/courses/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    } 

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.courses.admin');

        # Find the row
        $row = Course::findOrFail($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'courses';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::courses')->with('success', trans('Course updated.'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.courses.admin');

        # Get all the data
        $data_index = 'courses';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/courses/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.courses.admin');

        # create new course
        $row = new Course;

        # Save the data
        $data_index = 'courses';
        require('Data/Create/Save.php');

        
        # Return the admin to the courses page with a success message
        return redirect()->route('OHMS::courses')->with('success', trans('Course Added'));
    }

    
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.courses.admin');
        

        # Select Course
        $course = OHMS::course('id', $id);

        if(!$course->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Check if it's su
        if($course->su) {
            return abort(403, trans('ohms.error_security_reasons'));
        }

        

        # Delete all relationships

        # Permission Relation
        $rels = Permission_Course::where('course_id', $id)->get();
        foreach($rels as $rel) {
            $rel->delete();
        }
        # Users Relation
        $rels = Course_Student::where('course_id', $id)->get();
        foreach($rels as $rel) {
            $rel->delete();
        }

        # Delete Course
        $course->delete();

        # Redirect the admin
        return redirect()->route('OHMS::courses')->with('success', trans('Course Deleted!'));
    }


    public function remove($course_id, $student_id)
    {
        OHMS::permissionToAccess('ohms.courses.admin');
        

       $student = Student::find($student_id);
       $student->courses()->detach($course_id);
        # Redirect the admin
        return redirect()->route('OHMS::courses')->with('success', trans('Student removed.'));
    }

    public function classList($id)
    {
         
        # Student Relation
        $course = Course::find($id)->with('students', 'sessions.attendances')->first();

          # Return the view
        return view('ohms/courses/students', ['course' => $course]);                      
    }

    public function search(Request $request)
    {
      $search = "";
      $solusi_id = $request['solusi_id'];
      $course_id = $request['course_id'];

      $student = Student::where('solusi_id', $solusi_id)->first();
      $crse = Course::where('id', $course_id)->first();
       if($student) {
        $val = 2;
            foreach($student->courses as $course) {
                if($course->id == $crse->id) {
                    $val = 1;
                }
            }
        } else{
                $val = 3;
        } 
    
        $search = view('ohms/courses/search', ['student' => $student, 'val' => $val]);
        return $search;
     }

    public function add(Request $request)
    {
      $search = "";
      $solusi_id = $request['solusi_id'];
      $course_id = $request['course_id'];
      
      $student = Student::where('solusi_id', $solusi_id)->first();
      $student->courses()->attach($course_id);
        
     }
}