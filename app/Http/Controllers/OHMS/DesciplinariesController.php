<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Disciplinary;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class DisciplinariesController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.disciplinaries.access');

        # Get all the disciplinaries
        $disciplinaries = Disciplinary::with('staff')->paginate(50);

        # Return the view
        return view('ohms/disciplinaries/index', ['disciplinaries' => $disciplinaries]);
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.disciplinaries.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $disciplinaries = Disciplinary::with('staff')->paginate(100);
        }
        else {       
          $disciplinaries = Disciplinary::with('staff', 'staff.user')
                             ->where('disciplinary_number','like', $post . '%')              
                             ->orwherehas('staff', function($o) use ($post) {$o->where('su_id', 'like',  $post . '%');})
                             ->orwherehas('staff.user', function($r) use ($post) {$r->where('fullname', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/disciplinaries/search', ['disciplinaries' => $disciplinaries]);
        return $read;
    }
    public function show($id)
    {
        OHMS::permissionToAccess('ohms.disciplinaries.access');

        # Get the disciplinary
        $disciplinary = Disciplinary::find($id);

        # Return the view
        return view('admin/disciplinaries/show', ['disciplinary' => $disciplinary]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.disciplinaries.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.disciplinaries.admin');

        # Find the disciplinary
        $row = Disciplinary::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'disciplinaries';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/disciplinaries/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.disciplinaries.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.disciplinaries.admin');

        # Find the row
        $row = Disciplinary::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'disciplinaries';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::disciplinaries')->with('success', trans('ohms.msg_disciplinary_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.disciplinaries.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.disciplinaries.admin');
        # Get all the data
        $data_index = 'disciplinaries';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/disciplinaries/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.disciplinaries.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.disciplinaries.create');

        # create new role
        $row = new Disciplinary;

        # Save the data
        $data_index = 'disciplinaries';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::disciplinaries')->with('success', trans('ohms.msg_disciplinary_created'));

    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.disciplinaries.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.disciplinaries.admin');

        # Select Item
        $disciplinary = OHMS::disciplinary('id', $id);

        if(!$disciplinary->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $disciplinary->delete();

        # Redirect the admin
        return redirect()->route('OHMS::disciplinaries')->with('success', trans('ohms.msg_deciplinary_deleted'));
    }
     
}
