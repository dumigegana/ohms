
@component('mail::message')
<body width="100%" bgcolor="#FFF" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #FFF; text-align: left;">

               

        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">

            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#980000" style="padding: 40px 40px 20px; ">
                    <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 27px; color: #ffeaea; font-weight: normal; text-align: center;">{{ $contents['title'] }}</h1>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffeaea" style="padding: 0 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #6d0000; text-align: center;">
                    <br>
					<p style="margin: 0; color: #6d0000;">{{ $contents['body'] }}</p>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffeaea" style="padding: 0 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto">
                        <tr>
                            <td style="border-radius: 3px; background: #ffeaea; text-align: center;" class="button-td">
                                <a href="{{ $contents['link'] }}" style="background: #980000; border: 15px solid #980000; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#ffeaea;">Confirm</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                </a>
                            </td>
                        </tr>
                    </table>
                    <!-- Button : END -->
                </td>
            </tr>
            <!-- 1 Column Text + Button : END 
        <!-- Clear Spacer : BEGIN -->
        <tr>
            <td aria-hidden="true" height="10" style="font-size: 0; line-height: 0;">
                &nbsp;
            </td>
        </tr>
        <!-- Clear Spacer : END -->

       

    </table>
    <!-- Email Body : END -->

    <!-- Email Footer : BEGIN -->
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto; font-family: sans-serif; color: #888888; line-height:18px;" class="email-container">
        <tr>
            <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; line-height:18px; text-align: center; color: #888888;" class="x-gmail-data-detectors">
                OHMS<br>Solusi University, PO Solusi Bulawayo,<br> Zimbabwe
            </td>
        </tr>
    </table>
    <!-- Email Footer : END -->

   

    </center>
</body>
@endcomponent    
</html>