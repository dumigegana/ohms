@extends('student.master3')

@section('main-contant')
<div class="w3-container">
  <div class="theme_light" style="padding: 100px 200px;">
    <div class="w3-center"><h1> {{ $courses->cname}} </h1> </div>
    <div>
      <label class="w3-col" style="width:150px">Attendance Code</label>
      <div class="w3-rest w3-center"> 
      </div> 
    </div> 
  <form method="post" action="{{ route('storetend') }}" id="form">
    @csrf    
      <input type="hidden" name="" value="{{ $courses->maximum }}"  id="max" />
      <input type="hidden" name="" value="{{ $courses->minimum }}" id="min" />
      <input type="hidden" name="" value="{{ $courses->factor }}" id="fact" />
      <input type="hidden" name="" value="{{ $courses->subtract }}" id="subt" />
      <input type="hidden" name="session_id" value="{{ $courses->id }}" id="sess" />
      <input type="text" id="code" class="w3-input w3-round-xlarge" name="code" />
    <p class="w3-center">
      <button id="sub" class="w3-button theme w3-round-xlarge">Submit </button>
    </p>
  </div>
</form>
</div>
@endsection
@section('js')
<script>
$(document).ready(function(){ 
  $('#sub').attr("disabled", true); 
  $('#code').val(" "); 
  }); 
  function delay(fn, ms) {
    let timer = 0
    return function(...args) {
      clearTimeout(timer)
      timer = setTimeout(fn.bind(this, ...args), ms || 0)
  }
}
$('#code').change(function() {  
  $('#sub').attr("disabled", true);   
});
$('#code').keyup(delay(function (e) {
    $('#sub').attr("disabled", true); 
    var num = parseInt($('#code').val());
    var max = parseInt($('#max').val());
    var min = parseInt($('#min').val());
    var subt = parseInt($('#subt').val()); 
    var fact = parseInt($('#fact').val()); 
    if ((min <= num) && (num <= max)){
      var muliple = num - subt;
      if(muliple%fact == 0){ 
          //$('input[name="code"]').val($('#code').val());       
          $('#sub').attr("disabled", false);          
      }
      else{
        $('#sub').attr("disabled", true);
        $('#code').val(" "); 
      }
   }
   else{
        $('#sub').attr("disabled", true);
        $('#code').val(" ");  
      }
}, 6000));
</script>
@endsection
