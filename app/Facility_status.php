<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class facility_status extends Model
{
     public function check_temps()
    {
        return $this->hasMany('App\Check_temp');
    } 

    public function checklists() 
    {
        return $this->hasMany('App\Checklist');
    }
    public function maintbs()
    {
        return $this->hasMany('App\Maintb');
    } 

    public function maintenances() 
    {
        return $this->hasMany('App\Maintenance');
    } 
}
