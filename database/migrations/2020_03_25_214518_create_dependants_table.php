<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDependantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependants', function (Blueprint $table) {
            $table->bigIncrements('id');                        
            $table->bigInteger('staff_id'); 
            $table->string('full_name'); 
            $table->string('nat_id#'); 
            $table->string('date_of_bith'); 
            $table->string('nat_id_file'); 
            $table->string('birth_cert'); 
            $table->string('relationship'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependants');
    }
}
