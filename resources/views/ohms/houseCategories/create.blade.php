@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::house_categories') }}">{{ trans('ohms.houseCategory_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.houseCategory_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.houseCategory_create_title'))
@section('icon', "bank ")
@section('subtitle', trans('ohms.houseCategory_create_subtitle'))
@section('content')
<div class="ui doubling stackable grid container">
    <div class="ten wide column centered">
        <div class="ui very padded segment" style="background: #ffeaea;">
            <form method="POST" class="ui form">
                {{ csrf_field() }}            
                @include('ohms.forms.master') 
                <br>
                <div class="field">
                    <button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button>
                </div>
            </form>
        </div>
    </div>        
</div>
@endsection
