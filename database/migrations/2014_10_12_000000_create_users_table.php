<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('username')->unique();
			$table->string('email')->unique();
            $table->string('password');
            $table->boolean('active')->default(true);
            $table->boolean('banned')->default(0);
            $table->boolean('su')->default(false);            
            $table->string('cell_number')->nullable(); 
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        }); 
        schema::table('users', function ($table) {
            
        $user = \OHMS::newUser();
        $user->full_name = env('USER_FULL_NAME', 'admin');
		$user->username = env('USER_NAME', 'admin');
        $user->email = env('USER_EMAIL', 'admin@admin.com');
        $user->password = bcrypt(env('USER_PASSWORD', 'admin123')   );
        $user->active = true;
        $user->banned = false;
        $user->cell_number = "1234567";
        $user->su = true;
        $user->save();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
