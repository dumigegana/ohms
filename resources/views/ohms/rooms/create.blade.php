@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::rooms') }}">{{ trans('ohms.rooms_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.rooms_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.rooms_create_title'))
@section('icon', "plus")
@section('subtitle', trans('ohms.rooms_create_subtitle'))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST">
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column centered">
                    <div class="ui very padded segment" style="background: #ffeaea">
                         {{ csrf_field() }}
                           
                        <div class="required field">
                            <label style="color: #980000;">Room Number</label>
                            <input name="room_number" type="text" placeholder="Room number" value="{{ old('room_number')}}">
                            <input name='occupants' type="hidden" value='0'>
                        </div>
                        <div class="required field">
                            <label style="color: #980000;">Block</label>
                            <select name="block_id" class="form-control" required>
                                <option value="" >Block</option>
                            @foreach($blocks as $block)
                                <option value="{{ $block->id }}" {{ (old("block_id") == $block->id ? "selected":"") }}>{{ $block->name }}</option>
                                @endforeach
                            </select>
                        </div>
                            <br/>
                        <div class="required field">
                            <label style="color: #980000;">Room Capacity</label>
                                <input name="capacity" type="number" placeholder="Students per Room" value="{{ old('capacity')}}">
                        </div>

                        <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                    </div>
                </div>
            </div>
        </div> 
    </form>
</div>
@endsection
