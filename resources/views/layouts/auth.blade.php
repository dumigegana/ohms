<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>OHMS</title>
 
        <!-- Fonts --> 
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
         <!--link rel="shortcut icon" href="{{-- URL::to('favicon.ico') --}}" -->
        <!-- Bootstrap Core CSS -->
         <!-- Bootstrap Core CSS -->
         <link href="{{ URL::to( 'css/bootstrap.min.css') }}" rel="stylesheet">

        {!! OHMS::includeAssets('header') !!}
        <link rel="shortcut icon" href="{{ URL::to('favicon.ico') }}" >
        <!-- Styles -->
        <style>
           body {
                background: url('img/b05.png') no-repeat;
            }

            .full-height {
                height: 100%;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }
            .bottom {
                align-items: center;
                position: absolute;
                bottom: 18px;
                justify-content: center;
            }
            .infor {
                text-align: center;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            input[type=text], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=email], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=password], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

div.loginSection{
  background: #ff8b8b;
  width: 100%;
  font-family: lato;
  position: relative;
  color: #333;
  border-radius: 10px;
}
div.header{

    background: #980000;
    padding: 30px 20px;
    color: #ff8b8b;
    font-size: 2.0em;
    font-weight: 600;
    border-radius: 10px 10px 0 0;
    
}

div.innerBox{
    width: 100%;
    padding: 30px 20px;
     background: #ff8b8b;

}

.btn-raised {
  // shadow depth 1
    -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
  0 1px 2px 0 rgba(0, 0, 0, .24);
    -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
  0 1px 2px 0 rgba(0, 0, 0, .24); 
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
  0 1px 2px 0 rgba(0, 0, 0, .24); 
   
  &:active, &.active, &:active:focus, &.active:focus {
    // shadow depth 2
    -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
  0 3px 6px rgba(0, 0, 0, .23); 
    -moz-box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
  0 3px 6px rgba(0, 0, 0, .23); 
    box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
  0 3px 6px rgba(0, 0, 0, .23);
  }
  
  &:focus {
    // shadow depth 2
    -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
  0 3px 6px rgba(0, 0, 0, .23); 
    -moz-box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
  0 3px 6px rgba(0, 0, 0, .23); 
    box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
  0 3px 6px rgba(0, 0, 0, .23);
  }
  
}

.btn {
  font-family: "Roboto", 'Helvetica Neue, Helvetica, Arial', sans-serif; 
  font-size: 14px;
  font-weight: 400;
  line-height: 1.1;
  text-transform: uppercase;
  letter-spacing: inherit;
  background-color: #980000;
  color: #ff8b8b;
}

.btn-default, .btn-link {
  background-color: #980000;
    color: #FFF;
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
}

li {
    float: left;
    align-items: center;
}

li a {
    display: block;
    color: #980000;
    text-align: center;
    padding-left: 68px;
    text-decoration: none;
}

li a:hover {
    background-color: #ff8b8b;
    color: brown;
}

</style>
    

  @yield('css')
    </head>
    <body style="background: url('img/img/bg1.jpg') no-repeat; background-size: 100%, 100%;">
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form> 
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
            @if(!Auth::check())
            <div class="top-right links">
                @if(!OHMS::checkInstalled())
                 <a style="color: #980000;" href="{{ route('OHMS::install') }}">Install OHMS</a>
                                             
                @endif
            </div>
                @endif
                @endif


            <div class="content" style="padding-top: 50px;">

                @yield('content')
                 
                <button  class="btn btn-raised">{{ trans('ohms.submit') }}</button>
       </form>
    </div>  
    </div>
    </div>
    </div>
            </div><center>
            <div class="  info">
                @if(session('error'))
                    <p style='font-size: 40px;color:red;'>{!! session('error') !!}</p>
                @endif
                @if(session('warning'))
                    <p style='font-size: 40px;color:orange;'>{!! session('warning') !!}</p>
                @endif
                @if(session('info'))
                    <p style='font-size: 40px;color:blue;'>{!! session('info') !!}</p>
                @endif
                @if (session('status'))
                    <p style='font-size: 40px;color:green;'>{!! session('status') !!}</p>
                @endif
                @if (count($errors) > 0)
                    <?php foreach($errors->all() as $error){ echo "<p style='font-size: 40px;color:red;'>$error<p>"; } ?>
                @endif
            </div></center>
        </div>

    </body>
</html>
