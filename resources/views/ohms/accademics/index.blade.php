@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.rooms_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.rooms_title')) 
@section('icon', "bed")
@section('subtitle', trans('ohms.rooms_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $accademics->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.accademics.admin'))
             <center><a href="{{ route('OHMS::users_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add Accademic Achievement</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr>
              <th style="color: #980000;">Name</th>
              <th style="color: #980000;">Achievement</th>
              <th style="color: #980000;">Institution</th>
              <th style="color: #980000;">Year</th>
              <th style="color: #980000;">File</th>
              <th style="color: #980000;">Options</th>
            </tr> 
          </thead>
          <tbody id="read">
            @foreach($accademics as $accademic) 
            <tr style="color: #980000;">
              <td>{{ $accademic->staff->user->fullname }}</td>
              <td>{{ $accademic->name }}</td>
              <td>{{ $accademic->institution }}</td>
              <td>{{ $accademic->year }}</td>
              <td>                
                <a href="{{ route('OHMS::accademics_edit', ['id' => $accademic->id]) }}" class="ui btn button">{{ trans('ohms.accademics_edit') }}</a>
              </td>       
            </tr>
        @endforeach
          </tbody>
        </table>
        {!! $accademics->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection