<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Block;
use Illuminate\Support\Facades\DB;


class BlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
         Model::unguard();
        DB::table('blocks');
        $blocks = array(
                ['hostel_id'=>'1', 'name' => 'Sweeden House', 'stud_year' => '3', 'theology'=>'0' ],
                ['hostel_id'=>'2', 'name' => 'Kamuzu', 'stud_year' => '1', 'theology'=>'0' ],
                ['hostel_id'=>'3', 'name' => 'Palmer Hall', 'stud_year' => '2', 'theology'=>'0' ],
                ['hostel_id'=>'4', 'name' => 'Raelly Hall Ground', 'stud_year' => '1', 'theology'=>'0' ],
                ['hostel_id'=>'4', 'name' => 'Raelly Hall Middle', 'stud_year' => '2', 'theology'=>'0' ],
                ['hostel_id'=>'4', 'name' => 'Raelly Hall Top', 'stud_year' => '3', 'theology'=>'0' ],
                ['hostel_id'=>'5', 'name' => 'Mayenza North', 'stud_year' => '3', 'theology'=>'1' ],
                ['hostel_id'=>'5', 'name' => 'Mayenza South', 'stud_year' => '2', 'theology'=>'1' ],
                ['hostel_id'=>'5', 'name' => 'Mayenza East', 'stud_year' => '3', 'theology'=>'0' ],
                ['hostel_id'=>'5', 'name' => 'Mayenza West', 'stud_year' => '3', 'theology'=>'0' ],
                ['hostel_id'=>'6', 'name' => 'Tshabangu', 'stud_year' => '1', 'theology'=>'0' ],
                ['hostel_id'=>'7', 'name' => 'Kamwendo', 'stud_year' => '1', 'theology'=>'0' ],
                ['hostel_id'=>'8', 'name' => 'Mlevu', 'stud_year' => '1', 'theology'=>'0' ],
                ['hostel_id'=>'9', 'name' => 'Senior Dorm', 'stud_year' => '10', 'theology'=>'0' ],

        );

        // Loop through each user above and create the record for them in the database
        foreach ($blocks as $block)
        {
            Block::create($block);

            $block = new Block;
        }
        Model::reguard();
    }
}
