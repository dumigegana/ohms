<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Room;
use App\Block;
use App\Hostel;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class RoomsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.rooms.access');

        # Get all the rooms
        $rooms = Room::with('block')->paginate(100);

        # Return the view
        return view('ohms/rooms/index', ['rooms' => $rooms]);
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.rooms.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $rooms = Room::with('block')->paginate(100);
        }
        else {       
          $rooms = Room::with('block', 'block.hostel')
                             ->where('room_number','like', $post . '%')                   
                             ->orwherehas('block', function($o) use ($post) {$o->where('name', 'like',  $post . '%');})
                             ->orwherehas('block.hostel', function($r) use ($post) {$r->where('gender', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/rooms/search', ['rooms' => $rooms]);
        return $read;
    }
    public function show($id)
    {
        OHMS::permissionToAccess('ohms.rooms.access');

        # Get the room
        $room = Room::find($id);

        # Return the view
        return view('admin/rooms/show', ['room' => $room]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.rooms.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.rooms.admin');

        # Find the room
        $row = Room::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'rooms';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/rooms/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.rooms.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.rooms.admin');

        # Find the row
        $row = Room::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'rooms';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::rooms')->with('success', trans('ohms.msg_room_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.rooms.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.rooms.admin');

         $blocks = Block::orderBy('name', 'asc')->get();

        # Return the view
        return view('ohms/rooms/create', ['blocks' => $blocks]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.rooms.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.rooms.admin');

        # create new role
        $row = new Room;

        # Save the data
        $data_index = 'rooms';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::rooms')->with('success', trans('ohms.msg_room_created'));

    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.rooms.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.rooms.admin');

        # Select Item
        $room = OHMS::room('id', $id);

        if(!$room->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $room->delete();

        # Redirect the admin
        return redirect()->route('OHMS::rooms')->with('success', trans('ohms.msg_hostel_deleted'));
    }
     
}
