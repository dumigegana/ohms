<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Benefit;
use App\Block;
use App\Hostel;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class BenefitsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.benefits.access');

        # Get all the benefits
        $benefits = Benefit::paginate(50);

        # Return the view
        return view('ohms/benefits/index', ['benefits' => $benefits]);
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.benefits.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $benefits = Benefit::paginate(100);
        }
        else {       
          $benefits = Benefit::where('name','like', $post . '%') 
                            ->paginate(100);
        }
        $read = view('ohms/benefits/search', ['benefits' => $benefits]);
        return $read;
    }
    public function show($id)
    {
        OHMS::permissionToAccess('ohms.benefits.access');

        # Get the benefit
        $benefit = Benefit::find($id);

        # Return the view
        return view('admin/benefits/show', ['benefit' => $benefit]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.benefits.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.benefits.admin');

        # Find the benefit
        $row = Benefit::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'benefits';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/benefits/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.benefits.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.benefits.admin');

        # Find the row
        $row = Benefit::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'benefits';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::benefits')->with('success', trans('ohms.msg_benefit_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.benefits.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.benefits.admin');

        # Get all the data
        $data_index = 'benefits';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/benefits/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.benefits.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.benefits.admin');

        # create new role
        $row = new Benefit;

        # Save the data
        $data_index = 'benefits';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::benefits')->with('success', trans('ohms.msg_benefit_created'));

    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.benefits.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.benefits.admin');

        # Select Item
        $benefit = OHMS::benefit('id', $id);

        if(!$benefit->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $benefit->delete();

        # Redirect the admin
        return redirect()->route('OHMS::benefits')->with('success', trans('ohms.msg_hostel_deleted'));
    }
     
}
