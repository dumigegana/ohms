<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staffs';

      protected $dates = [
        'created_at', 'updated_at',  
    ];
    protected $fillable = [
        'user_id', 'solusi_id', 'title', 'gender', 'date_of_birth', 'national_id', 'home_address', 'next_kin', 'next_of_kin_number', 'department_id', 'status','marital', 'cell_number', 'has_house', 
    ];

    protected $with = ['user'];    

     public function user() 
    {
        return $this->belongsTo('App\User');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function resume()
    {
        return $this->hasOne('App\Resume');
    }

    public function work_records()
    {
        return $this->hasManny('App\Work_record');
    }

     public function Benefit_records()
    {
        return $this->hasManny('App\Benefit_record');
    }

    public function accademics()
    {
        return $this->hasOne('App\Accademic');
    }


    public function checkists()
    {
        return $this->hasManyThrough('App\Checkist', 'App\Allocation', 'student_id', 'allocation_id', 'id');
    }
     public function house_allocations()
    {
        return $this->morphMany('App\House_allocations', 'house_allocable');
    }

}
