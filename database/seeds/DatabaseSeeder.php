<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call(SemestersTableSeeder::class);
      $this->call(ItemsTableSeeder::class);
      $this->call(HostelsTableSeeder::class);
      $this->call(BlocksTableSeeder::class);
      $this->call(RoomsTableSeeder::class);
      $this->call(UsersTableSeeder::class);
      $this->call(Role_UserTableSeeder::class);
      $this->call(StudentsTableSeeder::class);
      $this->call(NoticesTableSeeder::class);
    }
}
