 @foreach($rooms as $room) 
            <tr style="color: #980000;">
              <td>{{ $room->room_number }}</td>
              <td>{{ $room->block->hostel->gender }}</td>
              <td>{{ $room->block->name }}</td>
              <td>{{ $room->capacity }}</td>
              <td class="center alligned">
                      @if ($room->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Room ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this room " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::rooms_edit', ['id' => $room->id]) }}" class="ui btn button">{{ trans('ohms.rooms_edit') }}</a>
              </td>       
            </tr>
        @endforeach