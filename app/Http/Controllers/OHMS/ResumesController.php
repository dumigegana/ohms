<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Resume;
use App\Block;
use App\Hostel;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class ResumesController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.resumes.access');

        # Get all the resumes
        $resumes = Resume::with('block')->paginate(50);

        # Return the view
        return view('ohms/resumes/index', ['resumes' => $resumes]);
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.resumes.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $resumes = Resume::with('staff')->paginate(100);
        }
        else {       
          $resumes = Resume::with('staff',)
                             ->where('qalification','like', $post . '%')                   
                             ->orwherehas('staff', function($o) use ($post) {$o->where('su_id', 'like',  $post . '%');})
                             ->orwherehas('staff.user', function($r) use ($post) {$r->where('fullname', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/resumes/search', ['resumes' => $resumes]);
        return $read;
    }
    public function show($id)
    {
        OHMS::permissionToAccess('ohms.resumes.access');

        # Get the resume
        $resume = Resume::find($id);

        # Return the view
        return view('admin/resumes/show', ['resume' => $resume]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.resumes.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.resumes.admin');

        # Find the resume
        $row = Resume::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'resumes';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/resumes/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.resumes.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.resumes.admin');

        # Find the row
        $row = Resume::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'resumes';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::resumes')->with('success', trans('ohms.msg_resume_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.resumes.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.resumes.admin');

         $blocks = Block::orderBy('name', 'asc')->get();

        # Get all the data
        $data_index = 'resumes';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/resumes/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.resumes.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.resumes.admin');

        # create new role
        $row = new Resume;

        # Save the data
        $data_index = 'resumes';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::resumes')->with('success', trans('ohms.msg_resume_created'));

    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.resumes.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.resumes.admin');

        # Select Item
        $resume = OHMS::resume('id', $id);

        if(!$resume->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $resume->delete();

        # Redirect the admin
        return redirect()->route('OHMS::resumes')->with('success', trans('ohms.msg_hostel_deleted'));
    }
     
}
