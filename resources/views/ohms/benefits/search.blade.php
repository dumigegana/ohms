 @foreach($benefits as $benefit) 
            <tr style="color: #980000;">
              <td>{{ $benefit->benefit_number }}</td>
              <td>{{ $benefit->block->hostel->gender }}</td>
              <td>{{ $benefit->block->name }}</td>
              <td>{{ $benefit->capacity }}</td>
              <td class="center alligned">
                      @if ($benefit->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Benefit ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this benefit " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::benefits_edit', ['id' => $benefit->id]) }}" class="ui btn button">{{ trans('ohms.benefits_edit') }}</a>
              </td>       
            </tr>
        @endforeach