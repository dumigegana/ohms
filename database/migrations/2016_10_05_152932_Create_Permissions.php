<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->boolean('assignable')->default(false);
            $table->boolean('su')->default(false);
            $table->timestamps();
        });

        $permissions = [
            'ohms.access',
            'ohms.users.access',
            'ohms.users.admin',
            'ohms.roles.access',
            'ohms.roles.permissions',
            'ohms.roles.admin',            
            'ohms.CRUD.access',
            'ohms.students.access',
            'ohms.students.admin',            
            'ohms.visitors.access',
            'ohms.visitors.admin',
            'ohms.majors.access',
            'ohms.majors.admin',
            'ohms.hostels.access',
            'ohms.hostels.admin',
            'ohms.semesters.access',
            'ohms.semesters.admin',
            'ohms.blocks.access',
            'ohms.blocks.admin',
            'ohms.rooms.access',
            'ohms.rooms.admin',
            'ohms.facilities.access',
            'ohms.facilities.admin',
            'ohms.checklists.access',
            'ohms.checklists.admin',
            'ohms.maintenances.access',
            'ohms.maintenances.admin',
            'ohms.notices.access',
            'ohms.notices.admin',
            'ohms.flights.access',
            'ohms.flights.admin',
            'ohms.allocations.access',
            'ohms.allocations.create',
            'ohms.reports.access',
            'ohms.items.access',
            'ohms.items.admin',
            'ohms.maint_deps.access', 
            'ohms.maint_deps.admin',
            'ohms.houses.access', 
            'ohms.houses.admin', 
            'ohms.house_allocations_dosa.access',
            'ohms.house_allocations.admin',
            'ohms.house_allocations_pr.access',
            'ohms.house_allocations_hr.access',
            'ohms.permissions.admin',   
            'ohms.permissions.access',
            'ohms.sessions.access',
            'ohms.sessions.admin',            
            'ohms.attendances.access',
            'ohms.attendances.admin',
            'ohms.courses.access',
            'ohms.courses.admin',
            'ohms.departments.access',
            'ohms.departments.admin',           
            'ohms.staffs.access',
            'ohms.staffs.admin',            
            'ohms.resumes.access',
            'ohms.resumes.admin',
            'ohms.dependants.access',
            'ohms.dependants.admin',
            'ohms.benefits.access',
            'ohms.benefits.admin',
            'ohms.policies.access',
            'ohms.policies.admin',
            'ohms.benefit_records.access',
            'ohms.benefit_records.admin',
            'ohms.deciplenaries.access',
            'ohms.deciplenaries.admin',
            'ohms.accademics.access',
            'ohms.accademics.admin',
            'ohms.work_records.access',
            'ohms.work_records.admin',
        ];

        foreach($permissions as $permission) {
            $perm = \OHMS::newPermission();
            $perm->slug = $permission;
            $perm->assignable = true;
            $perm->su = true;
            $perm->save();
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permissions');
    }
}
