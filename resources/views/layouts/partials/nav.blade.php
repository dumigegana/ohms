<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
           {{ csrf_field() }}
        </form>
        
    <nav class="navbar navbar-fixed-top" style="background: #6b0000;" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-header hidden-xs">
                <a class="navbar-brand" rel="home" href="{{ url('/') }}">
                    <img style="max-width:50px; margin-top: -13px; margin-left: -20px;" src="{{ URL::to( 'img/logo.png') }}">
                </a>
                
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                @if (Route::has('login'))

                    @if(!OHMS::checkInstalled())
                    <li>
                        <a href="{{ route('OHMS::install') }}"><b>INSTALL</b> OHMS</a>
                    </li>
                    @else
                    @if(Auth::check())

                        @if(OHMS::loggedInUser()->isStudent())
                     <li>
                        <a href="{{ route('attGet') }}">Attend</a>
                    </li>
                        @endif      
                    <li class="nav-item active"> 
                        <a href="{{ url('/') }}">Home<span class="sr-only">(current)</span></a>
                    </li>
                    <li>
                        <a href="http://www.solusi.ac.zw" target="_blank">Solusi WebSite</a>
                   </li>
                            @if(OHMS::loggedInUser()->isAdmin())
                    <li>
                        <a href="{{ route('OHMS::dashboard') }}">Administration</a>
                    </li>
                             @elseif(!OHMS::loggedInUser()->isAdmin())
                    <li>                         
                        <a href="{{ route('dash') }}"> OHMS </a>
                    </li>         
                            @endif
                    <li>
                        <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                    </li>
                        @else
                    <li>
                        <a href="http://www.solusi.ac.zw">Solusi WebSite</a>
                    </li>
                    <li>
                        <a data-toggle="modal" href="#login">Login </a>
                      </li>
                    <li>
                        <a data-toggle="modal" href="#confirm">Register</a>
                   </li>
                        @endif
                        @endif
                        @endif
                </ul>
           </div>
        </div>
      <!-- /.container -->
    </nav>