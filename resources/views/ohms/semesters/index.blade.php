@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.semesters_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.semesters_title'))
@section('icon', "calender")
@section('subtitle', trans('ohms.semesters_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $semesters->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.semesters.admin'))
             <center><a href="{{ route('OHMS::semesters_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add Semester</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b;">
            <tr>
                  <th style="color: #980000;">Name</th>
                  <th style="color: #980000;">Chech In</th>
                  <th style="color: #980000;">Term</th>
                  <th style="color: #980000;">Check Out</th>
                  <th style="color: #980000;">Options</th>
            </tr>
          </thead>
          <tbody>
                @foreach($semesters as $semester)
          <tr style="color: #980000;">
            <td> {{ $semester->name }}</td>
            <td class="center alligned">
                    @if ($semester->checkin == 1)
                        <div class="ui green tiny basic label pop" data-title="Active" data-variation="wide" data-content="This semester is activated for room allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                   @else
                        <div class="ui red tiny basic label pop" data-title="Disabled" data-variation="wide" data-content="Currently no students can book room for this semester." data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                    @endif
              </td>
              <td>{{$semester->term}}</td>
              <td>
                  @if ($semester->checkout == 1)
                        <div class="ui green tiny basic label pop" data-title="Active" data-variation="wide" data-content="This semester is activated for students to check out." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                   @else
                        <div class="ui red tiny basic label pop" data-title="Disabled" data-variation="wide" data-content="Currently no students can check out for this semester." data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                    @endif
              </td>
              <td>
                @if($semester->allow_editing or OHMS::loggedInUser()->su)
                   <div class="ui btn top icon left pointing dropdown button">
                  <i class="options icon"></i>
                    <div class="menu">
                      <div class="header">{{ trans('ohms.editing_options') }}</div>
                        <a href="{{ route('OHMS::sem_checkin', ['id' => $semester->id]) }}" class="item">
                          <i class="edit icon"></i>Toggle Check In
                        </a>
                        <a href="{{ route('OHMS::sem_checkout', ['id' => $semester->id]) }}" class="item">
                          <i class="configure icon"></i>Toggle Check Out
                        </a>
                        <a href="{{ route('OHMS::semesters_edit', ['id' => $semester->id]) }}" class="item">
                          <i class="configure icon"></i>Edit
                        </a>
                    </div> 
                  </div>
                @else
                  <div class="ui disabled btn icon button">
                    <i class="lock icon"></i>
                 @endif
                </td>
                        
          </tr>
        @endforeach
          </tbody>
        </table>
        {!! $semesters->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection