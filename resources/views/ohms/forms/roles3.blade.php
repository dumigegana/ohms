<?php

// Setup the $row variable if it's not set but intended
if(isset($user)){
    $row = $user;
} elseif(isset($blog)){
    $row = $blog;
}

?>
@foreach($roles as $role)
    <p>
        <label class="switch">
            <input
            <?php
                if(isset($row)){
                    if($row->hasRole($role->name)) {
                        echo "checked='checked' ";
                    }
                }

                $disabled = false;

                if($role->su) {
                    $disabled = true;
                }

                if(!$role->assignable and !Dumi::loggedInUser()->su){
                    $disabled = true;
                }

                if($disabled) {
                    echo "disabled";
                }
            ?>
            type="checkbox" name="{{ $role->id }}" tabindex="0" class="hidden w3-check">

            <span class="@if($disabled) slider-off @else slider @endif round"></span>
            <label class="label">{{ $role->name }}
                @if($role->su)
                <i class="w3-red fa fa-asterisk tooltip" >
                    <span class="tooltiptext w3-tag w3-round-xlarge">{{ trans('dumi.su_role_desc') }}</span>
                </i>@endif
                @if(!$role->assignable and !Dumi::loggedInUser()->su)
                <i class="w3-red fa fa-asterisk tooltip">
                    <span class="tooltiptext w3-tag w3-round-xlarge">{{ trans('dumi.unassignable_role_desc') }}</span>
                </i>
                @endif
                @if(!$role->assignable and Dumi::loggedInUser()->su)
                <i class="w3-red fa fa-asterisk tooltip" >
                    <span class="tooltiptext w3-tag w3-round-xlarge">{{ trans('dumi.unassignable_role_unlocked_desc') }}</span>
                </i>
            @endif
            </label>                
        </label>
    </p>
@endforeach
