@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::courses') }}">Courses</a>
        <i class="right angle icon divider"></i>
        <div class="active section">Class List</div>
    </div>
@endsection
@section('title', 'Class List')
@section('icon', "star")
@section('subtitle') {{$course->name}}@endsection
@section('content')
  <!-- <div class="w3-container w3-center" id="table">
  <div class="w3-row">
    <div class="w3-col m2 s4 text_themeD" style="position: relative;  top: 10px;">
     <label class="">Add Student</label>
    </div>
    <div class="w3-col l3 m6 s4">
      <input type="hidden" id="crs_id" name="course" value="{{-- $course->id --}}">
      <input class="w3-input" type="text" id="newStud" placeholder="Student ID">
    </div>
    <div class="w3-col m1" style="position: relative;  left: -15px;">
      <button type="submit" class="w3-button theme" id="add">Add</button>
    </div>
    <div class="w3-col m6 text_themeD" id="search">
     
    </div>
    <br>
  </div> -->
  


 <div class="ui one column doubling stackable grid container" id="table">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;">
       <!-- <div class="text" id="prnt"> -->
       <div class="ui doubling stackable grid">
            <div class="eight wide column" style="padding-top: 8px !important; color: #990000"><h3>Add/Search Student</h3></div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">                  
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                <input type="hidden" id="crs_id" name="course" value="{{ $course->id }}">
                <input type="text" class="prompt" placeholder="Student ID" id="newStud">
                    <i class="search icon" id="srch" style="color: #6b0000;"></i>
                    <!-- <i class="plus icon" id="add" style="color: #6b0000;"></i> -->
                  </div>
              </div>
          </div>
            <div class="four wide column">              
              @if(OHMS::loggedInUser()->hasPermission('ohms.students.access'))             
              <button type="submit" class="ui btn button" id="add">Add</button>
             @endif
             <div class="w3-col m6 text_themeD" id="search">
               
             </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr >
                  <th>Studnent ID</th>
                  <th>Name</th>
                  <th>Attendance</th>
                  <th>Options</th>
  			    </tr>
  			  </thead>
  			  <tbody>
            @if($course->students)
                @foreach($course->students as $student)
					<tr style="color: #980000;">
						<td>
              <div class="text">
                {{ $student->solusi_id }}
              </div>
              </td>
              <td>
               <div class="text">
                  {{ $student->user->full_name }}
                  
              </div>
                        </td>
                        <td>{{ $course->sessions->count() }}</td>
                        <td>
                          <div class="ui btn top icon left pointing dropdown button">
                            <i class="options icon"></i>
                              <div class="menu">
                                <div class="header">Options</div>
                              <a href="{{ route('OHMS::stud_class_remove', ['student_id' => $student->id, 'course_id' => $course->id]) }}" class="item">
                                <i class="trash icon"></i>
                                Remove
                              </a>
                              <a href="{{ route('OHMS::stud_attend', ['student_id' => $student->id, 'course_id' => $course->id]) }}" class="item">
                                <i class="eye icon"></i>
                                View
                              </a>
                              
                            </div>
                          </div>                              
						</td>
					</tr>
				@endforeach
        @endif
  			  </tbody>
  			</table>
      </div>
    </div>
  </div>
@endsection

@section('js')      
<!-- <script type="text/javascript" src="{{-- asset(OHMS::publicPath().'/js/jquery-3.4.1.min.js')--}}  "> </script> -->
<script type="text/javascript">
  $(document).ready(function(){
     $('#add').hide('400');
    $('#newStud').on('keyup', function(){
    var solusi_id = $(this).val();
    var course_id = $('#crs_id').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'); 
    console.log()
    $.ajax({
      url:"{{ url('admin/courses/search_stud') }}" ,
      type:"POST",
      data:{"_token": $('#token').val(), 'solusi_id' : solusi_id, 'course_id' : course_id},
      success : function(search){
        $('#search').html(search); 
        var tst = $('#tst').val();
        if(tst == 2) {             
          $('#add').show('400');
        }else{
          $('#add').hide('400');
        }
        }
      });
              
    });
    $('#add').on('click', function(){
    var solusi_id = $('#newStud').val();
    var course_id = $('#crs_id').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'); 
    console.log(solusi_id); 
    $.ajax({
      url:"{{ url('admin/courses/add_stud') }}" ,
      type:"POST",
      data:{"_token": $('#token').val(), 'solusi_id' : solusi_id, 'course_id' : course_id},
      success : function(search){
        $('#table').load(location.href + ' #table', function(){
          $('#add').hide('400');          
          $('#srch').show('400');     
              // $('#room_list').show('400');            
            });    
        }
      });
              
    });
  });  
</script>
@endsection