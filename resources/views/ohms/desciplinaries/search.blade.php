 @foreach($desciplinaries as $desciplinary) 
            <tr style="color: #980000;">
              <td>{{ $desciplinary->desciplinary_number }}</td>
              <td>{{ $desciplinary->block->hostel->gender }}</td>
              <td>{{ $desciplinary->block->name }}</td>
              <td>{{ $desciplinary->capacity }}</td>
              <td class="center alligned">
                      @if ($desciplinary->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Desciplinary ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this desciplinary " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::desciplinaries_edit', ['id' => $desciplinary->id]) }}" class="ui btn button">{{ trans('ohms.desciplinaries_edit') }}</a>
              </td>       
            </tr>
        @endforeach