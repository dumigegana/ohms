<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('staff_id');
            $table->date('emp_date');
            $table->string('number');
            $table->string('qualification');
            $table->string('emergency_number');
            $table->string('home_address');
            $table->string('department');
            $table->string('scale');
            $table->string('account_number');
            $table->string('bank_name');
            $table->string('employment_number');
            $table->string('sun_account_number');
            $table->string('nssa_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
