<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use App\Student;
use App\User;
use App\Room; 
use App\Block;
use App\Check_temp;
use App\Allocation;

class OutBookCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'book:cron';

    /**
     * The console command description.
     * 
     * @var string
     */
    protected $description = 'Clear room allocations not approved with 72hrs.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
       //book:cron
       $allocations = Allocation::whereDate('created_at', '<=', Carbon::now()->adddays(-2)->toDateString())
                ->where([['check_out', 0], ['approved', 0]])
                ->with('student', 'room', 'check_temps')->get();
        if($allocations){
            foreach ($allocations as $key => $allocation) {
                foreach ($allocation->check_temps as $key => $check_temp) {
                    $check_temp->delete();
                }
                $allocation->room->decrement('occupants');
                $allocation->student->update(['has_room' => 0]);
                Allocation::where('id', $allocation->id)->update(['check_out' => 1]);
           }
        }
    }
}
