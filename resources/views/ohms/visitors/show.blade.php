@extends('layouts.admin.panel')
@section('css')
   <style type="text/css">
       .hdr{
        font-size:40px;"
       }
       .bdy{

       }
   </style> 
@endsection
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;"></a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.visitors_edit_title') }}</div>
    </div>
@endsection
@section('content')
<div class="ui container">
    <div class="ui doubling stackable grid">
        <div class="row">
            <div class="four wide column">
           </div>
        <div class="ui very padded segment" > 
                      
            <div class="eight wide column" >                
                <div class="ui very padded segment" >
                    <div class="ui list">
                        <div class="item">
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
Full Name:              {{$user->full_name}}</pre>
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
Username:               {{$user->username}}</pre>
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
Email Address:          {{$user->email}}</pre>
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
Cell Number:            {{$user->cell_number}}</pre>        
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
Arrival:                {{$visitor->arrival_date}}</pre>
                           <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
Return:                 {{$visitor->departure_date}} </pre>
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
Room:                   {{$visitor->room->block->name}} {{$visitor->room->room_number}}</pre>
                            @isset($visitor->a_flight)
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
Flight Arrival:        <strong> {{$visitor->ar_flight->flight}}</strong> </pre>
                                
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                Date:       {{$visitor->ar_flight->flight_date}} </pre>
                           <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                Time       {{$visitor->ar_flight->flight_time}}</pre>
                            @endisset
                            @isset($visitor->r_flight)
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
Flight Return:          <strong>{{$visitor->re_flight->flight}}</strong>  </pre>
                                
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                Date        {{$visitor->re_flight->flight_date}} </pre>
                            <pre style="font-size:16px; color: #6b0000; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                Time        {{$visitor->re_flight->flight_time}}  </pre>
                                </div>
                            </div>
                            @endisset
                        </div>      
                      </div>
                    </div>              
                </div>  
            </div>
        </div> 
@endsection