<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\House;
use App\House_category;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class HousesController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Get all the houses
        $houses = House::paginate(25);

        
        # Return the view
        return view('ohms/houses/index', ['houses' => $houses]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Get the house
        $house = House::find($id);

        # Return the view
        return view('admin/houses/show', ['house' => $house]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

        # Find the house
        $row = House::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'houses';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/houses/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

        # Find the row 
        $row = House::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'houses';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::houses')->with('success', trans('ohms.msg_house_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

         $house_categories = House_category::orderBy('name', 'asc')->get();

          # Get all the data
        $data_index = 'houses';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/houses/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.houses.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

        # create new role
        $row = new House;

        # Save the data
        $data_index = 'houses';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::houses')->with('success', trans('ohms.msg_house_created'));

    }
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.houses.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.houses.admin');

        # Select Item
        $house = House::find($id);

        if(!$house->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $house->delete();

        # Redirect the admin
        return redirect()->route('OHMS::houses')->with('success', trans('ohms.msg_house_deleted'));
    }
}
