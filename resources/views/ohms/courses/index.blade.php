@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">Courses</div>
    </div>
@endsection
@section('title', "Courses")
@section('icon', "star")
@section('subtitle', "Courses")
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $courses->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.courses.admin'))
             <center><a href="{{ route('OHMS::courses_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add Course</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr>
                  <th>Name</th>
                  <th>Sessions</th>
                  <th>Description</th>
                  <th>Options</th>
  			    </tr>
  			  </thead>
  			  <tbody>
                @foreach($courses as $course)
					 <tr style="color: #980000;">
						<td>
                            <div class="text">
                                {{ $course->name }}
                                
                            </div>
                        </td>
                        <td>{{ $course->sessions->count() }}</td>
                        <td>{{ $course->description }}</td>
                        <td>
                          <div class="ui btn top icon left pointing dropdown button">
                                <i class="options icon"></i>
                                <div class="menu">
                                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                                  <a href="{{ route('OHMS::courses_edit', ['id' => $course->id]) }}" class="item">
                                    <i class="edit icon"></i>
                                   Edit
                                  </a> 
                                  <a href="{{ route('OHMS::courses_classList', ['id' => $course->id]) }}" class="item ">
                                <i class="list icon"></i>
                               Class List
                              </a> 
                              <a href="{{ route('OHMS::courses_delete', ['id' => $course->id]) }}" class="item">
                                <i class="trash icon"></i>
                                Delete
                              </a>                                                                  
                                </div>
                              </div>


                            </div>
                          </div>
						</td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
  </div>
</div>
</div>
@endsection
