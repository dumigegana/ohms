@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.blocks_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.blocks_title'))
@section('icon', "hospital")
@section('subtitle', trans('ohms.blocks_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{{-- !! $blocks->links() !!--}}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.blocks.admin'))
             <center><a href="{{ route('OHMS::blocks_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_block') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b;">
            <tr >
                  <th style="color: #980000;">Name</th>
                  <th style="color: #980000;">Criterion</th>
                  <th style="color: #980000;">Hostel</th>
                  <th style="color: #980000;"></th>
            </tr>
          </thead>
          <tbody>
                @foreach($blocks as $block)
          <tr style="color: #980000;">
            <td>
                <div class="text">
                  <?php $var = 'Male'; ?>
                    {{ $block->name }}
                </div>
              </td>
              <td>{{$block->hostel->gender}}</td>
              <td>{{ $block->hostel->name }}</td>
              <td>
                @if($block->allow_editing or OHMS::loggedInUser()->su)
                  <a href="{{ route('OHMS::blocks_edit', ['id' => $block->id]) }}" class="ui btn button"><i class="options icon"></i>{{ trans('ohms.blocks_edit') }}</a>
                @else
                  <div class="ui disabled btn icon button">
                    <i class="lock icon"></i>
                  </div>
                 @endif
                </td>
                        
          </tr>
        @endforeach
          </tbody>
        </table>
      </div>
        <br>
    </div>
  </div>
</div>
@endsection
