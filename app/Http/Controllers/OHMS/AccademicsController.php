<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Accademic;
use App\Block;
use App\Hostel;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class AccademicsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.accademics.access');

        # Get all the accademics
        $accademics = Accademic::with('block')->paginate(50);

        # Return the view
        return view('ohms/accademics/index', ['accademics' => $accademics]);
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.accademics.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $accademics = Accademic::with('block')->paginate(100);
        }
        else {       
          $accademics = Accademic::with('block', 'block.hostel')
                             ->where('accademic_number','like', $post . '%')                   
                             ->orwherehas('block', function($o) use ($post) {$o->where('name', 'like',  $post . '%');})
                             ->orwherehas('block.hostel', function($r) use ($post) {$r->where('gender', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/accademics/search', ['accademics' => $accademics]);
        return $read;
    }
    public function show($id)
    {
        OHMS::permissionToAccess('ohms.accademics.access');

        # Get the accademic
        $accademic = Accademic::find($id);

        # Return the view
        return view('admin/accademics/show', ['accademic' => $accademic]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.accademics.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.accademics.admin');

        # Find the accademic
        $row = Accademic::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'accademics';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/accademics/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.accademics.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.accademics.admin');

        # Find the row
        $row = Accademic::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'accademics';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::accademics')->with('success', trans('ohms.msg_accademic_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.accademics.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.accademics.create');

        # Get all the data
        $data_index = 'accademics';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/accademics/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.accademics.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.accademics.admin');

        # create new role
        $row = new Accademic;

        # Save the data
        $data_index = 'accademics';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::accademics')->with('success', trans('ohms.msg_accademic_created'));

    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.accademics.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.accademics.admin');

        # Select Item
        $accademic = OHMS::accademic('id', $id);

        if(!$accademic->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $accademic->delete();

        # Redirect the admin
        return redirect()->route('OHMS::accademics')->with('success', trans('ohms.msg_hostel_deleted'));
    }
     
}
