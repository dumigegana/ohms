<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LogNameCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logname';

    /**
     * The console command description.
     * 
     * @var string
     */
    protected $description = 'Allows users to login using username instead of email.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
       //logname        
        $file_location = base_path() . '/vendor/laravel/framework/src/Illuminate/Foundation/Auth/AuthenticatesUsers.php';
        $contant = file_get_contents($file_location);
        $oldstr = "/email/";
        $newstr = "username";
        $contant = preg_replace($oldstr, $newstr, $contant);
        file_put_contents($file_location, $contant);
        

    }
}
