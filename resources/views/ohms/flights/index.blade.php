@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.flights_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.flights_title'))
@section('icon', "thumbs outline up")
@section('subtitle', trans('ohms.flights_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $users->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.users.admin'))
             <center><a href="{{ route('OHMS::users_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_user') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b;">
            <tr>
                  <th style="color: #980000;">Flight</th>
                  <th style="color: #980000;">Status</th>
                  <th style="color: #980000;">Flight Date</th>
                  <th style="color: #980000;">Flight Time</th>
                  <th style="color: #980000;">{{ trans('ohms.options') }}</th>
            </tr>
          </thead>
          <tbody>
                @foreach($flights as $flight)
          <tr style="color: #980000;">
            <td>
              <div class="text">
                  {{ $flight->flight }}
                </div>
                  </td>
            <td>
              <div class="text">
                  {{ $flight->status }}
                </div>
                  </td>
            <td>
              <div class="text">
                  {{ $flight->flight_date }}
                </div>
                  </td>
            <td>
              <div class="text">
                  {{ $flight->flight_time }}
                </div>
                  </td>
                        <td>
                          @if($flight->allow_editing or OHMS::loggedInUser()->su)
                              <div class="ui btn top icon left pointing dropdown button">
                                <i class="options icon"></i>
                                <div class="menu">
                                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                                  <a href="{{ route('OHMS::flights_edit', ['id' => $flight->id]) }}" class="item">
                                    <i class="edit icon"></i>
                                    {{ trans('ohms.flights_edit') }}
                                  </a>                                                                    
                                </div>
                              </div>
                          @else
                              <div class="ui disabled btn icon button">
                                  <i class="lock icon"></i>
                              </div>
                          @endif
            </td>
          </tr>
        @endforeach
          </tbody>
        </table>
      </div>
        <br>
    </div>
  </div>
@endsection
