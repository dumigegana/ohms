<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefit_Record extends Model
{
     public function staff()
    {
        return $this->belongsTo('App\Staff');
    }

	public function dependant()
    {
        return $this->belongsTo('App\Dependant');
    }

    public function benefit()
    {
        return $this->belongsTo('App\Benefit');
    }

}
