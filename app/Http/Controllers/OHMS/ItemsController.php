<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Item;
use App\Maint_dep;
use App\Permission;
use Schema;
use Auth;
use OHMS;

class ItemsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.items.access');

        # Get all the items
        $items = Item::with('maint_dep')->get();

        
        # Return the view
        return view('ohms/items/index', ['items' => $items]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.items.access');

        # Get the item
        $item = Item::find($id);

        # Return the view
        return view('admin/items/show', ['item' => $item]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.items.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.items.admin');

        # Find the item
        $row = Item::find($id);
        $maint_deps = Maint_dep::all();

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        };

        # Return the view
        return view('ohms/items/edit', [ 'maint_deps' => $maint_deps, 'row' => $row]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.items.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.items.admin');

        # Find the row
        $row = Item::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'items';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::items')->with('success', trans('ohms.msg_item_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.items.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.items.admin');
        $maint_deps = Maint_dep::all();
        # Return the view
        return view('ohms/items/create', ['maint_deps' =>  $maint_deps ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.items.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.items.admin');

        # create new item
        $row = new Item;

        # Save the data
        $data_index = 'items';
        require('Data/Create/Save.php');

        # Return the admin to the items page with a success message
        return redirect()->route('OHMS::items')->with('success', trans('ohms.msg_item_created'));
    }
   

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.items.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.items.admin');

        # Select Item
        $item = OHMS::item('id', $id);

        if(!$item->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $item->delete();

        # Redirect the admin
        return redirect()->route('OHMS::items')->with('success', trans('ohms.msg_item_deleted'));
    }
}
