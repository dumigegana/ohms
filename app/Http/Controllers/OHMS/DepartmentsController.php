<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Department;
use App\Permission;
use Schema;
use Auth;
use OHMS;

class DepartmentsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.departments.access');

        # Get all the departments
        $departments = Department::paginate(30);

        
        # Return the view
        return view('ohms/departments/index', ['departments' => $departments]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.departments.access');

        # Get the department
        $department = Department::find($id);

        # Return the view
        return view('admin/departments/show', ['department' => $department]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.departments.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.departments.admin');

        # Find the department
        $row = Department::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'departments';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/departments/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.departments.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.departments.admin');

        # Find the row
        $row = Department::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'departments';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::departments')->with('success', trans('ohms.msg_department_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.departments.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.departments.admin');

        # Get all the data
        $data_index = 'departments';
        require('Data/Create/Get.php');        

        # Return the view
        return view('ohms/departments/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.departments.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.departments.admin');

        # create new department
        $row = new Department;

        # Save the data
        $data_index = 'departments';
        require('Data/Create/Save.php');        

        # Return the admin to the departments page with a success message
        return redirect()->route('OHMS::departments')->with('success', trans('ohms.msg_department_created'));
    }
   

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.departments.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.departmentse.admin');

        # Select Department
        $department = OHMS::department('id', $id);

        if(!$department->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }
        
        # Delete Department
        $department->delete();

        # Redirect the admin
        return redirect()->route('OHMS::departments')->with('success', trans('ohms.msg_department_deleted'));
    }
}
