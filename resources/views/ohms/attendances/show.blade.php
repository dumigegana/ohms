@extends('layouts.admin.w3css')
@section('breadcrumb') 
<a href="{{ route('OHMS::sessions') }}" class="w3-bar-item">{{ trans('Sessions') }}</a>
<div class="w3-bar-item">Class Attendance List</div>
@endsection
@section('title', $session->course->name)
@section('icon', "pencil")
@section('subtitle', $session->date_time )
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $attendances->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.attendances.admin'))
             <center><a href="{{ route('OHMS::attendances_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Mark Attendance}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="w3-table-all">
          <thead>
            <tr class="theme">
              <th>Name</th>
              <th>ID#</th>
              <th>Mark</th>
              <th>Code</th>
              <th>IP</th>                  
              <th>Date Time</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
             @foreach($attendances as $attendance)
          <tr> 
            <td> {{ $attendance->student->user->full_name }}</td>
            <td>{{ $attendance->student->solusi_id}}</td>
            <td>{{ $attendance->mark }}</td>
            <td>{{ $attendance->code }}</td>
            <td>{{ $attendance->ip}}</td>
            <td>{{ $attendance->updated_at}}</td>
            <td>
                <a href="{{ route('OHMS::attendances_edit', ['id' => $attendance->id]) }}" class="w3-theme w3-button">
            <i class="fa fa-edit"></i>
            Edit
          </a></td>
          </tr>
        @endforeach
          </tbody>
        </table>
      </div>
        <br>
    </div>
  </div>
@endsection