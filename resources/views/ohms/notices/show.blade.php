@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">Notice</div>
    </div>
@endsection
@section('title', trans('ohms.notices_title'))
@section('icon', "star")
@section('subtitle', trans('ohms.s_subtitle'))
@section('content')
   
  <br><br>

  <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea">
        <div class="inline">
                <div class="media">
              <div class="madia-body"><center>
              
                  <h1 class="media-heading">{{$notice->title}}</h1></center>
                  <div class="row">
                  <p class="text-right">{!! $notice->body!!}</p>
                  <div class="row">
                       <div class=" ">{{$notice['created_at']->diffForHumans()}}</div>
                       
                       </div>
                  &nbsp; 
&nbsp;
              </div>
          </div>
      </div>
          </tbody>
        </table>
      </div>
        <br>
    </div>
  </div>
@endsection
