@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::hostels') }}">{{ trans('ohms.hostels_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.hostels_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.hostels_create_title'))
@section('icon', "bank ")
@section('subtitle', trans('ohms.hostels_create_subtitle'))
@section('content')
<div class="ui doubling stackable grid container">
    <div class="ten wide column centered">
        <div class="ui very padded segment" style="background: #ffeaea;">
            <form class="ui form" method="POST">
                {{ csrf_field() }}                           
                <div class="required field">
                    <label style="color: #980000;">Hostel Name</label>
                    <input name="name" type="text" placeholder="Hostel Name" value="{{ old('name')}}" required>
                </div>                                                  
                <div class="fields">
                    <div class="required field">
                        <label style="color: #980000;">Gender</label>
                        <input type="radio" name="gender" required value="Male" {{ (old("gender") == 'Male' ? "checked":"") }}> Male<br>
                  <input type="radio" name="gender" value="Female" {{ (old("gender") == 'Female' ? "checked":"") }}> Female<br>
                    </div>
                </div>
                <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
            </form>
        </div>
    </div>        
</div>
@endsection
