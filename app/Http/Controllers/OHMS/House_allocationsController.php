<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Allocations;
use App\Student;
use App\House;
use App\House_allocation;
use App\House_category;
use App\Visitor;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use OHMS;
use Session;

class House_allocationsController extends Controller
{
  public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.house_allocations.access'); 
        $read = "";
        $post = $request['keywords'];
        $allocable = "App\\" . $request['alloc'];
        if(empty($post)) {
          $house_allocations = House_allocation::with('house_allocable')
                              ->where('house_house_allocable_type', $allocable)
                              ->orderBy('cleared', 'asc')->paginate(100);
        }
        else {       
          $house_allocations = House_allocation::with('house_allocable', 'house_allocable')
                             ->where('house_allocable_type', $house_allocable) 
                             ->wherehas('house', function($o) use ($post) {$o->where('number', 'like',  $post . '%');})
                             ->orwherehas('house_allocable', function($o) use ($post) {$o->where('su_id', 'like',  $post . '%');})
                             ->orwherehas('house_allocable.user', function($r) use ($post) {$r->where('fullname', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/house_allocations/search', ['house_allocations' => $house_allocations]);
        return $read;
    }

  public function index_hr($alloc)
  {
   OHMS::permissionToAccess('ohms.house_allocations.access');
     
     $allocable = "App\\" .$alloc;
        # Get all the House_allocations
    $house_allocations = House_allocation::with('house_allocable')
                              ->where('house_allocable_type', $allocable)
                              ->orderBy('cleared', 'asc')->paginate(100);
        
        # Return the view
    return view('ohms/house_allocations/index_hr', ['house_allocations' => $house_allocations, 'alloc' => $alloc]);
  }  

//   public function index_dosa()
//   {
//     OHMS::permissionToAccess('ohms.allocations.access');

//         # Get all the House_allocations
//     $house_allocations = House_allocation::where('house_allocable_type', 'App\Student')
//                               ->with('house_allocable')->
//                               orderBy('cleared', 'asc')->paginate(100);
        
//         # Return the view
//     return view('ohms/house_allocations/index_dosa', ['house_allocations' => $house_allocations]);
//   }

// public function index_pro()
//   {
//     OHMS::permissionToAccess('ohms.allocations.access');

//         # Get all the House_allocations
//     $house_allocations = House_allocation::where('house_allocable_type', 'App\Visitor')
//                               ->with('house_allocable')->
//                               orderBy('cleared', 'asc')->paginate(100);
        
//         # Return the view
//     return view('ohms/house_allocations/index_pro', ['house_allocations' => $house_allocations]);
//   }

  public function change($id, $type)
  {
        # Check permissions
    OHMS::permissionToAccess('ohms.house_allocations.admin');

        # Find The student
     House_allocation::where('id', $id)->update(['cleared' => 1]);
    if($type === 'b')
    {
      return redirect()->route('OHMS::maintbs')->with('success', trans('ohms.msg_house_allocation_changed'));  
    }
    elseif ($type === 'c')
    {
      return redirect()->route('OHMS::maintbs')->with('success', trans('ohms.msg_house_allocation_changed'));  
    }
    else{
      return redirect()->route('OHMS::house_allocations')->with('success', trans('ohms.msg_house_allocation_changed'));      
    }
    
  }
    
  public function delete(Request $request)
  {
        # Check permissions
    OHMS::permissionToAccess('ohms.house_allocations.admin');

        # Delete row
    House_allocation::where('id', $request['id'])->delete();

    if($request['type'] === 'b')
    {
      return redirect()->route('OHMS::maintbs')->with('success', trans('ohms.msg_house_allocation_deleted'));  
    }
    else{
      return redirect()->route('OHMS::house_allocations')->with('success', trans('ohms.msg_house_allocation_deleted'));      
    }
  }

  public function create_pro()
  {
    OHMS::permissionToAccess('ohms.house_allocations.admin');

   $visitors = Visitor::with('user');
    $houses = House::all();

        # Return the view
    return view('ohms/house_allocations/creat_pro', ['houses' => $houses, 'visitors' => $visitors]);
  }

  public function create_dosa()
  {
   OHMS::permissionToAccess('ohms.house_allocations.admin');

    $houses = House::all();
    $students = Student::all('user');

        # Return the view
    return view('ohms/house_allocations/create_dosa', ['students' => $students, 'houses' => $houses]);
  }

   public function create_staff()
  {
   OHMS::permissionToAccess('ohms.house_allocations.admin');

    $houses = House::all();
    $staff = Staff::all('user');

        # Return the view
    return view('ohms/house_allocations/create_staff', ['staff' => $staff, 'houses' => $houses]);
  }

  public function store(Request $data)
  {      
    OHMS::permissionToAccess('ohms.house_allocations.admin');
    $this->validate($data, [
            'comments' => 'required|max:120',
            'house_allocable_id' => 'required',
            'house_allocable_type' => 'required',
            'house_id' => 'required',
        ]);

     House_allocation::updateOrCreate([
                'house_allocable_type' => $data['house_allocable_type'],
                'house_allocable_id' => $data['house_allocable_id'],
                'house_id' => $data['house_id']
                //'comments' => $data['comments']
                ]);


    if($data['house_allocable_type'] === 'App\Student'){
      return redirect()->route('OHMS::dosa_allocations')->with('success', trans('ohms.msg_house_allocation_created'));
    }
    elseif($data['house_allocable_type'] === 'App\Visitor'){
      return redirect()->route('OHMS::pro_allocations')->with('success', trans('ohms.msg_house_allocation_created'));
    }
    elseif($data['house_allocable_type'] === 'App\Staff'){
      return redirect()->route('OHMS::staff_allocations')->with('success', trans('ohms.msg_house_allocation_created'));
    }
    else{
      return redirect()->route('OHMS::house_allocations')->with('success', trans('ohms.msg_house_allocation_created')); 
    }  
  }

  public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.house_allocations.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.house_allocations.delete');

        # Select Item
        $house_allocation = OHMS::house_allocation('id', $id);

        if(!$house_allocation->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $house_allocation->delete();

        # Redirect the admin
        return redirect()->route('OHMS::house_allocations')->with('success', trans('ohms.msg_hostel_deleted'));
    }
}
