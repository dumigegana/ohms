<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facility;
use App\Facility_User;
use App\User;
use App\Permission;
use App\Permission_Facility;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class FacilitiesController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Get all the facilities
        $facilities = Facility::all();

        
        # Return the view
        return view('ohms/facilities/index', ['facilities' => $facilities]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Get the facility
        $facility = Facility::find($id);

        # Return the view
        return view('admin/facilities/show', ['facility' => $facility]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # Find the facility
        $row = Facility::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'facilities';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/facilities/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # Find the row
        $row = Facility::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'facilities';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::facilities')->with('success', trans('ohms.msg_facility_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # Get all the data
        $data_index = 'facilities';
        require('Data/Create/Get.php');        

        # Return the view
        return view('ohms/facilities/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # create new facility
        $row = new Facility;

        # Save the data
        $data_index = 'facilities';
        require('Data/Create/Save.php');

        # Return the admin to the facilities page with a success message
        return redirect()->route('OHMS::facilities')->with('success', trans('ohms.msg_facility_created'));
    }

   
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.facilities.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # Select Facility
        $facility = OHMS::facility('id', $id);

        if(!$facility->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Delete Facility
        $facility->delete();

        # Redirect the admin
        return redirect()->route('OHMS::facilities')->with('success', trans('ohms.msg_facility_deleted'));
    }
}
