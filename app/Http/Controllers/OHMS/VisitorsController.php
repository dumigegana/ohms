<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;
use App\Role_User;
use App\User;
use App\Visitor;
use App\Room;
use App\Block;
use App\Flight;
use OHMS;
use Auth; 
use DB;
Use Carbon\Carbon;

class VisitorsController extends Controller
{
 /*
    |--------------------------------------------------------------------------
    | Visitors Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new visitors as well as their
    | validation and creation. 
    |
    */

     public function index()
    { 
         OHMS::permissionToAccess('ohms.users.access');

        # Get all users
        $visitors = Visitor:: with('user')->where('has_accomo', true)->paginate(100);
        # Get the active users
        $active_users = OHMS::users('active', true);

        # Get Banned Users
        $banned_users = OHMS::users('banned', true);

        # Get all roles
        $roles = OHMS::roles();

        # Return the view
        return view('ohms/reports/visitor_list', [ 
            'users'         =>  $users,
            'roles'         =>  $roles, 
            'active_users'  =>  $active_users,
            'banned_users'  =>  $banned_users,
        ]);
        
      
    }

     public function add()
    { 
         OHMS::permissionToAccess('ohms.users.access');

        # Get all users
        $visitors = User::with('visitor')->paginate(100);
        # Get the active users
        $active_visitors = OHMS::users('active', true);

        # Get Banned Users
        $banned_visitors = OHMS::users('banned', true);

        # Get all roles
        $roles = OHMS::roles();
        # Return the view
        return view('ohms/visitors/add', [ 
            'visitors'         =>  $visitors,
            'roles'         =>  $roles, 
            'active_visitors'  =>  $active_visitors,
            'banned_visitors'  =>  $banned_visitors,
        ]);
        
      
    }
    
    public function getrooms(Request $request)
    {
        $room_list = "";
        $rooms = Room::where([
            ['block_id', '=', $request['bl_id']],
            ['enable', '1']
            ])->whereColumn('capacity', '>', 'occupants')
        ->get();
        $block = Block::find($request['bl_id']);    
        //$room_list = view('student/includes/vrooms', ['rooms' =>  $rooms, 'block' => $block]);
        $room_list = view('ohms/visitors/vrooms', ['rooms' =>  $rooms, 'block' => $block]);
        return $room_list;
    }
    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.visitors.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $visitors = User::paginate(100);
        }
        else {       
          $visitors = User::where('full_name','like', $post . '%')->paginate(100);
        }
        # Get the active users
        $active_visitors = OHMS::users('active', true);

        # Get Banned Users
        $banned_visitors = OHMS::users('banned', true);

        # Get all roles
        $roles = OHMS::roles();
        
        $read = view('ohms/visitors/search', [ 
            'visitors'         =>  $visitors,
            'roles'         =>  $roles, 
            'active_visitors'  =>  $active_visitors,
            'banned_visitors'  =>  $banned_visitors,
        ]);
        return $read;

    }

    public function search_reo(Request $request)
    {
       $visitors = Visitor::with('user')->where('has_accomo', true)->paginate(100);
        # Get the active users
        $active_users = OHMS::users('active', true);

        # Get Banned Users
        $banned_users = OHMS::users('banned', true);

        # Get all roles
        $roles = OHMS::roles();

        # Return the view
        return view('ohms/reports/visitor_list', [ 
            'users'         =>  $users,
            'roles'         =>  $roles, 
            'active_users'  =>  $active_users,
            'banned_users'  =>  $banned_users,
        ]);
        
        OHMS::permissionToAccess('ohms.visitors.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $visitors = User::paginate(100);
        }
        else {       
          $visitors = User::where('full_name','like', $post . '%')->paginate(100);
        }
        # Get the active users
        $active_visitors = OHMS::users('active', true);

        # Get Banned Users
        $banned_visitors = OHMS::users('banned', true);

        # Get all roles
        $roles = OHMS::roles();
        
        $read = view('ohms/reports/search/visitor', [ 
            'visitors'         =>  $visitors,
            'roles'         =>  $roles, 
            'active_visitors'  =>  $active_visitors,
            'banned_visitors'  =>  $banned_visitors,
        ]);
        return $read;

    }

    public function show($id)
    {        //OHMS::permissionToAccess('ohms.visitors.access');

        # Find the Visotor
        $user = User::FindOrFail($id);
        $visitor =Visitor::where('user_id', $id)->orderBy('id', 'desc')->first();

        # Return the view
        return view('ohms/visitors/show', ['user' => $user, 'visitor' => $visitor]);
    }

    public function create($id)
    {
        # Check if admin access
        //OHMS::mustBeVisitor($visitor);

        OHMS::permissionToAccess('ohms.visitors.access');  

        # Check permissions
        OHMS::permissionToAccess('ohms.visitors.create');
        $visitor = User::where('id',$id)->first();
        # Get all roles
        $hostels = OHMS::hostels()->all();

        $yesterday = Carbon::yesterday()->toDateString();

        # Get all flights
        $flights = OHMS::flights()->where('flight_date', '<=', $yesterday);        

        # Return the view
        return view('ohms/visitors/create', [
            'visitor'  => $visitor,
            'hostels'   =>  $hostels,
            'flights'   =>  $flights,
        ] );
    }

    public function store(Request $request)
    {
       OHMS::permissionToAccess('ohms.visitors.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.visitors.create');

           

        //return redirect()->route('OHMS::visitors')->with('success', trans("ohms.msg_visitor_created"));

        $this->validate($request, [
            'depature_date' => 'required',
            'arrival_date' => 'required',
            'user_id' => 'required|integer',
            'room_id' => 'required|integer',
            'gender' => 'required',
            'spause' => 'required',
        ]);
        $visitor = Visitor::FindOrFail($request['visitor_id']);
       if ($visitor->has_accomo == false) {
        
            $row = new Visitor;
            $row->room_id = $request['room_id']; 
            $row->user_id = $request['user_id']; 
            $row->arrival_date = $request['arrival_date'];
            $row->departure_date = $request['depature_date'];
            $row->transport = $request['transport']; 
            $row->a_flight = $request['a_flight']; 
            $row->r_flight = $request['r_flight'];
            $row->spause = $request['spause']; 
            $row->gender = $request['gender'];
            $row->has_accomo = 1;
            $row->save();

            Room::find($request['room_id'])->increment('occupants');
            return redirect()->route('OHMS::visitors_add')->with('success', trans('ohms.msg_visitor_created'));
        } else {
            return redirect()->route('OHMS::visitors_add')->with('error', trans('The user has room already.'));
        }
    }

     public function clear($id, $room)
    {
        User::find($id)->decrement('has_room');
        Room::find($room)->decrement('occupants');       

        return redirect()->route('OHMS::visitors_add')->with('success', trans('ohms.msg_visitor_edited'));
    }
  
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.visitors.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.visitors.delete');

        # Find The student
        $visitor = OHMS::visitors('id', $id)->first();

        # Delete Relationships
            $rels = User::where('id', $visitor->user_id)->get();
            foreach($rels as $rel) {
                $rel->delete();
            

            # Delete visitor
            $visitor->delete();

            # Return the admin with a success message
            return redirect()->route('OHMS::visitors')->with('success', trans('ohms.msg_visitor_deleted'));
        }
    }

    
}