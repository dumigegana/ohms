@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.majors_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.majors_title'))
@section('icon', "lab")
@section('subtitle', trans('ohms.majors_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $majors->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.majors.admin'))
             <center><a href="{{ route('OHMS::majors_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_major') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
  			<table class="ui basic table ">
          <thead>
  			    <tr>
                  <th style="color: #980000;">{{ trans('ohms.name') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.options') }}</th>
  			    </tr>
  			  </thead>
  			  <tbody>
                @foreach($majors as $major)
					<tr style="color: #980000;">
						<td>
                            <div class="text">
                                {{ $major->programme }}
                            </div>
                        </td>
                        <td>
                          @if($major->allow_editing or OHMS::loggedInUser()->su)
                              <div class="ui btn top icon left pointing dropdown button">
                                <i class="options icon"></i>
                                <div class="menu">
                                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                                  <a href="{{ route('OHMS::majors_edit', ['id' => $major->id]) }}" class="item">
                                    <i class="edit icon"></i>
                                    {{ trans('ohms.majors_edit') }}
                                  </a>
                                  
                                </div>
                              </div>
                          @else
                              <div class="ui disabled btn icon button">
                                  <i class="lock icon"></i>
                              </div>
                          @endif
						</td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
        {!! $majors->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection