
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMajorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('majors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('programme')->unique();
            $table->timestamps();
        });
        $majors = [
            'BA History',
            'BA English & Communication',
            'BA Peace & Conflict Studies',
            'BA Religious Studies',
            'BA Theology',
            'BBA Accounting',
            'BBA Business Management',
            'BBA Finance',
            'BBA C and MIS',
            'BBA Marketing',
            'BSc Agribusiness',
            'BSc Clothing and Textiles',
            'BSc Environmental Health',
            'BSc Family & Consumer Science',
            'BSc Clothing and Family Studies',
            'BSc Food Science and Nutrition',
        ];

        foreach($majors as $major) {
            $mjr = \OHMS::newMajor();
            $mjr->programme = $major;
            $mjr->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('majors');
    }
}
