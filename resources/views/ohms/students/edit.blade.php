@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::students') }}">{{ trans('ohms.student_list') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.students_edit_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.students_edit_title'))
@section('icon', "edit")
@section('subtitle', trans('ohms.students_edit_subtitle', ['suid' => $row->solusi_id]))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST">
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea;">
                        {{ csrf_field() }}
                        <input name="id" type="hidden" value="$row->id">
                        
                        <div class="field">
                        <label style="color: #980000;">Student ID number</label>
                       <input name="solusi_id" type="text" value="{{-- Request::old('solusi_id') ? Request::old('solusi_id') : isset($row) ? $row->solusi_id : "" --}} {{$row->solusi_id }}">
                        </div>
                        <br/> 
                        <div class="field">
                        <label style="color: #980000;">Date of Birth</label>
                        <input name='date_of_birth' value="{{ $row->date_of_birth }}" type="date" placeholder="">
                        </div>
                        <br/>
                        <div class="field">
                        <label style="color: #980000;">National ID number</label>
                        <input name='national_id' value="{{ $row->national_id }}" type="text" placeholder="">
                        </div> 
                        <br/>
                        <div class="ui equal width form">
                            <div class="fields">
                                <div class="required field">
                                <label style="color: #980000;">Degree Level</label>
                                <select id="deg_level" name="degree_level" class="form-control" >
                                    <option value="Undergraduate" {{ ($row->degree_level == 'Undergraduate' ? "selected":"") }}> Undergraduate</option>
                                    <option value="Postgraduate" {{ ($row->degree_level == 'Postgraduate' ? "selected":"") }}> Postgraduate</option>
                                </select>
                                </div>
                                <br/>
                                <div class="field" id="level_yr">
                                <label style="color: #980000;">Year of study</label>
                                <select name="year" class="form-control">
                                    <option value="1" {{ ($row->year == '1' ? "selected":"") }}>First </option>
                                    <option value="2" {{ ($row->year == '2' ? "selected":"") }}>Second </option>
                                    <option value="3" {{ ($row->year == '3' ? "selected":"") }}>Third</option>
                                    <option value="4" {{ ($row->year == '4' ? "selected":"") }}>Fouth </option>
                                 </select>
                                </div>
                            </div> 
                        </div> 
                        <br>                              
                        <div class="field">
                            <label style="color: #980000;">Cell number</label>
                            <input name='cell_number' type="text" value="{{ $row->cell_number }}">
                            </div>                       
                        </div>
                </div>                
                <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea">
                        {{ csrf_field() }}
                                                                     
                        <div class="field">
                            <label style="color: #980000;">Personal Email</label>
                            <input name="email" value="{{ $row->email }}" type="text" placeholder="">
                        </div>
                        <br>                                             
                        <div class="field">
                            <label style="color: #980000;">Home Address</label>
                            <input name="home_address" value="{{ $row->home_address }}" type="text" placeholder="">
                        </div>
                        <br>
                        <div class="field">
                            <label style="color: #980000;">Next of Kin contact number</label>
                            <input name="next_of_kin" value="{{ $row->next_of_kin }}" type="text" placeholder="">
                        </div>
                        <br>
                        <div class="field" id="swh"> 
                            <label style="color: #980000;">Major</label>
                            <select name="major_id" class="form-control">
                            <option value="{{ $row->major_id }}"> {{ $row->major->programme }}</option>
                            @foreach($majors as $major)
                                <option value="{{ $major->id }}">{{ $major->programme }}</option>
                                @endforeach
                            </select>
                        </div>
                        <br/>
                        <div class="ui equal width form">
                            <div class="fields">
                                 <div class="field">
                                    <label style="color: #980000;">Gender</label>
                                    <select name="gender" class="form-control">
                                        <option value="Male" {{ ($row->gender == 'Male' ? "selected":"") }}>Male</option>
                                        <option value="Female" {{ ($row->gender == 'Female' ? "selected":"") }}>Female</option>
                                     </select>
                                </div>                
                            </div>
                        </div>
                        <br>
                        <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                        <br>
                    </div> 
               </div>
           </div>
        </div>
    </form>
</div>
@endsection
@section('js')
<script src="{{ URL::to( 'js/jquery.min.js') }}"></script>
<script>
$(document).ready(function(){
    if ($('select[id="deg_level"] option:selected').val() == "Postgraduate"){
        $("#level_yr").hide();
    }
    else{
        $("#level_yr").addClass("required field").show();
    }
    $(document).on('change','#deg_level',function(){
        if ($('select[id="deg_level"] option:selected').val() == "Postgraduate"){
            $("#level_yr").hide();
        }
        else{
            $("#level_yr").addClass("required field").show();
        }
    });
  });
    $(function(){           
        if (!Modernizr.inputtypes.date) {
            $('input[type=date]').datepicker({
                  dateFormat : 'yy-mm-dd'
                }
             );
        }
    });
</script>
@endsection