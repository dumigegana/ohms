<style type="text/css">
  @media screen and (max-width: 768px){
    .rwd-break { display: none; }
  }  
  /* remove outer padding */
  .row{
    padding: 0px;
    margin: 0px;
  }
  .confirm{
    background:#990000 !important; 
  }
  /*Remove rounded coners*/

  nav.sidebar.navbar {
    border-radius: 0px;
  }
  .navbar-toggle
{
    position:relative;
    float:right;
    padding:9px 10px;
    margin-top:8px;
    margin-right:15px;
    margin-bottom:8px;
    background-color:#6b0000 ;
    background-image:none;         
    border-radius:4px
}
.icon-bar
{
    display:block;
    width:22px;
    height:2px;
    border-radius:1px;
    border:1px solid #ff8b8b  ;
}

  nav.sidebar{
    -webkit-transition: margin 200ms ease-out;
      -moz-transition: margin 200ms ease-out;
      -o-transition: margin 200ms ease-out;
      transition: margin 200ms ease-out;
  }

  /* Add gap to nav and right windows.*/
  

  /* .....NavBar: Icon only with coloring/layout.....*/

  
  /* remove outer padding */
  .main .row{
    padding: 0px;
    margin: 0px;
  }
  .main{
    min-height:100%;
    background-color:#ff8b8b; 
  }
  .ohms_body{
    background-color: #ffeaea;
  }
  .btn{
    background-color: #980000;
    color: #ffeaea;
  }
  .btn:hover{
     background-color: #ff8b8b;
  }
  .btn:active{
     background-color: #ff7070;
  }

  /*Remove rounded coners*/

  nav.sidebar.navbar {
    border-radius: 0px;
  }

  nav.sidebar, .main{
    -webkit-transition: margin 200ms ease-out;
      -moz-transition: margin 200ms ease-out;
      -o-transition: margin 200ms ease-out;
      transition: margin 200ms ease-out;
  }

  /* Add gap to nav and right windows.*/
  .main{
    padding: 10px 10px 0 10px;
  }

  /* .....NavBar: Icon only with coloring/layout.....*/

  /*small/medium side display*/
  @media (min-width: 768px) {

    /*Allow main to be next to Nav*/
    .main{
      position: absolute;
      width: calc(100% - 40px); /*keeps 100% minus nav size*/
      margin-left: 40px;
      float: right;
    }

    /*lets nav bar to be showed on mouseover*/
    nav.sidebar:hover + .main{
      margin-left: 200px;
    }

    /*Center Brand*/
    nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
      margin-left: 0px;
    }
    /*Center Brand*/
    nav.sidebar .navbar-brand, nav.sidebar .navbar-header{
      text-align: center;
      width: 100%;
      margin-left: 0px;
    }

    /*Center Icons*/
    nav.sidebar a{
      padding-right: 13px;
    }

    /*adds border top to first nav box */
    nav.sidebar .nav-pills > li:first-child{
      border-top: 1px #ff2929 solid;
    }

    /*adds border to bottom nav boxes*/
    nav.sidebar .nav-pills > li{
      border-bottom: 1px #ff2929 solid;
      color: #777;
    }

    /* Colors/style dropdown box*/
   

    /*allows nav box to use 100% width*/
    nav.sidebar .navbar-collapse, nav.sidebar .container-fluid{
      padding: 0 0px 0 0px;
    }

    /*colors dropdown box text */
    .navbar-inverse .nav-pills .open .dropdown-menu>li>a {
      color: #777;
    }

    /*gives sidebar width/height*/
    nav.sidebar{
      width: 200px;
      height: 100%;
      margin-left: -160px;
      float: left;
      z-index: 8000;
      margin-bottom: 0px;
    }

    /*give sidebar 100% width;*/
    nav.sidebar li {
      width: 100%;
    }

    /* Move nav to full on mouse over*/
    nav.sidebar:hover{
      margin-left: 0px;
    }
    /*for hiden things when navbar hidden*/
    .forAnimate{
      opacity: 0;
    }
  }

  /* .....NavBar: Fully showing nav bar..... */

  @media (min-width: 1330px) {

    /*Allow main to be next to Nav*/
    .main{
      width: calc(100% - 200px); /*keeps 100% minus nav size*/
      margin-left: 200px;
    }

    /*Show all nav*/
    nav.sidebar{
      margin-left: 0px;
      float: left;
    }
    /*Show hidden items on nav*/
    nav.sidebar .forAnimate{
      opacity: 1;
    }
  }

  nav.sidebar .nav-pills .open .dropdown-menu>li>a:hover, nav.sidebar .nav-pills .open .dropdown-menu>li>a:focus {
    color: #990000;
    background-color: transparent;
  }

  nav:hover .forAnimate{
    opacity: 1;
  }
  section{
    padding-left: 15px;
  }  
  .nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus{
background-color:#980000;
color:#ffeaea;
}      
.nav-pills>li>a, .nav-pills>li>a:focus{
color:#ffeaea;
} 
.nav-pills>li>a:hover{
background-color: #ff8b8b;
}
.main, .sidebar {
  float: none;
  padding: 20px;
  vertical-align: top;
}     
</style>