@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.house_allocations_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.house_allocations_title'))
@section('icon', "warning sign") 
@section('subtitle', trans('ohms.house_allocations_subtitle'))
@section('content')
   <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $house_allocations->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.house_allocations_hr.access'))
             <center><a href="{{ route('OHMS::house_allocations_create', ['alloc' => $alloc]) }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Create New Allocation</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                <input type="hidden" name="alloc" id="alloc" value="{{$alloc}}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr>  
                  <th style="color: #980000;">Full Name</th> 
                  <th style="color: #980000;">Cell Number</th> 
                  <th style="color: #980000;">House number</th>
                  <th style="color: #980000;">Address</th>
                  <th style="color: #980000;">Status</th>
                  <th style="color: #980000;">Options</th>
                  
            </tr>
          </thead>
          <tbody id="read">
                @foreach($house_allocations as $house_allocation) 
          <tr style="color: #980000;">
            <td>
              {{ $house_allocation->house_allocable->user->fullname }}
            </td>
            <td>{{ $house_allocation->house_allocable->cell }}</td>
            <td>{{ $house_allocation->house->number }}</td>
            <td>{{ $house_allocation->house->street }}</td>
            <td class="center alligned">
                @if ($house_allocation->cleared == 1)
                <div class="ui red tiny basic label pop" data-title="Moved" data-variation="wide" data-content="No longer staying Here." data-position="top center"><span>
                  <i class="large red checkmark icon"></i></span>
                </div>
                @else
                <div class="ui green tiny basic label pop" data-title="Present" data-variation="wide" data-content="Still Staying here." data-position="top center">
                    <span><i class="large green close icon"></i></span>
                </div> 
                @endif
              </td>
            
                        
          </tr>
        @endforeach
          </tbody>
        </table>
        {!! $house_allocations->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  var alloc = $('#alloc').val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords, 'alloc' : alloc},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection