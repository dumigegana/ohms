@extends('student.master3')
@section('main-contant')
<div class="theme_light w3-bar w3-center" style=" min-height: 50px; max-width: 400px; margin-left: 400px"> 
    <div class="w3-row">
      <div class="w3-center"></div>
        <label class="w3-col m3 l2" style="width:100px"><b>Course</b></label>
          <div class="w3-rest">
            <!-- <form method="Post" action="{{-- route('gg') --}}"> -->
          <select name="course_id" id="course" class="w3-input w3-round-large">
            <option disabled selected>Select</option>
              @foreach($courses as $course)
                  <option value="{{ $course->id  }}">{{ $course->name }}</option>
              @endforeach
        </select>
        </div>
      </div> 
</div>   
<div class="w3-container" id="attend_list"></div>
@endsection
@section('js')
<script>
              $(document).on('change','#course',function(){ 
                  var course_id= $('select[name="course_id"] option:selected').val();
                  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                  $.ajax({
                    type: 'get',
                    url: "{{ url('getattendances') }}",
                    data: {"_token": CSRF_TOKEN, 'course_id': course_id},
                    async: true,
                    success: function(data){
                      $('#attend_list').html(data);
                    },
                    error:function(){
                      alert('Ooops something went wrong!');
                    },
                  });
                });
// $(document).ready(function(){ 
//   $('#sub').attr("disabled", true); 
//   $('#code').val(" "); 
//   }); 
//   function delay(fn, ms) {
//     let timer = 0
//     return function(...args) {
//       clearTimeout(timer)
//       timer = setTimeout(fn.bind(this, ...args), ms || 0)
//   }
// }
// $('#code').change(function() {  
//   $('#sub').attr("disabled", true);   
// });
// $('#code').keyup(delay(function (e) {
//     $('#sub').attr("disabled", true); 
//     var num = parseInt($('#code').val());
//     var max = parseInt($('#max').val());
//     var min = parseInt($('#min').val());
//     var subt = parseInt($('#subt').val()); 
//     var fact = parseInt($('#fact').val()); 
//     if ((min <= num) && (num <= max)){
//       var muliple = num - subt;
//       if(muliple%fact == 0){ 
//           //$('input[name="code"]').val($('#code').val());       
//           $('#sub').attr("disabled", false);          
//       }
//       else{
//         $('#sub').attr("disabled", true);
//         $('#code').val(" "); 
//       }
//    }
//    else{
//         $('#sub').attr("disabled", true);
//         $('#code').val(" ");  
//       }
// }, 6000));
</script>
@endsection
