<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SecurityController extends Controller
{
    public function confirm()
    {
    	return view('ohms/security/confirm');
    }

    

    public function remove()
    {
    	return view('ohms/security/remove');
    }
}
