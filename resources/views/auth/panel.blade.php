<!DOCTYPE html>   

<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>OHMS</title>
	<meta name="description" content="OHMS">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	{!! OHMS::includeAssets('ohms_header') !!}
	<link href="{{ URL::to('intlTelInput/build/css/intlTelInput.css') }}" rel="stylesheet">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >
	<style type="text/css">
	  #error-msg {
	    color: red;
	  }
	  #valid-msg {
	    color: #00C900;
	  }
	  #error-msg1 {
	    color: red;
	  }
	  #valid-msg1 {
	    color: #00C900;
	  }
	  input.error {
	    border: 1px solid #FF7C7C;
	  }
	</style>
</head>

<body>
	<div class="ui inverted dimmer"><div class="ui text loader">Loading</div></div>
	
@if(session('success'))
  <script>
    swal({
      title: "Success!",
      text: "{!! session('success') !!}.",
      type: "success",
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('error'))
  <script>
    swal({
      title: "Sorry!",
      text: "{!! session('error') !!}",
      type: "error",
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('warning'))
  <script>
    swal({
      title: "Watch out!",
      text: "{!! session('warning') !!}",
      type: "warning",
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('info'))
  <script>
    swal({
      title: "Watch out!",
      text: "{!! session('info') !!}",
      type: "info",
      confirmButtonText: "OK",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if (count($errors) > 0)
  <script>
    swal({
      title: "Sorry!",
      text: "<?php foreach($errors->all() as $error){ echo "$error<br>"; } ?>",
      type: "error",
      confirmButtonText: "OK",
      confirmButtonColor: "#980000",
      html: true
    });
  </script> 
  @endif 
			<div class="pusher back">
				<div class="menu-margin">
					<div class="" style="background-color:#990000; height:70px; "> 
						<div class="menu-pusher">
							<div class="ui one column doubling stackable grid container">
								<div class="column">
									<h2 class="ui header">
										<i class="@yield('icon') icon white-text"></i>
										<div class="content white-text">
											@yield('title')
											<div class="sub header">
												<span class="white-text">@yield('subtitle')</span>
											</div>
										</div>
									</h2>
								</div>
							</div>
						</div>
					</div>
					<div class="page-content" style="background-color: #ff8b8b;">
						<div class="menu-pusher">
							@yield('content')
						</div>
					</div>
					<br>
					
				</div>
				@yield('js')
				<script>

					setInterval(function(){
						var footer = $('.page-footer');
						footer.removeAttr("style");
						var footerPosition = footer.position();
						var docHeight = $( document ).height();
						var winHeight = $( window ).height();
						if(winHeight == docHeight) {
							if((footerPosition.top + footer.height() + 3) < docHeight) {
								var topMargin = (docHeight - footer.height()) - footerPosition.top;
								footer.css({'margin-top' : topMargin + 'px'});
							}
						}
					}, 10);
				</script>

			</body>
			</html>
