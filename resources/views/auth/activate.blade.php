@extends('layouts.main')
@section('content')
    <div class="title m-b-md">
        {{ trans('ohms.activation_account') }}
    </div>
    <form method="POST">
        {{ csrf_field() }}
        <input name='token' type="text" placeholder="{{ trans('ohms.activation_key') }}" required>
        <br><br><br>
        <button class="button button5">{{ trans('ohms.submit') }}</button>
    </form>
@endsection
