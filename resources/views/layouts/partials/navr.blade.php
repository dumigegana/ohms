 <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
           {{ csrf_field() }}
</form>
        
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background: #980000;">
  <div class="container col-md-10 col-md-offset-1" >
    <div class="navbar-header hidden-xs" >
      <a class="navbar-brand" rel="home" href="{{ url('/') }}">
        <img style="max-width:50px; margin-top: -13px;" src="{{ URL::to( 'img/logo.png') }}">
      </a>        
    </div>
                 {{ csrf_field() }}                
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
               @if (Route::has('login'))
                    @if(Auth::check())                        
                        @if(OHMS::loggedInUser()->isAdmin())
        <li>
          <a href="{{ route('OHMS::index_report') }}">Dashboard</a>
        </li>
                         @elseif(!OHMS::loggedInUser()->isAdmin())
        <li>                         
          <a href="{{ route('dash') }}"> OHMS </a>
        </li>         
                        @endif
        <li>
          <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
        </li>
                         @else                     
        <li>
          <a href="http://www.solusi.ac.zw">Solusi WebSite</a>
        </li>
        <li>
          <a href="{{ url('/login') }}">Login</a>
        </li>
                        @endif
                        @endif
      </ul>
    </div>
  </div>
      <!-- /.container -->
</nav>
