<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>OHMS</title>
    <link rel="shortcut icon" href="{{ OHMS::ohmsFavicon() }}" >
    <link href="{{ URL::to('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel='stylesheet' type='text/css' href="{{ URL::to( 'ohms_public/sweetalert/sweetalert.css') }}">
    <script type="text/javascript" src="{{ asset('ohms_public/sweetalert/sweetalert.min.js') }}"></script>
    <style type="text/css">
       .navbar-toggle
        {
            position:relative;
            float:right;
            padding:9px 10px;
            margin-top:8px;
            margin-right:15px;
            margin-bottom:8px;
            background-color:#4b0000 ;
            background-image:none;         
            border-radius:4px
        }
        .icon-bar
        {
            display:block;
            width:22px;
            height:2px;
            border-radius:1px;
            border:1px solid #ff8b8b  ;
        }
          .navbar-right>li.active>a, .navbar-right>li.active>a:hover, .navbar-right>li.active>a:focus{
          background-color:#980000;
          color:#ff8b8b;
        }

        .navbar-right>li>a, .navbar-right>li>a:focus{
          color:#ff8b8b;
        }

        .navbar-right>li>a:hover{
          background-color: #4b0000;
        }
                    .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }
            .bottom {
                align-items: center;
                position: absolute;
                bottom: 18px;
                justify-content: center;
            }
            .infor {
                text-align: center;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            input, select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            

            div.loginSection{
              background: #ff8b8b;
              width: 100%;
              box-shadow: 3px 3px 4px 4px #777;
              font-family: lato;
              position: relative;
              color: #333;
              border-radius: 10px;
            }
            div.header{

                background: #6b0000;
                padding: 40px 20px;
                color: #ff8b8b;
                font-size: 2.0em;
                font-weight: 600;
                border-radius: 10px 10px 0 0;
                
            }

            div.innerBox{
                width: 90%;
                padding: 40px 20px;
                 background: #ff8b8b;

            }

            .btn-raised {
              // shadow depth 1
                -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
              0 1px 2px 0 rgba(0, 0, 0, .24);
                -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
              0 1px 2px 0 rgba(0, 0, 0, .24); 
                box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
              0 1px 2px 0 rgba(0, 0, 0, .24); 
               
              &:active, &.active, &:active:focus, &.active:focus {
                // shadow depth 2
                -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
              0 3px 6px rgba(0, 0, 0, .23); 
                -moz-box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
              0 3px 6px rgba(0, 0, 0, .23); 
                box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
              0 3px 6px rgba(0, 0, 0, .23);
              }
              
              &:focus {
                // shadow depth 2
                -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
              0 3px 6px rgba(0, 0, 0, .23); 
                -moz-box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
              0 3px 6px rgba(0, 0, 0, .23); 
                box-shadow: 0 3px 6px rgba(0, 0, 0, .16),
              0 3px 6px rgba(0, 0, 0, .23);
              }
              
            }

            .btn {
              font-family: "Roboto", 'Helvetica Neue, Helvetica, Arial', sans-serif; 
              font-size: 14px;
              font-weight: 400;
              line-height: 1.1;
              text-transform: uppercase;
              letter-spacing: inherit;
              background-color: #6b0000;
              color: #ff8b8b;
            }
            .btn:hover {
                background-color: #ff8b8b;
                color: #ffeaea;
            }
            .btn:active {
                background-color: #ff8b8b;
                color: brown;
            }

            .btn-default, .btn-link {
              background-color: #6b0000;
                color: #FFF;
            }

            ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
            }

            li {
                float: left;
                align-items: center;
            }

            li a {
                display: block;
                color: #980000;
                text-align: center;
                padding-left: 68px;
                text-decoration: none;
            }

            li a:hover {
                background-color: #ff8b8b;
                color: brown;
            }

        </style>
        
        @yield('css')
        
  
    </head>
    <body style="background-color:#ff8b8b;">
    <div style="padding:20px;"></div>
       @include('layouts.partials.nav')
     <div><br></div>

@if(session('success'))
  <script>
    swal({
      title: "Success!",
      text: "{!! session('success') !!}.",
      type: "success",
      timer: 4000,
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('error'))
  <script>
    swal({
      title: "Sorry!",
      text: "{!! session('error') !!}",
      type: "error",
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('warning'))
  <script>
    swal({
      title: "Watch out!",
      text: "{!! session('warning') !!}",
      type: "warning",
      timer: 4000,
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('info'))
  <script>
    swal({
      title: "Watch out!",
      text: "{!! session('info') !!}",
      type: "info",
      timer: 4000,
      confirmButtonText: "OK",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if (count($errors) > 0)
  <script>
    swal({
      title: "Sorry!",
      text: "<?php foreach($errors->all() as $error){ echo "$error<br>"; } ?>",
      type: "error",
      confirmButtonText: "OK",
      confirmButtonColor: "#980000",
      html: true
    });
  </script> 
  @endif 
       @yield('page-content') 
             
    <div class="navbar" style="background: #6b0000 ; color: #ffeaea; margin-top: -10px;height: 80px;clear:both;padding-top:20px;">
    <div class="container">
      <p><center>&copy; 2017 Copyright OHMS 1.0- Developed By 
           <a href="https://ohmssaniblog.wordpress.com/" target="_blank" >ohmssani Ndlovu</a>
      </center></p>
    </div>
</div>
    
  <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/bootstrap-3.3.7.min.js') }}"></script>

    @yield('js')

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', '#reset', function(event){
          var url = $("#conf").attr("name");
          $("#conf").attr("action", url);
          console.log(url);
        });    
});
</script>

    </body>
</html>
