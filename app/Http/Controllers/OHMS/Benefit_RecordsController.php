<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Benefit_Record;
use App\Benefit;
use App\Dependant;
use App\Staff;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class Benefit_RecordsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.benefit_records.access');

        # Get all the benefit_records
        $benefit_records = Benefit_Record::with('block')->paginate(50);

        # Return the view
        return view('ohms/benefit_records/index', ['benefit_records' => $benefit_records]);
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.benefit_records.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $benefit_records = Benefit_Record::with('block')->paginate(100);
        }
        else {       
          $benefit_records = Benefit_Record::with('block', 'block.hostel')
                             ->where('benefit_Record_number','like', $post . '%')                   
                             ->orwherehas('block', function($o) use ($post) {$o->where('name', 'like',  $post . '%');})
                             ->orwherehas('block.hostel', function($r) use ($post) {$r->where('gender', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/benefit_records/search', ['benefit_records' => $benefit_records]);
        return $read;
    }
    public function show($id)
    {
        OHMS::permissionToAccess('ohms.benefit_records.access');

        # Get the benefit_Record
        $benefit_Record = Benefit_Record::find($id);

        # Return the view
        return view('admin/benefit_records/show', ['benefit_Record' => $benefit_Record]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.benefit_records.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.benefit_records.admin');

        # Find the benefit_Record
        $row = Benefit_Record::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'benefit_records';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/benefit_records/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.benefit_records.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.benefit_records.admin');

        # Find the row
        $row = Benefit_Record::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'benefit_records';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::benefit_records')->with('success', trans('ohms.msg_benefit_Record_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.benefit_records.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.benefit_records.admin');

        # Get all the data
        $data_index = 'benefit_records';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/benefit_records/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.benefit_records.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.benefit_records.admin');

        # create new role
        $row = new Benefit_Record;

        # Save the data
        $data_index = 'benefit_records';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::benefit_records')->with('success', trans('ohms.msg_benefit_Record_created'));

    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.benefit_records.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.benefit_records.admin');

        # Select Item
        $benefit_Record = OHMS::benefit_Record('id', $id);

        if(!$benefit_Record->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $benefit_Record->delete();

        # Redirect the admin
        return redirect()->route('OHMS::benefit_records')->with('success', trans('ohms.msg_hostel_deleted'));
    }
     
}
