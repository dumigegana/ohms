@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">Desciplinary Actions</div>
    </div>
@endsection
@section('title', trans('ohms.desciplinaries_title')) 
@section('icon', "bed")
@section('subtitle', trans('ohms.desciplinaries_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $desciplinaries->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.desciplinaries.admin'))
             <center><a href="{{ route('OHMS::desciplinary_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add Desciplinary Action</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr>
              <th style="color: #980000;">Desciplinary Number</th>
              <th style="color: #980000;">Gender</th>
              <th style="color: #980000;">Block</th>
              <th style="color: #980000;">Capacity</th>
              <th style="color: #980000;">Active</th>
              <th style="color: #980000;">Options</th>
            </tr> 
          </thead>
          <tbody id="read">
            @foreach($desciplinaries as $desciplinary) 
            <tr style="color: #980000;">
              <td>{{ $desciplinary->desciplinary_number }}</td>
              <td>{{ $desciplinary->block->hostel->gender }}</td>
              <td>{{ $desciplinary->block->name }}</td>
              <td>{{ $desciplinary->capacity }}</td>
              <td class="center alligned">
                      @if ($desciplinary->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Desciplinary ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this desciplinary " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::desciplinaries_edit', ['id' => $desciplinary->id]) }}" class="ui btn button">{{ trans('ohms.desciplinaries_edit') }}</a>
              </td>       
            </tr>
        @endforeach
          </tbody>
        </table>
        {!! $desciplinaries->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_desciplinary') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection