<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('maintenable_id');
            $table->string('maintenable_type');
            $table->integer('item_id');
            $table->string('comments');
            $table->boolean('fixed')->default(false);
            $table->timestamps();

          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenances');
    }
}
