@extends('layouts.admin.w3css')
@section('breadcrumb')
<div class="w3-bar-item">{{ trans('Class session List') }}</div>
@endsection
@section('title', trans('Session List'))
@section('icon', "sessions")
@section('subtitle', trans('Session List'))
@section('content')
  
  <div class="w3-container w3-center">
        <table class="w3-table-all">
          <thead>
            <tr class="theme">
                  <th>Name</th>
                  <th>Course</th>
                  <th>Class time</th>
                  <th>Subtract</th>                  
                  <th>Factor</th>
                  <th>Min</th>
                  <th>Max</th>
                  <th>Attendants</th>
            </tr>
          </thead>
          <tbody>
                @foreach($sessions as $session)
          <tr> 
            <td> {{ $session->name }}</td>
              <td>{{ $session->course->name}}</td>
            <td>{{ $session->date_time }}</td>
            <td>{{ $session->subtract}}</td>
            <td>{{ $session->factor}}</td>
            <td>{{ $session->minimum}}</td>
            <td>{{ $session->maximum}}</td>
            <td>{{ $session->attendances->count() }}</td>
            <td >@if(OHMS::loggedInUser()->su)
                <div class="w3-dropdown-hover w3-mobile">
                 <button class="w3-button theme" onclick="w3_close()" name="drop_btn">
                    <i class="fa fa-tools"></i> Option</button>
                  <div class="w3-dropdown-content theme_light w3-bar-block w3-card-4 side_drop">
                    <a href="{{ route('OHMS::sessions_edit', ['id' => $session->id]) }}" class="w3-bar-item w3-button">
                      <i class="fa fa-edit"></i>Edit </a>
                    <a href="{{ route('OHMS::sessions_delete', ['id' => $session->id]) }}" class="w3-bar-item w3-button">
                      <i class="fa fa-trash"></i>Delete</a>
                    <a href="{{ route('OHMS::sess_attend', ['id' => $session->id]) }}" class="w3-bar-item w3-button">
                      <i class="fa fa-bath"></i>Attendance</a>
                    @endif
             </td>
                     
                        <div class="w3-button w3-theme">
                            <i class="lock icon"></i>
                        </div>
            </td>
          </tr>
        @endforeach
          </tbody>
        </table>
      </div>
        <br>
    </div>
  </div>
@endsection
