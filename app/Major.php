<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
	    public function students()
    {
        return $this->hasMany('App\Student');
    }
     public function blocks()
    {
        return $this->hasMany('App\Block');
    }

    public function allocations()
    {
        return $this->hasManyThrough('App\Allocation', 'App\Student', 'major_id', 'student_id', 'id');
    }
}
 