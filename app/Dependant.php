<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependant extends Model
{
    public function staff()
    {
        return $this->belongsTo('App\Staff');
    }

     public function benefit_records()
    {
        return $this->hasMany('App\Benefit_record');
    }


}
