<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Hostel;
use Illuminate\Support\Facades\DB;

class HostelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         Model::unguard();
        DB::table('hostels');
        $hostels = array(
                ['name' => 'Sweeden House', 'gender' => 'Female' ],
                ['name' => 'Kamuzu', 'gender' => 'Female'],
                ['name' => 'Palmer Hall', 'gender' => 'Female'],
                ['name' => 'Raelly Hall', 'gender' => 'Male'],
                ['name' => 'Mayenza', 'gender' => 'Male'],
                ['name' => 'Tshabangu', 'gender' => 'Male'],
                ['name' => 'Kamwendo', 'gender' => 'Male'],
                ['name' => 'Mlevu', 'gender' => 'Male'],
                ['name' => 'Senior Dorm', 'gender' => 'Male'],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($hostels as $hostel)
        {
            Hostel::create($hostel);
        }
        Model::reguard();
    }

}