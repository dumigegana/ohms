<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefit__records', function (Blueprint $table) {
            $table->bigIncrements('id');                       
            $table->bigInteger('staffs_id');
            $table->bigInteger('dependances_id')->nullable();
            $table->string('benefit_id');
            $table->date('date_from');
            $table->date('date_to');
            $table->string('qualification');
            $table->timestamps();
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefit__records');
    }
}
