@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::staffs') }}">{{ trans('ohms.staff_list') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.staffs_edit_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.staffs_edit_title'))
@section('icon', "edit")
@section('subtitle', trans('ohms.staffs_edit_subtitle', ['suid' => $row->solusi_id]))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST">
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea;">
                        {{ csrf_field() }}
                        <input name="id" type="hidden" value="$row->id">
                        
                        <div class="field">
                        <label style="color: #980000;">Username</label>
                       <input name="solusi_id" type="text" value="{{-- Request::old('solusi_id') ? Request::old('solusi_id') : isset($row) ? $row->solusi_id : "" --}} {{$row->solusi_id }}">
                        </div>
                         
                        <div class="field">
                            <label style="color: #980000;">Personal Email</label>
                            <input name="email" value="{{ $row->email }}" type="text" placeholder="">
                        </div>
                           
                        <div class="field">
                        <label style="color: #980000;">Title</label>
                  <select name="title" class="form-control" required>
                    <option value="Mr" {{ (old("title") == 'Mr' ? "selected":"") }}> Mr</option>
                    <option value="Mrs" {{ (old("title") == 'Mrs' ? "selected":"") }}>Mrs</option>
                    
                  </select>
                        </div>
                        <br/>                         
                        <div class="field" id="swh"> 
                            <label style="color: #980000;">Department</label>
                            <select name="department_id" class="form-control">
                            <option value="{{ $row->department_id }}"> {{ $row->department->name }}</option>
                            @foreach($departments as $department)
                                <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <br/>                        
                                
                                <div class="field">
                                <label style="color: #980000;">Employment Status</label>
                                <select name="status" class="form-control">
                                   <option value="Serving" {{ ($row->status == 'Serving' ? "selected":"") }}>Currently Serving</option>
                                   <option value="Suspended" {{ ($row->status == 'Suspended' ? "selected":"") }}>Suspended</option>
                                   <option value="Retired" {{ ($row->status == 'Retired' ? "selected":"") }}>Retired</option>
                                   <option value="on Leave" {{ ($row->status == 'On Leave' ? "selected":"") }}>On Leave</option>
                                   <option value="Expelled" {{ ($row->status == 'Expelled' ? "selected":"") }}>Expelled</option>
                                 </select>
                                 <br>
                                </div>

                        <div class="field">
                        <label style="color: #980000;">Date of Birth</label>
                        <input name='date_of_birth' value="{{ $row->date_of_birth }}" type="date" placeholder="">
                        </div>
                            </div> 
                        </div>       
                <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea">
                        {{ csrf_field() }}

                        <div class="field">
                        <label style="color: #980000;">National ID number</label>
                        <input name='national_id' value="{{ $row->national_id }}" type="text" placeholder="">
                        </div> 
                        <br>
                         <div class="ui equal width form">
                          <div class="fields">
                            <div class="required field">
                            <label style="color: #980000;">Gender</label>
                            <select name="gender" class="form-control" required>
                              <option value="Male" {{ ($row->gender == 'Male' ? "selected":"") }}> Male</option>
                              <option value="Female" {{ ($row->gender == 'Female' ? "selected":"") }}>Female</option>
                            </select>
                          </div>
                          <br>
                            <div class="required field" >
                              <div style="padding-left: 50px;">
                              <label style="color: #980000;">Marital Status <br></label>
                              <input id="hide" type="radio" name="marital" value="Single" {{ ($row->marital == 'Single' ? "checked":"") }}> Single<br>
                              <input  id="show" type="radio" name="marital" value="Maried" {{ ($row->marital == 'Maried' ? "checked":"") }}> Maried<br>
                            </div></div>
                            
                             </div>
                           </div>                        
                           
                             <br>                                        
                        <div class="field">
                            <label style="color: #980000;">Home Address</label>
                            <input name="home_address" value="{{ $row->home_address }}" type="text" placeholder="">
                        </div>
                        <br>
                        <div class="ui equal width form">
                            <div class="fields">
                                <div class="field">
                            <label style="color: #980000;">Cell number</label>
                            <input name='cell_number' type="text" value="{{ $row->cell_number }}">
                            </div>                       
                        
                        <div class="field">
                            <label style="color: #980000;">Next of Kin contact number</label>
                            <input name="next_of_kin_number" value="{{ $row->next_of_kin_number }}" type="text" placeholder="">
                        </div>
                         
                       </div>
                       <br>
                        <div class="field">
                            <label style="color: #980000;">Next of Kin Name</label>
                            <input name="next_kin" value="{{ $row->next_kin }}" type="text" placeholder="">
                        </div>
                        
                        
                        <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                        
                    </div> 
               </div>
           </div>
        </div>
    </form>
</div>
@endsection
@section('js')
<script src="{{ URL::to( 'js/jquery.min.js') }}"></script>
<script>
$(document).ready(function(){
    if ($('select[id="deg_level"] option:selected').val() == "Postgraduate"){
        $("#level_yr").hide();
    }
    else{
        $("#level_yr").addClass("required field").show();
    }
    $(document).on('change','#deg_level',function(){
        if ($('select[id="deg_level"] option:selected').val() == "Postgraduate"){
            $("#level_yr").hide();
        }
        else{
            $("#level_yr").addClass("required field").show();
        }
    });
  });
    $(function(){           
        if (!Modernizr.inputtypes.date) {
            $('input[type=date]').datepicker({
                  dateFormat : 'yy-mm-dd'
                }
             );
        }
    });
</script>
@endsection