
@if(!$fields)
    <div class="w3-container">
        <div class="w3-container theme">  
            {{ trans('dumi.form_no_fields_title') }}
        </div>
        <p>{{ trans('dumi.form_no_fields_subtitle') }}</p>
    </div>
@endif


@foreach($fields as $field)
   <?php

       $error = false;
       $warning = false;

       $code_script = false;
       $editor_script = false;
       $relation = false;

       # Setup the value
       $empty_value = false;
       if(isset($empty)) {
           foreach($empty as $emp) {
               if($field == $emp) {
                   $empty_value = true;
               }
           }
       }
       if($empty_value or !isset($row)) {
           $value = "";
       } else {
           $value = $row->$field;

           # Check if the value needs to be decrypted
           $decrypt = false;
           foreach($encrypted as $encrypt) {
               if($field == $encrypt) {
                   if($value != ''){
                       $decrypt = true;
                   }
               }
           }
           if($decrypt) {
               try {
                   $value = Crypt::decrypt($value);
               } catch (Exception $e) {
                   $error = trans('dumi.decrypt_fail');
               }
           }

           # Check if it's a hashed value, to display it empty
           $hashed_value = false;
           foreach($hashed as $hash) {
               if($field == $hash) {
                   $hashed_value = true;
               }
           }
           if($hashed_value) {
               $value = "";
           }
       }

       $show_field = str_replace('_', ' ', ucfirst($field));
       $show_field_id = str_replace('id', ' ', ucfirst($show_field));


       $type = Schema::getColumnType($table, $field);


       # Set the input type
       if($type == 'string') {
           $input_type = "text";
       } elseif($type == 'integer') {
           $input_type = "number";
       } else {
           $input_type = "text";
       }

       # Check if it needs to be masked
       foreach($masked as $mask) {
           if($mask == $field) {
               $input_type = "password";
           }
       }

       # Check if it's a code
       foreach($code as $cd) {
           if($cd == $field) {
               $code_script = true;
           }
       }

       # Check for the editor values
       foreach($wysiwyg as $ed) {
           if($ed == $field) {
               $editor_script = true;
           }
       }

       # Check if it's a relation
       if(array_key_exists($field, $relations)) {
           $relation = true;
       }

   ?>

   @if($editor_script or $code_script)      
          <label>{{ $show_field }}</label>
          @if($editor_script)
              <textarea  name="{{ $field }}" class="ckeditor w3-input">{{ $value }}</textarea>
          @elseif($code_script)
              <pre class="code" id="{{ $field }}-code">{{ $value }}</pre>
              <textarea hidden name="{{ $field }}" id="{{ $field }}">{{ $value }}</textarea><br>
              <script>
                  var editor = ace.edit("{{ $field }}-code");
                  editor.getSession().on('change', function(){
                      $("#{{ $field }}").val(editor.getSession().getValue());
                  });
              </script>

          @endif    

        @elseif($relation)
        <p>
            <label class="w3-col" style="width:100px">{{ $show_field_id }}</label>
            <div class="w3-rest">
            <select name="{{ $field }}" id="{{ $field }}" class="w3-input w3-round-large">
              <option disabled >Select</option>
                @foreach($relations[$field]['data'] as $relation_data)
                    <?php $relation_value = $relations[$field]['value']; $relation_show = $relations[$field]['show']; ?>
                    <option <?php if($value == $relation_data->$relation_value){ echo "selected"; } ?> value="{{ $relation_data->$relation_value }}">{{ $relation_data->$relation_show }}</option>
                @endforeach
            </select>
        </div>
        @if($error)
        <br>
            <div class="w3-red">
                {{ $error }}
            </div>
        @endif
        </p> <br>     

   @else

       @if($type == 'string')

             
                   <!-- STRING COLUMN -->
                   <p>
                        <label class="w3-col" style="width:100px">{{ $show_field }}</label>
                        <div class="w3-rest">
                          <input type="{{ $input_type }}"  id="{{ $field }}" name="{{ $field }}" placeholder="{{ $show_field }}" value="{{ $value }}" class="w3-input w3-round-xlarge">
                        </div>
                        @if($error)
                        <br>
                            <div class="w3-red">
                                {{ $error }}
                            </div>
                        @endif
                   </p> <br>

              
           @elseif($type == 'integer')

               <!-- INTEGER COLUMN -->
               <p>
                  <label class="w3-col" style="width:100px">{{ $show_field }}</label>
                    <div class="w3-rest">                      
                      <input type="{{ $input_type }}"  id="{{ $field }}" name="{{ $field }}" placeholder="{{ $show_field }}" value="{{ $value }}" class="w3-input w3-round-large">
                    </div>
                    @if($error)
                        <div class="w3-red">
                            {{ $error }}
                        </div>
                    @endif
             </p><br>

           @elseif($type == 'datetime')

               <!-- INTEGER COLUMN -->
               <p>
                  <label class="w3-col" style="width:100px">{{ $show_field }}</label>
                    <div class="w3-rest">                      
                      <input type="datetime-local" id="{{ $field }}" name="{{ $field }}" placeholder="{{ $show_field }}" value="{{ $value }}" class="w3-input w3-round-large">
                    </div>
                    @if($error)
                        <div class="w3-red">
                            {{ $error }}
                        </div>
                    @endif
             </p><br>

           @elseif($type == 'boolean')



                <!-- BOOLEAN COLUMN -->
                <br>
                <p style="margin-left: 50px">
                  <label class="switch @if($error) error @endif">
                    <input <?php if($value){ echo "checked='checked'"; } ?> type="checkbox" class="w3-check" tabindex="0" class="hidden" name="{{ $field }}">
                    <span class="slider round"></span>
                    <label class="label">{{ $show_field }} 
                    </label>
                  </label>
                                  @if($error)
                    <br>
                        <div class="w3-red">
                            {{ $error }}
                        </div>
                    @endif
                  </p><br>

           @elseif($type == 'text')

                <!-- TEXT COLUMN -->
                <p>
                <div class="field @if($error) error @endif">
                    <label>{{ $show_field }}</label>
                    <textarea placeholder="{{ $show_field }}" name="{{ $field }}" rows="3" id="{{ $field }}">{{ $value }}</textarea>
                    @if($error)
                        <div class="w3-red">
                            {{ $error }}
                        </div>
                    @endif
                </div>
                <p>

           @else

               <!-- ALL OTHER COLUMN -->
               <p>
               <div class="field @if($error) error @endif">
                    <label>{{ $show_field }}</label>
                    <input type="{{ $input_type }}"  id="{{ $field }}" name="{{ $field }}" placeholder="{{ $show_field }}" value="{{ $value }}">
                    @if($error)
                        <div class="ui pointing red basic label">
                            {{ $error }}
                        </div>
                    @endif
               </div>
             </p><br>

           @endif
       @foreach($confirmed as $confirm)
           @if($field == $confirm)
               @if($type == 'string')

                   <!-- STRING CONFIRMATION -->
                   <p>
                  <label class="w3-col" style="width:100px">{{ $show_field }}</label>
                    <div class="w3-rest">                      
                      <input type="{{ $input_type }}"  id="{{ $field }}" name="{{ $field }}" placeholder="{{ $show_field }}" value="{{ $value }}" class="w3-input w3-round-large">
                    </div>
                    @if($error)
                        <div class="w3-red">
                            {{ $error }}
                        </div>
                    @endif
             </p><br>

               @elseif($type == 'integer')

                   <!-- INTEGER COLUMN CONFIRMATION -->
                   <p>
                   <div class="field @if($error) error @endif">
                        <label>{{ $show_field }} {{ trans('dumi.confirmation') }}</label>
                        <input type="{{ $input_type }}"  id="{{ $field }}_confirmation" name="{{ $field }}_confirmation" placeholder="{{ $show_field }} confirmation" value="{{ $value }}">
                        @if($error)
                            <div class="ui pointing red basic label">
                                {{ $error }}
                            </div>
                        @endif
                   </div>
                 </p><br>

               @elseif($type == 'boolean')

                   <!-- BOOLEAN CONFIRMATION -->
                        <p>
                           <input <?php if($value){ echo "checked='checked'"; } ?> type="checkbox" tabindex="0" class="hidden" name="{{ $field }}_confirmation" class="w3-check">
                           <label class="w3-green">{{ $show_field }} {{ trans('dumi.confirmation') }}</label>
                       </div>
                       @if($error)
                           <div class="ui left pointing red basic label">
                               {{ $error }}
                           </div>
                       @endif
                   </p><br>

               @elseif($type == 'text') 

                   <!-- TEXT COLUMN -->
                   <p>
                   <div class="field @if($error) error @endif">
                       <label>{{ $show_field }} {{ trans('dumi.confirmation') }}</label>
                       <textarea placeholder="{{ $show_field }}" name="{{ $field }}_confirmation" rows="3" id="{{ $field }}_confirmation">{{ $value }}</textarea>
                       @if($error)
                           <div class="ui pointing red basic label">
                               {{ $error }}
                           </div>
                       @endif
                   </div>
                 </p><br>

               @else

               <!-- ALL OTHER COLUMN CONFIRMATION -->
               <p>
               <div class="field @if($error) error @endif">
                    <label>{{ $show_field }} {{ trans('dumi.confirmation') }}</label>
                    <input type="{{ $input_type }}"  id="{{ $field }}_confirmation" name="{{ $field }}_confirmation" placeholder="{{ $show_field }} confirmation" value="{{ $value }}">
                    @if($error)
                        <div class="ui pointing red basic label">
                            {{ $error }}
                        </div>
                    @endif
               </div>
             </p><br>

               @endif

           @endif
       @endforeach

   @endif

@endforeach
@section('js')
    @if(isset($cc_id) and isset($cc_value))
        <script>
            $("#{{ $cc_id }}_dropdown").dropdown("set selected", "{{ $cc_value }}");
            $("#{{ $cc_id }}_dropdown").dropdown("refresh");
        </script>
    @endif
@endsection
