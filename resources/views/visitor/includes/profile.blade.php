
<div class="row ohms_body">
   <article class="get" data-articleid="">
		<div class="col-md-6 ">		
			<dl class="row">
				<dt class="col-sm-4">Username: </dt>
				<dd class="col-sm-8">{{ Auth::user()->username }}</dd>
				<dt class="col-sm-4">Full name: </dt>
				<dd class="col-sm-8">{{ Auth::user()->full_name }}</dd>
				<dt class="col-sm-4">Email:</dt>
				<dd class="col-sm-8">{{ Auth::user()->email }}</dd>
				<dt class="col-sm-4">Gender:</dt>
				<dd class="col-sm-8">{{ Auth::user()->student->gender }}</dd>
				<dt class="col-sm-4">Date of Birth:</dt>
				<dd class="col-sm-8">{{ Auth::user()->student->date_of_birth }}</dd>
			</dl>
		</div>
		<div class="col-md-6 ">
			 <dl class="row">
				<dt class="col-sm-5">National ID:</dt>
				<dd class="col-sm-6">{{ Auth::user()->student->national_id }}</dd> 
				<dt class="col-sm-5">Major:</dt>
				<dd class="col-sm-6">{{ Auth::user()->student->major->name }}</dd>
				<dt class="col-sm-5">Degree level:</dt>
				<dd class="col-sm-6">{{ Auth::user()->student->degree_level }}</dd>
				<dt class="col-sm-5">Cell Number:</dt>
				<dd class="col-sm-6">{{ Auth::user()->student->cell_number }}</dd>
				<dt class="col-sm-5">Home Address:</dt>
				<dd class="col-sm-6">{{ Auth::user()->student->home_address }}</dd>
			</dl>
		</div>
	</article>
</div>    
 