@extends('layouts.admin.w3css')
@section('breadcrumb')
<div class="w3-bar-item">{{ trans('Mark Attendance') }}</div>
@endsection
@section('title', trans('Mark Attendance'))
@section('icon', "plus")
@section('subtitle', trans('Mark Attendance'))
@section('content')
<div class="w3-container">
    <div class="theme_light" style="padding: 50px 50px;">
        <form method="POST" >
            {{ csrf_field() }}
            <div class="w3-container pad">
                <div class="w3-card w3-round-large padding">     
                    @include('ohms/forms/master3')                    
            
            <br>
            <center>
                <button type="submit" class="theme w3-button w3-round-large">{{ trans('ohms.submit') }}</button>
            </center>
        </div>
        </div>
        </form>
        </div>
    </div>
@endsection
@section('js')

@endsection