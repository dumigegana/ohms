@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::roles') }}">{{ trans('ohms.roles_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.roles_edit_permissions_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.roles_edit_permissions_title'))
@section('icon', "lightning")
@section('subtitle', trans('ohms.roles_edit_permissions_subtitle', ['name' => $role->name]))
@section('content')
<div class="ui one column doubling stackable grid container">
    <div class="column">
        <div class="ui very padded segment" style="background: #ffeaea">
            <form method="POST" class="ui form">
                {{ csrf_field() }}
                @include('ohms.forms.permissions')
                <br>
                <div class="field">
                    <button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
