<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facility_status;
use App\Permission;
use Schema;
use Auth;
use OHMS;

class Facility_statusesController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Get all the facility_statuses
        $facility_statuses = Facility_status::all();

        
        # Return the view
        return view('ohms/facility_statuses/index', ['facility_statuses' => $facility_statuses]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Get the facility_status
        $facility_status = Facility_status::find($id);

        # Return the view
        return view('admin/facility_statuses/show', ['facility_status' => $facility_status]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # Find the facility_status
        $row = Facility_status::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'facility_statuses';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/facility_statuses/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # Find the row
        $row = Facility_status::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'facility_statuses';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::facility_statuses')->with('success', trans('ohms.msg_facility_status_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # Get all the data
        $data_index = 'facility_statuses';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/facility_statuses/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.facilities.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # create new facility_status
        $row = new Facility_status;

        # Save the data
        $data_index = 'facility_statuses';
        require('Data/Create/Save.php');

        # Return the admin to the facility_statuses page with a success message
        return redirect()->route('OHMS::facility_statuses')->with('success', trans('ohms.msg_facility_status_created'));
    }
   

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.facilities.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.facilities.admin');

        # Select Facility_status
        $facility_status = OHMS::facility_status('id', $id);
        # Select Item
        $benefit = OHMS::benefit('id', $id);


        if(!$facility_status->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }
        
        # Delete Facility_status
        $facility_status->delete();

        # Redirect the admin
        return redirect()->route('OHMS::facility_statuses')->with('success', trans('ohms.msg_facility_status_deleted'));
    }
}
