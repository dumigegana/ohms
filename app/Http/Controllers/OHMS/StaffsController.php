<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;
use App\Role_User;
use App\Department;
use App\User;
use App\Staff;
use OHMS;
use Auth;
use Gate; 
use DB;

class staffsController extends Controller
{
 /*
    |--------------------------------------------------------------------------
    | Staffs Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new staffs as well as their
    | validation and creation. 
    |
    */

     public function index()
    { 
        
       OHMS::permissionToAccess('ohms.staffs.access');

        # Get all staffs
        $staffs = Staff::with('user')->paginate(100);

        # Return the view
        return view('ohms/staffs/index', [
            'staffs' => $staffs,
        ]); 
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.staffs.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $staffs = Staff::with('user')->paginate(100);
        }
        else {       
          $staffs = Staff::with('user', 'department')
                             ->where('solusi_id','like', $post . '%')
                             ->orwherehas('department', function($q) use ($post) {$q->where('name',  'like', '%' . $post . '%');})                                         
                             ->orwherehas('user', function($o) use ($post) {$o->where('full_name', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/staffs/search', ['staffs' => $staffs]);
        return $read;
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.staffs.access');

        # Find the staff
        $staff = OHMS::staffs('id', $id);

        # Return the view
        return view('ohms/staffs/show', ['staff' => $staff]);
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.staffs.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.staffs.admin');

        # Get all roles
        $roles = OHMS::roles();

        $departments = Department::orderBy('name', 'asc')->get();
 
        # Get all the data
        $data_index = 'staffs';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/staffs/create', [
            'roles'     =>  $roles,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
            'departments' => $departments,
        ]);
    }

    public function store(Request $data)
    {
        $this->validate($data, [
            'full_name' => 'required|max:255',
            'username' => 'required|max:255|unique:users',
            'email' => 'email|max:255',
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-zA-Z])(?=.*[A-Z])(?=.*\d).+$/',
            'title' => 'required|max:5',
            'gender'    => 'required',
            'cell_number'  => 'required|unique:staffs',
            'cell_phone_number'  => 'accepted',
            'national_id'    => 'required',
            'home_address' => 'required',
            'next_kin'  => 'required',
            'next_of_kin_number'  => 'required',
            'role_id'=>'required',
            'department_id'=>'required',
            'date_of_birth'    => 'required',
            'marital'    => 'required'

        ]);
        # Create the user
        $user_data = [
            'full_name' => $data['full_name'],
            'username' => $data['username'],
            'email' => $data['username']. "@solusi.ac.zw",
            'password' => bcrypt($data['password']), 
             'active' => $data['active'],
             'banned' => 0,
        ];
        $user = User::create($user_data);

        # Add Relationshop
        $user->roles()->attach($data['role_id']); 

        # Add Relationshop
        $rel = new Staff;
        $rel->user_id = $user->id;
        $rel->solusi_id = $data['username'];
        $rel->title = $data['title'];
        $rel->gender = $data['gender'];
        $rel->email = $data['email'];
        $rel->status = $data['status'];
        $rel->marital = $data['marital'];
        $rel->department_id = $data['department_id'];
        $rel->cell_number = $data['cell_number'];
        $rel->home_address = $data['home_address'];
        $rel->next_kin = $data['next_kin'];
        $rel->next_of_kin_number = $data['next_of_kin_number'];
        $rel->date_of_birth = $data['date_of_birth'];
        $rel->national_id = $data['national_id'];
       
        $rel->save();

        return redirect()->route('OHMS::staffs')->with('success', trans("ohms.msg_staff_created"));
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.staffs.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.staffs.admin');

        # Find the staff
        $row = OHMS::staff('id', $id);
        $departments = Department::orderBy('name', 'asc')->get();
        
               # Return the view
         return view('ohms/staffs/edit', ['row' => $row, 'departments' => $departments]);
    }
     public function update($id, Request $request)
    {
         

        $row = Staff::find($id);

        $data_index = 'staffs';
        require('Data/Edit/Save.php');

        return redirect()->route('OHMS::staffs')->with('success', trans('ohms.msg_staff_edited'));
    }
  
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.staffs.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.staffs.admin');

        # Find The staff
        $staff = OHMS::staffs('id', $id)->first();

        # Delete Relationships
            $rels = User::where('id', $staff->user_id)->get();
            foreach($rels as $rel) {
                $rel->delete();
            

            # Delete staff
            $staff->delete();

            # Return the admin with a success message
            return redirect()->route('OHMS::staffs')->with('success', trans('ohms.msg_staff_deleted'));
        }
    }

    
}