<?php

use App\Mail\ConfirmId;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  
     
Route::group(['middleware' => 'ohms.base'], function () {
	/*
	|--------------------------------------------------------------------------
	| Add your website routes here
	|--------------------------------------------------------------------------
	|
	| The ohms.base middleware will be applied
	|*/ 
	

    # Auth Route
     
    # Default home route
    Route::get('/', function () {
        return view('home'); 
    })->name('home'); 

    Route::get('/home', function () {
        return view('home'); 
    })->name('home');  

        
    #Stundent SignUp
    // Route::get('/create_tst', 'Auth\RegisterController@create_tst');
    Route::get('/create', 'Auth\RegisterController@getcreate')->name('su_id');
    Route::post('/creat',  'Auth\RegisterController@confirm')->name('creat');
    Route::get('/registr/{conf_code}','Auth\RegisterController@create');
    Route::post('/storestud', 'Auth\RegisterController@store')->name('storestud');

    #Password Recovery
    // Route::get('/recover', 'Auth\RegisterController@getrecover');
    Route::post('/recovr', 'Auth\RegisterController@confirmp')->name('recovr');
    Route::get('/recov/{conf_code}', 'Auth\RegisterController@edit');
    Route::post('/updatestud', 'Auth\RegisterController@update')->name('updatestud');

    Route::get('/banned', function() {
        return view('auth/banned');
    })->name('banned');


     // Auth::routes();
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});
    Route::group(['middleware' => ['auth', 'ohms.base'], ], function () {
	/*
	|--------------------------------------------------------------------------
	| Student Interface routes (users are forced to be login as student to access those)
	|--------------------------------------------------------------------------
	|
	| The ohms.student and auth middlewares will be applied
	|
	*/
	
    Route::get('/dashboard', 'Student\StudentController@dashboard')->name('dash');
    Route::get('/index/{student_id}/{gender}', 'Student\StudentController@index')->name('index');
    Route::get('/getblocks', 'Student\StudentController@getblocks');
    Route::post('/bookin', 'Student\StudentController@bookin')->name('bookin');
    Route::post('/checkin_t', 'Student\StudentController@checkin_temp');
    Route::post('/checkin_store', 'Student\StudentController@checkin_store')->name('checkin_store');
    Route::post('/rejectroom', 'Student\StudentController@rejectroom');
    Route::get('/checkouts/{student_id}/{allocation_id}', 'Student\StudentController@checkoutstore')->name('check_out');
    Route::get('/read/{notice_id}', 'Student\StudentController@read')->name('read_notice');
    Route::get('/report/maint', 'Student\StudentController@maintenances')->name('get_report');
    Route::post('/maintenance/student', 'Student\StudentController@mainstore')->name('store_report');
    #Attendance
    Route::get('/student/attendance', 'Student\AttendancesController@attendance')->name('attGet');
    Route::post('/student/attendance', 'Student\AttendancesController@store')->name('storetend');


    Route::get('/student/attendances', 'Student\AttendancesController@attendances')->name('attView');
    Route::get('/getattendances', 'Student\AttendancesController@getattendances')->name('gg');

});
/*
 Administration Panel			|
*/

 Route::group(['middleware' => ['ohms.base'], 'prefix' => 'admin', 'namespace' => 'OHMS', 'as' => 'OHMS::'], function () {
	# Public document downloads
   Route::get('/install/', 'InstallerController@show')->name('install');
   Route::post('/install/', 'InstallerController@installConfig');
   Route::get('/install/confirm', 'InstallerController@install')->name('install_confirm');
});
 Route::group(['middleware' => ['auth', 'ohms.base', 'ohms.auth'], 'prefix' => 'admin', 'namespace' => 'OHMS', 'as' => 'OHMS::'], function () { 
	# Dashboard Controller
    Route::get('/', 'DashboardController@index')->name('dashboard');
    # Users Routes
    Route::get('/users', 'UsersController@index')->name('users');
    Route::post('/search_user','UsersController@search');
    Route::get('/users/create', 'UsersController@create')->name('users_create');
    Route::post('/users/create', 'UsersController@store');
    Route::get('/users/settings', 'UsersController@editSettings')->name('users_settings');
    Route::post('/users/settings', 'UsersController@updateSettings');
    Route::get('/users/{id}', 'UsersController@show')->name('users_profile');
    Route::get('/users/{id}/edit', 'UsersController@edit')->name('users_edit');
    Route::post('/users/{id}/edit', 'UsersController@update');
    Route::get('/users/{id}/roles', 'UsersController@editRoles')->name('users_roles');
    Route::post('/users/{id}/roles', 'UsersController@setRoles');
    Route::get('/users/{id}/delete', 'SecurityController@confirm')->name('users_delete');
    Route::post('/users/{id}/delete', 'UsersController@destroy');    
    # Student Routes
    Route::get('/students', 'StudentsController@index')->name('students');
    Route::post('/search_stud','StudentsController@search');
    Route::get('/students/create', 'StudentsController@create')->name('students_create');
    Route::post('/students/create', 'StudentsController@store');
    Route::get('/students/settings', 'StudentsController@editSettings')->name('users_settings');
    Route::post('/students/settings', 'StudentsController@updateSettings');
    Route::get('/students/{id}', 'StudentsController@show')->name('students_profile');
    Route::get('/students/{id}/edit', 'StudentsController@edit')->name('students_edit');
    Route::post('/students/{id}/edit', 'StudentsController@update');
    Route::get('/students/{id}/delete', 'SecurityController@confirm')->name('students_delete');
    Route::post('/students/{id}/delete', 'StudentsController@destroy');

   # Staff Routes
    Route::get('/staffs', 'StaffsController@index')->name('staffs');
    Route::post('/search_stud','StaffsController@search');
    Route::get('/staffs/create', 'StaffsController@create')->name('staffs_create');
    Route::post('/staffs/create', 'StaffsController@store');
    Route::get('/staffs/settings', 'StaffsController@editSettings')->name('users_settings');
    Route::post('/staffs/settings', 'StaffsController@updateSettings');
    Route::get('/staffs/{id}', 'StaffsController@show')->name('staffs_profile');
    Route::get('/staffs/{id}/edit', 'StaffsController@edit')->name('staffs_edit');
    Route::post('/staffs/{id}/edit', 'StaffsController@update');
    Route::get('/staffs/{id}/delete', 'SecurityController@confirm')->name('staffs_delete');
    Route::post('/staffs/{id}/delete', 'StaffsController@destroy');

    # Visitor Routes
    Route::get('/visitors', 'VisitorsController@index')->name('visitors');
    Route::post('/search_visitor','VisitorsController@search');
    Route::post('/search_visList','VisitorsController@search_reo');
    Route::get('/getrooms', 'VisitorsController@getrooms')->name('getrooms');
    Route::post('/visitors/create', 'VisitorsController@store')->name('visitors_store');
    Route::get('/visitors/create/{id}', 'VisitorsController@create')->name('visitors_create');
    Route::get('/visitors/create', 'VisitorsController@add')->name('visitors_add');
    Route::get('/visitors/view/{id}', 'VisitorsController@show')->name('visitors_view');
    Route::get('/visitors/clear/{id}/{room}', 'VisitorsController@clear')->name('visitors_clear');
    # Roles Routes
    Route::get('/roles', 'RolesController@index')->name('roles');
    Route::get('/roles/create', 'RolesController@create')->name('roles_create');
    Route::post('/roles/create', 'RolesController@store');
    Route::get('/roles/{id}', 'RolesController@show')->name('roles_show');
    Route::get('/roles/{id}/edit', 'RolesController@edit')->name('roles_edit');
    Route::post('/roles/{id}/edit', 'RolesController@update');
    Route::get('/roles/{id}/permissions', 'RolesController@editPermissions')->name('roles_permissions');
    Route::post('/roles/{id}/permissions', 'RolesController@setPermissions');
    Route::get('/roles/{id}/delete', 'SecurityController@confirm')->name('roles_delete');
    Route::post('/roles/{id}/delete', 'RolesController@destroy');
    # Hostels Routes
    Route::get('/hostels', 'HostelsController@index')->name('hostels');
    Route::get('/hostels/create', 'HostelsController@create')->name('hostels_create');
    Route::post('/hostels/create', 'HostelsController@store');
    Route::get('/hostels/{id}', 'HostelsController@show')->name('hostels_show');
    Route::get('/hostels/{id}/edit', 'HostelsController@edit')->name('hostels_edit');
    Route::post('/hostels/{id}/edit', 'HostelsController@update');
    Route::get('/hostels/{id}/delete', 'SecurityController@confirm')->name('hostels_delete');
    Route::post('/hostels/{id}/delete', 'HostelsController@destroy');
        # Blocks Routes
    Route::get('/blocks', 'BlocksController@index')->name('blocks');
    Route::get('/blocks/create', 'BlocksController@create')->name('blocks_create');
    Route::post('/blocks/create', 'BlocksController@store');
    Route::get('/blocks/{id}', 'BlocksController@show')->name('blocks_show');
    Route::get('/blocks/{id}/edit', 'BlocksController@edit')->name('blocks_edit');
    Route::post('/blocks/{id}/edit', 'BlocksController@update');
    Route::get('/blocks/{id}/delete', 'SecurityController@confirm')->name('blocks_delete');
    Route::post('/blocks/{id}/delete', 'BlocksController@destroy');
        # Rooms Routes
    Route::get('/rooms', 'RoomsController@index')->name('rooms');
    Route::get('/rooms/create', 'RoomsController@create')->name('rooms_create');
    Route::post('/rooms/create', 'RoomsController@store');
    Route::get('/rooms/{id}', 'RoomsController@show')->name('rooms_show');
    Route::post('/search_room','RoomsController@search');
    Route::get('/rooms/{id}/edit', 'RoomsController@edit')->name('rooms_edit');
    Route::post('/rooms/{id}/edit', 'RoomsController@update');
    Route::get('/rooms/{id}/delete', 'SecurityController@confirm')->name('rooms_delete');
    Route::post('/rooms/{id}/delete', 'RoomsController@destroy');
    # Maintenances Routes
    Route::get('/maintenances', 'MaintenancesController@index')->name('maintenances');
    Route::get('/maintbs', 'MaintenancesController@indexb')->name('maintbs');
    Route::get('/maintenances/{maintenance_id}/{type}', 'MaintenancesController@change')->name('maintenance_change');
    Route::get('/maintenances/{maintenance_id}/{type}/delte', 'SecurityController@confirm')->name('maintenance_delete');
    Route::post('/maintenances/{maintenance_id}/{type}/delte', 'MaintenancesController@delete');
    Route::get('/mas/create_room', 'MaintenancesController@create_room')->name('maint_creatr');
    Route::get('/mas/createh', 'MaintenancesController@create_block')->name('maint_createh');
    Route::post('/maintenances/room', 'MaintenancesController@store')->name('maint_store');
    
    #Notice Routes
    Route::get('/notices', 'NoticesController@index')->name('notices');
    Route::get('/notices/create', 'NoticesController@create')->name('notices_create');
    Route::post('/notices/create', 'NoticesController@store')->name('notices_store');
    Route::get('/notices/{id}', 'NoticesController@show')->name('notices_show');
    Route::get('/notices/{id}/edit', 'NoticesController@edit')->name('notices_edit');
    Route::post('/notices/{id}/edit', 'NoticesController@update');
    Route::get('/notices/{id}/delete', 'SecurityController@confirm')->name('notices_delete');
    Route::post('/notices/{id}/delete', 'NoticesController@destroy');
    
    # Permissions Routes
    Route::get('/permissions', 'PermissionsController@index')->name('permissions');
    Route::get('/permissions/create', 'PermissionsController@create')->name('permissions_create');
    Route::post('/permissions/create', 'PermissionsController@store');
    Route::get('/permissions/{id}/edit', 'PermissionsController@edit')->name('permissions_edit');
    Route::post('/permissions/{id}/edit', 'PermissionsController@update');
    Route::get('/permissions/{id}/delete', 'SecurityController@confirm')->name('permissions_delete');
    Route::post('/permissions/{id}/delete', 'PermissionsController@destroy');
	# Database CRUD
    Route::get('/CRUD', 'CRUDController@index')->name('CRUD');
    Route::get('/CRUD/{table}', 'CRUDController@table')->name('CRUD_table');
    Route::get('/CRUD/{table}/create', 'CRUDController@create')->name('CRUD_create');
    Route::post('/CRUD/{table}/create', 'CRUDController@createRow');
    Route::get('/CRUD/{table}/{id}', 'CRUDController@row')->name('CRUD_edit');
    Route::post('/CRUD/{table}/{id}', 'CRUDController@saveRow');
    Route::get('/CRUD/{table}/{id}/delete', 'SecurityController@confirm')->name('CRUD_delete');
    Route::post('/CRUD/{table}/{id}/delete', 'CRUDController@deleteRow');
	# API
    Route::get('/API', 'APIController@index')->name('API');
	# Reports
    Route::get('/report', 'ReportController@index')->name('index_report');
    Route::get('/studreport/{gender}', 'ReportController@students')->name('student_report');
    Route::post('/dept_maint', 'ReportController@dept')->name('dept_maint');
    Route::get('/block_maint/{gender}', 'ReportController@block_maint')->name('block_maint');
    Route::get('/room_maint/{gender}', 'ReportController@room_maint')->name('room_maint');
    Route::get('/chech/{gender}', 'ReportController@check')->name('chech_list');
    Route::post('/srchstudrep', 'ReportController@students_search');
    Route::post('/srchblkment', 'ReportController@block_maint_search');
    Route::post('/srchrmment', 'ReportController@room_maint_search');
 
    # Semesters Routes
    Route::get('/semesters', 'SemestersController@index')->name('semesters');
    Route::get('/semesters/create', 'SemestersController@create')->name('semesters_create');
    Route::post('/semesters/create', 'SemestersController@store');
    Route::get('/semesters/{id}', 'SemestersController@show')->name('semesters_show');
    Route::get('/semesters/{id}/edit', 'SemestersController@edit')->name('semesters_edit');
    Route::post('/semesters/{id}/edit', 'SemestersController@update');
    Route::get('/semesters/{id}/delete', 'SecurityController@confirm')->name('semesters_delete');
    Route::post('/semesters/{id}/delete', 'SemestersController@destroy');
    Route::get('/semesters/{id}/checkin', 'SemestersController@sem_checkin')->name('sem_checkin');
    Route::get('/semesters/{id}/checkout', 'SemestersController@sem_checkout')->name('sem_checkout');
    # Checklists Routes
    Route::get('/checklists', 'ChecklistsController@index')->name('checklists');
    Route::get('/checklists/create', 'ChecklistsController@create')->name('checklists_create');
    Route::post('/checklists/create', 'ChecklistsController@store');
    Route::get('/checklists/{id}', 'ChecklistsController@show')->name('checklists_show');
    Route::get('/checklists/{id}/edit', 'ChecklistsController@edit')->name('checklists_edit');
    Route::post('/checklists/{id}/edit', 'ChecklistsController@update');
    Route::get('/checklists/{id}/delete', 'SecurityController@confirm')->name('checklists_delete');
    Route::post('/checklists/{id}/delete', 'ChecklistsController@destroy');
    # Facility_statuses Routes
    Route::get('/facility_statuses', 'Facility_statusesController@index')->name('facility_statuses');
    Route::get('/facility_statuses/create', 'Facility_statusesController@create')->name('facility_statuses_create');
    Route::post('/facility_statuses/create', 'Facility_statusesController@store');
    Route::get('/facility_statuses/{id}', 'Facility_statusesController@show')->name('facility_statuses_show');
    Route::get('/facility_statuses/{id}/edit', 'Facility_statusesController@edit')->name('facility_statuses_edit');
    Route::post('/facility_statuses/{id}/edit', 'Facility_statusesController@update');
    Route::post('/facility_statuses/{id}/delete', 'Facility_statusesController@destroy');
    Route::get('/facility_statuses/{id}/delete', 'SecurityController@confirm')->name('facility_statuses_delete');
    # Flights Routes
    Route::get('/flights', 'FlightsController@index')->name('flights');
    Route::get('/flights/create', 'FlightsController@create')->name('flights_create');
    Route::post('/flights/create', 'FlightsController@store');
    Route::get('/flights/{id}', 'FlightsController@show')->name('flights_show');
    Route::get('/flights/{id}/edit', 'FlightsController@edit')->name('flights_edit');
    Route::post('/flights/{id}/edit', 'FlightsController@update');
    Route::get('/flights/{id}/delete', 'SecurityController@confirm')->name('flights_delete');
    Route::post('/flights/{id}/delete', 'FlightsController@destroy');

    # Maint_deps Routes
    Route::get('/maint_deps', 'Maint_depsController@index')->name('maint_deps');
    Route::get('/maint_deps/create', 'Maint_depsController@create')->name('maint_deps_create');
    Route::post('/maint_deps/create', 'Maint_depsController@store');
    Route::get('/maint_deps/{id}', 'Maint_depsController@show')->name('maint_deps_show');
    Route::get('/maint_deps/{id}/edit', 'Maint_depsController@edit')->name('maint_deps_edit');
    Route::post('/maint_deps/{id}/edit', 'Maint_depsController@update');
    Route::get('/maint_deps/{id}/delete', 'SecurityController@confirm')->name('maint_deps_delete');
    Route::post('/maint_deps/{id}/delete', 'Maint_depsController@destroy');
    # Facilities Routes
    Route::get('/facilities', 'FacilitiesController@index')->name('facilities');
    Route::get('/facilities/create', 'FacilitiesController@create')->name('facilities_create');
    Route::post('/facilities/create', 'FacilitiesController@store');
    Route::get('/facilities/{id}', 'FacilitiesController@show')->name('facilities_show');
    Route::get('/facilities/{id}/edit', 'FacilitiesController@edit')->name('facilities_edit');
    Route::post('/facilities/{id}/edit', 'FacilitiesController@update');
    Route::get('/facilities/{id}/delete', 'SecurityController@confirm')->name('facilities_delete');
    Route::post('/facilities/{id}/delete', 'FacilitiesController@destroy');
    # Items Routes
    Route::get('/items', 'ItemsController@index')->name('items');
    Route::get('/items/create', 'ItemsController@create')->name('items_create');
    Route::post('/items/create', 'ItemsController@store');
    Route::get('/items/{id}', 'ItemsController@show')->name('items_show');
    Route::get('/items/{id}/edit', 'ItemsController@edit')->name('items_edit');
    Route::post('/items/{id}/edit', 'ItemsController@update');
    Route::get('/items/{id}/delete', 'SecurityController@confirm')->name('items_delete');
    Route::post('/items/{id}/delete', 'ItemsController@destroy');
    # Majors Routes
    Route::get('/majors', 'MajorsController@index')->name('majors');
    Route::get('/majors/create', 'MajorsController@create')->name('majors_create');
    Route::post('/majors/create', 'MajorsController@store');
    Route::get('/majors/{id}', 'MajorsController@show')->name('majors_show');
    Route::get('/majors/{id}/edit', 'MajorsController@edit')->name('majors_edit');
    Route::post('/majors/{id}/edit', 'MajorsController@update');
    Route::get('/majors/{id}/delete', 'SecurityController@confirm')->name('majors_delete');
    Route::post('/majors/{id}/delete', 'MajorsController@destroy');
    # Houses Routes
    Route::get('/houses', 'HousesController@index')->name('houses');
    Route::get('/houses/create', 'HousesController@create')->name('houses_create');
    Route::post('/houses/create', 'HousesController@store');
    Route::get('/houses/{id}', 'HousesController@show')->name('houses_show');
    Route::get('/houses/{id}/edit', 'HousesController@edit')->name('houses_edit');
    Route::post('/houses/{id}/edit', 'HousesController@update');
    Route::get('/houses/{id}/delete', 'SecurityController@confirm')->name('houses_delete');
    Route::post('/houses/{id}/delete', 'HousesController@destroy');
    # House Categories Routes
    Route::get('/house_categories', 'HousesCategoriesController@index')->name('house_categories');
    Route::get('/house_categories/create', 'HousesCategoriesController@create')->name('house_categories_create');
    Route::post('/house_categories/create', 'HousesCategoriesController@store');
    Route::get('/house_categories/{id}', 'HousesCategoriesController@show')->name('house_categories_show');
    Route::get('/house_categories/{id}/edit', 'HousesCategoriesController@edit')->name('house_categories_edit');
    Route::post('/house_categories/{id}/edit', 'HousesCategoriesController@update');
    Route::get('/house_categories/{id}/delete', 'SecurityController@confirm')->name('house_categories_delete');
    Route::post('/house_categories/{id}/delete', 'HousesCategoriesController@destroy');
    # House_allocations Routes
    Route::get('/staff_allocations/{alloc}', 'House_allocationsController@index_hr')->name('staff_allocations');
    Route::get('/dosa_allocations', 'House_allocationsController@index_dosa')->name('dosa_allocations');
    Route::get('/pro_allocations', 'House_allocationsController@index_pro')->name('pro_allocations'); 
    Route::get('/house_allocations/{house_allocation_id}/{type}', 'House_allocationsController@change')->name('house_allocation_change');
    Route::get('/house_allocations/{house_allocation_id}/{type}/delte', 'SecurityController@confirm')->name('house_allocation_delete');
    Route::post('/house_allocations/{house_allocation_id}/{type}/delte', 'House_allocationsController@delete');
    Route::get('/dosa_allocations/student', 'House_allocationsController@create_dosa')->name('allo_dosa');
    Route::get('/hr_allocations/staff', 'House_allocationsController@create_staff')->name('allo_staff');
    Route::get('/pro_allocations/visitor', 'House_allocationsController@create_pro')->name('allo_guest');
    Route::post('/house_allocations', 'House_allocationsController@store')->name('house_allo');



                             
        # Sessions Routes
    Route::get('/sessions', 'SessionsController@index')->name('sessions');

    Route::get('/sessions/create', 'SessionsController@create')->name('sessions_create');
    Route::post('/sessions/create', 'SessionsController@store');

    Route::get('/sessions/{id}/edit', 'SessionsController@edit')->name('sessions_edit');
    Route::post('/sessions/{id}/edit', 'SessionsController@update');

    Route::get('/sessions/{id}/delete', 'SecurityController@confirm')->name('sessions_delete');
    Route::post('/sessions/{id}/delete', 'SessionsController@destroy');

    Route::get('/sessions/{id}/view_attend', 'SessionsController@view_student')->name('sess_attend');

     # Courses Routes
    Route::get('/courses', 'CoursesController@index')->name('courses'); 

    Route::get('/courses/create', 'CoursesController@create')->name('courses_create');
    Route::post('/courses/create', 'CoursesController@store');

    Route::get('/courses/{id}', 'CoursesController@show')->name('courses_show');

    Route::get('/courses/{id}/edit', 'CoursesController@edit')->name('courses_edit');
    Route::post('/courses/{id}/edit', 'CoursesController@update');

    Route::get('/courses/{id}/delete', 'SecurityController@confirm')->name('courses_delete');
    Route::post('/courses/{id}/delete', 'CoursesController@destroy');

    Route::get('/courses/{id}/classList', 'CoursesController@classList')->name('courses_classList');
    Route::post('/courses/{id}/add', 'CoursesController@add')->name('courses_add');

    Route::post('/courses/search_stud', 'CoursesController@search')->name('stud_class_sch');
    Route::post('/courses/add_stud', 'CoursesController@add')->name('stud_class_add');
    Route::get('/courses/{course_id}/{student_id}/remove', 'SecurityController@remove')->name('stud_class_remove');
    Route::post('/courses/{course_id}/{student_id}/remove', 'CoursesController@remove');

    # Attendances 

    Route::get('/attendances/create', 'AttendancesController@create')->name('attendances_create');
    Route::post('/attendances/create', 'AttendancesController@store');

    Route::get('/attendances/{id}/edit', 'AttendancesController@edit')->name('attendances_edit');
    Route::post('/attendances/{id}/edit', 'AttendancesController@update');

    Route::get('/attendances/{id}/delete', 'SecurityController@confirm')->name('attendances_delete');
    Route::post('/attendances/{id}/delete', 'AttendancesController@destroy');


    Route::get('/attendances/{course_id}/{student_id}/view', 'AttendancesController@view_student')->name('stud_attend');

     # Departments Routes
    Route::get('/departments', 'DepartmentsController@index')->name('departments');
    Route::get('/departments/create', 'DepartmentsController@create')->name('departments_create');
    Route::post('/departments/create', 'DepartmentsController@store');
    Route::get('/departments/{id}', 'DepartmentsController@show')->name('departments_show');
    Route::get('/departments/{id}/edit', 'DepartmentsController@edit')->name('departments_edit');
    Route::post('/departments/{id}/edit', 'DepartmentsController@update');
    Route::get('/departments/{id}/delete', 'SecurityController@confirm')->name('departments_delete');
    Route::post('/departments/{id}/delete', 'DepartmentsController@destroy');

    # Desciplinaries Routes
    Route::get('/desciplinaries', 'DesciplinariesController@index')->name('desciplinaries');
    Route::get('/desciplinaries/create', 'DesciplinariesController@create')->name('desciplinaries_create');
    Route::post('/desciplinaries/create', 'DesciplinariesController@store');
    Route::get('/desciplinaries/{id}', 'DesciplinariesController@show')->name('desciplinaries_show');
    Route::get('/desciplinaries/{id}/edit', 'DesciplinariesController@edit')->name('desciplinaries_edit');
    Route::post('/desciplinaries/{id}/edit', 'DesciplinariesController@update');
    Route::get('/desciplinaries/{id}/delete', 'SecurityController@confirm')->name('desciplinaries_delete');
    Route::post('/desciplinaries/{id}/delete', 'DesciplinariesController@destroy');
        # Policies Routes
    Route::get('/policies', 'PoliciesController@index')->name('policies');
    Route::get('/policies/create', 'PoliciesController@create')->name('policies_create');
    Route::post('/policies/create', 'PoliciesController@store');
    Route::get('/policies/{id}', 'PoliciesController@show')->name('policies_show');
    Route::get('/policies/{id}/edit', 'PoliciesController@edit')->name('policies_edit');
    Route::post('/policies/{id}/edit', 'PoliciesController@update');
    Route::get('/policies/{id}/delete', 'SecurityController@confirm')->name('policies_delete');
    Route::post('/policies/{id}/delete', 'PoliciesController@destroy');
        # Benefit_records Routes
    Route::get('/benefit_records', 'Benefit_recordsController@index')->name('benefit_records');
    Route::get('/benefit_records/create', 'Benefit_recordsController@create')->name('benefit_records_create');
    Route::post('/benefit_records/create', 'Benefit_recordsController@store');
    Route::get('/benefit_records/{id}', 'Benefit_recordsController@show')->name('benefit_records_show');
    Route::post('/search_benefit_record','Benefit_recordsController@search');
    Route::get('/benefit_records/{id}/edit', 'Benefit_recordsController@edit')->name('benefit_records_edit');
    Route::post('/benefit_records/{id}/edit', 'Benefit_recordsController@update');
    Route::get('/benefit_records/{id}/delete', 'SecurityController@confirm')->name('benefit_records_delete');
    Route::post('/benefit_records/{id}/delete', 'Benefit_recordsController@destroy');
    # Dependants Routes
    Route::get('/dependants', 'DependantsController@index')->name('dependants');
    Route::get('/dependants/create', 'DependantsController@create')->name('dependants_create');
    Route::post('/dependants/create', 'DependantsController@store');
    Route::get('/dependants/{id}', 'DependantsController@show')->name('dependants_show');
    Route::get('/dependants/{id}/edit', 'DependantsController@edit')->name('dependants_edit');
    Route::post('/dependants/{id}/edit', 'DependantsController@update');
    Route::get('/dependants/{id}/delete', 'SecurityController@confirm')->name('dependants_delete');
    Route::post('/dependants/{id}/delete', 'DependantsController@destroy');
        # Benefits Routes
    Route::get('/benefits', 'BenefitsController@index')->name('benefits');
    Route::get('/benefits/create', 'BenefitsController@create')->name('benefits_create');
    Route::post('/benefits/create', 'BenefitsController@store');
    Route::get('/benefits/{id}', 'BenefitsController@show')->name('benefits_show');
    Route::get('/benefits/{id}/edit', 'BenefitsController@edit')->name('benefits_edit');
    Route::post('/benefits/{id}/edit', 'BenefitsController@update');
    Route::get('/benefits/{id}/delete', 'SecurityController@confirm')->name('benefits_delete');
    Route::post('/benefits/{id}/delete', 'BenefitsController@destroy');
        # Accademics Routes
    Route::get('/accademics', 'AccademicsController@index')->name('accademics');
    Route::get('/accademics/create', 'AccademicsController@create')->name('accademics_create');
    Route::post('/accademics/create', 'AccademicsController@store');
    Route::get('/accademics/{id}', 'AccademicsController@show')->name('accademics_show');
    Route::post('/search_accademic','AccademicsController@search');
    Route::get('/accademics/{id}/edit', 'AccademicsController@edit')->name('accademics_edit');
    Route::post('/accademics/{id}/edit', 'AccademicsController@update');
    Route::get('/accademics/{id}/delete', 'SecurityController@confirm')->name('accademics_delete');
    Route::post('/accademics/{id}/delete', 'AccademicsController@destroy');
    # Work_records Routes
    Route::get('/work_records', 'Work_recordsController@index')->name('work_records');
    Route::get('/work_records/create', 'Work_recordsController@create')->name('work_records_create');
    Route::post('/work_records/create', 'Work_recordsController@store');
    Route::get('/work_records/{id}', 'Work_recordsController@show')->name('work_records_show');
    Route::get('/work_records/{id}/edit', 'Work_recordsController@edit')->name('work_records_edit');
    Route::post('/work_records/{id}/edit', 'Work_recordsController@update');
    Route::get('/work_records/{id}/delete', 'SecurityController@confirm')->name('work_records_delete');
    Route::post('/work_records/{id}/delete', 'Work_recordsController@destroy');
        # Resumes Routes
    Route::get('/resumes', 'ResumesController@index')->name('resumes');
    Route::get('/resumes/create', 'ResumesController@create')->name('resumes_create');
    Route::post('/resumes/create', 'ResumesController@store');
    Route::get('/resumes/{id}', 'ResumesController@show')->name('resumes_show');
    Route::post('/search_resume','ResumesController@search');
    Route::get('/resumes/{id}/edit', 'ResumesController@edit')->name('resumes_edit');
    Route::post('/resumes/{id}/edit', 'ResumesController@update');
    Route::get('/resumes/{id}/delete', 'SecurityController@confirm')->name('resumes_delete');
    Route::post('/resumes/{id}/delete', 'ResumesController@destroy');
});