@extends('layouts.admin.mainR')
@section('page-content')
<div class="container col-md-10 col-md-offset-1" >
  <div class="row">
    <div class="col-md-7" style="padding-top: 0 !important;">{!! $allocations->links() !!}</div>
    <div class="col-md-3" style="padding-top: 16px !important;">
        <form class="navbar-form" role="search" method="Post">
        {{ csrf_field() }}
        <div class="input-group">
          <input type="text" class="form-control" name="{{ $gender }}" placeholder="Search..." id="search">
        </div>
        </form>
    </div>
    <div class="" style="padding-top: 21px; position: absolute; right: 0;"> 
       <button type="button" class="btn" id="print">Print</button>
    </div>
  </div>
  <div class="" style="background-color:#FFF;">
    <div class="container" id="">
      <div class="panel" style="border-color: #ffeaea;">
        <div class="panel-heading" style="background-color:#ffeaea;">
          <center>  <h4 id="title"></i> Student Room List</h4></center>
        </div>
        <div class="panel-body">
          <table class="table table-bordered" id="myTable" >
            <thead>
              <tr>
                <th>Hostel</th>
                <th>Room #</th>
                <th>Name</th>
                <th>Student Id</th>
                <th>Year</th>
                <th>Major</th>
                <th>Date of Birth</th>
                <th>Home Address</th>
                <th>Cell</th>
                <th>Nat Id</th>
                <th>Date</th>
              </tr> 
            </thead>
            <tbody >
            @foreach($allocations as $allocation)
              @if($allocation->student->gender === $gender)
              <tr>
                <td>{{$allocation->room->block->name}}</td>
                <td>{{$allocation->room->room_number}}</td>
                <td>{{$allocation->student->user->full_name}}</td>
                <td>{{$allocation->student->solusi_id}}</td>
                <td>{{$allocation->student->year}}</td>
                <td>{{$allocation->student->major->name}}</td>
                <td>{{$allocation->student->date_of_birth}}</td>
                <td>{{$allocation->student->home_address}}</td>
                <td>{{$allocation->student->cell_number}}</td>
                <td>{{$allocation->student->national_id}}</td>
                <td>{{$allocation->created_at}}</td>               
              </tr>
              @endif
             @endforeach
            </tbody>
          </table>  
      </div>
    </div>
  </div>  
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function () {
  $("#print").on('click', function () {
    $("#myTable").printThis({
      debug: false,              // show the iframe for debugging
      importCSS: true,           // import page CSS
      printContainer: true,      // grab outer container as well as the contents of the selector
      //loadCSS: "path/to/my.css", // path to additional css file
      pageTitle: $("#title").text(),             // add title to print page
      removeInline: false        // remove all inline styles from print elements
  });
});
  $('#search').on('keyup', function(){
    var kwords = $(this).val();
    var gender = $(this).attr("name");
    $.ajax({
      type : "post",
      url : "{{ url('admin/srchstudrep') }}",
      data: {"_token": $('#token').val(), "kwords" : kwords, "gender" : gender },
      success: function(search_stud){
      $('#myTable').html(search_stud)
      },
    });  
  });
});           
</script>
@endsection