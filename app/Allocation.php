<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allocation extends Model
{   
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    
    protected $fillable = [
         'room_id', 'student_id', 'approved',  'check_in', 
    ]; 

	 public function student()
    {
         return $this->belongsTo('App\Student');
    }

    public function room()
    {
         return $this->belongsTo('App\Room');
    }

     public function semester()
    {
         return $this->belongsTo('App\Semester');
    }

   
    public function checklists()
    {
        return $this->hasMany('App\Checklist');
    }

    public function check_temps()
    {
        return $this->hasMany('App\Check_temp');
    }
}
