<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maint_dep extends Model
{
     public function items()
    {
        return $this->hasMany('App\Item');
    }
     public function maintenances()
    {
        return $this->hasManyThrough('App\Maintenance', 'App\Item', 'maint_dep_id', 'item_id', 'id');
    }
}
