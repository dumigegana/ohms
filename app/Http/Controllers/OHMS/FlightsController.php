<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Flight;
use App\Permission;
use Schema;
use Auth;
use OHMS;

class FlightsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.flights.access');

        # Get all the flights
        $flights = Flight::all();
        
        # Return the view
        return view('ohms/flights/index', ['flights' => $flights]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.flight.access');

        # Get the flight
        $flight = Flight::find($id);

        # Return the view
        return view('admin/flights/show', ['flight' => $flight]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.flights.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.flights.edit');

        # Find the flight
        $row = Flight::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'flights';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/flights/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.flights.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.flights.edit');

        # Find the row
        $row = Flight::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'flights';
         $row->status = $request['status']; 
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::flights')->with('success', trans('ohms.msg_flight_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.flights.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.flights.create');

        # Get all the data
        $data_index = 'flights';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/flights/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.flights.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.flights.create');

        # create new facility_status
        $row = new Flight;

        # Save the data
        $data_index = 'flights';
        $row->status = $request['status']; 
        require('Data/Create/Save.php');

        # Return the admin to the facility_statuses page with a success message
        return redirect()->route('OHMS::flights')->with('success', trans('ohms.msg_flight_created'));
    }
   

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.flights.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.flights.delete');

        # Select Facility_status
        $flight = OHMS::flight('id', $id);

        if(!$flight->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }
        
        # Delete flight
        $flight->delete();

        # Redirect the admin
        return redirect()->route('OHMS::flights')->with('success', trans('ohms.msg_flight_deleted'));
    }
}
