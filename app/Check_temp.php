<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Check_temp extends Model
{
    protected $fillable = ['allocation_id', 'facility_id', 'pos', 'facility_status_id', 'created_at', 'updated_at'];
    
    public function allocation()
    {
         return $this->belongsTo('App\Allocation');
    }

     public function facility()
    {
         return $this->belongsTo('App\Facility');
    }
    
    public function facility_status()
    {
        return $this->belongsTo('App\Facility_status');
    }
}
