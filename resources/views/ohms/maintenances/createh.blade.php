@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::maintbs') }}">{{ trans('ohms.maintenances_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.maintenances_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.maintenances_create_title'))
@section('icon', "plus ")
@section('subtitle', trans('ohms.maintenances_create_subtitle'))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST" action=" {{route('OHMS::maint_store') }}" >
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea !important; color: #980000;">
                         {{ csrf_field() }}
                        <input name="user_id" type="hidden" value="{{ Auth::user()->id}}">
                        <input name="maintenable_type" type="hidden" value="App\Block">
                        <div class="required field">
                        <label style="color: #980000;">Block</label>
                         <select name="maintenable_id" class="form-control" required>
                            <option value="" >Select hostel </option>
                                 @foreach($blocks as $block)
                                <option value="{{ $block->id }}" {{ (old("maintenable_id") == $block->id  ? "checked":"") }}>{{ $block->name }}</option>
                                @endforeach
                            </select>
                            <br>
                        </div>
                        <div class="required field">
                        <br><br><br>
                        <label style="color: #980000;">Select Item to Report </label>
                         <select name="item_id" class="form-control" required>
                         <option value="">Select item </option>                            
                                @foreach($items as $item)
                                <option value="{{ $item->id }}" {{ (old("item_id") == $item->id ? "checked":"") }}>{{ $item->name }} </option>
                                @endforeach
                            </select>
                            <br><br><br>  
                        </div>                                               
                    </div>
                    </div>                 
                    <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea !important; color: #980000;">
                         {{ csrf_field() }}
                       
                        <div class="field">
                            <label style="color: #980000;">Comments</label>
                            <textarea name="comments" type="text" placeholder="Comments" required value="{{ old('comments')}}"></textarea>
                            </div>
                            <br/>
                             

                        <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </form>
</div>
@endsection
