@extends('auth.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" {{ trans('Fill in this Form') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.students_create_title') }}</div>
    </div>
@endsection
@section('title', trans('Fill in this Form.'))
@section('icon', "plus")
@section('subtitle', trans('Create your account.'))
@section('content')
<div class="ui container" style="background-color: #ffeaea; color: #980000;">
  <form class="ui form" method="POST" action="{{ route('storestud') }}">
    <div class="ui doubling stackable grid">
      <div class="row">
        <div class="eight wide column">
          <div class="ui very padded segment" style="background-color: #ffeaea;">
            {{ csrf_field() }}
                 
            <div class="required field">
              <label style="color: #980000;">Full Name</label>
              <input name='full_name' type="text" placeholder="Full Name" value="{{ old('full_name')}}" required>
            </div><br>
              @php $email = $id->student_id.'@solusi.ac.zw';@endphp
            <input name='username' type="hidden" value="{{$id->student_id}}" >
            <input name='student_id' type="hidden" value="{{$id->student_id}}" >
            <input name='email' type="hidden" value="{{$email}}.">
            <div class="required field">
              <label style="color: #980000;">Password</label>
              <input name='password' type="password" placeholder="Password">
            </div>
            <div class="required field">
            <label style="color: #980000;">Confirm Password</label>
              <input name='password_confirmation' type="password" placeholder="Confirm Password">
            </div>
             <div>
              <input name='role_id' type="hidden" value="2" >
            </div>
            <br>             
            <div class="required field">
            <label>Date of Birth</label>
            <input name='date_of_birth' type="date" placeholder="">
            </div>
            
            <br>
            <div class="ui equal width form">
              <div class="fields">                         
                <div class="required field">
                <label style="color: #980000;">Cell number</label>
                <input type="hidden" value="" name="cell_phone_number" id="cell_phone_number">
                <input id="cell_number" name='cell' type="tel" value="{{ old('cell')}}" required>
                <input id="hidden" type="hidden" name="cell_number"><br>
                <span id="valid-msg" class="hide">✓ Valid</span>
                <span id="error-msg" class="hide">&times; Invalid number</span>
                </div>
                
                <div class="required field">
                <label style="color: #980000;">Next of Kin contact number</label>
                <input type="hidden" value="" name="next_of_kin_number" id="next_of_kin_number">
                <input id="next_of_kin" name='next' type="tel" value="{{ old('next')}}" required>
                <input id="hidden_n" type="hidden" name="next_of_kin"><br>
                <span id="valid-msg1" class="hide">✓ Valid</span>
                <span id="error-msg1" class="hide">&times; Invalid number</span>
                </div>
               </div>
            </div>                                            
                                    
          </div>
        </div>
          
        <div class="eight wide column">
          <div class="ui very padded segment" style="background: #ffeaea"> 
          <div class="required field">
              <label style="color: #980000;">Home Address</label>
              <input name='home_address' type="text" value="{{ old('home_address')}}" required>
            </div>           
            <div class="required field">
              <label style="color: #980000;">National ID number</label>
              <input name='national_id' type="text" value="{{ old('national_id')}}" required>
            </div>
            <div class="field" id="swh">
              <label style="color: #980000;">Major</label>
              <select name="major_id" class="form-control" required>
                <option value="">Enter major</option>
                @foreach($majors as $major)
                <option value="{{ $major->id }}" {{ (old("major_id") == '$major->id' ? "selected":"") }}>{{ $major->name }}</option>
                 @endforeach
              </select>
            </div>
            <br/>
                 
            <div class="ui equal width form">
              <div class="fields">
                <div class="required field">
                  <label style="color: #980000;">Gender</label>
                  <input type="radio" name="gender" value="Male" {{ (old("gender") == 'Male' ? "checked":"") }}> Male<br>
                  <input type="radio" name="gender" value="Female" {{ (old("gender") == 'Female' ? "checked":"") }}> Female<br>
                </div>
                 
                <div class="required field" >
                  <label style="color: #980000;">Degree Level</label>                              
                 <input id="show" type="radio" name="degree_level" value="Undergraduate" {{ (old("degree_level") == 'Undergraduate' ? "checked":"") }}> Undergraduate<br>
                  <input id="hide" type="radio" name="degree_level" value="Postgraduate" {{ (old("degree_level") == 'Postgraduate' ? "checked":"") }} > Postgraduate<br>                                
                </div>
                <div class="field" id="shw">
                  <label style="color: #980000;">Year of study</label>
                    <input type="radio" name="year" value="1" {{ (old("year") == 1 ? "checked":"") }}> First<br>
                    <input type="radio" name="year" value="2" {{ (old("year") == 2 ? "checked":"") }}> Second<br>
                    <input type="radio" name="year" value="3" {{ (old("year") == 3 ? "checked":"") }}> Third<br>
                    <input type="radio" name="year" value="4" {{ (old("year") == 4 ? "checked":"") }}> Fouth<br>
                </div>
              </div>
            </div>
            <div class="five wide column">
                <div class="ui padded segment" style="background-color: #ffeaea;">
                    <input type="hidden" name="active" value="1">       
                    <input type="hidden" name="banned" value="0">
                    <button type="submit" class="ui btn submit button" style="position: relative; left: 45%; background-color: #980000; color: #ffeaea;">{{ trans('ohms.submit') }}</button>
                </div>
            </div>    
    </form>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::to('js/jquery-3.4.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/utils.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#hide").click(function(){
        $("#shw").hide();
    });
    $("#show").click(function(){
        $("#shw").addClass("required field").show();
    });
  });  var telInput = $("#cell_number");
  $("#error-msg").hide();
  $("#valid-msg").hide();

// initialise plugin
  telInput.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset = function() {
  $("#error-msg").hide();
  $("#valid-msg").hide();
  $("#cell_phone_number").val("");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      $("#error-msg").hide();
      $("#valid-msg").show();
      $("#cell_phone_number").val(1);
    } else {
      telInput.addClass("error");
      $("#valid-msg").hide();
      $("#error-msg").show();
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
// End of Cell phone input
//-----------------------------------------------------------------------------------------------------
$("form").submit(function() {
  $("#hidden").val($("#cell_number").intlTelInput("getNumber"));
  $("#hidden_n").val($("#next_of_kin").intlTelInput("getNumber"));
});

//---------------------------------------------------------------------------------------------------
// Next of kin input Starts
  var telInput1 = $("#next_of_kin");
  $("#error-msg1").hide();
  $("#valid-msg1").hide();

// initialise plugin
  telInput1.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset1 = function() {
  $("#error-msg1").hide();
  $("#valid-msg1").hide();  
  $("#next_of_kin_number").val("");
};

// on blur: validate
telInput1.blur(function() {
  reset1();
  if ($.trim(telInput1.val())) {
    if (telInput1.intlTelInput("isValidNumber")) {
      $("#error-msg1").hide();
      $("#valid-msg1").show();  
      $("#next_of_kin_number").val(1);

    } else {
      telInput1.addClass("error");
      $("#valid-msg1").hide();
      $("#error-msg1").show();

    }
  }
});

// on keyup / change flag: reset
telInput1.on("keyup change", reset1);
 // End of Next of kin contact inputs
 //--------------------------------------------------------------------------------------------------

</script>
@endsection
