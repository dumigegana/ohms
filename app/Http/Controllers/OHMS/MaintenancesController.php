<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Maintenance;
use App\Block;
use App\Item;
use App\Hostel;
use App\Room;
use App\Status;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use OHMS;
use Session;

class MaintenancesController extends Controller
{

  public function index()
  {
    OHMS::permissionToAccess('ohms.maintenances.access');

        # Get all the Maintenances
    $maintenances = Maintenance::where('maintenable_type', 'App\Room')
                              ->with('maintenable')
                              ->orderBy('fixed', 'asc')->paginate(100);
        
        # Return the view
    return view('ohms/maintenances/index', ['maintenances' => $maintenances]);
  }

  public function indexb()
  {
    OHMS::permissionToAccess('ohms.maintenances.access');

        # Get all the Maintenances
    $maintenances = Maintenance::where('maintenable_type', 'App\Block')
                              ->with('maintenable')->
                              orderBy('fixed', 'asc')->paginate(100);
        
        # Return the view
    return view('ohms/maintenances/indexb', ['maintenances' => $maintenances]);
  }

  public function change($id, $type)
  {
        # Check permissions
    OHMS::permissionToAccess('ohms.maintenances.admin');

        # Find The student
     Maintenance::where('id', $id)->update(['fixed' => 1]);
    if($type === 'b')
    {
      return redirect()->route('OHMS::maintbs')->with('success', trans('ohms.msg_maintenance_changed'));  
    }
    else{
      return redirect()->route('OHMS::maintenances')->with('success', trans('ohms.msg_maintenance_changed'));      
    }
    
  }
    
  public function delete(Request $request)
  {
        # Check permissions
    OHMS::permissionToAccess('ohms.maintenances.admin');

        # Delete row
    Maintenance::where('id', $request['id'])->delete();

    if($request['type'] === 'b')
    {
      return redirect()->route('OHMS::maintbs')->with('success', trans('ohms.msg_maintenance_deleted'));  
    }
    else{
      return redirect()->route('OHMS::maintenances')->with('success', trans('ohms.msg_maintenance_deleted'));      
    }
  }

  public function create_room()
  {
    OHMS::permissionToAccess('ohms.maintenances.admin');

    $items = Item::where('type', '!=', 'block')->orderBy('name', 'asc')->get();
    $rooms = Room::with('block')->get();

        # Return the view
    return view('ohms/maintenances/creatr', ['rooms' => $rooms, 'items' => $items]);
  }

  public function create_block()
  {
    OHMS::permissionToAccess('ohms.maintenances.admin');

    $items = Item::where('type', '!=', 'room')->orderBy('name', 'asc')->get();
    $blocks = Block::all();

        # Return the view
    return view('ohms/maintenances/createh', ['blocks' => $blocks, 'items' => $items]);
  }

  public function store(Request $data)
  {      
    OHMS::permissionToAccess('ohms.maintenances.admin');
    $this->validate($data, [
            'comments' => 'required|max:120',
            'maintenable_id' => 'required',
            'maintenable_type' => 'required',
            'item_id' => 'required',
        ]);

     Maintenance::updateOrCreate([
                'maintenable_type' => $data['maintenable_type'],
                'maintenable_id' => $data['maintenable_id'],
                'item_id' => $data['item_id'],
                'fixed' => false
                ], [
                'comments' => $data['comments'],
                'user_id' => Auth::user()->id
                ]);


    if($data['maintenable_type'] === 'App\Block'){
      return redirect()->route('OHMS::maintbs')->with('success', trans('ohms.msg_maintenance_created'));
    }
    else{
      return redirect()->route('OHMS::maintenances')->with('success', trans('ohms.msg_maintenance_created')); 
    }  
  }

  public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.maintenances.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.maintenances.admin');

        # Select Item
        $maintenance = OHMS::maintenance('id', $id);

        if(!$maintenance->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $maintenance->delete();

        # Redirect the admin
        return redirect()->route('OHMS::maintenances')->with('success', trans('ohms.msg_hostel_deleted'));
    }
}
