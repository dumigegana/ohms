@extends('layouts.admin.w3css')
@section('breadcrumb')
<a href="{{ route('OHMS::sessions') }}" class="w3-bar-item">{{ trans('Class session List') }}</a>
<div class="w3-bar-item">{{ trans('Edit session') }}</div>
@endsection
@section('title', trans('Edit Session'))
@section('icon', "edit")
@section('subtitle', trans('Edit Session'))
@section('content')
<div class="w3-container w3-center">
            <form class="ui form" method="POST">
                {{ csrf_field() }}
                @include('ohms/forms/master3')
                <input type="hidden" name="id" value="{{ $row->id }}">
                <br> 
                <button type="submit" class="w3-theme w3-button">{{ trans('ohms.submit') }}</button>
            </form>
        </div>
@endsection
