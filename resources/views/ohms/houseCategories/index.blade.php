@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.houseCategory_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.houseCategory_title'))
@section('icon', "university")
@section('subtitle', trans('ohms.houseCategory_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;"></div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.houses.admin'))
             <center><a href="{{ route('OHMS::house_categories_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add Category</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
  			<table class="ui basic table ">
  			  <thead style="background: #ff8b8b;">
  			    <tr>
                  <th style="color: #980000;">Name</th>
                  <th style="color: #980000;">Street</th>
                  <th style="color: #980000;">Description</th>
                  <th style="color: #980000;">Action</th>
                 
  			  </thead>
  			  <tbody>
                @foreach($house_categories as $house_category)
					<tr style="color: #980000;">
						<td>
                    {{ $house_category->name }}
                      <!-- <div class="ui blue tiny left pointing basic label pop" data-title="Male Residance" data-variation="wide" data-content="This house_category is for male Students" data-position="top center" >Male</div> -->
                      <!-- <div class="ui pink tiny left pointing basic label pop" data-title="Female Residance" data-variation="wide" data-content="This house_category is for female Students" data-position="top center">Female</div> -->
            </td>
            <td>{{ $house_category->street }}</td>
            
            <td>{{ $house_category->description }}</td>
            <td>
              @if($house_category->allow_editing or OHMS::loggedInUser()->su)
                  <div class="ui btn top icon left pointing dropdown button">
                    <i class="options icon"></i>
                    <div class="menu">
                      <div class="header">{{ trans('ohms.editing_options') }}</div>
                      <a href="{{ route('OHMS::house_categories_edit', ['id' => $house_category->id]) }}" class="item">
                        <i class="edit icon"></i>
                        {{ trans('ohms.house_categories_edit') }}
                      </a>
                    </div>
                  </div>
              @else
                  <div class="ui disabled btn icon button">
                      <i class="lock icon"></i>
                  </div>
              @endif
						</td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection