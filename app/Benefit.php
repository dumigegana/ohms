<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
     public function benefit_records()
    {
        return $this->hasMany('App\Benefit_record');
    }

}
