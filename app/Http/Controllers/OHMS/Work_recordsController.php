<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Work_record;
use App\Staff;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class Work_recordsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.work_records.access');

        # Get all the work_records
        $work_records = Work_record::with('block')->paginate(50);

        # Return the view
        return view('ohms/work_records/index', ['work_records' => $work_records]);
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.work_records.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $work_records = Work_record::with('staff')->paginate(50);
        }
        else {       
          $work_records = Work_record::with('staff', )
                             ->where('changes','like', $post . '%')                   
                             ->orwherehas('staff', function($o) use ($post) {$o->where('su_id', 'like',  $post . '%');})
                             ->orwherehas('staff.user', function($r) use ($post) {$r->where('fullname', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/work_records/search', ['work_records' => $work_records]);
        return $read;
    }
    public function show($id)
    {
        OHMS::permissionToAccess('ohms.work_records.access');

        # Get the work_record
        $work_record = Work_record::find($id);

        # Return the view
        return view('admin/work_records/show', ['work_record' => $work_record]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.work_records.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.work_records.admin');

        # Find the work_record
        $row = Work_record::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'work_records';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/work_records/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.work_records.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.work_records.admin');

        # Find the row
        $row = Work_record::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'work_records';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::work_records')->with('success', trans('ohms.msg_work_record_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.work_records.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.work_records.admin');

        # Get all the data
        $data_index = 'work_records';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/work_records/create', [
            'fields'        =>  $fields,  
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.work_records.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.work_records.admin');

        # create new role
        $row = new Work_record;

        # Save the data
        $data_index = 'work_records';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::work_records')->with('success', trans('ohms.msg_work_record_created'));

    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.work_records.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.work_records.admin');

        # Select Item
        $work_record = OHMS::work_record('id', $id);

        if(!$work_record->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $work_record->delete();

        # Redirect the admin
        return redirect()->route('OHMS::work_records')->with('success', trans('ohms.msg_hostel_deleted'));
    }
     
}
