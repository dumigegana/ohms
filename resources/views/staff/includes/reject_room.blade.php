<div class="modal fade" id="rejectModal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background: #ff8b8b;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: red">&times;</span></button>
            <h4 class="modal-title">Please complite this Form </h4>
          </div>
          <div class="modal-body" style="background: #ffeaea;">
              <div class="row mod_body">
                <input type="hidden"  name="rej_all" value="{{ $allocation->id }}">               
                  <p>Are you sure you want to reject <b> {{ $allocation->room->block->name }} Room {{ $allocation->room->room_number }}</b></p>
              </div>            
          </div>
          <div class="modal-footer" style="background: #ff8b8b;">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary reject_room" data-dismiss="modal">Submit</button>
          </div>
        </div>
      </div>
    </div>  