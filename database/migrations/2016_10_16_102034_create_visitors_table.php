<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('room_id');
            $table->integer('a_flight')->nullable(); 
            $table->integer('r_flight')->nullable(); 
            $table->string('gender');
            $table->date('arrival_date')->nullable();
            $table->date('departure_date')->nullable();
            $table->string('transport');
            $table->boolean('has_accomo')->default(false);
            $table->string('spause');
            $table->timestamps();

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}
