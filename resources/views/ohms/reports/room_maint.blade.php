@extends('layouts.admin.mainR')
@section('page-content')
<div class="container col-md-10 col-md-offset-1" >
  <div class="row">
    <div class="col-md-7" style="padding-top: 0 !important;">{!! $rooms->links() !!}</div>
    <div class="col-md-3" style="padding-top: 16px !important;">
        <form class="navbar-form" role="search" method="Post">
        {{ csrf_field() }}
        <div class="input-group">
          <input type="text" class="form-control" name="{{ $gender }}" placeholder="Search..." id="search">
        </div>
        </form>
    </div>
    <div class="" style="padding-top: 21px; position: absolute; right: 0;"> 
       <button type="button" class="btn" id="print">Print</button>
    </div>
  </div>
  <div class="" style="background-color:#FFF;">
    <div class="container" id="">
      <div class="panel" style="border-color: #ffeaea;">
        <div class="panel-heading" style="background-color:#ffeaea;">
          <center>  <h4 id="title"></i> Student Room List</h4></center>
        </div>
        <div class="panel-body">
       <table class="table table-bordered" " id="myTable">
          <thead>
            <tr>
              <th>Hostel</th>  
              <th>Room number</th>
              <th>Item</th>
              <th>Comments</th>
              <th>Date Reported</th>
              <th>Reported By</th>
          </tr> 
      </thead>
      <tbody>
      @foreach($rooms as $room)
          @foreach($room->maintenances as $maintenance) 
         
        <tr>
        
         <td>{{ $room->block->name }}</td>
          <td>{{ $room->room_number }}</td>
          <td>{{ $maintenance->item->name }}</td>
          <td>{{ $maintenance->comments }} </td>          
          <td>{{ $maintenance->created_at}}</td>
          <td>{{ $maintenance->user->username }}</td>
      </tr>
     @endforeach
      @endforeach
      </tbody>
      </table>  
</div>
    </div>
  </div>  
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function () {
  $("#print").on('click', function () {
    $("#myTable").printThis({
      debug: false,              // show the iframe for debugging
      importCSS: true,           // import page CSS
      printContainer: true,      // grab outer container as well as the contents of the selector
      //loadCSS: "path/to/my.css", // path to additional css file
      pageTitle: $("#title").text(),            // add title to print page
      removeInline: false        // remove all inline styles from print elements
  });
});
  $('#search').on('keyup', function(){
    var kwords = $(this).val();
    var gender = $(this).attr("name");
    $.ajax({
      type : "post",
      url : "{{ url('admin/srchrmment') }}",
      data: {"_token": $('#token').val(),, "kwords" : kwords, "gender" : gender },
      success: function(search_stud){
      $('#myTable').html(search_stud)
      },
    });  
  });
});           
</script>

@endsection