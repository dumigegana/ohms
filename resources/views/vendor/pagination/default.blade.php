<ul class="pagination">
    <!-- Previous Page Link -->
    @if ($paginator->onFirstPage())
    <li class="disabled"><span style="background-color: #990000 !important !important; color:#ffeaea !important;">&laquo;</span></li>
    @else
    <li><a href="{{ $paginator->previousPageUrl() }}" style="background-color: #990000 !important; color:#ffeaea !important;" rel="prev">&laquo;</a></li>
    @endif

    <!-- Pagination Elements -->
    @foreach ($elements as $element)
        <!-- "Three Dots" Separator -->
        @if (is_string($element))
    <li class="disabled"><span>{{ $element }}</span></li>
        @endif

        <!-- Array Of Links -->
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
    <li class="active"><span style="background-color: #ff8b8b !important; color: #980000 !important">{{ $page }}</span></li>
                @else
    <li><a style="background-color: #ffeaea !important; color: #980000 !important" href="{{ $url }}">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif
    @endforeach

    <!-- Next Page Link -->
    @if ($paginator->hasMorePages())
    <li><a href="{{ $paginator->nextPageUrl() }}" style="background-color: #990000 !important; color:#ffeaea !important;" rel="next">&raquo;</a></li>
    @else
    <li class="disabled"><span style="background-color: #990000 !important; color:#ffeaea !important;">&raquo;</span></li>
    @endif
</ul>
