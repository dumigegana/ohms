@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.facilities_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.facilities_title'))
@section('icon', "browser")
@section('subtitle', trans('ohms.facilities_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;"></div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.facilities.admin'))
             <center><a href="{{ route('OHMS::facilities_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_facility') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
  			<table class="ui basic table" style="background: #ffeaea;">
  			  <thead style="background: #ff8b8b;">
  			    <tr>
                <th style="color: #980000;">{{ trans('ohms.name') }}</th>
                <th style="color: #980000;">{{ trans('ohms.options') }}</th>
  			    </tr>
  			  </thead>
  			  <tbody>
                @foreach($facilities as $facility)
					<tr style="color: #980000;">
						<td>
                            <div class="text">
                                {{ $facility->name }}
                               
                            </div>
                        </td>                        
                        <td>
                          @if($facility->allow_editing or OHMS::loggedInUser()->su)
                              <div class="ui btn top icon left pointing dropdown button">
                                <i class="options icon"></i>
                                <div class="menu">
                                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                                  <a href="{{ route('OHMS::facilities_edit', ['id' => $facility->id]) }}" class="item">
                                    <i class="edit icon"></i>
                                    {{ trans('ohms.facilities_edit') }}
                                  </a>
                                  
                                </div>
                              </div>
                          @else
                              <div class="ui disabled btn icon button">
                                  <i class="lock icon"></i>
                              </div>
                          @endif
						</td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
  		</div>
        <br>
  	</div>
  </div>
@endsection
