<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work_record extends Model
{
     public function staff()
    {
        return $this->belongsTo('App\Staff');
    }

}
