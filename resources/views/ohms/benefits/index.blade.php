@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section"> Staff Benefits</div>
    </div>
@endsection
@section('title', "Staff Benefits") 
@section('icon', "bed")
@section('subtitle', "Staff Benefits")
@section('content')
  <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $benefits->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.users.create'))
             <center><a href="{{ route('OHMS::benefits_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add Benefits</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>  
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr>
              <th style="color: #980000;">Benefit</th>
              <th style="color: #980000;">Details</th>
              <th style="color: #980000;">Conditions</th>
              <th style="color: #980000;">Eligibility</th>
              <th style="color: #980000;">Action</th>
            </tr> 
          </thead>
          <tbody id="read">
            @foreach($benefits as $benefit) 
            <tr style="color: #980000;">
              <td>{{ $benefit->name }}</td>
              <td>{{ $benefit->description }}</td>
              <td>{{ $benefit->conditions }}</td>
              <td>{{ $benefit->eligibility }}</td>
              <td>                
                <a href="{{ route('OHMS::benefits_edit', ['id' => $benefit->id]) }}" class="ui btn button">Edit</a>
              </td>       
            </tr>
        @endforeach
          </tbody>
        </table>
        {!! $benefits->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_benefit') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection