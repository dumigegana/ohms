 @foreach($work_records as $work_record) 
            <tr style="color: #980000;">
              <td>{{ $work_record->work_record_number }}</td>
              <td>{{ $work_record->block->hostel->gender }}</td>
              <td>{{ $work_record->block->name }}</td>
              <td>{{ $work_record->capacity }}</td>
              <td class="center alligned">
                      @if ($work_record->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Work_record ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this work_record " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::work_records_edit', ['id' => $work_record->id]) }}" class="ui btn button">{{ trans('ohms.work_records_edit') }}</a>
              </td>       
            </tr>
        @endforeach