@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::maintb') }}">{{ trans('ohms.maintenances_list') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.maintenances_edit_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.maintenances_edit_title'))
@section('icon', "edit")
@section('subtitle', trans('ohms.maintenances_edit_subtitle'))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST">
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column">
                    <div class="ui very padded segment">
                        {{ csrf_field() }}
                         <input name="id" type="hidden" value="$row->id"> 
                         <div class="required field">
                        <label>Room Number</label>
                           <select name="room_id" class="form-control">
                            <option value="" disabled selected>Select Room</option>
                                 @foreach($blocks as $block)
                                <option value="{{Request::old('block_id') ? Request::old('block_id') : isset($row) ? $row->id : "" }}">{{ $row->block->name }} </option>
                                @endforeach
                            </select>
                            <br><br><br>
                        </div>
                        <div class="required field">
                        <label>Select Item to Report </label>
                         <select name="facility_id" class="form-control">
                            <option value="" disabled selected>Select Item</option>
                                 @foreach($items as $item)
                                <option value="{{Request::old('$item->id') ? Request::old('item_id') : isset($row) ? $row->item_id : "" }}">{{ $row->item->name }} </option>
                                @endforeach
                            </select>
                            <br><br><br>
                        </div>
                        v>
                       <br><br/>
                       <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                        </div> 
                   </div>
                </div>
            </div>
        </form>
    </div>
@endsection
