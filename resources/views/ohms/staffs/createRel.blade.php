@extends('layouts.admin.panel')
@section('css')
<link href="{{ URL::to('intlTelInput/build/css/intlTelInput.css') }}" rel="stylesheet">
<style type="text/css">
  #error-msg {
    color: red;
  }
  #valid-msg {
    color: #00C900;
  }
  #error-msg1 {
    color: red;
  }
  #valid-msg1 {
    color: #00C900;
  }
  input.error {
    border: 1px solid #FF7C7C;
  }
</style>
@endsection
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::staffs') }}">{{ trans('ohms.staff_create') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.staffs_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.staffs_create_title'))
@section('icon', "add user")
@section('subtitle', trans('ohms.staffs_create_subtitle'))
@section('content')
<div class="ui one wide column doubling stackable grid container ">
    <div class="ten wide column centered">
        <div class="ui very padded segment" style="background: #ffeaea;">
            <form method="POST" class="ui form">
                {{ csrf_field() }}            
                @include('ohms.forms.master') 
                <br>
                <div class="field">
                    <button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/utils.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#hide").click(function(){
        $("#shw").hide();
    });
    $("#show").click(function(){
        $("#shw").addClass("required field").show();
    });
  });

  var telInput = $("#cell_number");
  $("#error-msg").hide();
  $("#valid-msg").hide();

// initialise plugin
  telInput.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset = function() {
  $("#error-msg").hide();
  $("#valid-msg").hide();
  $("#cell_phone_number").val("");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      $("#error-msg").hide();
      $("#valid-msg").show();
      $("#cell_phone_number").val(1);
    } else {
      telInput.addClass("error");
      $("#valid-msg").hide();
      $("#error-msg").show();
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
// End of Cell phone input
//-----------------------------------------------------------------------------------------------------
$("form").submit(function() {
  $("#hidden").val($("#cell_number").intlTelInput("getNumber"));
  $("#hidden_n").val($("#next_of_kin").intlTelInput("getNumber"));
});

//---------------------------------------------------------------------------------------------------
// Next of kin input Starts
  var telInput1 = $("#next_of_kin");
  $("#error-msg1").hide();
  $("#valid-msg1").hide();

// initialise plugin
  telInput1.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset1 = function() {
  $("#error-msg1").hide();
  $("#valid-msg1").hide();  
  $("#next_of_kin_number").val("");
};

// on blur: validate
telInput1.blur(function() {
  reset1();
  if ($.trim(telInput1.val())) {
    if (telInput1.intlTelInput("isValidNumber")) {
      $("#error-msg1").hide();
      $("#valid-msg1").show();  
      $("#next_of_kin_number").val(1);

    } else {
      telInput1.addClass("error");
      $("#valid-msg1").hide();
      $("#error-msg1").show();

    }
  }
});

// on keyup / change flag: reset
telInput1.on("keyup change", reset1);
 // End of Next of kin contact inputs
 //--------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------- 
// Date selector
/**function call(){
 var kcyear = document.getElementsByName("yr")[0],
  kcmonth = document.getElementsByName("month")[0],
  kcday = document.getElementsByName("day")[0];
       
var d = new Date();
var n = d.getFullYear();
for (var i = n; i >= 1950; i--) {
  var opt = new Option();
  opt.value = opt.text = i;
  kcyear.add(opt);
  }
kcyear.addEventListener("change", validate_date);
kcmonth.addEventListener("change", validate_date);

function validate_date() {
  var y = +kcyear.value, m = kcmonth.value, d = kcday.value;
  if (m === "2")
    var mlength = 28 + (!(y & 3) && ((y % 100) !== 0 || !(y & 15)));
  else var mlength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1];
  kcday.length = 0;
  for (var i = 1; i <= mlength; i++) {
    var opt = new Option();
    opt.value = opt.text = i;
    if (i == d) opt.selected = true;
    kcday.add(opt);
   }
  }
  validate_date();
} **/
  //End of date input.
  //------------------------------------------------------------------------------------------------
 </script>
@endsection
