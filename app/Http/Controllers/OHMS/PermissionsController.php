<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission_Role;
use OHMS;

class PermissionsController extends Controller
{
    public function index()
    {
        OHMS::permissionToAccess('ohms.permissions.access');

    	# Get all the permissions
    	$permissions = OHMS::pagenate(20);

    	# Return the view
    	return view('ohms/permissions/index', ['permissions' => $permissions]);
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.permissions.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.permissions.admin');


        $data_index = 'permissions';
        require('Data/Create/Get.php');

    	# Return the creation view
    	return view('ohms/permissions/create', [
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.permissions.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.permissions.admin');

		# Create the permission
		$row = OHMS::newPermission();
        $data_index = 'permissions';
		require('Data/Create/Save.php');

		# return a redirect
		return redirect()->route('OHMS::permissions')->with('success', trans('ohms.msg_permission_created'));
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.permissions.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.permissions.admin');

    	# Get the permission
    	$row = OHMS::permission('id', $id);

        $data_index = 'permissions';
		require('Data/Edit/Get.php');


    	# Return the view
    	return view('ohms/permissions/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.permissions.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.permissions.admin');

        # Get the permission
    	$row = OHMS::permission('id', $id);

        $data_index = 'permissions';
		require('Data/Edit/Save.php');

		# return a redirect
		return redirect()->route('OHMS::permissions')->with('success', trans('ohms.msg_permission_updated'));
    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.permissions.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.permissions.admin');

    	# Get the permission
    	$perm = OHMS::permission('id', $id);

        # Check if it's su
        if($perm->su) {
            abort(403, trans('ohms.error_security_reasons'));
        }

    	# Delete relationships
    	$rels = Permission_Role::where('permission_id', $perm->id)->get();
    	foreach($rels as $rel) {
    		$rel->delete();
    	}

    	# Delete Permission
    	$perm->delete();

    	# Return a redirect
    	return redirect()->route('OHMS::permissions')->with('success', trans('ohms.msg_permission_deleted'));
    }

}
