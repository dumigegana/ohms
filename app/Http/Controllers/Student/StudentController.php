<?php

namespace App\Http\Controllers\Student;

use App\Http\Requests; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\Student;
use App\User;
use App\Room; 
use App\Block; 
use App\Hostel;
use App\Maintenance;
use App\Facility;
use App\Facility_status;
use App\Check_temp;
use App\Checklist; 
use App\Allocation;
use App\Permission;
use App\Notice;
use App\Item;
use App\Semester;
use Schema;    
use Auth;
use OHMS;
Use DB;
use Carbon\Carbon;


class StudentController extends Controller
{
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */

	/*
	|--------------------------------------------------------------------------
	| Allocations issues
	|--------------------------------------------------------------------------
	|
	*/

 	public function index($student_id, $gender)
	{
		$hostels = Hostel::where('gender', $gender)->get();
		$semesters = Semester::where('checkin', '1')->get();
		$allocation = Allocation::where([['student_id', $student_id], ['check_out', false],])->orderBy('id', 'desc')->first();
		$facilities = Facility::get();
        $statuses = Facility_status::get();
        if ($allocation) {        	
        	$checklists = Check_temp::where('allocation_id', $allocation->id)->get();
        	return view('student.index', [
        	'hostels' => $hostels,
        	'semesters' => $semesters,
        	'facilities' => $facilities,
        	'statuses' => $statuses,
        	'allocation' => $allocation,
        	'checklists' => $checklists,
        	]);
        }
        else{
        	return view('student.index', [
        	'hostels' => $hostels,
        	'semesters' => $semesters,
        	'facilities' => $facilities,
        	'statuses' => $statuses,
        	'allocation' => $allocation,
        	]);        	
        }
        
 	}

	public function getblocks(Request $request)
	{
		$room_list = "";
		$rooms = Room::where([
			['block_id', '=', $request['bl_id']],
			['enable', '1']
			])->whereColumn('capacity', '>', 'occupants')
		->paginate(10);
		$block = Block::find($request['bl_id']);	
		$room_list = view('student/includes/rooms', ['rooms' =>  $rooms, 'block' => $block]);
        return $room_list;
	}

	public function bookin(Request $request)
	{
		$this->validate($request, [
        	'semester_id' => 'required|integer',
        	'room_id' => 'required|integer',
        ]);

		$student_id = Auth::user()->student->id;
		$gender = Auth::user()->student->gender;

		$row = new Allocation;
		$row->room_id = $request['room_id']; 
		$row->student_id = $student_id; 
		$row->semester_id = $request['semester_id'];
		$row->save();

		Room::find($request['room_id'])->increment('occupants');
		Student::where('id', $student_id)->update(['has_room' => true]);
		return redirect()->route('index', ['student_id'=> $student_id, 'gender' =>$gender])->with('success', trans('ohms.msg_allocations_created'));
	}

	public function rejectroom(Request $data)
	{
		$student_id = Auth::user()->student->id;
        $allocation = Allocation::where('id', $data['allocation_id'])->where('student_id', $student_id )->first();
		Check_temp::where('allocation_id', $allocation->id)->delete();
		Room::where('id', $allocation->room_id)->decrement('occupants');
		Student::where('id', $student_id)->update(['has_room' => 0]);
		$allocation->update(['check_out' => 1]);
	}
	
	public function checkin_temp(Request $row){ 
		  Check_temp::updateOrCreate([
		 	'allocation_id' => $row['allocation_id'],
		 	'facility_id' => $row['facility_id'],
		 	'pos' => $row['pos'],
		 	],[ 
		 	'facility_status_id' => $row['facility_status_id']
		 	]);
		
	}

	public function checkin_store(Request $request){
		$allo = Allocation::findOrFail($request['allocation_id']);
		if ($allo->check_out == false) {
			$query = Check_temp::where('allocation_id', $request['allocation_id'])->orderBy('facility_id', 'asc')->get();
			
			foreach ($query as $data) {
				$check = new Checklist;
				$check->allocation_id = $data->allocation_id;
				$check->facility_id = $data->facility_id;
				$check->facility_status_id = $data->facility_status_id;
				$check->in_out = $request['cond'];
				$check->save();
			} 
			Check_temp::where('allocation_id', $request['allocation_id'])->delete();
			if ($request['cond'] == 1) {
				Allocation::where('id', $request['allocation_id'])->increment('approved');
		    	return redirect()->route('dash')->with('success', trans('ohms.msg_check_in')); 
			}
			else {
				$allo = Allocation::where('id', $request['allocation_id'])->first();				
				Room::where('id', $allo->room_id)->decrement('occupants');
				Student::where('id', $allo->student_id)->update(['has_room' => 0]);
				Allocation::where('id', $request['allocation_id'])->update(['check_out' => 1]);
				return redirect()->route('dash')->with('success', trans('ohms.msg_check_out'));
			}
		} else{
			return redirect()->route('dash')->with('error', trans('It seems You have no room!!!!')); 
		}
	}
	/*
	|--------------------------------------------------------------------------
	| Reporting maintenance issues
	|--------------------------------------------------------------------------
	|
	*/

	public function maintenances(){
	   $gender = Auth::user()->student->gender;
	   $hostels = Hostel::where('gender', $gender)->with('blocks.rooms')->get();
       $items = Item::where('type', '!=', 'block')->orderBy('name', 'asc')->get();
       $itembs = Item::where('type', '!=', 'room')->orderBy('name', 'asc')->get();
		return view("student/maintenance", ['items' => $items, 'itembs' => $itembs, 'hostels' => $hostels]);
	}

	public function mainstore(Request $data){
		$this->validate($data, [
            'comments' => 'required|max:120',
            'maintenable_id' => 'required',
            'maintenable_type' => 'required',
            'item_id' => 'required',
        ]);
         Maintenance::updateOrCreate([
        				'maintenable_type' => $data['maintenable_type'],
                        'maintenable_id' => $data['maintenable_id'],
                        'item_id' => $data['item_id'],
                        'fixed' => false
                        ], [
                        'comments' => $data['comments'],
                        'user_id' => Auth::user()->id
                        ]);
		return redirect()->route('dash')->with('success', trans('ohms.msg_maintenance_created'));
	}
	/*
	|--------------------------------------------------------------------------
	| Notices/Dashboard issues
	|--------------------------------------------------------------------------
	|
	*/

	public function dashboard()
	{
		$notices =DB::table('notices')->where('enable', '1')->orderby('id', 'desc')->paginate(2);
		return view('student/dashboard', ['notices' => $notices]);
	}

	public function read($notice_id){
		# Get the notice
        $notice = Notice::find($notice_id);

        # Return the view
        return view('student/notice', ['notice' => $notice]);
	}
}

