<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use Location;
use ohms;

class Session extends Model
{
    protected $table = 'sessions';

    protected $fillable = [
        'name', 'course_id', 'date_time', 'factor', 'subtract', 'minimum','maximum', 
    ];

    public function attendances()
    {
        return $this->hasMany('App\Attendance');
    }

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    
}