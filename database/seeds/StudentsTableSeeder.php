<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Student;
use Illuminate\Support\Facades\DB;


class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         Model::unguard();
        DB::table('students');
        $students = array(
            ['user_id' => '2', 'solusi_id' => 'male', 'gender' => 'Male', 'email' => 'male@tyh.op', 'date_of_birth' => '1991-01-19', 'national_id' => '14587889452', 'home_address' => 'dffgv olk 45', 'next_of_kin' => '5241', 'degree_level' => 'Undergraduate', 'year' => '4', 'major_id' => '5', 'cell_number' => '0216568', 'has_room' => '0' ],
            ['user_id' => '3', 'solusi_id' => 'female', 'gender' => 'Female', 'email' => 'female@tyh.op', 'date_of_birth' => '1991-08-10', 'national_id' => '14587589752', 'home_address' => 'dffgv olk 45', 'next_of_kin' => '5241', 'degree_level' => 'Undergraduate', 'year' => '4', 'major_id' => '9', 'cell_number' => '0386568', 'has_room' => '0' ],
            ['user_id' => '4', 'solusi_id' => 'male1', 'gender' => 'Male', 'email' => 'male1@tyh.op', 'date_of_birth' => '1995-03-09', 'national_id' => '14587589492', 'home_address' => 'dffgv olk 45', 'next_of_kin' => '5241', 'degree_level' => 'Undergraduate', 'year' => '1', 'major_id' => '3', 'cell_number' => '0216568', 'has_room' => '0' ],
            ['user_id' => '5', 'solusi_id' => 'female1', 'gender' => 'Female', 'email' => 'female1@tyh.op', 'date_of_birth' => '1995-09-22', 'national_id' => '28587589492', 'home_address' => 'dffgv olk 45', 'next_of_kin' => '5241', 'degree_level' => 'Undergraduate', 'year' => '1', 'major_id' => '6', 'cell_number' => '0216568', 'has_room' => '0' ],

        );

        // Loop through each student above and create the record for them in the database
        foreach ($students as $student)
        {
            Student::create($student);
        }
        Model::reguard();
    }
}
 