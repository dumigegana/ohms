@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::items') }}">{{ trans('ohms.items_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.items_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.items_create_title'))
@section('icon', "plus")
@section('subtitle', trans('ohms.items_create_subtitle'))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST">
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column centered">
                    <div class="ui very padded segment" style="background: #ffeaea;">
                         {{ csrf_field() }}
                       <div class="required field">
                            <label style="color: #980000;">Item Name</label>
                            <input name="name" type="text" placeholder="Item" value="{{ old('name')}}" required>
                        </div> 
                        <div class="required field">
                            <label style="color: #980000;">Maintnance Department</label>
                            <select name="maint_dep_id" class="form-control" required>
                                <option value="">Department</option>
                                 @foreach($maint_deps as $dep)
                                <option value="{{ $dep->id }}" {{ (old("maint_dep_id") == $dep->id ? "selected":"") }}>{{ $dep->name }}</option>
                                 @endforeach
                            </select>
                        </div>
                        <div class="required field">
                            <label style="color: #980000;">Item for:</label>
                            <select name="type" class="form-control" required>
                                <option value="">Select</option>
                                <option value="room" {{ (old("type") == 'room' ? "selected":"") }}>Rooms (interior)</option>
                                <option value="block" {{ (old("type") == 'block' ? "selected":"") }}>Hostel Blocks</option>
                                <option value="both" {{ (old("type") == 'both' ? "selected":"") }}>Both</option>
                            </select>
                        </div>
                        <div class="field">
                            <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
