<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccademicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accademics', function (Blueprint $table) {
            $table->bigIncrements('id');                        
            $table->bigInteger('staff_id');            
            $table->string('name');
            $table->string('grade');
            $table->string('cert-no');
            $table->string('cert_file');
            $table->string('trans-no');
            $table->string('trans_file');
            $table->string('year');
            $table->string('institution');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accademics');
    }
}
