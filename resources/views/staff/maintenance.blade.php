@extends('student.master')

@section('main-contant')
  <div style="padding-left: 0; max-width: 100;">
   <div style="height: 0.5cm" class="rwd-break"></div>
     <div class="col-xs-12 panel" style="background-color: #950000; height: 2.8cm; padding-left: 0;">
      <center> <h1 style="color:#ffeaea;">Report Maintenance Issues</h1></center>
    </div>
 
  <div class="main_index col-xs-12 " style="padding: 15px;">
    <div class="row" >
      <div class="panel col-sm-6" style="background-color: #ffeaea; border-color: #990000;"> <br>
        <p><h4><center>Use this form to report Issues within a room </center></h4></p><br>
        <div class="panel" style="background-color: #ffeaea; border-color: #ff8b8b; padding: 30px 50px 7px 50px;">
          <form class="form" method="POST" action="{{ route('store_report') }}">
              {{ csrf_field() }}
            <input name="maintenable_type" type="hidden" value="App\Room">
            <div class="form-group">
              <label>Room</label>
              <select name="maintenable_id" class="form-control">
              <option value="" disabled selected>Room </option>
                 @foreach($hostels as $hostel)
                 @foreach($hostel->blocks as $block)
                 @foreach($block->rooms as $room)
                <option value="{{ $room->id }}">{{ $room->block->name }}  {{ $room->room_number }} </option>
                @endforeach
                @endforeach
                @endforeach         
              </select>
            </div> 
            <div class="form-group">
              <label>Select Item to Report </label>
              <select name="item_id" class="form-control">
              <option value="" disabled selected>Item</option>
                @foreach($items as $item)
                <option value="{{ $item->id }}">{{ $item->name }} </option>
                   @endforeach
              </select>
            </div>                   
            <div class="form-group">
              <label>Comments</label>
              <textarea name="comments" type="text" placeholder="Comments..." class="form-control"></textarea>
            </div>
            <div class="form-group">                                                  
              <center><button type="submit" class="btn">{{ trans('ohms.submit') }}</button></center>
            </div>
          </form>
        </div>
      </div>
      <div class="panel col-sm-6" style="background-color: #ffeaea; border-color: #990000;"> <br>
        <p><h4><center>Use this form to report Issues around the Hostel </center></h4></p><br>
        <div class="panel" style="background-color: #ffeaea; border-color: #ff8b8b; padding: 30px 50px 7px 50px;">
          <form class="form" method="POST" action="{{ route('store_report') }}">
              {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
            <input name="maintenable_type" type="hidden" value="App\Block">
            <div class="form-group">
              <label>Hostel</label>
              <select name="maintenable_id" class="form-control">
              <option value="" disabled selected>Hostel</option>
                 @foreach($hostels as $hostel)
                 @foreach($hostel->blocks as $block)
                <option value="{{ $block->id }}">{{ $block->name }} </option>
                @endforeach
                @endforeach         
              </select>
            </div> 
            <div class="form-group">
              <label>Select Item to Report </label>
              <select name="item_id" class="form-control">
              <option value="" disabled selected>Item</option>
                @foreach($itembs as $itemb)
                <option value="{{ $itemb->id }}">{{ $itemb->name }} </option>
                   @endforeach
              </select>
            </div>                   
            <div class="form-group">
              <label>Comments</label>
              <textarea name="comments" type="text" placeholder="Comments" class="form-control"></textarea>
            </div>
            <div class="form-group">                                                  
              <center><button type="submit" class="btn">{{ trans('ohms.submit') }}</button></center>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
