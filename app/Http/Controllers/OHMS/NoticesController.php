<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Notice;
use App\Hostel;
use App\Category;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use OHMS;

class NoticesController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.notices.access');

        # Get all the notices
        $notices = Notice::orderby('id', 'desc')->paginate(50);

                # Return the view
        return view('ohms/notices/index', ['notices' => $notices]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.notices.access');

        # Get the notice
        $notice = Notice::find($id);

        # Return the view
        return view('ohms/notices/show', ['notice' => $notice]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.notices.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.notices.admin');

        # Find the notice
        $row = Notice::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'notices';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/notices/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.notices.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.notices.admin');

        # Find the row
        $row = Notice::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'notices';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::notices')->with('success', trans('ohms.msg_notice_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.notices.access');

        OHMS::permissionToAccess('ohms.notices.admin');

       
        # Return the view
        return view('ohms/notices/create');
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.notices.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.notices.admin');

        # create new role
        $row = new Notice;

        # Save the data
        $data_index = 'notices';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::notices')->with('success', trans('ohms.msg_notice_created'));

    }
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.notices.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.notices.admin');

        # Select Item
        $notice = OHMS::notice('id', $id);

        if(!$notice->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $notice->delete();

        # Redirect the admin
        return redirect()->route('OHMS::notices')->with('success', trans('ohms.msg_hostel_deleted'));
    }
}
