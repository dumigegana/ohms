<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    public function house_category() 
    {
        return $this->belongsTo('App\House_category');
    }

     public function house_allocations() 
    {
        return $this->HasMany('App\House_allocation');
    }
}


