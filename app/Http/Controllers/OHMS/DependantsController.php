<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Dependant;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class DependantsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.dependants.access');

        # Get all the dependants
        $dependants = Dependant::with('block')->paginate(50);

        # Return the view
        return view('ohms/dependants/index', ['dependants' => $dependants]);
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.dependants.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $dependants = Dependant::with('staff')->paginate(100);
        }
        else {       
          $dependants = Dependant::with('staff', 'staff.user')
                             ->where('name','like', $post . '%')                   
                             ->orwherehas('block', function($o) use ($post) {$o->where('name', 'like',  $post . '%');})
                             ->orwherehas('staff.user', function($r) use ($post) {$r->where('fullname', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/dependants/search', ['dependants' => $dependants]);
        return $read;
    }
    public function show($id)
    {
        OHMS::permissionToAccess('ohms.dependants.access');

        # Get the dependant
        $dependant = Dependant::find($id);

        # Return the view
        return view('admin/dependants/show', ['dependant' => $dependant]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.dependants.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.dependants.admin');

        # Find the dependant
        $row = Dependant::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'dependants';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/dependants/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.dependants.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.dependants.admin');

        # Find the row
        $row = Dependant::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'dependants';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::dependants')->with('success', trans('ohms.msg_dependant_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.dependants.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.dependants.admin');
        # Get all the data
        $data_index = 'dependants';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/dependants/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.dependants.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.dependants.create');

        # create new role
        $row = new Dependant;

        # Save the data
        $data_index = 'dependants';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::dependants')->with('success', trans('ohms.msg_dependant_created'));

    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.dependants.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.dependants.admin');

        # Select Item
        $dependant = OHMS::dependant('id', $id);

        if(!$dependant->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $dependant->delete();

        # Redirect the admin
        return redirect()->route('OHMS::dependants')->with('success', trans('ohms.msg_hostel_deleted'));
    }
     
}
