@foreach($students as $student)
<tr style="color: #980000;">
						<td>{{ $student->solusi_id }}</td>
    				<td>
                        
                        {{ $student->user->full_name }}
                    </td>
                   
                    <td>
                        
                        {{ $student->major->name }}
                    </td>
                    <td>
                        
                        {{ $student->gender }}
                    </td>
                    <td class="center alligned">
                      @if ($student->user->active == 1)
                        <div class="ui green tiny basic label pop" data-title="Active" data-variation="wide" data-content="Active user account" data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Disabled" data-variation="wide" data-content="Deactivated user account" data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
    				
                <td > 
                  <div class="ui btn button">
                    <i class="edit icon"></i>
                    <a href="{{ route('OHMS::students_edit', ['id' => $student->id ]) }}" class="item" style="color: #ffeaea;">
                      {{ trans('ohms.students_edit') }}
                    </a>
                </div> 
						</td>
					</tr>
          @endforeach