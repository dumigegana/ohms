<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
    
     $facilities = [
     'Bulb',
     'Airvents',
     'Woodrope',
     'Curtains',
     'Door',
     'Paint (Interior)',
     'Power Socket',
     'Window',
     'Chair',
     'Study Table',
     'Bed',
     'Ceiling',
     'Switch',
     'Broom',
     'Dust bin',
        ];

        foreach($facilities as $facility) {
            $facie = \OHMS::newFacility();
            $facie->name = $facility;
            $facie->save();
        }
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facilities');
    }
}
