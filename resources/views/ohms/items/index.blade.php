@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.items_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.items_title'))
@section('icon', "configure")
@section('subtitle', trans('ohms.items_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;"></div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.items.admin'))
             <center><a href="{{ route('OHMS::items_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_item') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
  			<table class="ui basic table ">
  			  <thead style="background: #ff8b8b; color: #980000;">
  			    <tr>
                  <th style="color: #980000;">Name</th>
                  <th style="color: #980000;">Department</th>
                  <th style="color: #980000;">Type</th>
                  <th style="color: #980000;">Options</th>
  			    </tr>
  			  </thead>
  			  <tbody>
                @foreach($items as $item)
					<tr style="color: #980000;">
						<td>
              <div class="text">
                {{ $item->name }}
              </div>
            </td>
            <td>{{ $item->maint_dep->name }}</td>
            <td>{{ $item->type }}</td>
            <td>
              @if($item->allow_editing or OHMS::loggedInUser()->su)
              <div class="ui btn top icon left pointing dropdown button">
                <i class="options icon"></i>
                <div class="menu">
                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                  <a href="{{ route('OHMS::items_edit', ['id' => $item->id]) }}" class="item">
                    <i class="edit icon"></i>
                    {{ trans('ohms.items_edit') }}
                 </a>
                </div>
             </div>
             @else
                <div class="ui disabled btn icon button">
                  <i class="lock icon"></i>
                </div>
              @endif
						</td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
  		</div>
        <br>
  	</div>
  </div>
@endsection
