@extends('layouts.admin.panel')
@section('css')
<link href="{{ URL::to('intlTelInput/build/css/intlTelInput.min.css') }}" rel="stylesheet">
<style type="text/css">
  #error-msg {
    color: red;
  }
  #valid-msg {
    color: #00C900;
  }
  #error-msg1 {
    color: red;
  }
  #valid-msg1 {
    color: #00C900;
  }
  #cell_number{
    text-indent: 30px;
  }
  #next_of_kin{
    text-indent: 30px;
  }
  input.error {
    border: 1px solid #FF7C7C;
  }
</style>
@endsection
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::staffs') }}">Staff List</a>
        <i class="right angle icon divider"></i>
        <div class="active section">Create Staff Account</div>
    </div>
@endsection
@section('title', "Staff Account")
@section('icon', "add user")
@section('subtitle', "Create Staff")
@section('content')
<div class="ui container">
  <form id="create" class="ui form" method="POST">
    <div class="ui doubling stackable grid">
      <div class="row">
        <div class="eight wide column">
          <div class="ui very padded segment" style="background: #ffeaea">
                     {{ csrf_field() }}  
            <div class="ui equal width form">
              <div class="fields">
                <div class="required field four wide column">
                  <label style="color: #980000;">Title</label>
                  <select name="title" class="form-control" required>
                    <option value="Mr" {{ (old("title") == 'Mr' ? "selected":"") }}> Mr</option>
                    <option value="Mrs" {{ (old("title") == 'Mrs' ? "selected":"") }}>Mrs</option>
                    
                  </select>
                </div>                              
            
                <div class="required field">
                    <label style="color: #980000;">Full Name</label>
                    <input name='full_name' type="text" value="{{ old('full_name')}}" required>
                </div>
              </div>
            </div>    
            <div class="required field">
              <label style="color: #980000;">Username </label>
                <input name='username' type="text" value="{{ old('username')}}" required>
            </div>
            <div class="required field">
              <label style="color: #980000;">Personal Email</label>
                <input name='email' type="email" value="{{ old('email')}}" required>
            </div>
            <div class="required field">
              <label style="color: #980000;">Password</label>
                <input name='password' type="password" placeholder="Password">
            </div>
            <div class="required field">
            <label style="color: #980000;">Confirm Password</label>
                <input name='password_confirmation' type="password" placeholder="Confirm Password">
            </div>
                 <div>
                <input name='role_id' type="hidden" value="5" >
            </div>
            
            <div class="required field">
            <label style="color: #980000;">Date of Birth</label>
            <input name='date_of_birth' type="date" placeholder="">
            </div> 

            <div class="ui equal width form">
              <div class="fields">                         
                <div class="required field">
                <label style="color: #980000;">Cell number</label>
                <input type="hidden" value="" name="cell_phone_number" id="cell_phone_number">
                <input id="cell_number" name='cell' type="tel" value="{{ old('cell')}}" required>
                <input id="hidden" type="hidden" name="cell_number"><br>
                <span id="valid-msg" class="hide">✓ Valid</span>
                <span id="error-msg" class="hide">&times; Invalid number</span>
                </div>
                
                <div class="required field">
                <label style="color: #980000;">Next of Kin contact number</label>
                <input type="hidden" value="" name="next_of_kin" id="next_of_kin_number">
                <input id="next_of_kin" name='next' type="tel" value="{{ old('next')}}" required>
                <input id="hidden_n" type="hidden" name="next_of_kin_number"><br>
                <span id="valid-msg1" class="hide">✓ Valid</span>
                <span id="error-msg1" class="hide">&times; Invalid number</span>
                </div>
               </div>
            </div>                                                                         
          </div>
        </div>

        <div class="eight wide column">
          <div class="ui very padded segment" style="background: #ffeaea">
            <div class="required field">
              <label style="color: #980000;">Next of kin</label>
              <input name='next_kin' type="text" value="{{ old('next_kin')}}" required>
            </div>                                            
            <div class="required field">
              <label style="color: #980000;">Home Address</label>
              <input name='home_address' type="text" value="{{ old('home_address')}}" required>
            </div>
            <div class="required field">
              <label style="color: #980000;">National ID number</label>
              <input name='national_id' type="text" value="{{ old('national_id')}}" required>
            </div>
            <div class="required field">
              <label style="color: #980000;">Department</label>
              <select name="department_id" class="form-control" required>
                <option value=""> Select department</option>
                @foreach($departments as $department)
               <option value="{{ $department->id }}" {{ (old("department_id") == '$department->id' ? "selected":"") }}>{{ $department->name }}</option>
                 @endforeach
              </select>
            </div>
            <br/>
                 
            <div class="ui equal width form">
              <div class="fields">
                <div class="required field">
                <label style="color: #980000;">Gender</label>
                <select name="gender" class="form-control" required>
                  <option value="Male" {{ (old("gender") == 'Male' ? "selected":"") }}> Male</option>
                  <option value="Female" {{ (old("gender") == 'Female' ? "selected":"") }}>Female</option>
                </select>
              </div>
                <div class="required field" >
                  <div style="padding-left: 50px;">
                  <label style="color: #980000;">Marital Status <br></label>
                  <input type="radio" name="marital" value="Single" {{ (old("marital") == 'Single' ? "checked":"") }}> Single<br>
                  <input type="radio" name="marital" value="Maried" {{ (old("marital") == 'Maried' ? "checked":"") }}> Maried<br>
                </div></div>
                <br><br>
                 </div>
                 <br>
               </div>
            <div class="ui equal width form">
              <div class="fields"> 
                <div class="required field">
                  <label style="color: #980000;">Employment Status</label>
                  <select name="status" class="form-control" required>
                    <option value="Serving" {{ (old("status") == 'Serving' ? "selected":"") }}>Currently Serving</option>
                   <option value="Suspended" {{ (old("status") == 'Suspended' ? "selected":"") }}>Suspended</option>
                   <option value="Retired" {{ (old("status") == 'Retired' ? "selected":"") }}>Retired</option>
                   <option value="on Leave" {{ (old("status") == 'On Leave' ? "selected":"") }}>On Leave</option>
                   <option value="Expelled" {{ (old("status") == 'Expelled' ? "selected":"") }}>Expelled</option>
                  </select>
                </div>
                <div class="inline field" >
                  <div class="ui slider checkbox" style="float: right;">
                    <input type="checkbox" value="1" class="hidden" name="active" checked>
                    <label style="color: #980000 !important;">Activate Staff account</label>
                  </div>
                </div>

                
                <br/>
              </div>
            </div>
            <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
          </div>
          <input type="hidden" name="banned" value="0"> 
        </div>
      </div>
    </div> 
  </form>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/intlTelInput-jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/utils.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
   $("#shw").hide();
    $("#hide").click(function(){
        $("#shw").hide();
    });
    $("#show").click(function(){
        $("#shw").addClass("required field").show();
    });
  });

  var telInput = $("#cell_number");
  $("#error-msg").hide();
  $("#valid-msg").hide();

// initialise plugin
  telInput.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset = function() {
  $("#error-msg").hide();
  $("#valid-msg").hide();
  $("#cell_phone_number").val("");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      $("#error-msg").hide();
      $("#valid-msg").show();
      $("#cell_phone_number").val(1);
    } else {
      telInput.addClass("error");
      $("#valid-msg").hide();
      $("#error-msg").show();
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
// End of Cell phone input
//-----------------------------------------------------------------------------------------------------
$("form").submit(function() {
  $("#hidden").val($("#cell_number").intlTelInput("getNumber"));
  $("#hidden_n").val($("#next_of_kin").intlTelInput("getNumber"));
});

//---------------------------------------------------------------------------------------------------
// Next of kin input Starts
  var telInput1 = $("#next_of_kin");
  $("#error-msg1").hide();
  $("#valid-msg1").hide();

// initialise plugin
  telInput1.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset1 = function() {
  $("#error-msg1").hide();
  $("#valid-msg1").hide();  
  $("#next_of_kin_number").val("");
};

// on blur: validate
telInput1.blur(function() {
  reset1();
  if ($.trim(telInput1.val())) {
    if (telInput1.intlTelInput("isValidNumber")) {
      $("#error-msg1").hide();
      $("#valid-msg1").show();  
      $("#next_of_kin_number").val(1);

    } else {
      telInput1.addClass("error");
      $("#valid-msg1").hide();
      $("#error-msg1").show();

    }
  }
});

// on keyup / change flag: reset
telInput1.on("keyup change", reset1);
 // End of Next of kin contact inputs
 //--------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------- 
// Date selector
/**function call(){
 var kcyear = document.getElementsByName("yr")[0],
  kcmonth = document.getElementsByName("month")[0],
  kcday = document.getElementsByName("day")[0];
       
var d = new Date();
var n = d.getFullYear();
for (var i = n; i >= 1950; i--) {
  var opt = new Option();
  opt.value = opt.text = i;
  kcyear.add(opt);
  }
kcyear.addEventListener("change", validate_date);
kcmonth.addEventListener("change", validate_date);

function validate_date() {
  var y = +kcyear.value, m = kcmonth.value, d = kcday.value;
  if (m === "2")
    var mlength = 28 + (!(y & 3) && ((y % 100) !== 0 || !(y & 15)));
  else var mlength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1];
  kcday.length = 0;
  for (var i = 1; i <= mlength; i++) {
    var opt = new Option();
    opt.value = opt.text = i;
    if (i == d) opt.selected = true;
    kcday.add(opt);
   }
  }
  validate_date();
} **/
  //End of date input.
  //------------------------------------------------------------------------------------------------
 </script>
@endsection
