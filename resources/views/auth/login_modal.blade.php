
<!-- Modal -->
<div class="modal fade" id="login" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!-- <div class="modal-content">      
      <div class="modal-body"> -->
        <div class="loginSection" >
    <div class="header" style="text-align: center;">Login 
      <button type="button" style="color: #FFF;" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" >&times;</span></button>
    </div>
      <div class="innerBox">
        <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}        

        <input name='username' type="text" placeholder="Username" value="{{ old('username')}}" required>
        <input name='password' type="password" placeholder="{{ trans('ohms.password') }}" required>
        <br>
        <button  class="btn btn-raised" style="position: relative; left: 45%;">{{ trans('ohms.submit') }}</button>
        <br>
        <ul>
        <li style="position: relative;left: 10%;"><a data-toggle="modal" href="#confirm" id="reset" onclick="$('#login').one('hidden.bs.modal', function() { $('#confirm').modal('show'); }).modal('hide');">{{ trans('ohms.forgot_password') }}</a></li>
        <li style="position: relative;left: 10%;"><a data-toggle="modal" href="#confirm" onclick="$('#login').one('hidden.bs.modal', function() { $('#confirm').modal('show'); }).modal('hide');">Create account </a></li>
       </ul>
       </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

<!-- -------------- Second Modal ---------------------------- -->

<div class="modal fade" id="confirm" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!-- <div class="modal-content">      
      <div class="modal-body"> -->
        <div class="loginSection" >
    <div class="header" style="text-align: center;">Enter Your Student ID 
      <button type="button" style="color: #FFF;" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" >&times;</span></button>
    </div>
      <div class="innerBox">
        <form method="POST" id="conf" action="{{ route('creat') }}" name="{{ route('recovr') }}">
        {{ csrf_field() }} 
        
        <input name='student_id' type="text" placeholder="Student ID number" size="40" required>
        <br>
                   
        <button  class="btn btn-raised" style="position: relative; left: 45%;">{{ trans('ohms.submit') }}</button>
       </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>