<?php

namespace App\Http\Controllers\Student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
// use App\Http\Requests\Post_Caller;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use OHMS;
use App\Student;
use App\User;
use App\Role;
use App\Attendance;
use App\Session;
use App\Course;
use Carbon\Carbon;


class AttendancesController extends Controller
{
   
    public function attendance()
    { 
        if(OHMS::loggedInUser()->isStudent()){
            $student_id = Auth::user()->student->id;
            $courses = DB::table('courses')
            ->join('Course_Student','courses.id', '=','Course_Student.course_id' )
            ->where('Course_Student.student_id', $student_id)
            ->select('courses.name as cname', 'sessions.*')
            ->join('sessions', 'courses.id', '=', 'sessions.course_id')            
            ->whereBetween('sessions.date_time', [Carbon::now()->subMinute(300), Carbon::now()])
            ->first();


               if($courses) {
                    return view('student.attendance', ['courses' => $courses]);               
                } else{
                        return view('student.notClassTime');
                } 

        
        }else{
            return redirect()->route('logout')->with('error', trans('You are not a student!')); 
        }
    }

    public function store(Request $request)
    {
                       
      // $this->validate($request, [
      //       'code' => 'required|integer',
      //       // 'session_id' => 'required|integer',
      //   ]);

       $clientIP = request()->ip();

       $student_id = Auth::user()->student->id;
       $dublicate = Attendance::where('code', $request['code'])
                              ->where('session_id', $request['session_id'])
                              ->get();

        if (!$dublicate) {
           $dub = Attendance::where('student_id', $student_id)
                            ->where('session_id', $request['session_id'])
                            ->get();
            if (!$dub){
                $row = new Attendance;
                $row->code = $request['code']; 
                $row->session_id = $request['session_id']; 
                $row->student_id = $student_id; 
                $row->ip = $clientIP;
                $row->mark = 1;
                $row->save();

                # Return the Student to the attendances page with a success message
                return redirect('/')->with('success', trans('Attendance marked succesifully')); 
                } 
                # Return the Student to the attendances page with a success message
            return redirect('/')->with('errors', trans('Attendance already existing!'));
            } 
        # Return the Student to the attendances page with a success message
        return redirect('/')->with('errors', trans('Dublicate Records not allowed!'));           
    }

   public function attendances()
    {
        # Find The Session
     //   $attendances = Attendance::with('student')->where('session_id', $id)->orderby('code')->get(); 
//        $session = Session::find($id)->first();      

        $student_id = Auth::user()->student->id;
        $solusi_id = Auth::user()->student->solusi_id;
        $courses = Course::with(['students' => function ($query) use ($solusi_id) {
                             $query->where('solusi_id', $solusi_id);
                            }])->get();

        return view('student/attendances', ['courses' => $courses]);
    }

    public function getattendances(Request $request)
    {
         $student_id = Auth::user()->student->id;
        $attend_list = "";
        $attendances = DB::table('courses') 
            ->join('Course_Student','courses.id', '=','Course_Student.course_id' )
            ->join('sessions', 'courses.id', '=', 'sessions.course_id')            
            ->Join('attendances', 'sessions.id', '=', 'attendances.session_id') 
            ->select('courses.name as cname', 'sessions.name as sname', 'attendances.*')      
            ->where('courses.id', $request['course_id'])
            ->where('attendances.student_id', $student_id)
            ->get();
               
        $attend_list = view('student/includes/attendances', ['attendances' =>  $attendances]);
        return $attend_list;
            //return view('ohms/student/tast', ['attendances'=>  $attendances]);
    }

   public function sessions($id)
    {
        OHMS::permissionToAccess('ohms.attendances.access');

        $attendances = OHMS::attendances('session_id',$id);

        return view('ohms/attendances/index', ['attendances'=>  $attendances]);
    }

 // public function attendances($solusi_id, $shortcode) {

 //        # Check permissions
 //        OHMS::permissionToAccess('ohms.attendances.create');

 //        $user = User::where(['username', $username])->first();
 //        Auth::login($user);

 //         # Get all the data
 //        $data_index = 'attendances';
 //        require('Data/Create/Get.php');

 //        # Return the view
 //        return view('/mark_me', [
 //            'attendance'      =>  $attendance,
 //            'fields'    =>  $fields,
 //            'confirmed' =>  $confirmed,
 //            'encrypted' =>  $encrypted,
 //            'hashed'    =>  $hashed,
 //            'masked'    =>  $masked,
 //            'table'     =>  $table,
 //            'code'      =>  $code,
 //            'wysiwyg'   =>  $wysiwyg,
 //            'relations' =>  $relations,
 //        ]);
    // }


    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.attendances.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.attendances.edit');

        
        $attendance = OHMS::Attendance('id', $id);
        if($attendance) {
            $row = $attendance;
            $data_index = 'attendances';
            require('Data/Edit/Get.php');

            return view('ohms/attendances/edit', [
                'attendance'      =>  $attendance,
                'row'       =>  $row,
                'fields'    =>  $fields,
                'confirmed' =>  $confirmed,
                'encrypted' =>  $encrypted,
                'hashed'    =>  $hashed,
                'masked'    =>  $masked,
                'table'     =>  $table,
                'code'      =>  $code,
                'wysiwyg'   =>  $wysiwyg,
                'relations' =>  $relations,
            ]);

        } else {
            abort(404);
        }
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.attendances.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.attendances.edit');

        
        $attendance = OHMS::Attendance('id', $id);

        if($attendance) {

            $row = $attendance;
            $data_index = 'attendances';
            require('Data/Edit/Save.php');

            return redirect()->route('OHMS::attendances')->with('success', trans('ohms.msg_attendance_created'));

        } else {
            abort(404);
        }
    }

    public function delete($id)
    {
        OHMS::permissionToAccess('ohms.attendances.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.attendances.delete');

        # Delete the attendance
        OHMS::Attendance('id', $id)->delete();

        return redirect()->route('OHMS::attendances')->with('success', trans('ohms.msg_attendance_deleted'));
    }
}
