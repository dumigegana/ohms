<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>OHMS</title>
    <meta name="description" content="OHMS - Laravel administration panel">
    <meta name="author" content="OHMSsani Ndlovu">
    <link rel="stylesheet" type="text/css" href="{{ asset('/ohms_public/css/w3.css') }}"> 
    
    <link rel="shortcut icon" href="{{ OHMS::ohmsFavicon() }}" >
    <link rel='stylesheet' type='text/css' href="{{ asset('ohms_public/sweetalert/sweetalert.css') }}">
    <script src="{{ asset('/ohms_public/sweetalert/sweetalert.min.js') }}"></script> 
 
    <style>
            .light {color: #6b0000; background-color: #ffefef }
            .theme_light {color: #6b0000; background-color: #ffeaea }
            .theme_dark {color: #ffeaea!important; background-color: #6b0000!important; }
            .theme_mild {color: #980000!important; background-color: #ffb8b8!important}
            .theme_light_hover {color: #980000; background-color: #ffeaea }
            .theme_paginator {color: #980000; background-color: #ffeaea }
            .theme_dark_hover {color: #ffeaea!important; background-color: #6b0000; }
            .theme_mild_hover {color: #980000!important; background-color: #ffefef!important}
            .theme_light_hover:hover {color: #980000!important; background-color: #ffb8b8!important; }
            .theme_dark_hover:hover {color: #6b0000!important; background-color: #ffeaea  }
            .theme_paginator:hover {color: #ffeaea!important; background-color: #980000  }
            .theme_mild_hover:hover {color: #ffeaea; background-color: #6b0000;}

            .w3-red,.w3-hover-red:hover{color:#fff7f7!important;background-color:#f44336!important}
            .theme {color: #ffeaea!important; background-color: #980000!important }
            .text_theme {color: #980000 }
            .text_themeD {color: #000 }
            .border_theme {border-color: #980000 }

            .hover_theme:hover {color: #ffeaea; background-color: #980000 }
            .hover_text_theme:hover {color: #980000 }
            .hover_border_theme:hover {border-color: #980000 }
            .margn { margin: 8px 15px; color:#ffeaea;}

          .side_drop {
            margin-left: 30px;
          }

          .padding {
            padding:30px 30px;
          }
          .pad {
            padding-right: 25%;
            padding-left: 25%;
          }

          hr {
                display: block;
                height: 1px;
                border: 0;
                border-top: 1px solid #dfdfdf;
                margin: 0;
                padding: 0;
            }

          img.resize {
                max-width:50%;
                max-height:50%;
              }
          /* Page content. The value of the margin-left property should match the value of the sidebar's width property */
           body{            
            background:#ffb8b8;
          }
          .main {
            margin-left: 200px;
            padding: 50px;
          }
          .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 10px;
          }

          .switch input { 
            opacity: 0;
            width: 0;
            height: 0;
          }

          .slider-off {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
          }

          .slider-off:before { 
            position: absolute;
            content: "";
            height: 9px;
            width: 9px;
            left: 1px;
            bottom: 1px;
            color: #ccc;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
          }
          .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
          }

          .slider:before {
            position: absolute;
            content: "";
            height: 9px;
            width: 9px;
            left: 1px;
            bottom: 1px;
            color: #980000;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
          }

          input:checked + .slider {
            background-color: #980000;
          }

          input:focus + .slider {
            box-shadow: 0 0 1px #980000;
          }

          input:checked + .slider:before {
            -webkit-transform: translateX(49px);
            -ms-transform: translateX(49px);
            transform: translateX(49px);
          }

          /* Rounded sliders */
          .slider.round {
            border-radius: 34px;
          }

          .slider.round:before {
            border-radius: 50%;
          }

          input:checked + .slider-off {
            background-color: #ccc;
          }

          input:focus + .slider-off {
            box-shadow: 0 0 1px #ccc;
          }

          input:checked + .slider-off:before {
            -webkit-transform: translateX(49px);
            -ms-transform: translateX(49px);
            transform: translateX(49px);
          }

          /* Rounded slider-offs */
          .slider-off.round {
            border-radius: 34px;
          }

          .slider-off.round:before {
            border-radius: 50%;
          }
          .label {
            position:absolute;
            left:70px;
            bottom:-3px;
            width: 200px
          }
          .tooltip .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: white;
            color: #980000;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
            z-index: 1;
            top: -5px;
            left: 60%;
          }

          .tooltip .tooltiptext::after {
            content: "";
            position: absolute;
            top: 50%;
            right: 100%;
            margin-top: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: transparent white transparent transparent;
          }
          .tooltip:hover .tooltiptext {
            visibility: visible;
          }
           @yield('css')

    </style>
  </head>
<body>

  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
  </form>

    @if(session('success'))
      <script>
        swal({
          title: "Nice!",
          text: "{!! session('success') !!}",
          type: "success",
          confirmButtonText: "Cool"
          confirmButtonColor: "#980000"
        });
      </script>
    @endif
    @if(session('error'))
      <script>
        swal({
          title: "Whops!",
          text: "{!! session('error') !!}",
          type: "error",
          confirmButtonText: "Okai"
          confirmButtonColor: "#980000"
        });
      </script>
    @endif
    @if(session('warning'))
      <script>
        swal({
          title: "Watch out!",
          text: "{!! session('warning') !!}",
          type: "warning",
          confirmButtonText: "Okai"
          confirmButtonColor: "#980000"
        });
      </script>
    @endif
    @if(session('info'))
      <script>
        swal({
          title: "Watch out!",
          text: "{!! session('info') !!}",
          type: "info",
          confirmButtonText: "{{ trans('ohms.okai') }}"
          confirmButtonColor: "#980000"
        });
      </script>
    @endif
    @if (count($errors) > 0)
      <script>
        swal({
          title: "Whops!",
          text: "<?php foreach($errors->all() as $error){ echo "$error<br>"; } ?>",
          type: "error",
          confirmButtonText: "Okai",
          html: true
          confirmButtonColor: "#980000"
        });
      </script>
    @endif
      <div class="theme_light w3-sidebar w3-bar-block w3-animate-left w3-collapse w3-top w3-center" id="mySidebar" style="z-index:3;width:200px;">
       
        <img class="resize" onclick="w3_close()" src="{{ OHMS::ohmsLogo() }}" alt="Logo">
          <hr/>
           <div id="menuTut" class="myMenu">
            
          <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-padding w3-hide-large">CLOSE</a>
          <hr/>
           @if (Route::has('login'))
            @if(Auth::check())
            <li class="{{ Request::is('/') ? 'active' : '' }}" ><a href="{{ url('/') }}"><span style="font-size:16px; ">Home</span></a></li>
            @if(OHMS::loggedInUser()->isAdmin())
            <li ><a href="{{ route('OHMS::users') }}"><span style="font-size:16px; ">Admin</span></a></li>
             @elseif(OHMS::loggedInUser()->isStudent())
            <li class="{{ Route::currentRouteNamed('index') ? 'active' : '' }}"><a href="{{ route('index', ['student_id' => auth()->user()->student->id, 'gender' => auth()->user()->student->gender]) }}"><span style="font-size:16px; ">Allocations</span></a></li>
            
            <li class="{{ Route::currentRouteNamed('attView') ? 'active' : '' }}"><a href="{{ route('attView') }}"><span style="font-size:16px; ">My Attendance</span></a></li>
            <li class="{{ Route::currentRouteNamed('dash') ? 'active' : '' }}"><a href="{{ route('dash') }}"><span style="font-size:16px; ">Dashboard</span></a></li>
            <li class="{{ Route::currentRouteNamed('get_report') ? 'active' : '' }}"><a href="{{ route('get_report') }}"><span style="font-size:16px; ">Report Maintenance</span></a></li>
             @endif
            <li ><a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span style="font-size:16px; ">Logout</span></a></li>
             @else
            <li ><a href="http://www.solusi.ac.zw" target="_blank"><span style="font-size:16px; ">Solusi WebSite</span></a></li>
            <li ><a href="{{ url('/login') }}"><span style="font-size:16px; ">Login</span></a></li>
            @endif
            <li ><a href="http://www.solusi.ac.zw" target="_blank"><span style="font-size:16px;">Solusi WebSite<span style="font-size:16px;"></span></a></li>
            


          @if(OHMS::loggedInUser()->hasPermission('ohms.users.access'))
          <div class="w3-dropdown-hover w3-mobile">
              <button class="w3-button theme_light_hover" onclick="w3_close()">{{ trans('ohms.user_manager') }} <i class="fa fa-caret-down"></i>
              </button>
              <div class="w3-dropdown-content theme_light w3-bar-block w3-card side_drop" onclick="w3_close()">
                <a href="{{ route('OHMS::users') }}" class="w3-bar-item w3-button ">{{ trans('ohms.user_list') }}</a>
                 @if(OHMS::loggedInUser()->hasPermission('ohms.users.create')) 
                <a href="{{ route('OHMS::users_create') }}" class="w3-bar-item w3-button">{{ trans('ohms.create_user') }}</a>
                 @endif 
                  @if(OHMS::loggedInUser()->hasPermission('ohms.users.settings')) 
                <a href="{{ route('OHMS::users_settings') }}" class="w3-bar-item w3-button ">{{ trans('ohms.users_settings') }}</a>
                 @endif 
              </div>
            </div>
            @endif
              <hr/>
            @if(OHMS::loggedInUser()->hasPermission('ohms.roles.access'))
               <div class="w3-dropdown-hover w3-mobile">
              <button class="w3-button theme_light_hover">{{ trans('ohms.role_manager') }} <i class="fa fa-caret-down"></i>
              </button>
              <div class="w3-dropdown-content theme_light w3-bar-block w3-card side_drop" onclick="w3_close()">
                  <a href="{{ route('OHMS::roles') }}" class="w3-bar-item w3-button ">{{ trans('ohms.role_list') }}</a>
                  @if(OHMS::loggedInUser()->hasPermission('ohms.roles.create'))
                    <a href="{{ route('OHMS::roles_create') }}" class="w3-bar-item w3-button">{{ trans('ohms.create_role') }}</a>
                  @endif
                </div>
              </div>
            @endif
              <hr/>
            @if(OHMS::loggedInUser()->hasPermission('ohms.permissions.access'))
               <div class="w3-dropdown-hover w3-mobile">
              <button class="w3-button theme_light_hover">{{ trans('ohms.permission_manager') }} <i class="fa fa-caret-down"></i>
              </button>
              <div class="w3-dropdown-content theme_light w3-bar-block w3-card side_drop" onclick="w3_close()">
                  <a href="{{ route('OHMS::permissions') }}" class="w3-bar-item w3-button">{{ trans('ohms.permission_list') }}</a>
                  @if(OHMS::loggedInUser()->hasPermission('ohms.permissions.create'))
                    <a href="{{ route('OHMS::permissions_create') }}" class="w3-bar-item w3-button">{{ trans('ohms.create_permission') }}</a>
                  @endif
                </div>
              </div>
            @endif
              <hr/>
          @if(OHMS::loggedInUser()->hasPermission('ohms.sessions.access'))
            <div class="w3-dropdown-hover w3-right w3-mobile">
            <button class="w3-button theme_light_hover" >Sessions<i class="fa fa-caret-down"></i>
            </button>
            <div class="w3-dropdown-content theme_light w3-bar-block w3-card side_drop" onclick="w3_close()">
                <a href="{{ route('OHMS::sessions') }}" class="w3-bar-item w3-button">Session list</a>
                @if(OHMS::loggedInUser()->hasPermission('ohms.sessions.access'))
                  <a href="{{ route('OHMS::sessions_create') }}" class="w3-bar-item w3-button">Create session</a>
                @endif
              </div>
            </div>
          @endif
          <hr/>
          @if(OHMS::loggedInUser()->hasPermission('ohms.courses.access'))
            <div class="w3-dropdown-hover w3-right w3-mobile">
            <button class="w3-button theme_light_hover" >Courses<i class="fa fa-caret-down"></i>
            </button>
            <div class="w3-dropdown-content theme_light w3-bar-block w3-card side_drop" onclick="w3_close()">
                <a href="{{ route('OHMS::courses') }}" class="w3-bar-item w3-button">Course list</a>
                @if(OHMS::loggedInUser()->hasPermission('ohms.courses.access'))
                  <a href="{{ route('OHMS::courses_create') }}" class="w3-bar-item w3-button">Create course</a>
                @endif
              </div>
            </div>
          @endif
          <hr/>
      {{--
          @if(OHMS::loggedInUser()->hasPermission('ohms.attendances.access'))
             <div class="w3-dropdown-hover w3-right w3-mobile">
            <button class="w3-button theme_light_hover" >{{ trans('ohms.attendance_manager') }}<i class="fa fa-caret-down"></i>
            </button>
            <div class="w3-dropdown-content theme_light w3-bar-block w3-card side_drop" onclick="w3_close()">
                <a href="{{ route('OHMS::attendances') }}" class="w3-bar-item w3-button">{{ trans('ohms.attendance_list') }}</a>
                @if(OHMS::loggedInUser()->hasPermission('ohms.attendances.create'))
                  <a href="{{ route('OHMS::attendances_create') }}" class="w3-bar-item w3-button">{{ trans('ohms.create_attendance') }}</a>
                @endif
              </div>
            </div>
          @endif--}}
              <hr/>
             <div class="w3-dropdown-hover w3-mobile">
              <button class="w3-button theme_light_hover"> 
              <div class="header">{{ trans('ohms.developer_tools') }} <i class="fa fa-caret-down"></i></div>
              </button>
              <div class="w3-dropdown-content theme_light w3-bar-block w3-card side_drop" onclick="w3_close()">
                @if(OHMS::loggedInUser()->hasPermission('ohms.CRUD.access'))
                  <a href="{{ route('OHMS::CRUD') }}" class="w3-bar-item w3-button">{{ trans('ohms.database_CRUD') }}</a>
                @endif
                  <a href="{{ route('OHMS::API') }}" class="w3-bar-item w3-button">{{ trans('ohms.ohms_API') }}</a>
              </div>
            </div>
              <hr/>
            @if(OHMS::loggedInUser()->hasPermission('ohms.settings.access'))
            <div class="w3-dropdown-hover w3-mobile">
              <button class="w3-button theme_light_hover">{{ trans('ohms.settings') }} <i class="fa fa-caret-down"></i>
              </button>
              <div class="w3-dropdown-content theme_light w3-bar-block w3-card side_drop" onclick="w3_close()">
                  <a href="{{ route('OHMS::settings') }}" class="w3-bar-item w3-button">{{ trans('ohms.general_settings') }}</a>
                </div>
              </div>
            @endif
              <hr/>
             <div class="w3-mobile">
             <a href="{{-- route('OHMS::about') --}}" class="w3-button">{{-- trans('ohms.about') --}}</a>      </div >
               <hr/>
               @endif
        </div>     
 
      </div>
      <!-- MAIN CONTENT SECTION -->
      <div class="content w3-main">
        <!-- Top menu on small screens -->
        <header class="w3-container w3-top w3-hide-large theme w3-xlarge w3-padding-16">
          <a href="javascript:void(0)" class="w3-button theme w3-hide-large" onclick="w3_openS()">???</a>
          <div class="w3-dropdown-click w3-right w3-mobile margn">
            <a class="w3-button drop w3-right theme_dark w3-border w3-border-red w3-round-xlarge" onclick="fun(this);" data-id="drop_mobile">section</a>
            <div class="w3-dropdown-content w3-bar-block w3-card theme" id="drop_mobile">
              <a class="w3-bar-item w3-button w3-hide-large theme" href="#" >Profile</a>
              <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="w3-button theme_mild w3-hide-large">
                    {{ trans('ohms.logout') }}
                </a>
            </div>
          </div>  
        </header>        

        <!-- Overlay effect when opening sidebar on small screens -->
        <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <!-- TOP NAVBAR -->
        <div class="w3-pannel w3-top w3-hide-medium w3-hide-small" style="position:sticky;">
         <div class="theme_dark w3-bar" style="z-index:4;">  
              @yield('breadcrumb')

           <div class="w3-dropdown-click w3-right w3-mobile margn">
            @if(Auth::check())
            <button class="w3-button drop theme_dark_hover w3-border w3-border-red w3-round-xlarge w3-hide-medium w3-hide-small" onclick="fun(this);" data-id="drop_top">
             <i class="fa fa-user"></i> {{ Auth::user()->username }}
            </button>
              <div class="w3-dropdown-content w3-bar-block w3-card theme" id="drop_top">
                <a href="{{-- route('OHMS::profile') --}}" onclick="w3_close()" class="w3-bar-item w3-button theme_dark_hover">
                  {{-- trans('ohms.profile') --}}
                </a>
                <a href="{{ url('/') }}" onclick="w3_close()" class="w3-bar-item w3-button theme_dark_hover">
                  {{ trans('ohms.visit_site') }}
                </a>
                <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="w3-bar-item w3-button theme_dark_hover">
                    {{ trans('ohms.logout') }}
                </a>
              </div>
              @endif
          </div>
        </div> 
      </div>        

        <!-- Main CONTANT -->
        <div class="page-content theme_mild" onclick="w3_close()">
          <div class="main" style="padding-left: 0; min-height: 400px;">
                @yield('main-contant')
            </div>
        </div>

        <!-- FOOTER -->
        <div class="w3-container theme_dark w3-center" style="position: fixed; bottom: 0; width:100%">
          <a href="{{ url('/') }}" class="w3-bar-item w3-button">Online Hostel Management </a>
          <a href="#" class="w3-bar-item w3-button">Developed OHMSsani Ndlovu</a>
          <a href="https://www.solusi.ac.zw" class="w3-bar-item w3-button">&copy; Copyright OHMS 1.0</a>
          <a href="#" class="w3-bar-item w3-button">Link 3</a>
        </div>         
      </div>
<script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
  @yield('js')
<script>
function fun(obj) {
    var id = obj.dataset.id;
      console.log(id);
      var x = document.getElementById(id);
      if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
      } else { 
        x.className = x.className.replace(" w3-show", "");
      }
}
  // Script to open and close the sidebar
      function w3_openS() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
      }
       
      function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
      }

      function w3_show_nav(name) {
        document.getElementById("menuTut").style.display = "none";
        document.getElementById("menuRef").style.display = "none";
        document.getElementById(name).style.display = "block";
        w3-openS();
      }
      
// setInterval(function(){
//       var footer = $('.page-footer');
//       footer.removeAttr("style");
//       var footerPosition = footer.position();
//       var docHeight = $( document ).height();
//       var winHeight = $( window ).height();
//       if(winHeight == docHeight) {
//         if((footerPosition.top + footer.height() + 3) < docHeight) {
//           var topMargin = (docHeight - footer.height()) - footerPosition.top;
//           footer.css({'margin-top' : topMargin + 'px'});
//         }
//       }
//     }, 10);
</script>

</body>
</html>