<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Maint_dep;
use App\Permission;
use Schema;
use Auth;
use OHMS;

class Maint_depsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.maint_deps.access');

        # Get all the maint_deps
        $maint_deps = Maint_dep::all();

        
        # Return the view
        return view('ohms/maint_deps/index', ['maint_deps' => $maint_deps]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.maint_deps.access');

        # Get the maint_dep
        $maint_dep = Maint_dep::find($id);

        # Return the view
        return view('admin/maint_deps/show', ['maint_dep' => $maint_dep]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.maint_deps.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.maint_deps.admin');

        # Find the maint_dep
        $row = Maint_dep::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'maint_deps';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/maint_deps/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.maint_deps.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.maint_deps.admin');

        # Find the row
        $row = Maint_dep::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'maint_deps';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::maint_deps')->with('success', trans('ohms.msg_maint_dep_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.maint_deps.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.maint_deps.admin');

        # Get all the data
        $data_index = 'maint_deps';
        require('Data/Create/Get.php');        

        # Return the view
        return view('ohms/maint_deps/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.maint_deps.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.maint_deps.admin');

        # create new maint_dep
        $row = new Maint_dep;

        # Save the data
        $data_index = 'maint_deps';
        require('Data/Create/Save.php');        

        # Return the admin to the maint_deps page with a success message
        return redirect()->route('OHMS::maint_deps')->with('success', trans('ohms.msg_maint_dep_created'));
    }
   

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.maint_deps.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.maint_deps.admin');

        # Select Maint_dep
        $maint_dep = OHMS::maint_dep('id', $id);

        if(!$maint_dep->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }
        
        # Delete Maint_dep
        $maint_dep->delete();

        # Redirect the admin
        return redirect()->route('OHMS::maint_deps')->with('success', trans('ohms.msg_maint_dep_deleted'));
    }
}
