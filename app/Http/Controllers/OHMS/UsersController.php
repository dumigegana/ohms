<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Role_User;
use OHMS;
use Auth;
use Gate;
use App\User;
use App\Student;
use App\Staff;
use App\Visitor;

class UsersController extends Controller
{
     /* 
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. 
    |
    */
    public function search(Request $request){
     OHMS::permissionToAccess('ohms.users.access');
    $read = "";
    $post = $request['keywords'];
    if(empty($post)) {
         $users = User::paginate(100);
     }
    else{
        $users = User::where('full_name','like', '%' . $post . '%')
                       ->orwhere('username','like', $post . '%')
                       ->orwhere('email','like', $post . '%')->get();
    }
    $active_users = OHMS::users('active', true);

        # Get Banned Users
    $banned_users = OHMS::users('banned', true);

        # Get all roles
    $roles = OHMS::roles();
    $read = view('ohms/users/search', [ 
            'users'         =>  $users,
            'roles'         =>  $roles, 
            'active_users'  =>  $active_users,
            'banned_users'  =>  $banned_users,
        ]);
  return $read;

 }


    public function index()
    {
        OHMS::permissionToAccess('ohms.users.access');

    	# Get all users
    	$users = User::paginate(100);
    	# Get the active users
    	$active_users = OHMS::users('active', true);

    	# Get Banned Users
    	$banned_users = OHMS::users('banned', true);

    	# Get all roles
    	$roles = OHMS::roles();

    	# Return the view
    	return view('ohms/users/index', [ 
    		'users' 		=> 	$users,
    		'roles'			=>	$roles, 
    		'active_users'	=>	$active_users,
    		'banned_users'	=>	$banned_users,
		]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.users.access');

    	# Find the user
    	$user = OHMS::user('id', $id);

    	# Return the view
    	return view('ohms/users/show', ['user' => $user]);
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.users.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.users.admin');

        # Get all roles
        $roles = OHMS::roles()->where('name', '!=', 'Student');
         

        # Get all the data
        $data_index = 'users';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/users/create', [
            'roles'     =>  $roles,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.users.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.users.admin');

        # create the user
        $row = OHMS::newUser();

        
        # Save the data
        $data_index = 'users';
        require('Data/Create/Save.php');

                
        # Activate the user if set
        if($request->input('active')){
            $row->active = true;
        }
                
        

        # Save the user
        $row->save();

       

        $this->setRoles($row->id, $request);

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::users')->with('success', trans("ohms.msg_user_created"));
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.users.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.users.admin');

        # Find the user
        $row = OHMS::user('id', $id);

        # Check if admin access
        OHMS::mustNotBeAdmin($row);

        # Get all the data
        $data_index = 'users';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/users/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.users.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.users.admin');

        # Find the user
        $row = OHMS::user('id', $id);

        # Check if admin access
        OHMS::mustNotBeAdmin($row);

        # Save the data
        $data_index = 'users';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::users')->with('success', trans('ohms.msg_user_edited'));
    }

    public function editRoles($id)
    {
        OHMS::permissionToAccess('ohms.users.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.users.roles');

    	# Find the user
    	$user = OHMS::user('id', $id);

        # Check if admin access
        OHMS::mustNotBeAdmin($user);

    	# Get all roles
    	$roles = OHMS::roles()->where('name', '!=', 'Student');

    	# Return the view
    	return view('ohms/users/roles', ['user' => $user, 'roles' => $roles]);
    }

    public function setRoles($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.users.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.users.roles');

		# Find the user
    	$user = OHMS::user('id', $id);

        # Check if admin access
        OHMS::mustNotBeAdmin($user);

    	# Get all roles
    	$roles = OHMS::roles();

    	# Change user's roles
    	foreach($roles as $role) {

            $modify = true;

            # Check for su
            if($role->su) {
                $modify = false;
            }

            # Check if it's assignable
            if(!$role->assignable and !OHMS::loggedInUser()->su) {
                $modify = false;
            }

            if($modify) {
                if($request->input($role->id)){
                    # The admin selected that role

                    # Check if the user was already in that role
                    if($this->checkRole($user->id, $role->id)) {
                        # The user is already in that role, so no change is made
                    } else {
                        # Add the user to the selected role
                        $this->addRel($user->id, $role->id);
                    }
                } else {
                    # The admin did not select that role

                    # Check if the user was in that role
                    if($this->checkRole($user->id, $role->id)) {
                        # The user is in that role, so as the admin did not select it, we need to delete the relationship
                        $this->deleteRel($user->id, $role->id);
                    } else {
                        # The user is not in that role and the admin did not select it
                    }
                }
            }
    	}

    	# Return Redirect
        return redirect()->route('OHMS::users')->with('success', trans('ohms.msg_user_roles_edited'));
    }

    public function checkRole($user_id, $role_id)
    {
        OHMS::permissionToAccess('ohms.users.access');

    	# This function returns true if the specified user is found in the specified role and false if not

    	if(Role_User::whereUser_idAndRole_id($user_id, $role_id)->first()) {
    		return true;
    	} else {
    		return false;
    	}

    }

    public function deleteRel($user_id, $role_id)
    {
        OHMS::permissionToAccess('ohms.users.delete');

    	$rel = Role_User::whereUser_idAndRole_id($user_id, $role_id)->first();
    	if($rel) {
           $rel->delete();
    	}
    }

    public function addRel($user_id, $role_id)
    {
        OHMS::permissionToAccess('ohms.users.access');

    	$rel = Role_User::whereUser_idAndRole_id($user_id, $role_id)->first();
    	if(!$rel) {
    		$rel = new Role_User;
    		$rel->user_id = $user_id;
    		$rel->role_id = $role_id;
    		$rel->save();
    	}
    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.users.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.users.admin');

        # Find The User
        $user = OHMS::user('id', $id);

        # Check if admin access
        OHMS::mustNotBeAdmin($user);

        # Check if it's su
        if($user->su) {
            abort(403, trans('ohms.error_security_reasons'));
        }

    	# Check before deleting
    	if($id == OHMS::loggedInUser()->id) {
            abort(403, trans('ohms.error_user_delete_yourself'));
    	} else {

    		# Delete Relationships
    		$rels = Role_User::where('user_id', $user->id)->get();
    		foreach($rels as $rel) {
    			$rel->delete();
    		}
             $stud = Student::where('user_id', $user->id)->first();
            if($stud){
                $stud ->delete();
            }

             $staff = Staff::where('user_id', $user->id)->first();
            if($staff){
                $staff ->delete();
            }

             $visit = Visitor::where('user_id', $user->id)->first();
            if($visit){
                $visit ->delete();
            }

    		# Delete User
    		$user->delete();

    		# Return the admin with a success message
            return redirect()->route('OHMS::users')->with('success', trans('ohms.msg_user_deleted'));
    	}
    }

   
}
