<?php

namespace App\Http\Middleware\OHMS;

use Closure;
use Auth;
use OHMS;
use App;

class Base
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(OHMS::checkInstalled()) {
            # Check if the user is activated
            if(Auth::check()) {

                $user = OHMS::loggedInuser();

                if(!$user->active) {
                    if(OHMS::currentURL() != url('/logout')) {
                        if (strpos(OHMS::currentURL(), route('OHMS::activate_form')) !== false) {
                            // Seems to be ok
                        } else {
                            return redirect()->route('OHMS::activate_form');
                        }
                    }

                }

                if($user->banned and OHMS::currentURL() != route('OHMS::banned')) {
                    if(OHMS::currentURL() != url('/logout')) {
                        return redirect()->route('OHMS::banned');
                    }
                }

                # Set App Locale
                if($user->locale) {
                    App::setLocale($user->locale);
                }

            } else {
                if ($request->session()->has('locale')) {
                	App::setLocale(session('locale'));
                }
            }
        }

        return $next($request);
    }
}
