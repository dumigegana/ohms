@extends('layouts.admin.panel')
@section('breadcrumb')
<div class="ui breadcrumb">
  <div class="active section">{{ trans('ohms.reports_title') }}</div>
</div>
@endsection
@section('title', trans('ohms.reports_title'))
@section('icon', "bar chart")
@section('subtitle', trans('ohms.reports_subtitle'))
@section('content')
<div class="ui one column doubling stackable grid container">
  <div class="column">
    <div class="ui padded segment" style=" background: #ffeaea">
     <div class="well">
      <div class="media">
        <div class="madia-body">
          <div class="ui container">
            <div class="ui doubling stackable grid">
              <div class="sixteen wide column">
                <div class="ui padded segment" style=" background: #ffeaea;">
                  <center><h3 style="color: #980000;">Reports By Maintenance Departments</h3></center><br> 
                    <form class="ui form" method="Post" action="{{ route('OHMS::dept_maint') }}">
                      {{ csrf_field() }}
                      <div class="ui doubling stackable grid">
                        <div class="row">          
                      <!-- form fields-->
                          <div class="four wide column">
                            <div class="required field" >
                              <label style="color: #980000;">Maintenance Dep</label>
                              <select name="dt">
                                <option value="" disabled selected> Department</option>
                                    @foreach($depts as $dept)
                                <option value="{{ $dept->id }}">{{ $dept->name }}</option>
                                        @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="four wide column"><center>
                            <div class="required field">
                              <label style="color: #980000;">Select By</label>
                              <input type="radio" name="tp" value="block"> Hostel<br>
                              <input type="radio" name="tp" value="room"> Room<br>
                            </div> </center> 
                          </div>
                          <div class="four wide column">
                            <div class="required field">
                              <label style="color: #980000;">Hostel</label>
                              <select name="bk" class="form-control">
                                <option value="" disabled selected> Select Hostel</option>
                                    @foreach($blocks as $block)
                                <option value="{{ $block->id }}">{{ $block->name }}</option>
                                        @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="four wide column"><br>
                            <center>
                              <button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button>
                            </center>
                          </div>
                        </div>
                      </div>  
                    </form>
                  </div>
                </div>  
              <div class="row">
                <div class="four wide column">
                  <div class="ui padded segment" style="background: #ffeaea;">
                    <div class="field">
                      <center><h5 class="media-heading">Student Room </h5></center><br>
                    </div>
                    <center>
                    <div class="eight wide column">@php $gender = 'Male' @endphp<a href="{{ route('OHMS::student_report', ['gender' => $gender]) }}" type="submit" class="ui btn orange submit button">Male</a></div>
                    <br>
                    <div class="eight wide column">@php $gender = 'Female' @endphp<a href="{{ route('OHMS::student_report', ['gender' => $gender]) }}" type="submit" class="ui btn orange submit button">Female</a></div>
                    </center>           
                  </div>     
                </div>
                <div class="four wide column">
                  <div class="ui padded segment" style="background: #ffeaea;">
                    <div class="field">
                      <center><h5 class="media-heading">Room Reports</h5></center><br>
                    </div>
                    
                    <center><div class="eight wide column">@php $gender = 'Male' @endphp <a href="{{ route('OHMS::room_maint', ['gender' => $gender]) }}" type="submit" class="ui btn submit button">Male</a></div><br>
                    <div class="eight wide column">@php $gender = 'Female' @endphp <a href="{{ route('OHMS::room_maint', ['gender' => $gender]) }}" type="submit" class="ui btn orange submit button">Female</a></div></center>
                  </div>     
                </div>
                <div class="four wide column">
                  <div class="ui padded segment" style="background: #ffeaea;">
                    <div class="field">
                      <center><h5 class="media-heading">Block Reports</h5></center><br>
                    </div>
                    <center><div class="eight wide column">@php $gender = 'Male' @endphp <a href="{{ route('OHMS::block_maint', ['gender' => $gender]) }}" type="submit" class="ui btn orange submit button">Male</a></div><br>
                    <div class="eight wide column">@php $gender = 'Female' @endphp <a href="{{ route('OHMS::block_maint', ['gender' => $gender]) }}" type="submit" class="ui btn orange submit button">Female</a></div></center> 
                  </div>
                </div>
                <div class="four wide column">
                  <div class="ui padded segment" style="background: #ffeaea;">
                    <div class="field">
                      <center><h5 class="media-heading">Check lists</h5></center><br>
                    </div>
                    <center><div class="eight wide column">@php $gender = 'Male' @endphp <a href="{{ route('OHMS::chech_list', ['gender' => $gender]) }}" type="submit" class="ui btn orange submit button">Male</a></div><br>
                    <div class="eight wide column">@php $gender = 'Female' @endphp <a href="{{ route('OHMS::chech_list', ['gender' => $gender]) }}" type="submit" class="ui btn orange submit button">Female</a></div></center> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection
