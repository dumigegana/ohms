<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Hostel;
use App\Item;
use App\Block;
use App\Room; 
use App\Allocation;
use App\Permission; 
use App\Student;
use App\Major;
use App\Semester;
use App\Maintenance;
use App\Maint_dep;
use App\Facility;
use App\Notice;
use App\Category;
use App\Checklist;
use Hash;
use Auth;
use OHMS;


class ReportController extends Controller
{
    /**
     * Show the profile edit page
     */
    public function index()
    {
        OHMS::permissionToAccess('ohms.reports.access');
        $blocks = Block::all();
        $depts = Maint_dep::all();
        return view('ohms/reports/index', ['depts' => $depts, 'blocks' =>$blocks]);
    }

    public function students($gender)
    {
       OHMS::permissionToAccess('ohms.students.access');

        $allocations = Allocation::where('approved', true)
                                 ->where('check_out', false)
                                 ->with('student', 'room', 'semester')
                                 ->orderBy('room_id')->paginate(50);
        return view('ohms/reports/student_list', ['allocations' => $allocations, 'gender' => $gender]);
    }

    public function students_search(Request $request)
    {
        OHMS::permissionToAccess('ohms.users.access');
        $read = "";
        $search = $request['kwords'];
        $gender = $request['gender'];
        if(empty($search)){
            $allocations = Allocation::where('approved', true)
                                     ->where('check_out', false)
                                     ->with('student', 'room', 'semester')
                                     ->orderBy('room_id')->paginate(50);
        }
        else{
            $allocations = Allocation::where('approved', true)
                                     ->where('check_out', false)
                                     ->with('student', 'room', 'semester')
                                     ->wherehas('room', function($o) use ($search) {$o->where('room_number', 'like','%' . $search . '%');}) 
                                     ->orwherehas('room.block', function($r)use ($search) {$r->where('name', 'like','%'. $search .'%');}) 
                                     ->orwherehas('student.user', function($r)use ($search) {$r->where('full_name', 'like','%'. $search .'%');}) 
                                     ->orwherehas('student', function($q)use ($search) {$q->where('solusi_id', 'like','%'. $search .'%');})
                                     // ->orwherehas('student.major', function($q)use ($search) {$q->where('name', 'like','%'. $search .'%');})
                                     ->orderBy('room_id')->paginate(50);
        }
        $read = view(' ohms/reports/search_stud', ['allocations' => $allocations, 'gender' => $gender,]);
        return $read;
     }

    public function dept(Request $request)
    {
        $this->validate($request, [
            'dt' => 'required',
            'tp' => 'required',
            'bk' => 'required',
        ]);

        $maint_dep_id = $request['dt'];
        $type = $request['tp'];
        $block_id = $request['bk'];
           
        $blck = Block::find($block_id);
        $dept = Maint_dep::find($maint_dep_id);

        OHMS::permissionToAccess('ohms.maintenances.access');
        if ($type === 'block')
        {
            $blocks = Block::with('maintenances.item', 'maintenances.user')
                            ->wherehas('maintenances', function($q) {$q->where('fixed',  false)->orderBy('id', 'asc');})
                             ->wherehas('maintenances', function($q) use ($block_id) {$q->where('maintenable_id',  $block_id);})
                             ->wherehas('maintenances.item', function($r) use ($maint_dep_id) {$r->where('maint_dep_id',  $maint_dep_id);})
                            ->paginate(50);
            return view('ohms/reports/department', [ 
                                    'blocks' => $blocks, 
                                    'type' => $type, 
                                    'blck' => $blck , 
                                    'dept' => $dept 
                                    ]);                            
        }
        else
        {  
            $rooms = Room::where('block_id',  $block_id)->with('maintenances.item', 'maintenances.user')
                        ->wherehas('maintenances', function($q) {$q->where('fixed',  false)->orderBy('id', 'asc');})
                        ->wherehas('maintenances.item', function($r) use ($maint_dep_id) {$r->where('maint_dep_id',  $maint_dep_id);})
                        ->paginate(50);
            return view('ohms/reports/department', [
                                 'blck' => $blck, 
                                 'rooms' => $rooms,
                                  'type' => $type, 
                                  'dept' => $dept
                                  ]);
        }
        
    }

    public function check($gender)
    {
        OHMS::permissionToAccess('ohms.checklists.access');
        $facilities = Facility::orderBy('id', 'asc')->get();
        $allocations = Allocation::with(
                'checklists.facility', 
                'checklists.facility_status',
                'student', 'room', 'semester' )
                ->wherehas('student', function($o) use ($gender) {$o->where('gender',  $gender );})
                ->paginate(100);
        return view('ohms/reports/check', [ 'facilities' => $facilities, 'allocations' => $allocations, ]);
    }

    public function room_maint($gender)
    {
        OHMS::permissionToAccess('ohms.maintenances.access');
        $rooms= Room::with('maintenances.item', 'maintenances.user', 'block.hostel')
                    ->wherehas('block.hostel', function($t) use ($gender) {$t->where('gender', $gender);})
                    ->wherehas('maintenances', function($q) {$q->where('fixed',  false)->orderBy('id', 'asc');})
                    ->paginate(100);
        return view('ohms/reports/room_maint', ['rooms' => $rooms, 'gender' => $gender]);
    }
    public function block_maint($gender)
    {
        OHMS::permissionToAccess('ohms.maintenances.access');
       # Get all the Maintenances
        $blocks = Block::wherehas('hostel', function($t) use ($gender) {$t->where('gender', $gender);})
                        ->with('maintenances.item', 'maintenances.user')
                        ->wherehas('maintenances', function($q) {$q->where('fixed',  false)->orderBy('id', 'asc');})
                        ->paginate(100);
        return view('ohms/reports/block_maint', ['blocks' => $blocks, 'gender' => $gender]);
    }

    public function room_maint_search(Request $request)
    {
        OHMS::permissionToAccess('ohms.maintenances.access');
        $read = "";
        $gender = $request['gender'];
        $post = $request['kwords'];
        if(empty($post)) {
            $rooms= Room::with('maintenances.item', 'maintenances.user', 'block.hostel')
                        ->wherehas('block.hostel', function($t) use ($gender) {$t->where('gender', $gender);})
                        ->wherehas('maintenances', function($q) {$q->where('fixed',  false)->orderBy('id', 'asc');})
                        ->paginate(100);
        }
        else {
            $rooms= Room::with('maintenances.item', 'maintenances.user', 'block.hostel')
                        ->wherehas('block.hostel', function($t) use ($gender) {$t->where('gender', $gender);})
                        ->wherehas('maintenances', function($q) {$q->where('fixed',  false)->orderBy('id', 'asc');})
                        ->where('room_number', 'like', $post .'%')
                        ->orwherehas('block', function($b) use ($post) {$b->where('name', 'like', $post .'%');})
                        ->paginate(100);
            }
            $read = view('ohms.reports.search.room_maint', ['rooms' => $rooms, 'gender' => $gender]);
        return $read;
    }
    public function block_maint_search(Request $request)
    {
        OHMS::permissionToAccess('ohms.maintenances.access');
        $read = "";
        $gender = $request['gender'];
        $post = $request['kwords'];
        if(empty($post)) {
            $blocks = Block::wherehas('hostel', function($t) use ($gender) {$t->where('gender', $gender);})
                        ->with('maintenances.item', 'maintenances.user')
                        ->wherehas('maintenances', function($q) {$q->where('fixed',  false)->orderBy('id', 'asc');})
                        ->paginate(100);
        }
        else {
            $blocks = Block::wherehas('hostel', function($t) use ($gender) {$t->where('gender', $gender);})
                        ->with('maintenances.item', 'maintenances.user')
                        ->wherehas('maintenances', function($q) {$q->where('fixed',  false)->orderBy('id', 'asc');})
                        ->where('name', 'like', $post .'%')
                        ->paginate(100);            
        }
        $read = view('ohms/reports/search/block_maint', ['blocks' => $blocks, 'gender' => $gender]);
        return $read;
    }     
}
