<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    public function allocations()
    {
        return $this->hasMany('App\Allocation');
    }

}
