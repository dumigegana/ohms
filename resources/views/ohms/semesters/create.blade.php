@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::semesters') }}">{{ trans('ohms.semesters_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.semesters_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.semesters_create_title'))
@section('icon', "plus")
@section('subtitle', trans('ohms.semesters_create_subtitle'))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST">
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column centered">
                    <div class="ui very padded segment" style="background: #ffeaea">
                         {{ csrf_field() }}
                           
                        <div class="required field">
                            <label style="color: #980000">Semester Name</label>
                            <input name="name" type="text" placeholder="Semester Name" required value="{{ old('name')}}">
                        </div>
                        <div class="required field">
                            <label style="color: #980000">Semester Month/s</label>
                            <input name="term" type="text" placeholder="May - September " required value="{{ old('term')}}">
                        </div>
                        <br/>
                        <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                    </div>
                </div>
            </div>
        </div> 
    </form>
</div>
@endsection
