@extends('layouts.admin.panel')
@section('breadcrumb')
<div class="ui breadcrumb">
    <div class="active section">Class session List</div>
</div>
@endsection
@section('title', "Session List")
@section('icon', "sessions")
@section('subtitle', "Session List")
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $sessions->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.sessions.admin'))
             <center><a href="{{ route('OHMS::sessions_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add</a></center>
             @endif 
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>  
  <div class="w3-container w3-center">      
        <table class="ui basic table " >
          <thead style="background: #ff8b8b;">
        <tr style="color: #980000;">
          <th>Name</th>
          <th>Course</th>
          <th>Class time</th>
          <th>Subtract</th>                  
          <th>Factor</th>
          <th>Min</th>
          <th>Max</th>
          <th>Attendants</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
                @foreach($sessions as $session)
        <tr> 
          <td>  <div class="text">    {{ $session->name }} </div> </td>
            <td>  <div class="text">   {{ $session->course->name}} </div> </td>
          <td>  <div class="text">   {{ $session->date_time }} </div> </td>
          <td>  <div class="text">   {{ $session->subtract}} </div> </td>
          <td>  <div class="text">   {{ $session->factor}} </div> </td>
          <td>  <div class="text">   {{ $session->minimum}} </div> </td>
          <td>  <div class="text">   {{ $session->maximum}} </div> </td>
          <td class="w3-center">{{ $session->attendances->count() }} </td> </td>
          <td >@if(OHMS::loggedInUser()->su)
            <div class="ui btn top icon left pointing dropdown button">
              <i class="options icon"></i>
              <div class="menu">
                <div class="header">{{ trans('ohms.editing_options') }}</div>
                <a href="{{ route('OHMS::sessions_edit', ['id' => $session->id]) }}" class="item">
                  <i class="edit icon"></i>
                  Edit
                </a>
                <a href="{{ route('OHMS::sessions_delete', ['id' => $session->id]) }}" class="item">
                  <i class="trash icon"></i>
                  Delete
                </a>     
                <a href="{{-- route('OHMS::sessions_attend', ['id' => $session->id]) --}}" class="item">
                  <i class="lightning icon"></i>
                  Attendance
                </a>                
              </div>
            </div>  
                @endif
        </tr>
      @endforeach
        </tbody>
      </table>
        {!! $sessions->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection