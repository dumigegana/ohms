 @foreach($benefit_records as $benefit_record) 
            <tr style="color: #980000;">
              <td>{{ $benefit_record->benefit_record_number }}</td>
              <td>{{ $benefit_record->block->hostel->gender }}</td>
              <td>{{ $benefit_record->block->name }}</td>
              <td>{{ $benefit_record->capacity }}</td>
              <td class="center alligned">
                      @if ($benefit_record->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Benefit_record ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this benefit_record " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::benefit_records_edit', ['id' => $benefit_record->id]) }}" class="ui btn button">{{ trans('ohms.benefit_records_edit') }}</a>
              </td>       
            </tr>
        @endforeach