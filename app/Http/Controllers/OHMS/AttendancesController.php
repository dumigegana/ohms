<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Session;
use App\Attendance;
use App\Course;
use OHMS;
use Auth;

class attendancesController extends Controller
{
    public function create()
    {
        # Check permissions
        OHMS::permissionToAccess('ohms.attendances.admin');
        

        # Get all the data
        $data_index = 'attendances';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/attendances/create', [
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
      OHMS::permissionToAccess('ohms.attendances.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.attendances.admin');

        # create the user
        $row = OHMS::newAttendance();

        # Save the data
        $data_index = 'attendances';
        require('Data/Create/Save.php');

        # Return the admin to the attendances page with a success message
        return redirect()->route('OHMS::attendances')->with('success', trans('Attendance marked succesifully'));  
    }


    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.attendances.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.attendances.admin');

        
        $attendance = OHMS::Attendance('id', $id);
        if($attendance) {
            $row = $attendance;
            $data_index = 'attendances';
            require('Data/Edit/Get.php');

            return view('ohms/attendances/edit', [
                'attendance'      =>  $attendance,
                'row'       =>  $row,
                'fields'    =>  $fields,
                'confirmed' =>  $confirmed,
                'encrypted' =>  $encrypted,
                'hashed'    =>  $hashed,
                'masked'    =>  $masked,
                'table'     =>  $table,
                'code'      =>  $code,
                'wysiwyg'   =>  $wysiwyg,
                'relations' =>  $relations,
            ]);

        } else {
            abort(404);
        }
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.attendances.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.attendances.admin');

        
        $attendance = OHMS::Attendance('id', $id);

        if($attendance) {

            $row = $attendance;
            $data_index = 'attendances';
            require('Data/Edit/Save.php');
             # Find The Session
        $attendances = Attendance::with('student')->where('session_id', $attendance ->session_id)->get(); 
        $session = Session::find($attendance ->session_id)->first();      

        return view('ohms/attendances/show', ['attendances' => $attendances, 'session' => $session])->with('success', trans('Attendance marked succesifully'));  
            abort(404);
        }
    }

    public function delete($id)
    {
        OHMS::permissionToAccess('ohms.attendances.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.attendances.admin');

        # Delete the attendance
        OHMS::Attendance('id', $id)->delete();

        return redirect()->route('OHMS::attendances')->with('success', trans('ohms.msg_attendance_deleted'));
    }


}
