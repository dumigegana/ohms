<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use OHMS;
use App\Session;
use App\Attendance;

class SessionsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.sessions.access');

        $sessions = Session::paginate(50);

        return view('ohms/sessions/index', ['sessions'=>  $sessions]);
    }

    public function graphics($id)
    {
        OHMS::permissionToAccess('ohms.sessions.access');

        $session = OHMS::session('id', $id);

        return view('ohms/sessions/graphics', ['session' => $session]);
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.sessions.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.sessions.admin');

        $roles = OHMS::roles();
        //$relations =  OHMS::roles();

        $data_index = 'sessions';
        require('Data/Create/Get.php');

        return view('ohms/sessions/create', [
           'roles'    =>  $roles,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.sessions.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.sessions.admin');

        # create the user
        $row = OHMS::newSession();

        # Save the data
        $data_index = 'sessions';
        require('Data/Create/Save.php');

        # Return the admin to the sessions page with a success message
        return redirect()->route('OHMS::sessions')->with('success', trans('Class Session Created'));
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.sessions.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.sessions.admin');

        $row = OHMS::session('id', $id);

        $data_index = 'sessions';
        require('Data/Edit/Get.php');

        return view('ohms/sessions/edit',[
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update(Request $request)
    {
        OHMS::permissionToAccess('ohms.sessions.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.sessions.admin');

        $row = OHMS::session('id', $request['id']);

        $data_index = 'sessions';
        require('Data/Edit/Save.php');
        $row->save();

        return redirect()->route('OHMS::sessions')->with('success', trans('Class Session updated'));
    }


    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.sessions.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.sessions.admin');

        # Find The Session
        $session = OHMS::session('id', $id);

        $session->delete();

        return redirect()->route('OHMS::sessions')->with('success', trans('Class session Deleted'));
    }

    public function view_student($id)
    {
        # Find The Session
        $attendances = Attendance::with('student')->where('session_id', $id)->orderby('code')->get(); 
        $session = Session::find($id)->first();      

        return view('ohms/attendances/show', ['attendances' => $attendances, 'session' => $session]);
    }

}
