<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Semester;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class SemestersController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.semesters.access');

        # Get all the semesters
        $semesters = Semester::paginate(50);

        
        # Return the view
        return view('ohms/semesters/index', ['semesters' => $semesters]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.semesters.access');

        # Get the semester
        $semester = Semester::find($id);

        # Return the view
        return view('admin/semesters/show', ['semester' => $semester]);
    }
    public function sem_checkin($id)
    {
        OHMS::permissionToAccess('ohms.semesters.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.semesters.admin');

        # Find the semester
        $row = Semester::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
            }
        $sem = Semester::where('id', $id)->first();
        if ($sem->checkin == 1){
            Semester::where('id', $id)->update(['checkin' => 0]);
            }
        else{
            Semester::where('id', $id)->update(['checkin' => 1]);
            }
        return redirect()->back()->with('success', 'Check in Status changed!!');

    }
    public function sem_checkout($id)
    {
        OHMS::permissionToAccess('ohms.semesters.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.semesters.admin');

        # Find the semester
        $row = Semester::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
            }
        $sem = Semester::where('id', $id)->first();
        if ($sem->checkout == 1){
            Semester::where('id', $id)->update(['checkout' => 0]);
            }
        else{
            Semester::where('id', $id)->update(['checkout' => 1]);
            }
        return redirect()->back()->with('success', 'Check out Status changed!!');

    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.semesters.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.semesters.admin');

        # Find the semester
        $row = Semester::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'semesters';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/semesters/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.semesters.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.semesters.admin');

        # Find the row
        $row = Semester::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'semesters';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::semesters')->with('success', trans('ohms.msg_semester_edited'));
    }

    public function create()
    {
        
        # Check permissions
        OHMS::permissionToAccess('ohms.semesters.admin');

        
        # All permissions
        $permissions = OHMS::permissions();

        # Return the view
        return view('ohms/semesters/create');
    }

    public function store(Request $request)
    {
       
        # Check permissions
        OHMS::permissionToAccess('ohms.semesters.admin');

        # create new role
        $row = new Semester;

        # Save the data
        $data_index = 'semesters';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::semesters')->with('success', trans('ohms.msg_semester_created'));

    }
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.hostels.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.hostels.admin');

        # Select Item
        $semester = OHMS::semester('id', $id);

        if(!$semester->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $semester->delete();

        # Redirect the admin
        return redirect()->route('OHMS::hostels')->with('success', trans('ohms.msg_hostel_deleted'));
    }
}
