 
@extends('layouts.admin.panel')
@section('css')
<link href="{{ URL::to('intlTelInput/build/css/intlTelInput.css') }}" rel="stylesheet">
<style type="text/css">
  #error-msg {
    color: red;
  }
  #valid-msg {
    color: #00C900;
  }
  #error-msg1 {
    color: red;
  }
  #valid-msg1 {
    color: #00C900;
  }
  input.error {
    border: 1px solid #FF7C7C;
  }
</style>
@endsection
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" >{{ trans('ohms.visitor_create') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.visitors_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.visitors_create_title'))
@section('icon', "add user")
@section('subtitle', trans('ohms.visitors_create_subtitle'))
@section('content')
<div class="ui container">
  <form id="create" class="ui form" action ="{{ route('OHMS::visitors_store') }}" method="POST">
    <div class="ui doubling stackable grid">
      <div class="row">
        <div class="eight wide column">
          <div class="ui very padded segment" style="background: #ffeaea">
                     {{ csrf_field() }}
             
            <div class="required field" style="color: #980000;"><center><H1>{{ $visitor->full_name }}</H1></center>
              <input type="hidden" name="user_id" value="{{ $visitor->id}}">
            </div>
            <br>
            <div class="ui equal width form">
              <div class="fields">  
                <div class="required field">
                  <label style="color: #980000;">Arrival Date</label>
                  <input name='arrival_date' type="date" value="{{ old('arrival_date')}}">
                </div> 
                <div class="required field">
                  <label style="color: #980000;">Depature Date</label>
                  <input name='depature_date' type="date" value="{{ old('depature_date')}}">
                </div> 
              </div>
            </div>
            <div class="ui equal width form">
              <div class="fields">  
                <div class="required field" >
                  <label style="color: #980000;">Transport</label>                              
                  <input id="hide" type="radio" name="transport" value="Road" {{ (old("transport") == 'Road' ? "checked":"") }}> Road<br>
                  <input id="show" type="radio" name="transport" value="Flight" {{ (old("transport") == 'Flight' ? "checked":"") }}>Flight<br>                                
                </div>
                <div class="required field" id="shw">
                  <label style="color: #980000;">Arrival Fight</label> 
                  <select name="a_flight" id="">              
                    <option value=" " disabled selected>select Flight</option> 
                    @foreach($flights as $flight) 
                       @if($flight->status == 'Arrival')
                    <option value="{{ $flight->id }}">{{ $flight->flight }} </option>   
                        @endif 
                    @endforeach
                  </select>
                </div>
                <div class="required field" id="shw1">
                  <label style="color: #980000;">Return Fight</label> 
                  <select name="r_flight" id="">              
                    <option value=" " disabled selected>select Flight</option> 
                    @foreach($flights as $flight) 
                       @if($flight->status == 'Return')
                    <option value="{{ $flight->id }}">{{ $flight->flight }} </option>   
                        @endif 
                    @endforeach
                  </select>
                </div>
              </div>
            </div>                                  
          </div>
        </div>
          
        <div class="eight wide column">
          <div class="ui very padded segment" style="background: #ffeaea">
            <div class="ui equal width form">
              <div class="fields">                         
                
            </div>                            
            <div class="ui equal width form">
              <div class="fields">
                <div class="required field">
                  <label style="color: #980000;">Number</label>
                  <input type="radio" name="spause" value="Single" {{ (old("spause") == 'Single' ? "checked":"") }}> Single<br>
                  <input type="radio" name="spause" value="Couple" {{ (old("spause") == 'Couple' ? "checked":"") }}> Couple<br>
                </div>
                <div class="required field">
                  <label style="color: #980000;">Gender</label>
                  <input type="radio" name="gender" value="Male" {{ (old("gender") == 'Male' ? "checked":"") }}> Male<br>
                  <input type="radio" name="gender" value="Female" {{ (old("gender") == 'Female' ? "checked":"") }}> Female<br>
                </div>             
                
              </div>
            </div>

      <div class="ui equal width form">
        <div class="fields">
                           
          <div class="required field" id="block">
            <label style="color: #980000;">Block</label> 
            <select name="block_id" id="sel_blck">              
              <option value=" " disabled selected>select </option> 
              @foreach($hostels as $hostel)                
                @foreach($hostel->blocks as $block)
                 @if($block->stud_year >= 3)
              <option value="{{ $block->id }}">{{ $block->name }} </option>   
                  @endif              
                @endforeach
              @endforeach
            </select>
          </div>
          <div class="field" id="room_list"  > 
        </div>
        </div>
      </div>
            <br>
            <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
          </div>          
        </div>
      </div>
    </div> 
  </form>
</div>
</div></div>
@endsection
@section('js')
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('intlTelInput/build/js/utils.js') }}"></script>
<script type="text/javascript">
  var telInput = $("#cell_number");
  $("#error-msg").hide();
  $("#valid-msg").hide();

// initialise plugin
  telInput.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset = function() {
  $("#error-msg").hide();
  $("#valid-msg").hide();
  $("#cell_phone_number").val("");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      $("#error-msg").hide();
      $("#valid-msg").show();
      $("#cell_phone_number").val(1);
    } else {
      telInput.addClass("error");
      $("#valid-msg").hide();
      $("#error-msg").show();
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
// End of Cell phone input
//-----------------------------------------------------------------------------------------------------
$("form").submit(function() {
  $("#hidden").val($("#cell_number").intlTelInput("getNumber"));
  $("#hidden_n").val($("#next_of_kin").intlTelInput("getNumber"));
});

//---------------------------------------------------------------------------------------------------
// Next of kin input Starts
  var telInput1 = $("#next_of_kin");
  $("#error-msg1").hide();
  $("#valid-msg1").hide();

// initialise plugin
  telInput1.intlTelInput({
    initialCountry: "zw",
    nationalMode: true,
    onlyCountries: ['za', 'zw', 'zm', 'ls', 'bw', 'ao', 'mz', 'na', 'sz', 'mw'],
    placeholderNumberType: "MOBILE"
  });

var reset1 = function() {
  $("#error-msg1").hide();
  $("#valid-msg1").hide();  
  $("#next_of_kin_number").val("");
};

// on blur: validate
telInput1.blur(function() {
  reset1();
  if ($.trim(telInput1.val())) {
    if (telInput1.intlTelInput("isValidNumber")) {
      $("#error-msg1").hide();
      $("#valid-msg1").show();  
      $("#next_of_kin_number").val(1);

    } else {
      telInput1.addClass("error");
      $("#valid-msg1").hide();
      $("#error-msg1").show();

    }
  }
});

// on keyup / change flag: reset
telInput1.on("keyup change", reset1);
 // End of Next of kin contact inputs
 //--------------------------------------------------------------------------------------------------
$(document).ready(function(){
  $("#shw").hide();
  $("#shw1").hide();
  $("#hide").click(function(){
        $("#shw").hide();
         $("#shw1").hide();
    });
    $("#show").click(function(){
        $("#shw").addClass("required field").show();
         $("#shw1").addClass("required field").show();
    });
    $(document).on('change','#block',function(){ 
      $('#room_list').show('400');
      var block_id= $('select[id="sel_blck"] option:selected').val();
      $.ajax({
        type: 'Get',
        url:"{{ url('admin/getrooms') }}",
        data: {"_token": $('#token').val(), 'bl_id':block_id},
        async: true,
        success: function(data){;
          $('#room_list').html(data);
        },
        error:function(){
          alert('Ooops something went wrong!');
        },
      });      
    });
  });
 </script>
@endsection