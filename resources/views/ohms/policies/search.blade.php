 @foreach($policies as $policy) 
            <tr style="color: #980000;">
              <td>{{ $policy->policy_number }}</td>
              <td>{{ $policy->block->hostel->gender }}</td>
              <td>{{ $policy->block->name }}</td>
              <td>{{ $policy->capacity }}</td>
              <td class="center alligned">
                      @if ($policy->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Policy ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this policy " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::policies_edit', ['id' => $policy->id]) }}" class="ui btn button">{{ trans('ohms.policies_edit') }}</a>
              </td>       
            </tr>
        @endforeach