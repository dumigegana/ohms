<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    public function allocation()
    {
         return $this->belongsTo('App\Allocation');
    }

     public function facility()
    {
         return $this->belongsTo('App\Facility');
    }

    public function facility_status()
    {
        return $this->belongsTo('App\Facility_status');
    }

} 
