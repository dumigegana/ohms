<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House_category extends Model
{
   protected $dates = [
        'created_at', 'updated_at',  
    ];
    public function houses()
    {
        return $this->hasMany('App\House');
    }
     
    
}
