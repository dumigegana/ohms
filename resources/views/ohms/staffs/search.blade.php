@foreach($staffs as $staff)
<tr style="color: #980000;">
            <td>{{ $staff->solusi_id }}</td>
            <td>
                        
                        {{ $staff->user->full_name }}
                    </td>
                   
                    <td>
                        
                        {{ $staff->major->name }}
                    </td>
                    <td>
                        
                        {{ $staff->gender }}
                    </td>
                    <td class="center alligned">
                      @if ($staff->user->active == 1)
                        <div class="ui green tiny basic label pop" data-title="Active" data-variation="wide" data-content="Active user account" data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Disabled" data-variation="wide" data-content="Deactivated user account" data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
            
                <td > 
                  <div class="ui btn button">
                    <i class="edit icon"></i>
                    <a href="{{ route('OHMS::staffs_edit', ['id' => $staff->id ]) }}" class="item" style="color: #ffeaea;">
                      {{ trans('ohms.staffs_edit') }}
                    </a>
                </div> 
            </td>
          </tr>
          @endforeach