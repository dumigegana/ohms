<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_allocations', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('house_allocable_id');
            $table->string('house_allocable_type');
            $table->integer('house_id');
            $table->boolean('accepted')->default(false);
            $table->boolean('currrent')->default(false);
            $table->boolean('cleared')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_allocations');
    }
}
