@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::facilities') }}">{{ trans('ohms.facilities_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.facilities_edit_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.facilities_edit_title'))
@section('icon', "edit")
@section('subtitle', trans('ohms.facilities_edit_subtitle', ['name' => $row->name]))
@section('content')
<div class="ui doubling stackable grid container">
    <div class="ten wide column centered">
        <div class="ui very padded segment" style="background: #ffeaea; color: #980000;">
            <form class="ui form" method="POST">
                {{ csrf_field() }}
                @include('ohms/forms/master')
                <br>
                <button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button>
            </form>
        </div>
    </div>
</div>
@endsection
