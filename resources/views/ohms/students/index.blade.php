@extends('layouts.admin.panel')
@section('breadcrumb')
<div class="ui breadcrumb">
    <div class="active section">{{ trans('ohms.student_list') }}</div>
</div>
@endsection
@section('title', trans('ohms.student_list'))
@section('icon', "graduation")
@section('subtitle', trans('ohms.students_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $students->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.students.admin'))
             <center><a href="{{ route('OHMS::students_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_student') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
  			<table class="ui basic table ">
          <thead style="background: #ff8b8b;">
  			    <tr> 
                  <th style="color: #980000;">{{ trans('ohms.solusi_id') }}</th>
                   <th style="color: #980000;">{{ trans('ohms.full_name') }}</th>
                  <th style="color: #980000;">Major</th>
                  <th style="color: #980000;">Gender</th>
                  <th style="color: #980000;">Account Status</th>
                  <th style="color: #980000;">{{ trans('ohms.options') }}</th>
  			    </tr>
  			  </thead> 
  			  <tbody id="read">
                
          @foreach($students as $student)
					<tr style="color: #980000;">
						<td> {{ $student->solusi_id }}</td>
    				<td> {{ $student->user->full_name }} </td>
            <td> {{ $student->major->programme }} </td>
            <td> {{ $student->gender }} </td>
            <td class="center alligned">
               @if ($student->user->active == 1)
                <div class="ui green tiny basic label pop" data-title="Active" data-variation="wide" data-content="Active user account" data-position="top center"><span>
                  <i class="large green checkmark icon"></i></span>
                </div>
                  @else
                <div class="ui red tiny basic label pop" data-title="Disabled" data-variation="wide" data-content="Deactivated user account" data-position="top center">
                  <span><i class="large red close icon"></i></span>
                </div> 
                @endif
            </td>    				
            <td >
              <div class="ui btn top icon left pointing dropdown button">
                <i class="options icon"></i>
                <div class="menu">
                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                  <a href="{{ route('OHMS::students_edit', ['id' => $student->id]) }}" class="item">
                    <i class="edit icon"></i>
                      {{ trans('ohms.students_edit') }}
                  </a>
                </div>
                </div>
            </td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
        
          {!! $students->links() !!}
           
  		</div>
        <br>
  	</div>
  </div>
@endsection
@section('js')
<script type="text/javascript">
$('#search').on('keyup', function(){
  var keywords = $(this).val();          
  $.ajax({
    url:"{{ url('admin/search_stud') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);
      }
    });
            
  });

</script>
@endsection
