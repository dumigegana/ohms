@extends('layouts.admin.panel')
@section('breadcrumb')
<div class="ui breadcrumb">
    <div class="active section">{{ trans('ohms.user_list') }}</div>
</div>
@endsection
@section('title', trans('ohms.user_list'))
@section('icon', "users")
@section('subtitle', trans('ohms.users_subtitle'))

@section('content')
  <div class="ui one column doubling stackable grid container">
  	<div class="column">
  		<div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $users->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.users.admin'))
             <center><a href="{{ route('OHMS::users_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_user') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>  
  			<table class="ui basic table " >
          <thead style="background: #ff8b8b;">
  			    <tr>
                  <th style="color: #980000;">{{ trans('ohms.full_name') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.username') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.email') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.options') }}</th>                  
  			    </tr> 
  			  </thead>          
  			  <tbody id="read">
                @foreach($users as $user)
					<tr style="color: #980000;">
						<td>
                            <div class="text">                                
                                {{ $user->full_name }}
                                @if($user->su)
                                  <div class="ui red tiny left pointing basic label pop" data-title="{{ trans('ohms.super_user') }}" data-variation="wide" data-content="{{ trans('ohms.super_user_desc') }}" data-position="top center" >{{ trans('ohms.super_user') }}</div>
                                @elseif(OHMS::isAdmin($user))
                                  <div class="ui blue tiny left pointing basic label pop" data-title="{{ trans('ohms.admin_access') }}" data-variation="wide" data-content="{{ trans('ohms.admin_access_desc') }}" data-position="top center">{{ trans('ohms.admin_access') }}</div>
                                  @elseif(OHMS::isVisitor($user))                  
                    
                                            <div class="ui blue tiny left pointing basic label pop" data-title="{{ trans('ohms.admin_access') }}" data-variation="wide" data-content="{{ trans('ohms.admin_access_desc') }}" data-position="top center">Guest</div>       
                                  @elseif(OHMS::isStaff($user))                  
                    
                                            <div class="ui orange tiny left pointing basic label pop" data-title="{{ trans('ohms.admin_access') }}" data-variation="wide" data-content="{{ trans('ohms.admin_access_desc') }}" data-position="top center">Staff</div>                           
                                       
                                @else
                                  <div class="ui green tiny left pointing basic label pop" data-title="{{ trans('ohms.student_user') }}" data-variation="wide" data-content="{{ trans('ohms.student_user_desc') }}" data-position="top center">{{ trans('ohms.student_user') }}</div>
                                  
                                @endif
                            </div> 
                </td>
    				<td>
                        @if($user->banned)
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_banned') }}" class="pop red close icon"></i>
                        @elseif(!$user->active)
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_unactive') }}" class="pop btn warning icon"></i>
                        @endif
                        {{ $user->username }}
                    </td>

             <td>
                        @if($user->banned)
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_banned') }}" class="pop red close icon"></i>
                        @elseif(!$user->active)
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_unactive') }}" class="pop btn warning icon"></i>
                        
                        @endif
                        {{ $user->email }}
                    </td>
                <td >
   @if(!OHMS::isAdmin($user) or OHMS::loggedInUser()->su)
     <div class="five wide column">
      <center><a href="{{ route('OHMS::users_edit', ['id' => $user->id]) }}" type="submit" class="ui btn btn submit button"> <i class="edit icon"></i>{{ trans('ohms.users_edit') }}</a></center>
       @endif
       </div>
                      
						</td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
        {!! $users->links() !!}
  		</div>
        <br>
  	</div>
  </div>
@endsection
@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val(); 
  $.ajax({
    url:"{{ url('admin/search_user') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);
      }
    });
            
  });
</script>
@endsection