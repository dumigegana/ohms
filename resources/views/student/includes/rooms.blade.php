<div class="panel col-xs-12 ohms_body"> 
  <div class="panel-heading">
    <h3 class="panel-title col-xs-12" style="background: #FFBABA;"><center>{{ $block->name}}</center></h3>
  </div>
  <table class="table">
    <thead class="thead-inverse">
      <tr>
        <th>Room#</th>
        <th>Capacity</th>
        <th>Occupants</th>
      </tr>
    </thead>
    <tbody >
    @foreach($rooms as $room)
  @if($room->capacity > $room->occupants)
      <tr >
        <td  class="room" id="{{$room->id}}" data-toggle="modal" data-target="#roomModal" ><a href="#" style="text-decoration: underline; color: #990000;"> {{ $room->room_number}}</a></td>
        <td>{{ $room->capacity}}</td>
        <td>{{ $room->occupants}}</td>                  
      </tr>
        @endif
  @endforeach
    </tbody>
  </table>
</div>
    
    