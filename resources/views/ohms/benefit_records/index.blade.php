@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.benefit_records_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.benefit_records_title')) 
@section('icon', "bed")
@section('subtitle', trans('ohms.benefit_records_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $benefit_records->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.benefit_records.admin'))
             <center><a href="{{ route('OHMS::benefit_records_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add Record</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr>
              <th style="color: #980000;">Benefit</th>
              <th style="color: #980000;">Details</th>
              <th style="color: #980000;">Conditions</th>
              <th style="color: #980000;">Eligibility</th>
            </tr> 
          </thead>
          <tbody id="read">
            @foreach($benefit_records as $benefit_record) 
            <tr style="color: #980000;">
              <td>{{ $benefit_record->benefit }}</td>
              <td>{{ $benefit_record->block->hostel->gender }}</td>
              <td>{{ $benefit_record->block->name }}</td>
              <td>{{ $benefit_record->capacity }}</td>
              <td class="center alligned">
                      @if ($benefit_record->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Benefit_record ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this benefit_record " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::benefit_records_edit', ['id' => $benefit_record->id]) }}" class="ui btn button">{{ trans('ohms.benefit_records_edit') }}</a>
              </td>       
            </tr>
        @endforeach
          </tbody>
        </table>
        {!! $benefit_records->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_benefit_record') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection