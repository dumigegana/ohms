<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OHMS;
use Mail;
use File;


class User extends Authenticatable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at', 'created_at', 'updated_at',  
    ];
    
    protected $fillable = [
        'full_name', 'username', 'email', 'password', 'active', 'activation_key', 'register_ip','cell_number', 
    ];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_key',
    ];

    /**
    * Mutator to capitalize the name
    *
    * @param mixed $value
    */
    public function setNameAttribute($value){
        $this->attributes['name'] = ucwords($value);
    }

    /**
    * Returns all the roles from the user
    *
    */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
    * Returns true if the user has access to App
    *
    */
    public function isAdmin()
    {
        return $this->hasPermission('ohms.access');
    }

    public function isStudent()
        {
            return $this->hasRole('Student');
        }

    public function isVisitor()
        {
            return $this->hasRole('Visitor');
        }

    public function isStaff()
        {
            return $this->hasRole('Staff');
        }

    /**
    * Returns true if the user has the permission slug
    *
    * @param string $slug
    */
    public function hasPermission($slug)
    {
        foreach($this->roles as $role) {
            foreach($role->permissions as $perm) {
                if($perm->slug == $slug) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
    * Returns true if the user has the role
    *
    * @param string $name
    */
    public function hasRole($name)
    {
        foreach($this->roles as $role) {
            if($role->name == $name) {
                return true;
            }
        }
        return false;
    }

    // App 
    public function student()
    {
        return $this->hasOne('App\Student');
    }

    public function visitor()
    {
        return $this->hasOne('App\Visitor');
    }

    public function staff()
    {
        return $this->hasOne('App\Staff');
    }

    public function maintenances()
    {
        return $this->hasMany('App\Maintenance');
    }

    public function allocations()
    {
        return $this->hasManyThrough('App\Allocation', 'App\Student', 'user_id', 'student_id', 'id');
    }

}
