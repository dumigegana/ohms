     @foreach($house_allocations as $house_allocation) 
          <tr style="color: #980000;">
            <td>
              {{ $house_allocation->house_allocable->user->fullname }}
            </td>
            <td>{{ $house_allocation->house_allocable->cell }}</td>
            <td>{{ $house_allocation->house->number }}</td>
            <td>{{ $house_allocation->house->street }}</td>
            <td class="center alligned">
                @if ($house_allocation->cleared == 1)
                <div class="ui red tiny basic label pop" data-title="Moved" data-variation="wide" data-content="No longer staying Here." data-position="top center"><span>
                  <i class="large red checkmark icon"></i></span>
                </div>
                @else
                <div class="ui green tiny basic label pop" data-title="Present" data-variation="wide" data-content="Still Staying here." data-position="top center">
                    <span><i class="large green close icon"></i></span>
                </div> 
                @endif
              </td>
          </tr>
        @endforeach