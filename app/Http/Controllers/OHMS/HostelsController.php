<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Hostel;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class HostelsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.hostels.access');

        # Get all the hostels
        $hostels = Hostel::all();

        
        # Return the view
        return view('ohms/hostels/index', ['hostels' => $hostels]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.hostels.access');

        # Get the hostel
        $hostel = Hostel::find($id);

        # Return the view
        return view('admin/hostels/show', ['hostel' => $hostel]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.hostels.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.hostels.admin');

        # Find the hostel
        $row = Hostel::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'hostels';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/hostels/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.hostels.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.hostels.admin');

        # Find the row
        $row = Hostel::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'hostels';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::hostels')->with('success', trans('ohms.msg_hostel_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.hostels.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.hostels.admin');

        
        # All permissions
        $permissions = OHMS::permissions();

        # Return the view
        return view('ohms/hostels/create');
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.hostels.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.hostels.admin');

        # create new role
        $row = new Hostel;

        # Save the data
        $data_index = 'hostels';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::hostels')->with('success', trans('ohms.msg_hostel_created'));

    }
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.hostels.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.hostels.admin');

        # Select Item
        $hostel = OHMS::hostel('id', $id);

        if(!$hostel->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $hostel->delete();

        # Redirect the admin
        return redirect()->route('OHMS::hostels')->with('success', trans('ohms.msg_hostel_deleted'));
    }
}
