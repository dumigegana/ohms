@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::notices') }}">{{ trans('ohms.notices_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.notices_edit_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.notices_edit_title'))
@section('icon', "edit ")
@section('subtitle', trans('ohms.notices_edit_subtitle'))
@section('content')
<div class="ui container"> 
  <form class="ui form" method="POST">
    <div class="ui doubling stackable grid">
      <div class="row">
        <div class="ui one column doubling stackable grid container">
          <div class="ui very padded segment" style="background: #ffeaea;">
            {{ csrf_field() }}
            <div class="required field">
              <label style="color: #980000;">Title</label>
                <input type="text" name="title" value="{{ Request::old('title') ? Request::old('title') : isset($row) ? $row->title : "" }}">
            </div>
            <div class="field">
              <label style="color: #980000;">Body</label>
              <textarea  name="body" class="ckeditor" id="ckeditor-code"> {{ Request::old('body') ? Request::old('body') : isset($row) ? $row->body : "" }}</textarea>
            </div>
            <div class="field">
              <div class="inline field">
                <div class="ui slider checkbox">
                  <input value='checked' type="checkbox" tabindex="0" class="hidden" name="enable" {{ ($row->enable == 1 ? "checked":"") }}>
                  <label>Enable</label>
                </div>  
                <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
  </form>
</div>
@endsection
@section('js')
<script>
  var editor = ace.edit("ckeditor");
  editor.getSession().on('change', function(){
      $("#ckeditor-code").val(editor.getSession().getValue());
  });
</script>
@endsection