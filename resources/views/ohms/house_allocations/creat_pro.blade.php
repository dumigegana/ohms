@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::maintbs') }}">{{ trans('ohms.house_allocations_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.house_allocations_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.house_allocations_create_title'))
@section('icon', "plus ")
@section('subtitle', trans('ohms.house_allocations_create_subtitle'))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST" action=" {{route('OHMS::house_allo') }}" >
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea !important; color: #980000;">
                         {{ csrf_field() }}
                        <input name="house_allocable_type" type="hidden" value="App\Visitor">
                        <div class="required field">
                        <label style="color: #980000;">Visitor</label>
                         <select name="house_allocable_id" class="form-control" required>
                            <option value="" >Select visitor </option>
                                 @foreach($visitors as $visitor)
                                <option value="{{ $visitor->id }}" {{ (old("house_allocable_id") == $visitor->id  ? "checked":"") }}>{{ $visitor->user->fullname }}</option>
                                @endforeach
                            </select>
                            <br>
                        </div>
                        <div class="required field">
                        <br><br><br>
                        <label style="color: #980000;">Select the House to Allocate </label>
                         <select name="house_id" class="form-control" required>
                         <option value="">Select house </option>                            
                                @foreach($houses as $house)
                                <option value="{{ $house->id }}" {{ (old("house_id") == $house->id ? "checked":"") }}>{{ $house->number }} </option>
                                @endforeach
                            </select>
                            <br><br><br>  
                        </div>                                               
                    </div>
                    </div>                 
                    <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea !important; color: #980000;">
                         {{ csrf_field() }}
                       
                        <div class="field">
                            <label style="color: #980000;">Comments</label>
                            <textarea name="comments" type="text" placeholder="Comments" required value="{{ old('comments')}}"></textarea>
                            </div>
                            <br/>
                             

                        <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </form>
</div>
@endsection
