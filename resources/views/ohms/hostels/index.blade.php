@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.hostels_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.hostels_title'))
@section('icon', "university")
@section('subtitle', trans('ohms.hostels_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;"></div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.hostels.admin'))
             <center><a href="{{ route('OHMS::hostels_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.create_hostel') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
  			<table class="ui basic table ">
  			  <thead style="background: #ff8b8b;">
  			    <tr>
                  <th style="color: #980000;">Name</th>
                  <th style="color: #980000;">Gender</th>
                  <th style="color: #980000;">Options</th>
                 
  			  </thead>
  			  <tbody>
                @foreach($hostels as $hostel)
					<tr style="color: #980000;">
						<td>
                            <div class="text">
                            <?php $var = 'Male'; ?>
                                {{ $hostel->name }}
                                @if($hostel->gender === $var) 
                                  <div class="ui blue tiny left pointing basic label pop" data-title="Male Residance" data-variation="wide" data-content="This hostel is for male Students" data-position="top center" >Male</div>
                                @else
                                  <div class="ui pink tiny left pointing basic label pop" data-title="Female Residance" data-variation="wide" data-content="This hostel is for female Students" data-position="top center">Female</div>
                                @endif
                            </div>
                        </td>
                        <td>{{ $hostel->gender }}</td>
                        
                        <td>
                          @if($hostel->allow_editing or OHMS::loggedInUser()->su)
                              <div class="ui btn top icon left pointing dropdown button">
                                <i class="options icon"></i>
                                <div class="menu">
                                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                                  <a href="{{ route('OHMS::hostels_edit', ['id' => $hostel->id]) }}" class="item">
                                    <i class="edit icon"></i>
                                    {{ trans('ohms.hostels_edit') }}
                                  </a>
                                </div>
                              </div>
                          @else
                              <div class="ui disabled btn icon button">
                                  <i class="lock icon"></i>
                              </div>
                          @endif
						</td>
					</tr>
				@endforeach
  			  </tbody>
  			</table>
  		</div>
        <br>
  	</div>
  </div>
@endsection
