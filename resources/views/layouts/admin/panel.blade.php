<!DOCTYPE html>   

<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<title>Admin</title>
	<meta name="description" content="OHMS - Administration panel">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style type="text/css">
		body {
			margin: 0;
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 13px;
			line-height: 18px;
			color: #333333;
			background-color: #ffffff;

			padding: 0 20px; 
		}
		a {
			color: #0088cc;
			text-decoration: none;
		}
		a:hover {
			color: #005580;
			text-decoration: underline;
		}

		h2 { padding-top: 20px; }
		h2:first-of-type { padding-top: 0; }
		ul { padding: 0; }

		.no-inputtypes .box { color: red; }
		.inputtypes .box { color: green; }

		.bt_check{
		    background-color: #ffeaea !important;
		    color: #990000 !important;
	  	}
		.bt_check:hover{
		    background-color: #ff8b8b !important;
		}
		.bt_check:active{
		    background-color: #ffeaea !important;
		}

		.btn{
		    background-color: #990000 !important;
		    color: #ffeaea !important;
	  	}
		.btn:hover{
		    background-color: #ff8b8b !important;
		}
		.btn:active{
		    background-color: #ff7070 !important;
		}
		</style>
	{!! OHMS::includeAssets('ohms_header') !!}

	<link href="{{ URL::to( 'css/pagination.css') }}" rel="stylesheet">
	@yield('css') 

	<link rel="shortcut icon" href="{{ OHMS::ohmsFavicon() }}" >
</head>

<body onbeforeunload="" >
	<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
		{{ csrf_field() }}
	</form>

	<div class="ui inverted dimmer"><div class="ui text loader">Loading</div></div>


	@if(session('success'))
  <script>
    swal({
      title: "Success!",
      text: "{!! session('success') !!}.",
      type: "success",
      timer: 4000,
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('error'))
  <script>
    swal({
      title: "Sorry!",
      text: "{!! session('error') !!}",
      type: "error",
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('warning'))
  <script>
    swal({
      title: "Watch out!",
      text: "{!! session('warning') !!}",
      type: "warning",
      timer: 4000,
      confirmButtonText: "Ok",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if(session('info'))
  <script>
    swal({
      title: "Watch out!",
      text: "{!! session('info') !!}",
      type: "info",
      timer: 4000,
      confirmButtonText: "OK",
      confirmButtonColor: "#980000"
    });
  </script>
  @endif
  @if (count($errors) > 0)
  <script>
    swal({
      title: "Sorry!",
      text: "<?php foreach($errors->all() as $error){ echo "$error<br>"; } ?>",
      type: "error",
      confirmButtonText: "OK",
      confirmButtonColor: "#980000",
      html: true
    });
  </script> 
  @endif
	<div class="ui sidebar left-menu ">
		<div class="ui left fixed vertical menu" id="vertical-menu" style="background-color: #ffeaea !important;">
			<div id="vertical-menu-height">
				<a href="{{ route('OHMS::dashboard') }}" class="item logo-box">
					<div class="logo-container">
						<img class="logo-image ui fluid small image" src="{{ OHMS::ohmsLogo() }}">
					</div>
				</a>
					@if(OHMS::loggedInUser()->hasPermission('ohms.users.access'))
				<div class="item" style="background-color: #ffeaea !important;">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text"  style="color: #980000 !important;">{{ trans('ohms.user_manager') }}</span>
							<div class="menu">
								<a href="{{ route('OHMS::users') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.user_list') }}</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.users.create'))
								<a href="{{ route('OHMS::users_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.create_user') }}</a>
								@endif							
   
								<a href="{{ route('OHMS::staffs') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.staff_list') }}</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.users.create'))
								<a href="{{ route('OHMS::staffs_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.staff_create') }}</a>
								@endif
	                     
								<a href="{{ route('OHMS::students') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.student_list') }}</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.users.create'))
								<a href="{{ route('OHMS::students_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.student_create') }}</a>
								@endif
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.roles.access'))
					<div class="item" style="background-color: #ffeaea !important; color: #980000 !important;">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">{{ trans('ohms.role_manager') }}</span>
							<div class="menu">
								<a href="{{ route('OHMS::roles') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.role_list') }}</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.roles.create'))
								<a href="{{ route('OHMS::roles_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.create_role') }}</a>								
								@endif
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.visitors.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">Guests</span>
							<div class="menu">																
								<a href="{{ route('OHMS::visitors') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Guest List</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.visitors.create'))
								<a href="{{ route('OHMS::visitors_add') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Allocate Guests</a>
								@endif
								
								@if(OHMS::loggedInUser()->hasPermission('ohms.flights.access'))
								<a href="{{ route('OHMS::flights') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Flights</a>
								@endif
								@if(OHMS::loggedInUser()->hasPermission('ohms.flights.create'))
								<a href="{{ route('OHMS::flights_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add Flight</a>
								@endif
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.hostels.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">{{ trans('ohms.hostel_manager') }}</span>
							<div class="menu">
								<a href="{{ route('OHMS::hostels') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.hostel_list') }}</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.hostels.create'))
								<a href="{{ route('OHMS::hostels_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.create_hostel') }}</a>
								@endif
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.blocks.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">{{ trans('ohms.block_manager') }}</span>
							<div class="menu">
								<a href="{{ route('OHMS::blocks') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.block_list') }}</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.blocks.create'))
								<a href="{{ route('OHMS::blocks_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.create_block') }}</a>
								@endif
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.rooms.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">{{ trans('ohms.room_manager') }}</span>
							<div class="menu">
								<a href="{{ route('OHMS::rooms') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.room_list') }}</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.rooms.create'))
								<a href="{{ route('OHMS::rooms_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.create_room') }}</a>
								@endif
							</div>
						</div>
					</div>
					@endif					
					@if(OHMS::loggedInUser()->hasPermission('ohms.notices.create'))					
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">Notices</span>
							<div class="menu">
								<a href="{{ route('OHMS::notices_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Create Notice </a>
								<a href="{{ route('OHMS::notices') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">View Notices</a>
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.semesters.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">Semesters</span>
							<div class="menu">
								<a href="{{ route('OHMS::semesters') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">View Semesters</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.semesters.create'))
								<a href="{{ route('OHMS::semesters_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add Semesters</a>
								@endif
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.majors.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">Majors</span>
							<div class="menu">
								<a href="{{ route('OHMS::majors') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">View Majors</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.majors.create'))
								<a href="{{ route('OHMS::majors_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add Major</a>
								@endif
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.items.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">Maintenance Items</span>
							<div class="menu">
								@if(OHMS::loggedInUser()->hasPermission('ohms.maint_deps.access'))
								<a href="{{ route('OHMS::maint_deps') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">View Maintenance Departments</a>
								@endif
								@if(OHMS::loggedInUser()->hasPermission('ohms.maint_deps.create'))
								<a href="{{ route('OHMS::maint_deps_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add Maintenance Department</a>
								@endif
								<a href="{{ route('OHMS::items') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">View Maintenance Items</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.items.create'))
								<a href="{{ route('OHMS::items_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add Maintenance Item</a>
								@endif
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.facilities.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">Checklist Items</span>
							<div class="menu">
								<a href="{{ route('OHMS::facilities') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">View Checklist Items</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.facilities.create'))
								<a href="{{ route('OHMS::facilities_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add Checklist Item</a>
								@endif
								<a href="{{ route('OHMS::facility_statuses') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">View Checklist Statuses</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.facilities.create'))
								<a href="{{ route('OHMS::facility_statuses_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add Checklist Status</a>
								@endif
							</div>
						</div>
					</div>
					@endif

					@if(OHMS::loggedInUser()->hasPermission('ohms.houses.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">{{ trans('ohms.houseCategory_manager') }}</span>
							<div class="menu">
								<a href="{{ route('OHMS::house_categories') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.house_categories_list') }}</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.houses.create'))
								<a href="{{ route('OHMS::house_categories_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.create_house_category') }}</a>
								@endif
							</div>
						</div>
					</div>
					@endif
					
					
					@if(OHMS::loggedInUser()->hasPermission('ohms.houses.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">{{ trans('ohms.house_manager') }}</span>
							<div class="menu">
								<a href="{{ route('OHMS::houses') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.house_list') }}</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.houses.create'))
								<a href="{{ route('OHMS::houses_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">{{ trans('ohms.create_house') }}</a>
								@endif
							</div>
						</div>
					</div>
					@endif

					@if(OHMS::loggedInUser()->hasPermission('ohms.houses.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">House Allocations</span> 
							<div class="menu">
								@if(OHMS::loggedInUser()->hasPermission('ohms.house_allocations_dosa.access'))
								@php $alloc = 'Student' @endphp
								<a href="{{ route('OHMS::staff_allocations', ['alloc' =>$alloc]) }}"" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">MSQ Allocations</a>
								@endif
                                @if(OHMS::loggedInUser()->hasPermission('ohms.house_allocations_hr.access'))
								@php $alloc = 'Staff' @endphp
								<a href="{{ route('OHMS::staff_allocations', ['alloc' =>$alloc]) }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Staff Allocations</a>
								@endif
                                @if(OHMS::loggedInUser()->hasPermission('ohms.house_allocations_pr.access'))
								@php $alloc = 'Visitor' @endphp
								<a href="{{ route('OHMS::staff_allocations', ['alloc' =>$alloc]) }}"" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Guest Allocations</a>
								@endif
								@if(OHMS::loggedInUser()->hasPermission('ohms.houses.create'))
								<a href="{{ route('OHMS::allo_dosa') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Allocate Student</a>
								@endif
								@if(OHMS::loggedInUser()->hasPermission('ohms.houses.create'))
								<a href="{{ route('OHMS::allo_guest') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Allocate Guest</a>
								@endif
								@if(OHMS::loggedInUser()->hasPermission('ohms.houses.create'))
								<a href="{{ route('OHMS::allo_staff') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Allocate Staff</a>
								@endif
							</div>
						</div>
					</div>
					@endif

					@if(OHMS::loggedInUser()->hasPermission('ohms.courses.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">Manage Courses</span>
							<div class="menu">
								<a href="{{ route('OHMS::courses') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Course List</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.courses.create'))
								<a href="{{ route('OHMS::courses_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add Course</a>
								@endif
							</div>
						</div>
					</div>
					@endif

					@if(OHMS::loggedInUser()->hasPermission('ohms.sessions.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">Manage Class Sessions</span>
							<div class="menu">
								<a href="{{ route('OHMS::sessions') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Session List</a>
								@if(OHMS::loggedInUser()->hasPermission('ohms.sessions.create'))
								<a href="{{ route('OHMS::sessions_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add session</a>
								@endif
							</div>
						</div>
					</div>
					@endif
					@if(OHMS::loggedInUser()->hasPermission('ohms.departments.access'))
					<div class="item">
						<div class="ui btn-primary top labeled pointing dropdown button responsive-button">
							<span class="text responsive-text" style="color: #980000 !important;">Departments</span>
							<div class="menu">
								@if(OHMS::loggedInUser()->hasPermission('ohms.departments.access'))
								<a href="{{ route('OHMS::departments') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">View Maintenance Departments</a>
								@endif
								@if(OHMS::loggedInUser()->hasPermission('ohms.departments.admin'))
								<a href="{{ route('OHMS::departments_create') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Add Maintenance Department</a>
								@endif
							</div>
						</div>
					</div>
					@endif
										
					@if(OHMS::loggedInUser()->hasPermission('ohms.CRUD.access'))
					<div class="item">
						<div class="menu">
							<a href="{{ route('OHMS::CRUD') }}" class="item" style="color:#FF0000;">{{ trans('ohms.database_CRUD') }}</a>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>


		<div class="ui top fixed menu" style="background: #6b0000 ;" id="menu-div">
			<div class="item" id="menu">
				<div class="ui btn secondary button"><i class="bars icon"></i> {{ trans('ohms.menu') }}</div>
			</div>
			<div class="item" id="breadcrumb" style="color: #ffeaea; margin-left: 210px !important;">
				@yield('breadcrumb')
			</div>
			<div class="right menu">
				<div class="item">
					<div class="ui top labeled icon left pointing  button responsive-button" style="background: #6b0000 ;border: 2px; border-color: #990000; border-style: solid; border-radius: 10px;">
						<i class="dashboard icon" style="color: #ffeaea !important;"></i>
						<div class="menu">       
							@if(OHMS::loggedInUser()->isAdmin())
							<a style="color: #ffeaea !important;" href="{{ route('OHMS::dashboard') }}">Dashboard</a>
							@endif
						</div>
					</div>
				</div>
				<div class="item">
					<div class="ui btn top labeled icon left pointing dropdown button responsive-button" style="background: #6b0000  !important; border: 2px; border-color: #990000; border-style: solid; border-radius: 10px;">
							<i class="configure icon"></i>
							<span class="text responsive-text">Maintenance</span>
						<div class="menu" style="background-color: #ffeaea !important; ">
							<a href="{{ route('OHMS::maintenances') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Room Reports</a>
							<a href="{{ route('OHMS::maintbs') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Hostel Reports</a>
							@if(OHMS::loggedInUser()->hasPermission('ohms.maintenances.create'))
							<a href="{{ route('OHMS::maint_creatr') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Create Room Report</a>
							<a href="{{ route('OHMS::maint_createh') }}" class="item" style="background-color: #ffeaea !important; color: #980000 !important;">Create Hostel Report</a>
							@endif
						</div>
					</div>
				</div>
				@if(OHMS::loggedInUser()->hasPermission('ohms.reports.access'))
				<div class="item">
					<div class="ui top labeled icon left pointing button responsive-button" style="background: #6b0000 ;border: 2px; border-color: #990000; border-style: solid; border-radius: 10px;">
						<i class="bar chart icon" style="color: #ffeaea !important;"></i>
						<div class="menu">       
							@if(OHMS::loggedInUser()->isAdmin())
							<a style="color: #ffeaea !important;" href="{{ route('OHMS::index_report') }}">Reports</a>
							@endif
						</div>
					</div>
				</div>
				@endif         
					<div class="item">
						<div class="ui btn top labeled icon left pointing dropdown button responsive-button" style="background: #6b0000  !important; border: 2px; border-color: #990000; border-style: solid; border-radius: 10px;">
							<i class="user icon"></i>
							<span class="text responsive-text">{{ Auth::user()->username }}</span>
							<div class="menu" style="background-color: #ffeaea !important; ">
								<a href="{{ url('/') }}" style="color: #990000 !important;" class="item"><i class="home icon"></i>Home </a>
								<a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" style="color: #990000 !important;" class="item"><i class="sign out icon"></i>
									{{ trans('ohms.logout') }}
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="pusher back">
				<div class="menu-margin">
					<div class="content-title" style="padding-bottom: 20px; padding-left: 20px;"> 
						<div class="menu-pusher">
							<div class="column">
								<h2 class="ui header">
									<i class="@yield('icon') icon" style="color: #ffeaea"></i>
									<div class="content" style="color: #ffeaea">
										@yield('title')
										<div class="sub header" style="color: #ffeaea">
											<span>@yield('subtitle')</span>
										</div>
									</div>
								</h2>
							</div>
						</div>
					</div>
				</div>
				<div class="page-content" style="background: #ff8b8b;">
					<div class="menu-pusher" >
							@yield('content')
					</div>
				</div>
				<div class="page-footer" style="background: #4d0000 !important;">
					<div class="ui bottom fixed padded segment" style="background: #4d0000;" >
						<div class="menu-pusher">
							<div class="ui container">
								<a href="{{ url('/') }}" class="ui tiny header" style="color: #ffeaea;">
										Solusi Online Hostel Management System. Developed By</a>
								<a class="ui tiny header" style="color: #ffeaea;" href="https://ohmssaniblog.wordpress.com/"> Dumisani Ndlovu</a>
								<a class="ui tiny header right floated" style="color: #ffeaea;">&copy; Copyright OHMS 1.0</a>
								<a class="ui tiny header right floated" style="color: #ffeaea;">2016</a>
							</div>
						</div>
					</div>
				</div>
			</div>

				{!! OHMS::includeAssets('ohms_bottom') !!}

				@yield('js')
				<script>

					setInterval(function(){
						var footer = $('.page-footer');
						footer.removeAttr("style");
						var footerPosition = footer.position();
						var docHeight = $( document ).height();
						var winHeight = $( window ).height();
						if(winHeight == docHeight) {
							if((footerPosition.top + footer.height() + 3) < docHeight) {
								var topMargin = (docHeight - footer.height()) - footerPosition.top;
								footer.css({'margin-top' : topMargin + 'px'});
							}
						}
					}, 10);
					
				</script>

			</body>
			</html>