<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
     protected $dates = [
        'created_at', 'updated_at',  
    ];
     public function hostel() 
    {
        return $this->belongsTo('App\Hostel');
    }
     public function rooms()
    {
        return $this->hasMany('App\Room');
    } 
     public function majors()
    {
        return $this->belongsToMany('App\Major');
    }
    public function maintenances()
    {
        return $this->morphMany('App\Maintenance', 'maintenable');
    }
    public function allocations()
    {
        return $this->hasManyThrough('App\Allocation', 'App\Room', 'block_id', 'room_id', 'id');
    }

}
