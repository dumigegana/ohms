<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Mail;
use OHMS;

class ActivationController extends Controller
{
    public function activate($token = null)
    {
        if($token) {

            $user = OHMS::user('activation_key', $token);

            if($token == 'resend') {
    			
    			# Redirect the user to the main page
                return redirect()->route('OHMS::activate_account')->with('success', trans('ohms.activation_email_sent'));

    		} else {

                if($user) {
                    if($user->active) {
                        return redirect()->route('OHMS::activate_account')->with('error', trans('ohms.activation_user_already_activated'));
                    } else {
                        $user->active = true;
        				$user->save();
                        return redirect()->route('OHMS::activate_account')->with('success', trans('ohms.activation_account_activated'));
                    }
                } else {
                    # Redirect the user back to the activation page
                    return redirect()->route('OHMS::activate_account')->with('error', trans('ohms.activation_not_valid'));
                }

            }

        } else {
            # Return the activation form
    		return view('auth/deactivated');
        }
    }

    public function activateWithForm(Request $request)
    {
    	

    	# Call the activate function
    	return view('auth/deactivated');
    }
}
