<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Mail\Mailer;
use App\Mail\ConfirmId;
use Mail;
use App\Conf_stud;

class SendResetEmail implements ShouldQueue
{ 
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $conf_stud;
    /**
     * Create a new job instance.
     *
     * @param  Conf_stud  $conf_stud
     * @return void
     */ 
    public function __construct(Conf_stud $conf_stud)
    {
        $this->conf_stud = $conf_stud;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $emailAd = $this->conf_stud->student_id.'@solusi.ac.zw';
        $contents = [
            'title'=> 'Password Recovery ',
            'body'=> 'Hello, You recieved this email because your student Id number was used to attempt changing your Solusi OHMS password. If you recognise this attempt please confirm by clicking the button bellow or else just ignore this email.',
            'link'=> env('APP_URL').'/recov/'.$this->conf_stud->conf,
        ];
        Mail::to($emailAd)->send(new ConfirmId($contents)); 

    }
}
