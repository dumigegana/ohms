@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::maintenances') }}">{{ trans('ohms.maintenances_list') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.maintenances_edit_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.maintenances_edit_title'))
@section('icon', "edit")
@section('subtitle', trans('ohms.maintenances_edit_subtitle'))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST">
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column">
                    <div class="ui very padded segment">
                        {{ csrf_field() }}
                         <input name="id" type="hidden" value="$row->id"> 
                         <div class="required field">
                        <label>Room Number</label>
                         <select name="room_id" class="form-control">
                            <option value="" disabled selected>Select Room</option>
                                 @foreach($rooms as $room)
                                <option value="{{Request::old('room_id') ? Request::old('room_id') : isset($row) ? $row->id : "" }}">{{ $row->room->block->name }} {{$row->room_number}}</option>
                                @endforeach
                            </select>
                            <br><br><br>
                        </div>
                        <div class="required field">
                        <label>Select Item to Report </label>
                         <select name="facility_id" class="form-control">
                            <option value="" disabled selected>Select Item</option>
                                 @foreach($facilities as $facility)
                                <option value="{{Request::old('$facility->id') ? Request::old('facility_id') : isset($row) ? $row->facility_id : "" }}">{{ $row->facility->name }} </option>
                                @endforeach
                            </select>
                            <br><br><br>
                        </div>
                        v>
                       <br><br/>
                       <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                        </div> 
                   </div>
                </div>
            </div>
        </form>
    </div>
@endsection
