@extends('visitor.master')

@section('main-contant')

<div style="padding-left: 0; max-width: 100;">
 <div style="height: 0.5cm" class="rwd-break"></div>
   <div class="col-xs-12 panel" style="background-color: #950000; height: 1.8cm; padding-left: 0; padding-top: 0;">
    <center> <h1 style="color:#ffeaea;">Allocations</h1></center>
  </div>

  <div class="main_index col-xs-12" style="padding-left: 0;">
      
@if (Auth::check())  
  
  @php $has_accomo = auth()->user()->visitor->has_accomo @endphp 
   
  @if($has_accomo == true)
    @if($allocation->approved == 1)
        <div class="panel panel-default check_infor col-xs-12 ohms_body " style="padding-left: 0">  
          <p><h4>You may Check out of {{$allocation->room->block->name}} {{$allocation->room->room_number}} using the button bellow.</h4></p>
           <center> <button class="btn" name="{{ $allocation->id}}" id="addlist" data-toggle="modal" data-target="#checkModal0">Check Out </button></center>
           <br>
        </div>
    @else 
      <div class="panel panel-default check_infor col-xs-12 ohms_body" style="padding-left: 0;">  
        <p><h4>{{$allocation->room->block->name}} {{$allocation->room->room_number}} is now on hold. To be allocated to this room the check list must be submited within the next <b><span id="clock"  data-value="{{ $allocation->created_at->addDays(2) }}"></span></b>. Please check the condition of the room and select the appropriate button to eigther accept or reject this room.</h4></p>
        <div class="row">
          <div class="col-xs-6">
            <button class="btn col-xs-9  col-sm-4 col-sm-offset-5" name="{{ $allocation->id}}" id="addtolist" data-toggle="modal" data-target="#checkModal0">Accept </button>
          </div>
          <div class="col-xs-6">
            <a class="btn col-xs-9 col-sm-4 col-sm-offset-5" name="{{ $allocation->id}}" id="reject_room" data-toggle="modal" data-target="#rejectModal">Reject </a>
          </div>
        </div> 
        <br>
      </div>
      @include('visitor.includes.reject_room')
      @endif     
    @include('visitor.includes.modal')
  @else
    @php 
      $theo = auth()->user()->visitor->major_id;  
      $theology = 0;
    @endphp 
    
  <div class="panel panel-default ohms_body">
    <div class="container-fluid">
      <div class="row">         

         <div class="col-sm-4 col-sm-offset-2 col-xs-offset-1">
        <div class="required field" id="block">
          <br>
          <label>Hostel </label>
            <select name="facility_id" class="form-control" id="sel_blck">
             <option value=" " disabled selected>select </option>
            @if($theo === 5)
              @foreach($hostels as $hostel)                
                @foreach($hostel->blocks as $block)
                  @if($block->stud_year <= auth()->user()->visitor->year)

              <option value="{{ $block->id }}">{{ $block->name }} </option>
                  @endif
                @endforeach
              @endforeach

            @elseif($theo != 5)
              @foreach($hostels as $hostel)
                @foreach($hostel->blocks as $block)
                  @if($block->stud_year <= auth()->user()->visitor->year)
                    @if($block->theology ===  $theology)
                      <option value="{{ $block->id }}">{{ $block->name }} </option>
                    @endif
                  @endif
                @endforeach
              @endforeach
            @endif
            </select>
              </div>
      </div>
      </div><br>
      </div>
      </div>
      </div>        
        <div class="col-xs-12" id="room_list" style="padding-left: 0;" > 
        </div>
        @include('visitor.includes.confirm_room')
      @endif
      <div class="col-xs-12" id="profile" style="padding-left: 0;">     
    @include('visitor.includes.profile') 
    </div> 
@endif

</div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/jquery.countdown.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.check_ajax').hide('400');
    $('#block').hide('400');
    var all_date = $('#clock').attr("data-value");
    $('#clock').countdown(all_date, function(event) {
      var totalHours = event.offset.totalDays * 24 + event.offset.hours;
      $(this).html(event.strftime(totalHours + ' hr %M min'));
    });
    $(document).on('change','#semO',function(){
      $('#block').show('400');
    });
    $(document).on('change','#block',function(){      
      $('#profile').hide('400');      
      $('#room_list').show('400');
      var block_id= $('select[id="sel_blck"] option:selected').val();
      var block_name = $('select[id="sel_blck"] option:selected').text();
      $.ajax({
        type: 'get',
        url: "{{ url('getblocks') }}",
        data: {"_token": $('#token').val(), 'bl_id':block_id},
        async: true,
        success: function(data){;
          $('#room_list').html(data);
        },
        error:function(){
          alert('Ooops something went wrong!');
        },
      });
    });
    $(document).on('click','.room', function(){
      var room_id = $(this).attr("id");
      var room = $(this).text();
      var block = $('select[id="sel_blck"] option:selected').text();
      $('#room').val(room_id);
      $("#roomModal .mod_body p").html('<b>'+block+'<b> Room Number <b>'+room+'.<b>');
    })
    $(document).on('click','.accept_room', function(){
      var room_id = $('#room').val();
      var semester_id = $('#semO').val();
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');  
      $.ajax({
        type:'POST',
        url:"{{ url('bookin') }}",
        data:{"_token": CSRF_TOKEN, 'room_id': room_id, 'semester_id': semester_id},
        async: true,
        success:function(data){
          $('.main_index').load(location.href + ' .main_index', function(){            
            $('#profile').show('400');
            $('#room_list').hide('400');   
            $('#check_ajax').hide('400');
            var all_date = $('#clock').attr("data-value");
            $('#clock').countdown(all_date, function(event) {
              var totalHours = event.offset.totalDays * 24 + event.offset.hours;
              $(this).html(event.strftime(totalHours + ' hr %M min'));
            });
          });           
        },
        error:function(){
          alert('Ooops something went wrong!');
        },
      });
    });

    $(document).on('click', '.status', function(event){
      var text = $(this).attr('id');        
      $('.pos').text(' ');
      $('.mod_tittle').text('Change the status of the '+text);
      $('.saveTemp').hide('400');
      $('.saveChanges').show('400');             
    });

    $(document).on('click', '.reject_room', function(event){
      var allocation_id = $('input[name="rej_all"]').val();
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        type:'post',
        url:"{{ url('rejectroom') }}",
        data:{"_token": CSRF_TOKEN, "allocation_id": allocation_id},
        async: true,
        success:function(data){
          $('.main_index').load(location.href + ' .main_index', function(){
            $('#block').hide('400');           
            $('#profile').show('400');     
            // $('#room_list').show('400');            
          });           
        },
      });
    });   

    $(document).on('click', '#addtolist', function(event){
      $('.check_infor').hide('400');
      $('.check_ajax').show('400');
      $('#profile').hide('400');
      $('#cond_in').val('1');
      if ($('.row_li').length == $('.itcount').val()) {
        $('.check_submit').show();
      }
    });

    $(document).on('click', '#addlist', function(event){
      $('.check_infor').hide('400');
      $('.check_ajax').show('400');
      $('#profile').hide('400');
      $('#cond_in').val('0');
      if ($('.row_li').length == $('.itcount').val()) {
        $('.check_submit').show();
      }
    });

    $(document).on('click', '.saveTemp', function(event){
      var pos = $(this).attr('data-target');
      var facility_id = $('.fac_'+pos).val();
      var allocation_id = $('.alloc_'+pos).val();
      var facility_status_id = $('select[name="status_' + pos + '"] option:selected').val();
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        type:'POST',
        url:"{{ url('checkin_t') }}",
        data:{"_token": CSRF_TOKEN, "facility_id": facility_id, "allocation_id": allocation_id, "facility_status_id": facility_status_id, "pos": pos},
        async: true,
        success:function(data){
           $('#list').load(location.href + ' #list')
          },
      });
    });

    $(document).on('click', '.saveChanges', function(event){
      var pos = $(this).attr('data-target');
      var facility_id = $('.fac_'+pos).val();
      var allocation_id = $('.alloc_'+pos).val();
      var facility_status_id = $('select[name="status_' + pos + '"] option:selected').val();
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        type:'post',
        url:"{{ url('checkin_t') }}",
        data:{"_token": CSRF_TOKEN, 'facility_id': facility_id, 'allocation_id': allocation_id, 'facility_status_id': facility_status_id, 'pos': pos},
        async: true,
        success:function(data){
          $('.check_ajax').load(location.href + ' .check_ajax', function(event){
            if ($('.row_li').length == $('.itcount').val()) {
              $('.check_submit').show();
            }
          });
        },  
      });
    });
    $(document).on('click', '#mod_last', function(event){
      $('.check_submit').show();
    });      
  });
    
    
    </script>
@endsection