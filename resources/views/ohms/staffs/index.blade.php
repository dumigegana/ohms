@extends('layouts.admin.panel')
@section('breadcrumb')
<div class="ui breadcrumb">
    <div class="active section">{{ trans('ohms.staff_list') }}</div>
</div>
@endsection
@section('title', trans('ohms.staff_list'))
@section('icon', "graduation")
@section('subtitle', trans('ohms.staffs_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $staffs->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.staffs.admin'))
             <center><a href="{{ route('OHMS::staffs_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add Staff</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b;">
            <tr> 
                  <th style="color: #980000;">{{ trans('ohms.solusi_id') }}</th>
                   <th style="color: #980000;">{{ trans('ohms.full_name') }}</th>
                  <th style="color: #980000;">Department</th>
                  <th style="color: #980000;">Gender</th>
                  <th style="color: #980000;">Status</th>
                  <th style="color: #980000;">National Id</th>
                  <th style="color: #980000;">Marital Status</th>
                  <th style="color: #980000;">{{ trans('ohms.options') }}</th>
            </tr>
          </thead> 
          <tbody id="read">
                
          @foreach($staffs as $staff)
          <tr style="color: #980000;">
            <td> {{ $staff->solusi_id }}</td>
            <td> {{ $staff->title}} {{ $staff->user->full_name }} </td>
            <td> {{ $staff->department->name }} </td>
            <td> {{ $staff->gender }} </td>
            <td class="center alligned">
               @if ($staff->status == "Serving")
                <div class="ui green tiny basic label pop" data-title="Active" data-variation="wide" data-content="Currently Serving" data-position="top center"><span>
                  <i class="large green checkmark icon"></i></span>
                </div>
                  @else
                <div class="ui red tiny basic label pop" data-title="Disabled" data-variation="wide" data-content="{{$status}}" data-position="top center">
                  <span><i class="large red close icon"></i></span>
                </div> 
                @endif
            </td> 
            <td> {{ $staff->national_id }} </td>          
            <td> {{ $staff->marital }} </td>          
            <td >
              <div class="ui btn top icon left pointing dropdown button">
                <i class="options icon"></i>
                <div class="menu">
                  <div class="header">{{ trans('ohms.editing_options') }}</div>
                  <a href="{{ route('OHMS::staffs_edit', ['id' => $staff->id]) }}" class="item">
                    <i class="edit icon"></i>
                      {{ trans('ohms.staffs_edit') }}
                  </a>
                </div>
                </div>
            </td>
          </tr>
        @endforeach
          </tbody>
        </table>
        
          {!! $staffs->links() !!}
           
      </div>
        <br>
    </div>
  </div>
@endsection
@section('js')
<script type="text/javascript">
$('#search').on('keyup', function(){
  var keywords = $(this).val();          
  $.ajax({
    url:"{{ url('admin/search_stud') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);
      }
    });
            
  });

</script>
@endsection
