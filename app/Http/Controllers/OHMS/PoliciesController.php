<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Policy;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class PoliciesController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.policies.access');

        # Get all the policies
        $policies = Policy::paginate(50);

        # Return the view
        return view('ohms/policies/index', ['policies' => $policies]);
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.policies.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $policies = Policy::paginate(100);
        }
        else {       
          $policies = Policy::where('name','like', $post . '%')                   
                             ->orwhere('description', 'like',  $post . 
                            ->paginate(100);
        }
        $read = view('ohms/policies/search', ['policies' => $policies]);
        return $read;
    }
    public function show($id)
    {
        OHMS::permissionToAccess('ohms.policies.access');

        # Get the policy
        $policy = Policy::find($id);

        # Return the view
        return view('admin/policies/show', ['policy' => $policy]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.policies.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.policies.admin');

        # Find the policy
        $row = Policy::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'policies';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/policies/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.policies.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.policies.admin');

        # Find the row
        $row = Policy::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'policies';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::policies')->with('success', trans('ohms.msg_policy_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.policies.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.policies.admin');

        # Get all the data
        $data_index = 'policies';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/policies/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.policies.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.policies.admin');

        # create new role
        $row = new Policy;

        # Save the data
        $data_index = 'policies';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::policies')->with('success', trans('ohms.msg_policy_created'));

    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.policies.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.policies.admin');

        # Select Item
        $policy = OHMS::policy('id', $id);

        if(!$policy->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }     

        # Delete Item
        $policy->delete();

        # Redirect the admin
        return redirect()->route('OHMS::policies')->with('success', trans('ohms.msg_hostel_deleted'));
    }
     
}
