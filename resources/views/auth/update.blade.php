 @extends('layouts.auth')
@section('content')
        <div class="loginSection" >
        	<div class="header">{{ trans('Change Password') }}</div> 
        </div>
           <div class="innerBox">
            <form method="POST" action="{{ route('updatestud') }}"> 
                {{ csrf_field() }}
                <input name='username' type="hidden" value="{{ $id->student_id }}" required>
                <label style="color: #980000; float: left;">Password</label>
            	<input name='password' type="password" placeholder="Password" required>
            	 <br><br>
            	<label style="color: #980000; float: left;">{{trans('ohms.repeat_password')}}</label>
                <input name='password_confirmation' type="password" placeholder="Repeat Password" required>
                
@endsection



 