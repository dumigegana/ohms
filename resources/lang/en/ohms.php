<?php

return [

    /*
    |--------------------------------------------------------------------------
    | OHMS Language Lines
    |--------------------------------------------------------------------------
    |
    | Used on OHMS Administration Panel
    |
    */

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    */

    'welcome_user' => "Welcome back, :name",
    'dashboard' => "Dashboard",
    'options'   => "Options",
    'continue'  => "Continue",
    'back'  => "Back",
    'create' => "Add",
    'confirmation'  => "Confirmation",
    'submit'    => "Submit",
    'student_user'=> "Student",
    'visitor_user'=> "Visitor",
    'super_user'    => "Super User",
    'super_user_desc'   => "This user is a Webmaster",
    'super_user_role'   => "Super Role",
    'student_user_desc' => 'This user is a Student',
    'visitor_user_desc' => 'This user is a Visitor',
    'super_user_role_desc'  => "Reserved role for webmasters",
    'admin_access_role' => "OHMS Admin Access",
    'admin_access_role_desc'    => "The role users have access to the OHMS admin panel",
    'admin_access'  => "OHMS Admin Access",
    'admin_access_desc' => "This user have access to the OHMS admin panel",
    'name'  => "Name",
    'full_name'  => "Full Name",
    'username'  => "Username",
    'email' => "Email",
    'users' => "Users",
    'posts' => "Posts",
    'views' => "Views",
    'access'    => "Access",
    'permissions'   => "Permissions",
    'country'   => "Country",
    'editing_options'   => "Editing options",
    'advanced_options'  => "Advanced options",
    'su_role' => "Super Role",
    'su_role_desc' => "Super roles can't be assigned",
    'unassignable_role' => "Unassignable Role",
    'unassignable_role_desc' => "This role can't be assigned",
    'unassignable_permission'   => "Unassignable Permission",
    'unassignable_permission_desc'  => "This permission can't be assigned",
    'unassignable_permission_unlocked'  => "Unlocked Unassignable Permission",
    'unassignable_permission_unlocked_desc' => "This permission can't be assigned, however you have the rights to unlock it",
    'unassignable_role_unlocked'    => "Unlocked Unassignable Role",
    'unassignable_role_unlocked_desc'   => "This role can't be assigned, however you have the rights to unlock it",
    'su_permission_and_role'  => "Super Permission & Role",
    'su_permission_and_role_desc' => "You are not able to modify this permission due to itself and the fact that it's a super role",
    'roles' => "Roles",
    'hostels' => "Hostels",
    'description'   => "Description",
    'slug'  => "Slug",
    'check_all' => "Check All",
    'uncheck_all' => "Uncheck All",
    'missing_title'  => "We're sorry!",
    'missing_subtitle'   => "There are currently no :element created.",
    'okai'  => "Ok",
    'avatar'    => "Avatar",
    'information'   => "Information",
    'join_date' => "Join Date",
    'last_updated' => "Last Updated",
    'profile'   => "Profile",
    'logout'    => "Logout",
    'edit'  => "Edit",
    'delete'    => "Delete",
    'table' => "Table",
    'columns'   => "Columns",
    'rows'  => "Rows",
    'post_views'    => "Post Views",
    'password_not_correct'  => "The password is not correct",
    'visit_site'    => "Visit Site",
    'account_banned'    => "Your account has been banned",
    'account_deactivated'    => "Your account needs activation. Contact the System Adnim.",
    'login_title'   => "Login",
    'register_title'    => "Register",
    'password'  => "Password",
    'repeat_password'   => "Repeat Password",
    'reset_password'    => "Reset Password",
    'send_password_link'    => "Send Password Reset Link",
    'forgot_password'   => "Forgot password?",
    'whops' => "Whoops!",
    'hello' => "Hello!",
    'regards'   => "Regards",
    'link_fail' => "If you’re having trouble clicking the ':name' button, copy and paste the URL below into your web browser:",
    'decrypt_fail'  => "Cannot decrypt the value",
    'already_installed' => 'OHMS is already installed',
    'installing'    => "Installing OHMS...",
    'welcome_to_ohms'    => "Welcome to OHMS!",
    'solusi_id' => "Solusi ID",
    'gender' => "Gender",
    'degree_level' => "Degree Level",
    'year' => "Year",
    'cell_number' => "Cell",
    /*
    |--------------------------------------------------------------------------
    | Colors Lines
    |--------------------------------------------------------------------------
    */

    'colors_red'    => "Red",
    'colors_orange' => "Orange",
    'colors_yellow' => "Yellow",
    'colors_olive'  => "Olive",
    'colors_green'  => "Green",
    'colors_teal'   => "Teal",
    'colors_blue'   => "Blue",
    'colors_purple' => "Purple",
    'colors_pink'   => "Pink",
    'colors_brown'  => "Brown",
    'colors_gray'   => "Gray",
    'colors_black'  => "Black",

    /*
    |--------------------------------------------------------------------------
    | Dropdowns Lines
    |--------------------------------------------------------------------------
    */

    'dropdown_manual_activation'    => "Manual activation",
    'dropdown_send_activation_mail' => "Send Activation e-mail",
    'dropdown_activated_by_default' => "Activated by default",

    /*
    |--------------------------------------------------------------------------
    | Error Pages Lines
    |--------------------------------------------------------------------------
    */

    'error_registrations_disabled'  => "Registrations are not enabled",
    'error_relation_value'  => "The field :field has a value that is unavailable acording to it's relationship",
    'error_no_rights_against_admin' => "You have no rights agaist a user with admin access",
    'error_must_be_admin'   => "You need to have admin access to perform this action",
    'error_must_be_student'   => "Only students can perform this action.",    
    'error_must_be_visitor'   => "Only visitors can perform this action.",
    'error_editing_disabled'    => "You are not allowed to edit this",
    'error_security_reasons'    => "For security reasons you can't delete this",
    'error_security_reasons_default_role'   => "For security reasons you can't delete the default user role",
    'error_user_delete_yourself'  => "You can't delete yourself",
    'error_not_allowed' => "You are not allowed to perform this action",

    /*
    |--------------------------------------------------------------------------
    | Message Lines
    |--------------------------------------------------------------------------
    */


    'msg_mail_sent' => "A :subject Link sent to :emailAd",
    'msg_account_exist' => "The account for :sol_id already exist. Please login or resert your password.",
    'msg_account_not_exist' => "The account for :sol_id does not exist. Please signUp",
    'msg_user_created'  => "The user has been addeded",
    'msg_user_edited'   => "The user has been edited",
    'msg_user_roles_edited' => "The user roles have been updated",
    'msg_user_deleted'   => "The user has been deleted",
    'msg_user_update_settings'  => "The users settings has been updated",
    'msg_permission_created'    => "The permission has been added",
    'msg_permission_updated'    => "The permission has been updated",
    'msg_permission_deleted'    => "The permission has been deleted",
    'msg_role_edited'   => "The role has been edited",
    'msg_role_created'  => "The role has been added",
    'msg_role_perms_updated'    => "The role permissions have been updated",
    'msg_role_deleted'   => "The role has been deleted",
    'msg_hostel_edited'   => "The Hostel has been edited",
    'msg_hostel_created'  => "The Hostel has been added",
    'msg_hostel_perms_updated'    => "The Hostel permissions have been updated",
    'msg_hostel_deleted'   => "The Hostel has been deleted",    
    'msg_room_edited'   => "The room has been edited",
    'msg_room_created'  => "The room has been added",
    'msg_room_perms_updated'    => "The room permissions have been updated",
    'msg_room_deleted'   => "The room has been deleted",
    'msg_houseCategory_edited'   => "The House Category has been edited",
    'msg_houseCategory_created'  => "The House Category has been added",
    'msg_houseCategory_perms_updated'    => "The House Category permissions have been updated",
    'msg_houseCategory_deleted'   => "The House Category has been deleted",    
    'msg_house_edited'   => "The house has been edited",
    'msg_house_created'  => "The house has been added",
    'msg_house_perms_updated'    => "The house permissions have been updated",
    'msg_house_deleted'   => "The house has been deleted",
    'msg_row_created'   => "The row has been added",
    'msg_row_saved' => "The row #:id has been saved!", 
    'msg_row_deleted'   => "The row has been deleted",
    'msg_student_created'  => "The student has been added",
    'msg_student_edited'   => "The student details has been edited",
    'msg_student_deleted'   => "The student has been deleted",    
    'msg_visitor_created'  => "The Room has been allocated to the guest",
    'msg_visitor_edited'   => "The Guest details has been edited",
    'msg_visitor_deleted'   => "The Guest / Room details has been deleted",
    'msg_semester_created'  => "The semester has been added",
    'msg_semester_edited'   => "The semester has been edited",
    'msg_semester_deleted'   => "The semester has been deleted",
    'msg_semester_update_settings'  => "The semesters settings has been updated",
    

    /*
    |--------------------------------------------------------------------------
    | Mail Lines
    |--------------------------------------------------------------------------
    */

    'notifications_welcome_message_subject'  => "Welcome to :title!",
    'notifications_welcome_message_msg' => "Welcome :user to :title, thanks for joining us!",
    'notifications_welcome_message_action'  => "View our site",
    'notifications_account_activation_subject'  => "Activate your account on :title!",
    'notifications_account_activation_msg'  => "Please activate your account (:email) on :title!",
    'notifications_account_activation_action'   => "Activate account",

    /*
    |--------------------------------------------------------------------------
    | Master Editor Language Lines
    |--------------------------------------------------------------------------
    */

    'form_no_fields_title'  => "We're sorry!",
    'form_no_fields_subtitle'   => "There are currently no fields available to edit.",

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'menu'  => "Menu",
    'language'  => "Language",

    'user_manager'  => "Users",
    'role_manager'  => "Roles",
    'hostel_manager'  => "Hostels",
    'room_manager'  => "Rooms",
    'houseCategory_manager'  => "House Categories",
    'house_manager'  => "Houses",
    'permission_manager'    => "Permissions",
    'developer_tools'    => "Developer Tools",
    'settings'  => "Settings",

    'user_list' => "User List",
    'create_user'   => "Register User",
    'student_list' => "Student List",
    'student_create'   => "Register Student",    
    'visitor_list' => "Visitor List",
    'visitor_create'   => "Register Visitor",
    'hostel_list' => "hostel List",
    'create_hostel'   => "Add hostel",
    'room_list' => "Room List",
    'create_room'   => "Add Room",
    'users_settings'    => "Users Settings",
    'role_list' => "Role List",
    'room_list' => "Room List",
    'create_role'   => "Add Role",
    'permission_list'  => "Permission List",
    'create_permission' => "Add permission",
    'database_CRUD'  => "Advanced Settings ",
    'general_settings'   => "General Settings",
    'about_subtitle'    =>  "Information about OHM",
    'releases'  => "Releases",
    'commits'   => "Commits",

    /*
    |--------------------------------------------------------------------------
    | Install Lines
    |--------------------------------------------------------------------------
    */

    'installer' => "OHM Installer",
    'install_welcome'   => "Welcome!",

    'install_app_info'  => "Application Information",
    'install_app_name'  => "Application Name",

    'install_personal_info'    => "Supper Admin Information",
    'install_your_name' => "Your name",
    'install_your_email' => "Your email",
    'install_your_password' => "Password",
    'install_your_password_r' => "Repeat password",
   

    'install_roles_info'    => "Roles Information",
    'install_default_admin_role_name' => "Default Admin Role",
    'install_default_role_name' => "Default User Role",

    'install_database_info' => "Database Information",
    'install_database_username' => "Database username",
    'install_database_password' => "Database password",
    'install_database_name' => "Database name",
    'install_database_host' => "Database host",
    'install_database_port' => "Database port",

    'install_mail_info' => "Mail Information",
    'install_mail_driver'   => "Mail driver",
    'install_mail_host' => "Mail Host",
    'install_mail_port' => "Mail Port",
    'install_mail_username' => "Mail Username",
    'install_mail_password' => "Mail Password",
    'install_mail_encryption'   => "Mail Encryption",
    'install_mail_from' => "From Email",
    'install_mail_name' => "From Name",   

    'install_ohms'   => "Install OHMS",

    'installer_success' => "OHM :version has been installed!",


    /*
    |--------------------------------------------------------------------------
    | Security Confirmation Lines
    |--------------------------------------------------------------------------
    */

    'security_confirm_title'    => "Security Confirmation",
    'security_confirm_subtitle' => "Are you sure you want to continue?",

    'security_description_title'    => "What is this page?",
    'security_description'  => "This page is a confirmation page to avoid things
                                you might not wanted to do, or just to let you
                                know this action can not be undone and so it
                                requires a bit more than a single click to go
                                ahead with it.",

    /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    */

    'users_subtitle'    => "List of all the users",
    'students_subtitle'    => "List of all the students",
    'visitors_subtitle'    => "List of all the visitors",
    'hostels_subtitle'    => "List of all the hostels",
    'rooms_subtitle'    => "List of all the Rooms",
    'semesters_subtitle'    => "List of all the Semesters ",
    
    'users_status_ok'   => "Everything Good",
    'users_status_unactive' => "Activation Required",
    'users_status_banned'   => "Access Banned",
    'users_edit'  => "Edit User",
    'users_edit_roles'  => "Edit Roles",
    'users_delete'  => "Delete User",
    'users_no_users'    => "No Users",
    'students_edit'  => "Edit student",
    'students_delete'  => "Delete student",
    'students_no_students'    => "No students",
    'visitors_edit'  => "Edit visitor",
    'visitors_delete'  => "Delete visitor",
    'visitors_no_visitors'    => "No visitors",

    'hostels_edit'  => "Edit Hostel",
    'hostels_delete'  => "Delete Hostel",
    'hostels_no_hostel'    => "No Hostels",
    'hostels_total_hostel' => "Total Hostels",
    'rooms_edit'  => "Edit Room",
    'rooms_delete'  => "Delete Room",
    'rooms_no_rooms'    => "No Rooms",
    'rooms_total_rooms' => "Total Rooms",
    'semesters_edit'  => "Edit Semester",
    'semesters_delete'  => "Delete Semester",
    'semesters_no_semesters'    => "No Semesters",
    'semesters_total_semesters' => "Total Semesters",
    'semesters_create_title'    => "Add Semester",
    'semesters_create_subtitle' => "Add a new Semester",
    'semesters_edit_title'  => "Edit Semester",
    'semesters_edit_subtitle'   => "Editing Semester",
    'semesters_title'   => "Semester List",
    'maintenances_edit'  => "Edit Maintenance Report",
    'maintenances_delete'  => "Delete Maintenance Report",
    'maintenances_changed'  => "Change Maintenance Report status",
    'maintenances_no_maintenances'    => "No Maintenances",
    'maintenances_total_maintenances' => "Total Maintenances reports",
    'maintenances_create_title'    => "Add Maintenance report",
    'maintenances_create_subtitle' => "Add a new Maintenance report",
    'maintenances_edit_title'  => "Edit Maintenance report",
    'maintenances_edit_subtitle'   => "Editing Maintenance report",
    'maintenances_title'   => "Maintenance reports List",
    'msg_maintenance_created'  => "The maintenance report has been added",
    'msg_maintenance_edited'   => "The maintenance report has been edited",
    'msg_maintenance_deleted'   => "The maintenance report has been deleted",
    'msg_maintenance_changed'   => "The maintenance report status has been changeed",
    'msg_maintenance_update_settings'  => "The maintenances report settings has been updated",
    'maintenances_subtitle'    => "List of all the Rooms to be repaired",
    'maintenanceh_subtitle'    => "List of all the Hostels to be repaired",
    'notices_edit'  => "Edit Notice Report",
    'notices_delete'  => "Delete Notice Report",
    'notices_no_notices'    => "No Notices",
    'notices_total_notices' => "Total Notices reports",
    'notices_create_title'    => "Add Notice report",
    'notices_create_subtitle' => "Add a new Notice report",
    'notices_edit_title'  => "Edit Notice report",
    'notices_edit_subtitle'   => "Editing Notice report",
    'notices_title'   => "Notice reports List",
    'msg_notice_created'  => "The notice has been created",
    'msg_notice_edited'   => "The notice has been edited",
    'msg_notice_deleted'   => "The notice has been deleted",
    'msg_notice_update_settings'  => "The notices settings has been updated",
    'maint_deps_edit'  => "Edit Maintenance Department",
    'maint_deps_delete'  => "Delete the Maintenance Department",
    'maint_deps_no_maint_deps'    => "No Maintenance Department",
    'maint_deps_total_maint_deps' => "Total Maintenance Department",
    'maint_deps_create_title'    => "Add a Maintenance Department",
    'maint_deps_create_subtitle' => "Add a new Maintenance Department.",
    'maint_deps_edit_title'  => "Edit Maintenance Department Maint_dep",
    'maint_deps_edit_subtitle'   => "Editing Maintenance Department list",
    'maint_deps_subtitle'   =>'Maintenance Department',
    'maint_deps_title'   => "Maintenance Department List",
    'msg_maint_dep_created'  => "The Maintenance Department has been created",
    'msg_maint_dep_edited'   => "The Maintenance Department has been edited",
    'msg_maint_dep_deleted'   => "The Maintenance Department has been deleted",
    'msg_maint_dep_update_settings'  => "The Maintenance Department settings has been updated",
    'msg_check_out'  => "You have successfully check out. God bless you.",
    'msg_check_in'   => "You have successfully completed the booking process.",
    'msg_visitors_created'   => "The room has been Booked successfully",
    'msg_allocations_created'   => "The room has been Booked successfully. Please complete the Checkin form before the end of the next 3 days",
    'msg_report_submited'  => "The report has been submited. Thank you for your cooperation",
    'reports_subtitle'    => "View Reports",
    'reports_title'   => "OHMS Reports",
    'notices_subtitle'    => "List of all the Rooms to be repaired",

    'users_edit_title'  => "Edit User",
    'users_edit_subtitle'   => "Editing user: :full_name",
    'users_edit_roles_title'    => "Edit Roles",
    'users_edit_roles_subtitle' => "Editing user: :full_name",

    'students_edit_title'  => "Edit student",
    'students_edit_subtitle'   => "Editing student: :suid",
    'visitors_edit_title'  => "Edit visitor",
    'visitors_edit_subtitle'   => "Editing visitor: :suid",
    'rooms_edit_title'  => "Edit Room",
    'rooms_edit_subtitle'   => "Editing Room",
    'rooms_title'   => "Room List",
    'hostels_title'   => "Hostel List",
    'hostels_edit_title'  => "Edit Hostel",
    'hostels_edit_subtitle'   => "Editing Hostel",
    
    'users_create_title'    => "Add User",
    'users_create_subtitle' => "Add a new user",
    'hostels_create_title'    => "Add Hostel",
    'hostels_create_subtitle' => "Add a new Hostel",
    'rooms_create_title'    => "Add Room",
    'rooms_create_subtitle' => "Add a new Room",
    'blocks_create_title'    => "Add Block",
    'blocks_create_subtitle' => "Add a new Block",
    'blocks_edit_title'  => "Edit Block",
    'blocks_subtitle'    => "List of all the Blocks",
    'blocks_edit_subtitle'   => "Editing Block",
    'block_list' => "Block List",
    'blocks_title'   => "Block List",
    'create_block'   => "Add Block",
    'blocks_edit'  => "Edit Block",
    'blocks_delete'  => "Delete Block",
    'blocks_no_blocks'    => "No Blocks",
    'blocks_total_blocks' => "Total Blocks",    
    'block_manager'  => "Blocks",
    'msg_block_edited'   => "The Block has been edited",
    'msg_block_created'  => "The Block has been added",
    'msg_block_perms_updated'    => "The Block permissions have been updated",
    'msg_block_deleted'   => "The Block has been deleted",
    'students_create_title'    => "Register Student",
    'students_create_subtitle' => "Add a new Student",
    'visitors_create_title'    => "Register Visitor",
    'visitors_create_subtitle' => "Add a new Visitor",
    'items_title'   => "Maintenance Items List",
    'items_create_title'    => "Add Maintenance Item",
    'items_subtitle'    => "List of all Maintenance Items",
    'items_create_subtitle' => "Add a new Maintenance Item",
    'items_edit_title'  => "Edit Maintenance Item",
    'items_edit_subtitle'   => "Editing Maintenance Item",
    'items_list' => "Maintenance Item List",
    'create_item'   => "Add Maintenance Item",
    'items_edit'  => "Edit Maintenance Item",
    'items_delete'  => "Delete Maintenance Item",
    'items_no_items'    => "No Maintenance Items",
    'items_total_items' => "Total Maintenance Items",    
    'item_manager'  => "Maintenance Items",
    'msg_item_edited'   => "The Maintenance Item has been edited",
    'msg_item_created'  => "The Maintenance Item has been added",
    'msg_item_perms_updated'    => "The Maintenance Item permissions have been updated",
    'msg_item_deleted'   => "The Maintenance Item has been deleted",

    'majors_create_title'    => "Add Major",
    'majors_create_subtitle' => "Add a new Major",
    'majors_edit_title'  => "Edit Major",
    'majors_subtitle'    => "List of all the Majors",
    'majors_edit_subtitle'   => "Editing Major",
    'major_list' => "Major List",
    'majors_title'   => "Major List",
    'create_major'   => "Add Major",
    'majors_edit'  => "Edit Major",
    'majors_delete'  => "Delete Major",
    'majors_no_majors'    => "No Majors",
    'majors_total_majors' => "Total Majors",    
    'major_manager'  => "Majors",
    'msg_major_edited'   => "The Major has been edited",
    'msg_major_created'  => "The Major has been added",
    'msg_major_perms_updated'    => "The Major permissions have been updated",
    'msg_major_deleted'   => "The Major has been deleted",

    'facilities_create_title'    => "Add Checklist Item",
    'facilities_create_subtitle' => "Add a new Checklist Item",
    'facilities_edit_title'  => "Edit Checklist Item",
    'facilities_subtitle'    => "List of all the Checklist Items",
    'facilities_edit_subtitle'   => "Editing Checklist Item",
    'facility_list' => "Checklist Item List",
    'facilities_title'   => "Checklist Item List",
    'create_facility'   => "Add Checklist Item",
    'facilities_edit'  => "Edit Checklist Item",
    'facilities_delete'  => "Delete Checklist Item",
    'facilities_no_facilities'    => "No Checklist Items",
    'facilities_total_facilities' => "Total Checklist Items",    
    'facility_manager'  => "Checklist Items",
    'msg_facility_edited'   => "The Checklist Item has been edited",
    'msg_facility_created'  => "The Checklist Item has been added",
    'msg_facility_perms_updated'    => "The Checklist Item permissions have been updated",
    'msg_facility_deleted'   => "The Checklist Item has been deleted",

    'facility_statuses_create_title'    => "Add Checklist Item Status",
    'facility_statuses_create_subtitle' => "Add a new Checklist Item Status",
    'facility_statuses_edit_title'  => "Edit Checklist Item Status",
    'facility_statuses_subtitle'    => "List of all the Checklist Item Statuses",
    'facility_statuses_edit_subtitle'   => "Editing Checklist Item Status",
    'facility_status_list' => "Checklist Item Status List",
    'facility_statuses_title'   => "Checklist Item Status List",
    'create_facility_status'   => "Add Checklist Item Status",
    'facility_statuses_edit'  => "Edit Checklist Item Status",
    'facility_statuses_delete'  => "Delete Checklist Item Status",
    'facility_statuses_no_facility_statuses'    => "No Checklist Item Status Statuses",
    'facility_statuses_total_facility_statuses' => "Total Checklist Item Status Statuses",    
    'facility_status_manager'  => "Checklist Item Status Statuses",
    'msg_facility_status_edited'   => "The Checklist Item Status has been edited",
    'msg_facility_status_created'  => "The Checklist Item Status has been added",
    'msg_facility_status_perms_updated'    => "The Checklist Item Status permissions have been updated",
    'msg_facility_status_deleted'   => "The Checklist Item Status has been deleted",

    'house_categories_list' => "House Categories List",
    'create_house_category'   => "Add house_category",
    'house_list' => "House List",
    'create_house'   => "Add House",
    'houseCategory_subtitle'    => "List of all the House Categories",
    'houses_subtitle'    => "List of all the Houses",
    'house_categories_edit'  => "Edit House Category",
    'house_categories_delete'  => "Delete House Category",
    'house_categories_no_house_category'    => "No House Categories",
    'house_categories_total_house_category' => "Total House Categories",
    'houses_edit'  => "Edit House",
    'houses_delete'  => "Delete House",
    'houses_no_houses'    => "No Houses",
    'houses_total_houses' => "Total Houses",
    'houses_edit_title'  => "Edit House",
    'houses_edit_subtitle'   => "Editing House",
    'houses_title'   => "House List",
    'houses_create_title'    => "Add House",
    'houses_create_subtitle' => "Add a new House",
    'houseCategory_title'   => "House Category List",
    'houseCategory_edit_title'  => "Edit House Category",
    'houseCategory_edit_subtitle'   => "Editing House Category",
    'houseCategory_create_title'    => "Add House Category",
    'houseCategory_create_subtitle' => "Add a new House Category",

    'flights_create_title'    => "Add Flight",
    'flights_create_subtitle' => "Add a new Flight",
    'flights_edit_title'  => "Edit Flight",
    'flights_subtitle'    => "List of all the Flightes",
    'flights_edit_subtitle'   => "Editing Flight",
    'flight_list' => "Flight List",
    'flights_title'   => "Flight List",
    'create_flight'   => "Add Flight",
    'flights_edit'  => "Edit Flight",
    'flights_delete'  => "Delete Flight",
    'flights_no_flights'    => "No Flight Statuses",
    'flights_total_flights' => "Total Flight Statuses",    
    'flight_manager'  => "Flight Statuses",
    'msg_flight_edited'   => "The Flight has been edited",
    'msg_flight_created'  => "The Flight has been added",
    'msg_flight_perms_updated'    => "The Flight permissions have been updated",
    'msg_flight_deleted'   => "The Flight has been deleted",


    'users_activate_user'   => "Activate User",
    'users_send_activation'  => "Send Activation Email",
    'users_welcome_email'   => "Send Welcome Email",
    'users_send_password'   => "Send Password",

    /*
    |--------------------------------------------------------------------------
    | Roles Language Lines
    |--------------------------------------------------------------------------
    */

    'roles_title'   => "Role List",
    'roles_subtitle'    => "List of all the roles & statistics",
    'roles_graph1'  => "Total users per role",
    'roles_graph2'  => "Total permissions per role",
    'roles_no_roles'    => "No Roles",

    'roles_users'   => ":number Users",
    'roles_permissions'   => ":number Permissions",

    'roles_edit'    => "Edit Role",
    'roles_edit_permissions'    => "Edit Permissions",
    'roles_delete'  => "Delete Role",

    'roles_edit_title'  => "Edit Role",
    'roles_edit_subtitle'  => "Editing role: :name",

    'roles_create_title'  => "Add Role",
    'roles_create_subtitle'   => "Add a new role",

    'roles_edit_permissions_title'  => "Edit Permissions",
    'roles_edit_permissions_subtitle'   => "Editing role: :name",


    /*
    |--------------------------------------------------------------------------
    | Permissions Language Lines
    |--------------------------------------------------------------------------
    */

    'permissions_title' => "Permission List",
    'permissions_subtitle'  => "List of all the permissions",
    'permissions_roles' => ":number Roles",
    'permissions_edit'  => "Edit Permission",
    'permissions_delete'    => "Delete permission",

    'permissions_edit_title'    => "Edit Permission",
    'permissions_edit_subtitle'    => "Editing permission: :slug",

    'permissions_create_title'  => "Add Permission",
    'permissions_create_subtitle'   => "Add a new permission",
    /*
    |--------------------------------------------------------------------------
    | CRUD Language Lines
    |--------------------------------------------------------------------------
    */

    'CRUD_title'    => "Advanced Settings",
    'CRUD_subtitle' => "Add, Read, Update, Delete",

    'CRUD_table_title'    => "Edit Table",
    'CRUD_table_subtitle' => "Edit table: :table",

    'CRUD_create_title' => "Add Row",
    'CRUD_create_subtitle'  => "Add a new row in the table: :table",

    'CRUD_edit_title'   => "Edit Row",
    'CRUD_edit_subtitle'   => "Edit the row: #:id from table: :table",
   
];
