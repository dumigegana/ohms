@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{  trans('ohms.database_CRUD') }}</div>
    </div>
@endsection
@section('title', trans('ohms.CRUD_title'))
@section('icon', "cogs")
@section('subtitle', trans('ohms.CRUD_subtitle'))
@section('content')
<div class="ui doubling stackable one column grid container">
    <div class="column">
        <div class="ui very padded segment" style="background: #ffeaea !important;">
            <table class="ui basic table ">
  			  <thead style="background: #ff8b8b;">
  			    <tr>
                  <th style="color: #980000;">{{ trans('ohms.table') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.columns') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.rows') }}</th>
                  <th style="color: #980000;">{{ trans('ohms.edit') }}</th>
  			    </tr>
  			  </thead>
  			  <tbody>
                @foreach($tables as $table)
                    <tr style="color: #980000;">
                        <td>{{ $table }}</td>
                        <td>{{ count(\Schema::getColumnListing($table)) }}</td>
                        <td>{{ count(\DB::table($table)->get()) }}</td>
                        <td>
                            <a href="{{ route('OHMS::CRUD_table', ['table' => $table]) }}" class="ui btn button">{{ trans('ohms.edit') }}</a>
                        </td>
                    </tr>
				@endforeach
  			  </tbody>
  			</table>
        </div>
        <br>
    </div>
</div>
@endsection
