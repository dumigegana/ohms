@extends('layouts.admin.mainR')
@section('page-content')
<div class="container col-md-11">
  <div class="row">
    <div class="col-md-7" style="padding-top: 0 !important;">
    {!! $allocations->links() !!}
    </div>
    <div class="bt-print" style="padding-top: 21px; position: absolute; right: 0;"> 
      <button type="button" class="btn" id="print">Print</button>
    </div>
  </div>
</div>

  <div class="container-fluid col-md-12" style="background-color:#FFF;">
    <div class="container-fluid">
    <div class="panel panel-default">
                    <div class="panel-heading">
                      <center>  <h4 id="title"></i> Check List </h4></center>
                    </div>
                    </div>

       <table class="table table-bordered" id="myTable">
          <thead>
            <tr>
              <th>Semester</th>
              <th>ID</th>
              @foreach($facilities as $facility)
              <th>{{ $facility->name }}</th>
              @endforeach
          </tr> 
      </thead>
      <tbody>
      @foreach($allocations as $allocation)
        <tr>         
          <td>{{ $allocation->semester->name }}</td>
          <td>{{ $allocation->student->solusi_id }}</td>
          @foreach($allocation->checklists as $check )
          <td>{{ $check->facility_status->status }}</td>
          @endforeach
      </tr>               
        @endforeach
        </tbody>
        </table>
        </div>
          </div>
</div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function () {
  $("#print").on('click', function () {
    $("#myTable").printThis({
      debug: false,              // show the iframe for debugging
      importCSS: true,           // import page CSS
      printContainer: true,      // grab outer container as well as the contents of the selector
      //loadCSS: "path/to/my.css", // path to additional css file
      pageTitle: $("#title").text(),            // add title to print page
      removeInline: false        // remove all inline styles from print elements
    });
  });
});
</script>

@endsection