@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.maintenances_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.maintenances_title'))
@section('icon', "warning sign")
@section('subtitle', trans('ohms.maintenanceh_subtitle'))
@section('content')  
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $maintenances->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.maintenances.admin'))
             <center><a href="{{ route('OHMS::maintenances_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>{{ trans('ohms.maint_creath') }}</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr>  
                  <th style="color: #980000;">Hostel</th>
                  <th style="color: #980000;">Item</th>
                  <th style="color: #980000;">Comment</th>
                  <th style="color: #980000;">Status</th>
                  <th style="color: #980000;">Options</th>                  
            </tr>
          </thead>
          <tbody>
                @foreach($maintenances as $maintenance) 
          <tr style="color: #980000;">
            <td>
              {{ $maintenance->maintenable->name }}
            </td>
            <td>{{ $maintenance->item->name}}</td>
            <td>{{ $maintenance->comments }}</td>
            <td class="center alligned">
                @if ($maintenance->fixed == 1)
                <div class="ui green tiny basic label pop" data-title="Solved" data-variation="wide" data-content="This problem has been attended to." data-position="top center"><span>
                  <i class="large green checkmark icon"></i></span>
                </div>
                @else
                <div class="ui red tiny basic label pop" data-title="Pending" data-variation="wide" data-content="Not yet attended to." data-position="top center">
                    <span><i class="large red close icon"></i></span>
                </div> 
                @endif
              </td>
            <td>@php $type = "b"@endphp
                <div class="ui btn top icon left pointing dropdown button">
                  <i class="configure icon"></i>
                    <div class="menu">
                      <div class="header">{{ trans('ohms.editing_options') }}</div>
                        <a href="{{ route('OHMS::maintenance_change', ['id' => $maintenance->id, 'type' => $type]) }}" class="item">
                          <i class="edit icon"></i>Change status
                        </a>
                        <a href="{{ route('OHMS::maintenance_delete', ['id' => $maintenance->id, 'type' => $type]) }}" class="item">
                          <i class="trash icon"></i>Delete
                        </a>
                </td>
            
                        
          </tr>
        @endforeach
          </tbody>
        </table>
        {!! $maintenances->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_room') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection