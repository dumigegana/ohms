@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::blocks') }}">{{ trans('ohms.blocks_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.blocks_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.blocks_create_title'))
@section('icon', "plus")
@section('subtitle', trans('ohms.blocks_create_subtitle'))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST">
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column centered">
                    <div class="ui very padded segment" style="background: #ffeaea; color: #980000">
                         {{ csrf_field() }}
                           
                        <div class="required field">
                        <label>Block Name</label>
                        <input name="name" type="text" placeholder="Block Name" value="{{ old('name')}}" required>
                        </div>
                        <div class="required field">
                        <label>Student Year</label>
                        <input name="stud_year" type="number" placeholder=" " value="{{ old('stud_year')}}" required>
                        </div>
                        <div class="inline field">
                              <div class="ui slider checkbox">
                                 <input value='' type="checkbox" tabindex="0" class="hidden" name="theology" {{ (old("theology") == '1' ? "checked":"") }}>
                               <label>Theology Students Only</label>
                          </div>
            
                        <div class="required field">
                            <label>Hostel</label>
                            <select name="hostel_id" class="form-control" required>
                            <option value="" disabled selected>Hostel</option>
                            @foreach($hostels as $hostel)
                                <option value="{{ $hostel->id }}" {{ (old("hostel_id") == $hostel->id ? "checked":"") }}>{{ $hostel->name }}</option>
                                @endforeach
                            </select>
                            </div>
                            <br/>

                        <center><button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button></center>
                    </div>
                </div>
            </div>
        </div> 
    </form>
</div>
@endsection
