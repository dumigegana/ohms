<table class="table table-bordered"  id="myTable">
          <thead>
            <tr>              
              <th>Hostel</th> 
              <th>Item</th>
              <th>Comments</th>
              <th>Date Reported</th>
              <th>Reported By</th>
          </tr> 
      </thead>
      <tbody>
          @foreach($blocks as $block) 
          @foreach($block->maintenances as $maintenance)
         
        <tr>
          <td>{{ $block->name }}</td>
          <td>{{ $maintenance->item->name }}</td>
          <td>{{ $maintenance->comments}}</td>
          <td>{{ $maintenance->created_at}}</td>
          <td>{{ $maintenance->user->username }}</td>
      </tr>
     @endforeach
      @endforeach
  </tbody>
  </table>