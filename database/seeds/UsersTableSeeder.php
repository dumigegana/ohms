<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         Model::unguard();
        DB::table('users');
        $users = array(
            ['full_name' => 'Male Student', 'username' => 'male', 'email' => 'male@solusi.ac.zw','cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
            ['full_name' => 'Female Student', 'username' => 'female', 'email' => 'female@solusi.ac.zw', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
            ['full_name' => 'Male1 Student', 'username' => 'male1', 'email' => 'male1@solusi.ac.zw','cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
            ['full_name' => 'Female1 Student', 'username' => 'female1', 'email' => 'female1@solusi.ac.zw','cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ], 
              ['full_name' => 'Fernando Melo', 'username'=>'melof', 'email'=>'melof@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Mauritania Melo', 'username'=>'melom', 'email'=>'melom@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Valter Costa', 'username'=>'costav', 'email'=>'costav@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Robson Neves', 'username'=>'nevesr', 'email'=>'nevesr@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Edward Chigeda', 'username'=>'chigedae', 'email'=>'chigedae@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Mary Chigeda', 'username'=>'chigedam', 'email'=>'chigedam@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Petros Sukali', 'username'=>'sukalip', 'email'=>'sukalip@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Ken Aluwiziyo', 'username'=>'aluwiiyok', 'email'=>'aluwiiyok@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Petros Gomani', 'username'=>'gomanip', 'email'=>'gomanip@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Laston Banda', 'username'=>'bandal', 'email'=>'bandal@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Goodwin Sibanda', 'username'=>'sibandag', 'email'=>'sibandag@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Rose Sibanda', 'username'=>'sibandar', 'email'=>'sibandar@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Austine Banda', 'username'=>'bandaa', 'email'=>'bandaa@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Jane Banda', 'username'=>'bandaj', 'email'=>'bandaj@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],
             ['full_name' => 'Happy Magawa', 'username'=>'magawah', 'email'=>'magawah@mail.dummy', 'cell_number'=>'0000000', 'password' => bcrypt('Solusi19'), 'active' => '1', 'banned' => '0', 'su' => '0' ],



        );

        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            User::create($user);
        }
        Model::reguard();
    }
}


