@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <div class="active section">{{ trans('ohms.work_records_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.work_records_title')) 
@section('icon', "bed")
@section('subtitle', trans('ohms.work_records_subtitle'))
@section('content')
 <div class="ui one column doubling stackable grid container">
    <div class="column">
      <div class="ui very padded segment" style="background: #ffeaea !important;"> <div class="text" id="prnt">
       <div class="ui doubling stackable grid">
            <div class="four wide column" style="padding-top: 0 !important;">{!! $work_records->links() !!}</div>
            <div class="eight wide column">
              @if(OHMS::loggedInUser()->hasPermission('ohms.work_records.admin'))
             <center><a href="{{ route('OHMS::work_records_create') }}" type="submit" class="ui btn btn submit button"> <i class="add icon"></i>Add record</a></center>
             @endif
          </div>
            <div class="four wide column">
              <div class="ui category search">
                <div class="ui icon input">
                <input type="text" class="prompt" placeholder="Search..." id="search">
                <input type="hidden" name="_token" id="token" value="{{csrf_token() }}">
                    <i class="search icon" style="color: #6b0000;"></i>
                  </div>
              </div>
            </div>
        </div>  
        <table class="ui basic table ">
          <thead style="background: #ff8b8b; color: #980000;">
            <tr>
              <th style="color: #980000;">Work_record Number</th>
              <th style="color: #980000;">Gender</th>
              <th style="color: #980000;">Block</th>
              <th style="color: #980000;">Capacity</th>
              <th style="color: #980000;">Active</th>
              <th style="color: #980000;">Options</th>
            </tr> 
          </thead>
          <tbody id="read">
            @foreach($work_records as $work_record) 
            <tr style="color: #980000;">
              <td>{{ $work_record->work_record_number }}</td>
              <td>{{ $work_record->block->hostel->gender }}</td>
              <td>{{ $work_record->block->name }}</td>
              <td>{{ $work_record->capacity }}</td>
              <td class="center alligned">
                      @if ($work_record->enable == 1)
                        <div class="ui green tiny basic label pop" data-title="Allocations allowed" data-variation="wide" data-content="Work_record ready for allocations." data-position="top center"><span>
                          <i class="large green checkmark icon"></i></span>
                        </div>
                      @else
                        <div class="ui red tiny basic label pop" data-title="Allocations Disabled" data-variation="wide" data-content="Students cannot be allocated to this work_record " data-position="top center">
                          <span><i class="large red close icon"></i></span>
                        </div> 
                      @endif
                    </td>
              <td>                
                <a href="{{ route('OHMS::work_records_edit', ['id' => $work_record->id]) }}" class="ui btn button">{{ trans('ohms.work_records_edit') }}</a>
              </td>       
            </tr>
        @endforeach
          </tbody>
        </table>
        {!! $work_records->links() !!}
      </div>
        <br>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('#search').on('keyup', function(){
  var keywords = $(this).val();  
  $.ajax({
    url:"{{ url('admin/search_work_record') }}" ,
    type:"POST",
    data:{"_token": $('#token').val(), 'keywords' : keywords},
    success : function(search){
      $('#read').html(search);

  console.log(keywords);
      }
    });
            
  });
</script>
@endsection