# OHMS - INSTALLATION GUIDE.

## A. Preperations for Installation.

1. Download or clone the OHMS to your web server.
2. Open the OHMS directory on the shell/CLI and run the following commands.
		"composer install" and
		"php artisan key:generate".
3. Make sure you created the database.
4.Optionaly run "php artisan serve --host=Your host Name/IP --port=Your preffered Port".

## B. Installation

5. Open your browser and nevigate to your OHMS. This should lead you to home page of the OHMS.
6. On the right of the navigation bar clock on the INSTALL link.
7. You should be directed to the installation form. Complite the form and Submit.

Note this form collects data for the a) Admin account,
								 	 b) Database for the OHMS,
								 	 c) Email For OHMS to send automated emails.

8. After successfully submiting the installation form the following precesses takes place
			a) The dot env file is updated,
			b) The migrate and seed commands are run, 
			c) The New Admin user is login.
###For more Details read the User Manual in this repository.