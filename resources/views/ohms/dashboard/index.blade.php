@extends('layouts.admin.panel')
<div class="ui breadcrumb">
    <div class="active section">{{ trans('ohms.dashboard') }}</div>
</div>
@section('title', trans('ohms.dashboard'))
@section('icon', "dashboard")
@section('subtitle')
{{ trans('ohms.welcome_user', ['name' => OHMS::loggedInUser()->name]) }}
@endsection
@section('content')
<div class="ui doubling stackable three column grid container">
    <div class="column">        
        <div class="ui raised segment" style="background: #ffeaea;">
            <div class="ui red ribbon label">Male Students</div>
            <div class="ui red right corner label pop"  data-variation="wide" data-content=" {{$hmales_cap - $hmales_occ }} vaccant Male rooms" data-position="top center">{{$hmales_cap - $hmales_occ}}</div>
            <br><br>
                <center><div class="ui massive blue circular inverse label pop" data-variation="wide" data-content="{{ $hmales_occ }} Male Students has rooms" data-position="top center" href="#"><i class="male icon"></i>{{ $hmales_occ }}</div></center>
            
        </div>        
    </div>
    <div class="column">
        <div class="ui raised segment" style="background: #ffeaea;">
        <div class="ui red ribbon label">Total Students</div>
        <div class="ui red right corner label pop"  data-variation="wide" data-content=" {{$hmales_cap + $hfemales_cap - $hmales_occ - $hfemales_occ}} vaccant rooms" data-position="top center">{{$hmales_cap + $hfemales_cap - $hmales_occ - $hfemales_occ}}</div>
            <br><br>
                <center><div class="ui massive green circular inverse label pop" data-variation="wide" data-content="{{ $hmales_occ + $hfemales_occ}} Students has rooms" data-position="top center"><i class="users icon"></i>{{ $hmales_occ + $hfemales_occ}}</div></center>
            
        </div>
        <br>
    </div>
    <div class="column">        
        <div class="ui raised segment" style="background: #ffeaea;">
            <div class="ui red ribbon label">Female Student</div>
            <div class="ui red right corner label pop"  data-variation="wide" data-content=" {{ $hfemales_cap - $hfemales_occ}} vaccant Female rooms" data-position="top center">{{ $hfemales_cap - $hfemales_occ}}</div>
                <br><br>
            <center><div class="ui massive pink circular inverse label pop" data-variation="wide" data-content="{{ $hfemales_occ}} Female Students has rooms" data-position="top center"><i class="female icon"></i>{{ $hfemales_occ}}</div></center>
        </div>        
    </div>
</div>    
<div class="ui doubling stackable two column grid container">
    <div class="column">    
        <div class="ui padded segment" style="background: #ffeaea;">
            <div class="ui blue attached large label">Students Male Hostels</div>
                <br>
            <center>       
                <div id="pie_m" style="width:500px;height:300px"></div>
            </center>
            </div>
        <br>
        <div class="ui padded segment"> 
            <div class="app" >
                <center>       
                   <div id="bar_m" style="width:500px;height:300px"></div>
                </center>
            </div>    
        </div>        
    </div>
    <div class="column">
        <div class="ui padded segment" style="background: #ffeaea;">
            <div class="ui pink attached large label">Students Female Hostels</div>
            <br>
            <center>
               <div id="pie_f" style="width:500px;height:300px"></div>
            </center>
        </div>
        <br>
        <div class="ui padded segment">
           <div id="bar_f" style="width:500px;height:300px"></div>
        </div>
        <br>        
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('js/bootstrap-3.3.7.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/ohms_public/flot/jquery.flot.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/ohms_public/flot/jquery.flot.pie.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/ohms_public/flot/jquery.flot.axislabels.js') }}"></script>
<script type="text/javascript">
//*******  Percentage Occupancy - BAR CHART
/*
var data = {!! json_encode($occupance_m) !!};
var dataset = [{ data: data, color: "purple" }];
var ticks = {!! json_encode($ticks) !!};

var options = {
    series: {
        bars: {
            show: true
            }
    },
    bars: {
        align: "center",
        barWidth: 1
    },
    xaxis: {
        axisLabel: "Male Hostels",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10,
        ticks: ticks
        },
    yaxis: {
        axisLabel: "Percentage Occupancy",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 3,
        tickFormatter: function (v, axis) {
            return v + "%";
                }
        },
    legend: {
        noColumns: 0,
        labelBoxBorderColor: "#FF0000",
        position: "nw"
        },
    grid: {
        hoverable: true,
        borderWidth: 2,
        backgroundColor: { colors: ["#ffffff", "#ffeaea"] }
        }
    };
var dataf = {!! json_encode($occupance_f) !!};
var datasetf = [{ data: dataf, color: "violet" }];
var ticksf = {!! json_encode($ticksf) !!};

var optionsf = {
    series: {
        bars: {
            show: true
            }
    },
    bars: {
        align: "center",
        barWidth: 1
    },
    xaxis: {
        axisLabel: "Female Hostels",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10,
        ticks: ticksf
        },
    yaxis: {
        axisLabel: "Percentage Occupancy",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 3,
        tickFormatter: function (v, axis) {
            return v + "%";
                }
        },
    legend: {
        noColumns: 0,
        labelBoxBorderColor: "#FF0000",
        position: "nw"
        },
    grid: {
        hoverable: true,
        borderWidth: 2,
        backgroundColor: { colors: ["#ffeaea", "#ffffff"] }
        }
    };

$(document).ready(function () {
    $(function () { 
        var data =  {!! json_encode($pie_m) !!};     
        var options = {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    // tilt: 0.5,
                    label:{                        
                        radius: 3/4,
                        formatter: function (label, series) {
                            return '<div style="font-size:8pt;text-align:center;padding:5px;color:white;">' + label + '<br/>' +   
                            Math.round(series.percent) + '%</div>';
                        },
                        background: {
                            opacity: 0,
                            color: '#000'
                        }
                    }
                }

            },
            legend: {
                show: false,
                labelBoxBorderColor: "none"
            }        
        };
     
        $.plot($("#pie_m"), data, options);  
    });    
    $(function () { 
        var data =  {!! json_encode($pie_f) !!};     
        var options = {
            series: {
                pie: {
                    show: true,
                    innerRadius: 0.5,
                    radius: 1,
                    // tilt: 0.5,
                    label:{                        
                        radius: 3/4,
                        formatter: function (label, series) {
                            return '<div style="font-size:8pt;text-align:center;padding:5px;color:white;">' + label + '<br/>' +   
                            Math.round(series.percent) + '%</div>';
                        },
                        background: {
                            opacity: 0,
                            color: '#000'
                        }
                    }
                }

            },
            legend: {
                show: false,
                labelBoxBorderColor: "none"
            }        
        };
     
        $.plot($("#pie_f"), data, options);  
    });

    $.plot($("#bar_m"), dataset, options);
    $.plot($("#bar_f"), datasetf, optionsf);

});
        
        
   */    
    

    
    // $.plot(".chart", [ { label: "Neue Mitglieder", data: data }, { label: "Fällige Kündigungen", data: data2 } ], {
    //     series: {
    //         bars: {
    //             show: true,
    //             barWidth: 0.15,
    //             order: 1,

    //         }
    //     },
    //     xaxis: {
    //         mode: "categories",
    //         ticks: [[0,"Jan"], [1,"Feb"],  [2,"Mär"],  [3,"Apr"],  [4,"Mai"],  
    // [5,"Jun"],  [6,"Jul"],  [7,"Aug"],  [8,"Sep"],  [9,"Okt"],  [10,"Nov"],  [11,"Dez"]],
    //         tickLength: 1,

    //     },
    //     grid: {
    //         hoverable: true,
    //     },
    //     yAxis: {
    //         allowDecimals:false,
    // }
    // });
</script>
@endsection
