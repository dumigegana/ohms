<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Model::unguard();
        DB::table('items');
        $items = array(
            [ 'name' => 'Bathrooms', 'maint_dep_id' => '1' , 'type' => 'block'],
            [ 'name' => 'Toilets', 'maint_dep_id' => '1' , 'type' => 'block'],
            [ 'name' => 'Pipes', 'maint_dep_id' => '1' , 'type' => 'block'],
            [ 'name' => 'Drainage', 'maint_dep_id' => '1' , 'type' => 'block'],
            [ 'name' => 'Electricity', 'maint_dep_id' => '2' , 'type' => 'both'],
            [ 'name' => 'Switch', 'maint_dep_id' => '2' , 'type' => 'both'],
            [ 'name' => 'Bulb', 'maint_dep_id' => '2' , 'type' => 'both'],
            [ 'name' => 'Power Socket', 'maint_dep_id' => '2' , 'type' => 'both'],
            [ 'name' => 'Woodrope', 'maint_dep_id' => '3' , 'type' => 'room'],
            [ 'name' => 'Curtains', 'maint_dep_id' => '3' , 'type' => 'both'],
            [ 'name' => 'Door', 'maint_dep_id' => '3' , 'type' => 'both'],
            [ 'name' => 'Window', 'maint_dep_id' => '3' , 'type' => 'both'],
            [ 'name' => 'Chair', 'maint_dep_id' => '3' , 'type' => 'room'],
            [ 'name' => 'Study Table', 'maint_dep_id' => '3' , 'type' => 'room'],
            [ 'name' => 'Bed', 'maint_dep_id' => '3' , 'type' => 'room'],
            [ 'name' => 'Ceiling', 'maint_dep_id' => '3' , 'type' => 'both'],
          
        );
         foreach ($items as $items)
        {
            Item::create($items);
        }
        Model::reguard(); 
    }
}
