@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::work_records') }}">{{ trans('ohms.work_records_title') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.work_records_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.work_records_create_title'))
@section('icon', "plus")
@section('subtitle', trans('ohms.work_records_create_subtitle'))
@section('content')
<div class="ui one wide column doubling stackable grid container ">
    <div class="ten wide column centered">
        <div class="ui very padded segment" style="background: #ffeaea;">
            <form method="POST" class="ui form">
                {{ csrf_field() }}            
                @include('ohms.forms.master') 
                <br>
                <div class="field">
                    <button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
