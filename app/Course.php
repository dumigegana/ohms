<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
     public function students()
    {
      return $this->belongsToMany('App\Student', 'course_student', 'course_id', 'student_id');
    }

    public function sessions()
    {
        return $this->hasMany('App\Session');
    }

    /**
    * Returns true if the user has the course
    *
    * @param string $name
    */
    public function hasStudent($student_id)
    {
        foreach($this->students as $student) {
            if($student->student_id == $student_id) {
                return true;
            }
        }
        return false;
    }


}
