<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
   protected $dates = [
        'created_at', 'updated_at',  
    ];

    public function maint_dep()
    {
    	return $this->belongsTo('App\Maint_dep');
    }

    public function maintenances()
    {
        return $this->hasMany('App\Maintenance');
    }
}
 