@extends('layouts.admin.mainR')
@section('page-content')
<div class="container col-md-10 col-md-offset-1" >
  <div class="row">
    <div class="col-md-7" style="padding-top: 0 !important;">{!! $users->links() !!}</div>
    <div class="col-md-3" style="padding-top: 16px !important;">
        <form class="navbar-form" role="search" method="Post">
        {{ csrf_field() }}
        <div class="input-group">
          <input type="text" class="form-control" name="{{-- $gender --}}" placeholder="Search..." id="search">
        </div>
        </form>
    </div>
    <div class="" style="padding-top: 21px; position: absolute; right: 0;"> 
       <button type="button" class="btn" id="print">Print</button>
    </div>
  </div>
  <div class="" style="background-color:#FFF;">
    <div class="container" id="">
      <div class="panel" style="border-color: #ffeaea;">
        <div class="panel-heading" style="background-color:#ffeaea;">
          <center>  <h4 id="title"></i> Guest Room List</h4></center>
        </div>
        <div class="panel-body">
          <table class="table table-bordered" id="myTable" >
            <thead>
              <tr>
                <th>{{ trans('ohms.full_name') }}</th>
                <th>Room</th>
                <th>Arrival Date</th>
                <th>Return Date</th>
                <th>Arrival Flight</th>
                <th>Flight Date & Time</th>
                <th>Return Flight</th>
                <th>Flight Date & Time</th>
              </tr> 
            </thead>
            <tbody id="read">
            @foreach($users as $user)
              {{-- @if($user->has_room == true) --}} 
              <tr>
                <td>{{$user->full_name}}</td>
                <td>{{$user->visitor->room->block->name}}  <b>{{$user->visitor->room->room_number}}</b></td>
                <td>{{$user->visitor->departure_date}}</td>
                <td>{{$user->visitor->arrival_date}}</td>
                @isset($user->visitor->a_flight)
                <td>{{$user->visitor->ar_flight->flight}} </td>
                <td>
                  {{$user->visitor->ar_flight->flight_date}}  <b>  {{$user->visitor->ar_flight->flight_time}}</b> 
                </td> 
                @endisset 
                @isset($user->visitor->r_flight)                            
                <td>{{$user->visitor->re_flight->flight}} </td>
                <td>
                  {{$user->visitor->re_flight->flight_date}} <b> {{$user->visitor->re_flight->flight_time}} </b>
                </td> 
                @endisset                            
              </tr>
              {{-- @endif --}}
              
             @endforeach
            </tbody>
          </table>  
      </div>
    </div>
  </div>  
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function () {
  $("#print").on('click', function () {
    $("#myTable").printThis({
      debug: false,              // show the iframe for debugging
      importCSS: true,           // import page CSS
      printContainer: true,      // grab outer container as well as the contents of the selector
      //loadCSS: "path/to/my.css", // path to additional css file
      pageTitle: $("#title").text(),             // add title to print page
      removeInline: false        // remove all inline styles from print elements
  });
});
  $('#search').on('keyup', function(){
    var keywords = $(this).val(); 
    $.ajax({
      url:"{{ url('admin/search_visitor') }}" ,
      type:"POST",
      data:{"_token": $('#token').val(), 'keywords' : keywords},
      success : function(search){
        console.log("search")
        $('#read').html(search);
        }
      });
              
    });
});           
</script>

@endsection