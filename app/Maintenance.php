<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
	 protected $dates = [
        'created_at', 'updated_at',  
    ];

     protected $fillable = [
        'user_id', 'maintenable_type', 'maintenable_id', 'item_id',  'comments', 
    ]; 
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function maintenable()
    {
        return $this->morphTo();
    }
    public function item()
    { 
        return $this->belongsTo('App\Item');
    }
   
}
