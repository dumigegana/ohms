<?php
 
$includes = [
    'header'    =>  "",
    /*
    |--------------------------------------------------------------------------
    | OHMS Includes, please do not remove any lines as it may cause problems
    |--------------------------------------------------------------------------
    */
    'ohms_header'    =>  "
        <link rel='stylesheet' type='text/css' href='" . asset(OHMS::publicPath() . '/css/semantic.min.css') . "'>
        <link rel='stylesheet' type='text/css' href='" . asset(OHMS::publicPath() . '/sweetalert/sweetalert.css') . "'>
        <script src='" . asset(OHMS::publicPath() . '/sweetalert/sweetalert.min.js') . "'></script>
        <link rel='stylesheet' type='text/css' href='" . asset(OHMS::publicPath() . '/css/style.css') . "'>
       
    ",

    'ohms_bottom'    =>  "
        <script src=" . asset(OHMS::publicPath() . '/js/jquery-3.4.1.min.js') . "></script>
        <script src='" . asset(OHMS::publicPath() . '/js/semantic.min.js') . "'></script>
        <script src='" . asset(OHMS::publicPath() . '/ckeditor/ckeditor.js') . "'></script>
        
        <script src='" . asset(OHMS::publicPath() . '/js/script.js') . "'></script>
    ",

    'ohms_bottom_old'    =>  "
        <script src=" . asset(OHMS::publicPath() . '/js/jquery-3.4.1.min.js') . "></script>
        <script src='" . asset(OHMS::publicPath() . '/js/semantic.min.js') . "'></script>
        <script src='" . asset(OHMS::publicPath() . '/ckeditor/ckeditor.js') . "'></script>
        
        <script src='" . asset(OHMS::publicPath() . '/js/script.js') . "'></script>
    ",

    
]; 
