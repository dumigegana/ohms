@extends('layouts.admin.panel')
@section('breadcrumb')
    <div class="ui breadcrumb">
        <a class="section" style="color: #ffeaea; text-decoration: underline;" href="{{ route('OHMS::users') }}">{{ trans('ohms.user_list') }}</a>
        <i class="right angle icon divider"></i>
        <div class="active section">{{ trans('ohms.users_create_title') }}</div>
    </div>
@endsection
@section('title', trans('ohms.users_create_title'))
@section('icon', "add user")
@section('subtitle', trans('ohms.users_create_subtitle'))
@section('content')
<div class="ui container">
    <form class="ui form" method="POST">
        <div class="ui doubling stackable grid">
            <div class="row">
                <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea;">
                        {{ csrf_field() }}
                        @include('ohms/forms/master')
                    </div>
                </div>
                <div class="eight wide column">
                    <div class="ui very padded segment" style="background: #ffeaea;">
                        @include('ohms.forms.roles')
                    </div>
                    <div class="ui very padded segment" style="background: #ffeaea;">
                        <div class="inline field">
                            <div class="ui slider checkbox">
                                <input type="checkbox" id="active" name="active" tabindex="0" class="hidden">
                                <label style="color: #980000;">{{ trans('ohms.users_activate_user') }}</label>
                            </div>
                       </div>
                        <br>
                        <button type="submit" class="ui btn submit button">{{ trans('ohms.submit') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('js')
<script>
    $('#active').change(function(){
        if(this.checked) {
            $('#send_activation').prop('checked', false);
        }
    });
    $('#send_activation').change(function(){
        if(this.checked) {
            $('#active').prop('checked', false);
        }
    });
</script>
@endsection
