<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;
use App\Role_User;
use App\Major;
use App\User;
use App\Student;
use OHMS;
use Auth;
use Gate; 
use DB;

class studentsController extends Controller
{
 /*
    |--------------------------------------------------------------------------
    | Students Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new students as well as their
    | validation and creation. 
    |
    */

     public function index()
    { 
        
       OHMS::permissionToAccess('ohms.students.access');

        # Get all students
        $students = Student::with(['user', 'major'])->paginate(100);

        # Return the view
        return view('ohms/students/index', [
            'students' => $students,
        ]); 
    }

    public function search(Request $request)
    {
        OHMS::permissionToAccess('ohms.students.access'); 
        $read = "";
        $post = $request['keywords'];
        if(empty($post)) {
            $students = Student::with(['user', 'major'])->paginate(100);
        }
        else {       
          $students = Student::with('user', 'major')
                             ->where('solusi_id','like', $post . '%')
                             ->orwherehas('major', function($q) use ($post) {$q->where('name',  'like', '%' . $post . '%');})                                        
                             ->orwherehas('user', function($o) use ($post) {$o->where('full_name', 'like',  $post . '%');})
                            ->paginate(100);
        }
        $read = view('ohms/students/search', ['students' => $students]);
        return $read;
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.students.access');

        # Find the student
        $student = OHMS::students('id', $id);

        # Return the view
        return view('ohms/students/show', ['student' => $student]);
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.students.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.students.admin');

        # Get all roles
        $roles = OHMS::roles();

        $majors = Major::orderBy('programme', 'asc')->get();
 
        # Get all the data
        $data_index = 'students';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/students/create', [
            'roles'     =>  $roles,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
            'majors' => $majors,
        ]);
    }

    public function store(Request $data)
    {
        # Check permissions
        OHMS::permissionToAccess('ohms.students.admin');
        $this->validate($data, [
            'full_name' => 'required|max:255',
            'email' => 'email|max:255',
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-zA-Z])(?=.*[A-Z])(?=.*\d).+$/',
            'solusi_id' => 'required',
            'gender'    => 'required',
            'degree_level' => 'required',
            'cell_number'  => 'required|unique:students',
            'cell_phone_number'  => 'accepted',
            'national_id'    => 'required',
            'home_address' => 'required',
            'next_of_kin'  => 'required',
            'next_of_kin_number'  => 'accepted',
            'role_id'=>'required',
            'banned'=>'required',
            'active'=>'required',
            'date_of_birth'    => 'required'

        ]);


        # Create the user
        $user_data = [
            'full_name' => $data['full_name'],
            'email' => $data['solusi_id'] . "@solusi.ac.zw",
            'username' => $data['solusi_id'],
            'password' => bcrypt($data['password']), 
             'active' => $data['active'],
             'banned' => $data['banned'],
        ];
        $user = User::create($user_data);

        # Add Relationshop
        $user->roles()->attach($data['role_id']);       
 
        # Add Relationshop
        $rel = new Student;
        $rel->user_id = $user->id;
        $rel->solusi_id = $data['solusi_id'];
        $rel->gender = $data['gender'];
        $rel->email = $data['email'];
        $rel->degree_level = $data['degree_level'];
        $rel->year = $data['year'];
        $rel->major_id = $data['major_id'];
        $rel->cell_number = $data['cell_number'];
        $rel->home_address = $data['home_address'];
        $rel->next_of_kin = $data['next_of_kin'];
        $rel->date_of_birth = $data['date_of_birth'];
        $rel->national_id = $data['national_id'];
       
        $rel->save();

        return redirect()->route('OHMS::students')->with('success', trans("ohms.msg_student_created"));
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.students.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.students.admin');

        # Find the student
        $row = OHMS::student('id', $id);
        $majors = Major::orderBy('programme', 'asc')->get();
        
               # Return the view
         return view('ohms/students/edit', ['row' => $row, 'majors' => $majors]);
    }
     public function update($id, Request $request)
    {
         

        $row = Student::find($id);

        $data_index = 'students';
        require('Data/Edit/Save.php');

        return redirect()->route('OHMS::students')->with('success', trans('ohms.msg_student_edited'));
    }
  
    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.students.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.students.admin');

        # Find The student
        $student = OHMS::students('id', $id)->first();

        # Delete Relationships
            $rels = User::where('id', $student->user_id)->get();
            foreach($rels as $rel) {
                $rel->delete();
            

            # Delete student
            $student->delete();

            # Return the admin with a success message
            return redirect()->route('OHMS::students')->with('success', trans('ohms.msg_student_deleted'));
        }
    }

    
}