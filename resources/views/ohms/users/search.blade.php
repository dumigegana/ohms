
@foreach($users as $user)
	<tr style="color: #980000;">
		<td>
        <div class="text">
        {{ $user->full_name }}
        @if($user->su)
          <div class="ui red tiny left pointing basic label pop" data-title="{{ trans('ohms.super_user') }}" data-variation="wide" data-content="{{ trans('ohms.super_user_desc') }}" data-position="top center" >{{ trans('ohms.super_user') }}</div>
            @elseif(OHMS::isAdmin($user))
          <div class="ui blue tiny left pointing basic label pop" data-title="{{ trans('ohms.admin_access') }}" data-variation="wide" data-content="{{ trans('ohms.admin_access_desc') }}" data-position="top center">{{ trans('ohms.admin_access') }}</div>
                @else
          <div class="ui green tiny left pointing basic label pop" data-title="{{ trans('ohms.student_user') }}" data-variation="wide" data-content="{{ trans('ohms.student_user_desc') }}" data-position="top center">{{ trans('ohms.student_user') }}</div>
          @endif
        </div>
    </td>
   	<td>
                        @if($user->banned)
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_banned') }}" class="pop red close icon"></i>
                        @elseif(!$user->active)
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_unactive') }}" class="pop btn warning icon"></i>
                        @else
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_ok') }}" class="pop green checkmark icon"></i>
                        @endif
                        {{ $user->username }}
                    </td>

             <td>
                        @if($user->banned) 
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_banned') }}" class="pop red close icon"></i>
                        @elseif(!$user->active)
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_unactive') }}" class="pop btn warning icon"></i>
                        @else
                            <i data-position="top center" data-content="{{ trans('ohms.users_status_ok') }}" class="pop green checkmark icon"></i>
                        @endif
                        {{ $user->email }}
                    </td>
                   
    				
                <td >
    @if(!OHMS::isAdmin($user) or OHMS::loggedInUser()->su)
     <div class="five wide column">
      <center><a href="{{ route('OHMS::users_edit', ['id' => $user->id]) }}" type="submit" class="ui btn btn submit button"> <i class="edit icon"></i>{{ trans('ohms.users_edit') }}</a></center>
       @endif
                      </div>
                      
						</td>
					</tr>
				@endforeach
       