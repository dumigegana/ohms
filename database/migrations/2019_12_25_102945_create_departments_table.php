<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('hod_id')->nullable();
            $table->boolean('accademic')->default(false);
            $table->timestamps();
        });

        $deps =[
            'Information and Communication Technology',
            'Human Resource',
        ];
        foreach ($deps as $dep) {
            $maint = \OHMS::newDepartment();
            $maint->name = $dep;
            $maint->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
