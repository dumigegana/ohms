
        <!-- Sidebar -->
 <nav class="navbar sidebar" role="navigation" style="background-color: #6b0000 ;">
    <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse ">      
        <img class="img-squire img-responsive rwd-break" href="{{ route('dash') }}" style="margin-top: -30px;" src="{{ URL::to('img/img/logo.png') }}" width="100%" height="100" >  
        <ul class="nav nav-pills ">   
        @if (Route::has('login'))
                @if(Auth::check())
        <li class="{{ Request::is('/') ? 'active' : '' }}" ><a href="{{ url('/') }}"><span style="font-size:16px; ">Home</span></a></li>
        @if(OHMS::loggedInUser()->isAdmin())
        <li ><a href="{{ route('OHMS::users') }}"><span style="font-size:16px; ">Admin</span></a></li>
         @elseif(!OHMS::loggedInUser()->isAdmin())
        <li class="{{ Route::currentRouteNamed('index') ? 'active' : '' }}"><a href="{{ route('index', ['student_id' => auth()->user()->student->id, 'gender' => auth()->user()->student->gender]) }}"><span style="font-size:16px; ">Allocations</span></a></li>
        
        <li class="{{ Route::currentRouteNamed('dash') ? 'active' : '' }}"><a href="{{ route('dash') }}"><span style="font-size:16px; ">Dashboard</span></a></li>
        <li class="{{ Route::currentRouteNamed('get_report') ? 'active' : '' }}"><a href="{{ route('get_report') }}"><span style="font-size:16px; ">Report Maintenance</span></a></li>
         @endif
        <li ><a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span style="font-size:16px; ">Logout</span></a></li>
         @else
        <li ><a href="http://www.solusi.ac.zw" target="_blank"><span style="font-size:16px; ">Solusi WebSite</span></a></li>
        <li ><a href="{{ url('/login') }}"><span style="font-size:16px; ">Login</span></a></li>
        @endif
        <li ><a href="http://www.solusi.ac.zw" target="_blank"><span style="font-size:16px;">Solusi WebSite<span style="font-size:16px;"></span></a></li>
        @endif
      </ul>
    </div>
  </div>
</nav>
 <!-- /#sidebar-wrapper -->
