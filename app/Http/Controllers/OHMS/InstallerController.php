<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;
 
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use Auth;
use Artisan; 
use OHMS;

class InstallerController extends Controller
{
    public function show()
    {
        // Show the installation form 
        if(!OHMS::checkInstalled()){
            // Set the locale
            App::setLocale('en');


            return view('ohms/installer/index');
        } else {
            return redirect()->route('OHMS::dashboard')->with('warning', trans('ohms.already_installed'));
        }
    }
    public function installConfig(Request $request)
    {
        if(!OHMS::checkInstalled()){
            // Install OHMS

            $this->validate($request, [
                'USER_NAME' => 'required',
                'USER_FULL_NAME'=> 'required',
                'USER_PASSWORD' => 'required|min:8|confirmed',
                'USER_EMAIL' => 'required',
                'DB_HOST' => 'required',
                'DB_PORT' => 'required',
                'DB_DATABASE' => 'required',
                'DB_USERNAME' => 'required',
            ]);

            $file_location = base_path() . '/.env';
            $env = fopen($file_location, "w") or die("Unable to open file!");
            foreach($request->all() as $key => $data) {
                if($key != '_token' and $key != 'USER_PASSWORD_confirmation') {
                    fwrite($env, $key . "='" . $data . "'\n");
                }
            }
            $default = "\nREDIS_HOST=127.0.0.1\nREDIS_PASSWORD=null\nREDIS_PORT=6379\n\nPUSHER_KEY=\nPUSHER_SECRET=\nPUSHER_APP_ID=\n\nBROADCAST_DRIVER=log\nCACHE_DRIVER=file\nSESSION_DRIVER=file\nQUEUE_DRIVER=sync\n\nAPP_ENV=local\nAPP_KEY=" . env('APP_KEY') . "\nAPP_DEBUG=false\nAPP_LOG_LEVEL=debug\nAPP_URL=" . url('/') . "\n";
            fwrite($env, $default);
            fclose($env);

            return redirect()->route('OHMS::install_confirm');
        } else {
            return redirect()->route('OHMS::dashboard')->with('warning', trans('ohms.already_installed'));
        }
    }

    public function install()
    {
        if(!OHMS::checkInstalled()){

            $exitCode = Artisan::call('migrate');
            $exitCode = Artisan::call('db:seed');

            if (Auth::attempt(['username' => env('USER_NAME'), 'password' => env('USER_PASSWORD')])) {
                // Authentication passed...

                $file_location = base_path() . '/.env';
                $default = "\nOHMS_INSTALLED=true";
                file_put_contents($file_location,$default, FILE_APPEND);

                $url = route('OHMS::dashboard');
                return redirect()->intended($url)->with('success', trans('ohms.welcome_to_ohms'));
            } else{
                die("<b>ERROR: </b> Something went wrong, please post an issue about it to Solusi ICT department");
            }
        } else{
            return redirect()->route('OHMS::dashboard')->with('warning', trans('ohms.already_installed'));
        }
    }
}
