<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Major;
use App\Permission;
use Schema;
use Auth;
use OHMS;

class MajorsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.majors.access');

        # Get all the majors
        $majors = Major::paginate(20);

        
        # Return the view
        return view('ohms/majors/index', ['majors' => $majors]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.majors.access');

        # Get the major
        $major = Major::find($id);

        # Return the view
        return view('admin/majors/show', ['major' => $major]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.majors.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.majors.admin');

        # Find the major
        $row = Major::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'majors';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/majors/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.majors.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.majors.admin');

        # Find the row
        $row = Major::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'majors';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::majors')->with('success', trans('ohms.msg_major_edited'));
    }

    public function create()
    {
        OHMS::permissionToAccess('ohms.majors.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.majors.admin');

        # Get all the data
        $data_index = 'majors';
        require('Data/Create/Get.php');

        # Return the view
        return view('ohms/majors/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        OHMS::permissionToAccess('ohms.majors.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.majors.admin');

        # create new major
        $row = new Major;

        # Save the data
        $data_index = 'majors';
        require('Data/Create/Save.php');

        # Return the admin to the majors page with a success message
        return redirect()->route('OHMS::majors')->with('success', trans('ohms.msg_major_created'));
    }

    public function destroy($id)
    {
        OHMS::permissionToAccess('ohms.majors.access');
        
        # Check permissions
        OHMS::permissionToAccess('ohms.majors.admin');

        # Select Major
        $major = OHMS::major('id', $id);

        if(!$major->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }
        
        # Delete Major
        $major->delete();

        # Redirect the admin
        return redirect()->route('OHMS::majors')->with('success', trans('ohms.msg_major_deleted'));
    }
}
