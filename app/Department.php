<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function staff()
    {
        return $this->hasMany('App\Staff');
    }
     public function hod()
    {
        return $this->hasOne('App\Staff');
    }
}
