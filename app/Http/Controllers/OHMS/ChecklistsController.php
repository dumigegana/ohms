<?php

namespace App\Http\Controllers\OHMS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User;
use App\User;
use App\Checklist;
use App\Permission;
use App\Permission_Role;
use Schema;
use Auth;
use Hash;
use Crypt;
use Mail;
use OHMS;

class ChecklistsController extends Controller
{

    public function index()
    {
        OHMS::permissionToAccess('ohms.checklists.access');

        # Get all the checklists
        $checklists = Checklist::all();

        
        # Return the view
        return view('ohms/checklists/index', ['checklists' => $checklists]);
    }

    public function show($id)
    {
        OHMS::permissionToAccess('ohms.checklists.access');

        # Get the checklist
        $checklist = Checklist::find($id);

        # Return the view
        return view('admin/checklists/show', ['checklist' => $checklist]);
    }

    public function edit($id)
    {
        OHMS::permissionToAccess('ohms.checklists.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.checklists.admin');

        # Find the checklist
        $row = Checklist::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Get all the data
        $data_index = 'checklists';
        require('Data/Edit/Get.php');

        # Return the view
        return view('ohms/checklists/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        OHMS::permissionToAccess('ohms.checklists.access');

        # Check permissions
        OHMS::permissionToAccess('ohms.checklists.admin');

        # Find the row
        $row = Checklist::find($id);

        if(!$row->allow_editing and !OHMS::loggedInuser()->su) {
            abort(403, trans('ohms.error_editing_disabled'));
        }

        # Save the data
        $data_index = 'checklists';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('OHMS::checklists')->with('success', trans('ohms.msg_checklist_edited'));
    }

    public function create()
    {
        
        # Check permissions
        OHMS::permissionToAccess('ohms.checklists.admin');

        
        # All permissions
        $permissions = OHMS::permissions();

        # Return the view
        return view('ohms/checklists/create');
    }

    public function store(Request $request)
    {
       
        # Check permissions
        OHMS::permissionToAccess('ohms.checklists.admin');

        # create new role
        $row = new Checklist;

        # Save the data
        $data_index = 'checklists';
        require('Data/Create/Save.php');

        # Return the admin to the roles page with a success message
        return redirect()->route('OHMS::checklists')->with('success', trans('ohms.msg_checklist_created'));

    }
}
