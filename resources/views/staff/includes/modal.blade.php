 
      <div class="check_ajax col-md-12 ohms_body" id="check_ajax">
          <div class="panel panel-default col-md-8 col-md-offset-2" >
            <div class="panel-heading" style="background: #ff8b8b;">
              <div class="row"><center> 
                <h3 class="panel-title col-xs-5 col-xs-offset-1"> Check List</h3></center>
                <form method="POST" action="{{ route('checkin_store') }}">
                   {{ csrf_field() }} 
                   @if ($allocation->approved == false) @php $cond = true @endphp
                   @else @php $cond = true @endphp @endif
                  <input type="hidden" name="allocation_id" value="{{ $allocation->id}}">                  
                  <input type="hidden" name="cond" id="cond_in" value="{{ $cond }}">
                  <button href="" class="btn btn-primary col-xs-4 col-xs-offset-2 check_submit" style=" display: none;">Submit</button>
                </form>
              </div>
            </div>
            <div class="panel-body ohms_body" id="list">
              <table class="table table-responsive">
                <tr>
                  <th>ITEM</th>
                  <th>STATUS</th>
                </tr>                
                <tbody>   
                  
                  @foreach($checklists as $checklist)
                    <tr class="row_li">
                      <td class="facility">{{$checklist->facility->name}}</td>
                      <td><a class="status" id="{{$checklist->facility->name}}" data-toggle="modal" href="#checkModal{{$checklist->pos  }}" style="color: #980000;">{{$checklist->facility_status->status}}</td>
                    </tr>
                  @endforeach                  
                </tbody>
              </table>
            </div>
          </div>
      </div>
      @foreach ($facilities as $facility)  
      <input class="itcount" type="hidden" value="{{ $loop->count }}">  
    <div class="modal fade" id="checkModal{{$loop->index}}" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: red">&times;</span></button>
            <div class="col-md-3 pos"> {{$loop->iteration}} of {{ $loop->count }}</div>
            <h4 class="modal-title col-md-8 col-md-offset-4 mod_tittle"> Indicate the condition of the <span style=" color: #990000;"> {{$facility->name}}</span> </h4>
          </div>
          <div class="modal-body">
              <input class="fac_{{$loop->index}}" type="hidden" name="facility" value="{{$facility->id}}">
              <input class="alloc_{{$loop->index}}" type="hidden" value="{{$allocation->id}}">
              <select class="form-control" name="status_{{$loop->index}}" id="facility_status_id">
              <option value="" disabled="">Sellect  Optins</option>
               @foreach ($statuses as $status)
                <option value="{{$status->id}}"> {{$status->status}}</option>
              @endforeach
              </select>
          </div>
          <div class="modal-footer">
            @if ($loop->last)
            <button type="button" class="btn btn-primary saveTemp" data-target="{{$loop->index}}" id="mod_last"  data-dismiss="modal">Save</button>
            @else
            <button type="button" class="btn btn-primary saveTemp" name="{{$loop->iteration}}" data-target="{{$loop->index}}" onclick="$('#checkModal{{$loop->index}}').one('hidden.bs.modal', function() { $('#checkModal{{$loop->iteration}}').modal('show'); }).modal('hide');">Save</button>
            @endif
            <button type="button" class="btn btn-primary saveChanges" data-target="{{$loop->index}}" style="display: none;" data-dismiss="modal">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    @endforeach
    